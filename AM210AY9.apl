.***************************************************************************
.* === AS GROUP ===         : AS_0510000Q 2008/12/04 09:42
.* USER IF AS               : UAS0510000Q 2008/12/04 09:42
.* USER IF TP               : SMALL-TP connected
.* USER IF AS MESSAGE       : MAS05100QEN 2008/12/04 09:42
.* USER IF IPL              : QIP01090100 2007/11/02
.* ================
.* APPLICATION              : AM210AY5
.* No1 AMP                  : 0010 0010 0010 0010 0010  
.* No2 AMP                  : 0010 0010 0010 0010
.***************************************************************************
.*.NETCONF     192.9.200.70,"",255.255.255.0,0.0.0.0,0.0.0.0,0.0.0.0,""
.ROBOTDATA0
.*ZSYSTEM         2   8   2        -181
.*ZCONTROLER      324    -1    -1    -1    -1       -4801
ZDSX          OFF -3469
ZDSXCORT        0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0          -1
ZDSX2CORT       0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0          -1
ZSIGSPEC       64  64 256       -5761
ZMTNIMP_03    OFF -3819
ZSPEC01_05       99.900   374013948
ZSPEC01_06      999.000   -52576257
ZSPEC01_09          500       -7501
ZSPEC01_10           10        -151
OPTION01_05   OFF  -565
OPTION01_06   OFF  -568
OPTION01_07   OFF  -570
OPTION01_10   ON   -575
OPTION02_04   OFF  -512
OPTION02_05   OFF  -514
OPTION02_06   OFF  -517
OPTION02_10   ON   -671
OPTION03_06     0          -1
OPTION03_09     0          -1
OPTION03_10     0          -1
OPTION04_01   OFF  -519
OPTION04_02   OFF  -521
OPTION04_04   OFF  -604
OPTION04_06   OFF  -529
OPTION04_08   OFF  -533
OPTION04_09   ON   -534
OPTION05_04   OFF  -563
OPTION05_08   OFF  -601
OPTION05_09   OFF  -607
OPTION05_10   OFF  -609
OPTION07_04   OFF  -553
OPTION07_05   ON   -554
OPTION07_07   ON   -558
OPTION07_09   OFF  -582
OPTION07_10   ON   -665
OPTION08_01   ON  -2806
OPTION08_09   OFF  -680
OPTION08_10   OFF  -684
OPTION09_02   OFF  -688
OPTION09_04   OFF -2256
OPTION09_05   ON   -773
OPTION09_08   OFF -2260
OPTION09_09   OFF -2450
ZSPEC02_02        0.500  1325400063
OPTION10_06   ON  -1503
OPTION10_07   OFF -2457
OPTION010_8   OFF -2267
OPTION11_02   OFF -2464
OPTION11_06   OFF -2466
OPTION11_08   OFF -1508
OPTION11_09   OFF -2475
OPTION12_06   OFF -2489
OPTION12_07   OFF -2439
OPTION13_05   ON  -3955
OPTION14_01   OFF -6082
OPTION14_10   OFF -6205
OPTION15_06   ON  -7430
OPTION16_04   ON  -6208
OPTION16_05   ON  -3957
ZCPUCACHE     OFF -2572
ZSETCYCOVER   2         -31
ZSETIPTIME           32
ZF47ERRTIME   2.0  1073741823
.*NETCONF      192.9.200.70,"",255.255.255.0,0.0.0.0,0.0.0.0,0.0.0.0,""
ZSWITCH ZKILL_LOAD      ON   -509
ZSWITCH ZLANGUAGE       OFF  -509
ZSWITCH ZSAFE_CE        ON   -505
ZSWITCH ZSAFE_STD       OFF  -509
ZSWITCH ZDS_NOCONV      OFF  -511
ZSWITCH ZDS_XMANSPD     OFF  -513
ZSWITCH ZCLR_PASSWD     OFF  -513
ZSWITCH ZMASK_PSBL2     OFF  -513
ZSWITCH ZTPLIGHT_ON     OFF  -513
ZSWITCH ZDEVNOWCHK      OFF  -511
ZSWITCH ZDEVNEXTCHK     ON   -511
ZSWITCH ZLOADSTART.PC   OFF  -517
ZSWITCH ZNOCHK_MCODE    OFF  -515
ZSWITCH ZERRLOGALL      ON   -509
ZSWITCH ZIRMS.OLD       OFF  -509
ZSWITCH ZSPLMT          OFF  -503
ZSWITCH ZWSPACE         OFF  -505
ZSWITCH ZHYBINT         OFF  -505
ZSWITCH ZHYBINT.AUTO    OFF  -515
ZSWITCH ZHYBINT.TCH     OFF  -513
ZSWITCH ZIGNCONF        OFF  -507
ZSWITCH ZIGNCONF.TCH    OFF  -515
ZSWITCH ZMODEM          OFF  -503
ZSWITCH ZLOAD_NDATA     OFF  -513
ZSWITCH ZEMG_UNRESET    OFF  -515
ZSWITCH ZLOAD_DDATA     OFF  -513
ZSWITCH ZHPLOCK         ON   -503
ZSWITCH ZCONF_HERE      OFF  -511
ZSWITCH ZTCH_HOLD       OFF  -509
ZSWITCH ZRELATE         OFF  -505
ZSWITCH ZEMGRESET       ON   -507
ZSWITCH ZCHKHOME        OFF  -507
ZSWITCH ZIPEAKWRN       ON   -507
ZSWITCH ZI2MONWRN       ON   -507
ZSWITCH ZCMNDJ_STOP     ON   -511
ZSWITCH ZBLT_NOASK      ON   -509
.** harish 090421 m
ZSWITCH ZBLT_FNOASK     ON   -511
ZSWITCH ZSAFE_USA       OFF  -509
ZSWITCH ZLCCOMT.OLD     OFF  -513
ZSWITCH ZBLT_LIMIT      OFF  -511
ZSWITCH ZDSLCTCHK       OFF  -509
ZSWITCH ZDS2_HOSEI      OFF  -511
ZSWITCH ZDS2_NTHCAL     ON   -511
ZSWITCH ZDS2_HSPEED     OFF  -513
ZSWITCH ZDS2_EIGHT      OFF  -511
.** harish 090501-1 m++
ZSWITCH ZHSEN_ACCU      ON   -509
ZSWITCH ZGETDCEN        ON   -505
.** harish 090501-1 m--
ZSWITCH ZLSENCAL        ON   -505
ZSWITCH ZPCMDF_JA       OFF  -509
ZSWITCH ZTWICE.BRKCHK   OFF  -517
.*ZSWITCH ZEXIST_TP       ON   -507
ZSWITCH ZEXIST1_PNL     ON   -511
.*ZSWITCH ZEXIST2_PNL     ON   -511
.*ZSWITCH ZEXIST3_PNL     OFF  -513
.*ZSWITCH ZEXIST4_PNL     OFF  -513
ZSWITCH ZEXIST_ENC      OFF  -511
ZSWITCH ZENA_REC        ON   -505
ZSWITCH ZTP_TRIAL       OFF  -509
ZSWITCH ZTP.TRG.SW      ON   -509
.*ZSWITCH ZMASK1_SV       OFF  -509
.*ZSWITCH ZMASK2_SV       OFF  -509
.*ZSWITCH ZMASK3_SV       OFF  -509
.*ZSWITCH ZMASK4_SV       OFF  -509
.*ZSWITCH ZMASK1_LS       OFF  -509
.*ZSWITCH ZMASK2_LS       OFF  -509
.*ZSWITCH ZMASK3_LS       OFF  -509
.*ZSWITCH ZMASK4_LS       OFF  -509
.*ZSWITCH ZMASK1_PU       ON   -507
.*ZSWITCH ZMASK2_PU       ON   -507
.*ZSWITCH ZMASK3_PU       ON   -507
.*ZSWITCH ZMASK4_PU       ON   -507
ZSWITCH ZCONSTAD        OFF  -507
ZSWITCH ZSCP            OFF  -499
ZSWITCH ZFCP            OFF  -499
ZSWITCH ZF2CP           OFF  -501
ZSWITCH ZMCP            OFF  -499
ZSWITCH ZTCH_TOPPEN     OFF  -513
ZSWITCH ZCHK_TOPPEN     OFF  -513
ZSWITCH ZREP_TOPPEN     OFF  -513
ZSWITCH ZTCH2_TOPPEN    ON   -513
ZSWITCH ZCHK2_TOPPEN    ON   -513
ZSWITCH ZREP2_TOPPEN    ON   -513
ZSWITCH ZYMPONSEQ       ON   -507
ZSWITCH ZMTR_HNS_CHK    ON   -513
ZSWITCH Z_F47           ON   -499
ZSWITCH Z_F47.MTROFF    OFF  -515
ZSWITCH ZCOL_ILIM_DET   OFF  -517
ZSWITCH ZCOLDET_ILIM    ON   -513
ZSWITCH ZSV_MVERR_CHK   ON   -515
ZSWITCH ZYMDA_OUT       ON   -507
ZSWITCH ZYMCOM2T        OFF  -507
ZSWITCH ZMASK_PEERR     OFF  -513
.*ZSWITCH ZTPPORT         OFF  -505
ZSWITCH ZINSIG          OFF  -503
ZSWITCH ZEXTSW          OFF  -503
ZSWITCH ZADCCAL         OFF  -505
ZSWITCH Z_IVAR          OFF  -503
ZSWITCH ZNCHVAR         OFF  -505
ZSWITCH ZNOTCH          ON   -501
ZSWITCH ZS_KVFF         OFF  -505
ZSWITCH ZSRVCHK         ON   -503
ZSWITCH ZCHKASCYC       ON   -507
ZSWITCH ZPDFF           OFF  -501
ZSWITCH ZCOLDET         OFF  -505
ZSWITCH ZCHKROT6        ON   -505
ZSWITCH ZWX.OLD         OFF  -505
ZSWITCH ZREFSP          OFF  -503
ZSWITCH ZCMNT           OFF  -501
ZSWITCH ZCOL_ONCE_DET   ON   -515
ZSWITCH ZCHKLIMP0       OFF  -509
ZSWITCH ZBOOTDELAY      OFF  -511
ZSWITCH ZSET3L2KLIMR    OFF  -515
ZSWITCH ZSET3L2KMONI    OFF  -515
ZSWITCH ZLOCKOFFERR     OFF  -513
ZSWITCH ZSRV4MS         ON   -503
ZSWITCH ZOPLOG_DISP_ALL ON   -519
ZSWITCH ZSTP_COMCMD     OFF  -513
ZSWITCH ZSTP_ALGN       OFF  -509
ZSWITCH ZASHSP          OFF  -503
.END
.ROBOTDATA1
.*ZROBOT.TYPE    48 170   5   5       -3421   NT570C-A001  ( 2008-10-24 19:03 )
.*ZLINEAR        -1   0  -1   0   0   0   0  -1  -1  -1  -1  -1         104
.*ZZERO         268435456 268428781 268780995 268434331 268435392 268309674 268433971 268435456 268435456 268435456 268435456 268435456 -1076897945
ZWCOMPK           0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000          -1
ZULIMIT         0.00000 142.00000 472.00000 172.00000   0.00000  52.00000 185.00000  10.00000   0.00000   0.00000   0.00000   0.00000   -59826177
ZLLIMIT         0.00000 -202.00000  -3.00000 -172.00000   0.00000 -292.00003  -5.00000 -10.00000   0.00000   0.00000   0.00000   0.00000 -2122645505
ZSETACCEL       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETDECEL       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETKVFF        0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   764831298
ZCONV_CODE      0          -1
.*ZSAVEMR                 0       65515       63450       65527           0       65520       16371           0       32767       32767       32767       32767    -6111766
.*ZRADIUS           1.000  1500.000     1.000  1100.000     1.000   640.000   500.000     1.000     1.000     1.000     1.000     1.000  -288997377
.*ZJTSPEED       1143.000   160.000   500.000   320.000     1.000   360.000   360.000    10.000    10.000    10.000    10.000    10.000  -236527617
.*ZACCTIME          0.570     0.450     0.300     0.450     0.384     0.300     0.300     0.384     0.384     0.384     0.384     0.384   926710503
.*ZDEG_BIT      -2.1004820000e-03 -4.3083639700e-04 -1.2555803570e-03 -8.6167279400e-04  3.6928830000e-04 -7.3198616900e-04  1.0958230000e-03  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04 -1506821078  -917388605
.*ZMOTOR_RPM       9999.00    2720.00    2916.67    2720.00    9999.00    3602.14    2406.15    9999.00    9999.00    9999.00    9999.00    9999.00  -849833142
.*COUPFA7        0.0000000000e+00          -1          -1
.*ZSIGWAIT        0          -1
.*ZSETTPNK          1.100     0.900     0.900     0.900
ZGACCEF       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGDECEF       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGFRIST       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGFRID0       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGFRID1       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
TIMECONST       2   2   2   2   2   2   2   2   0   0   0   0        -241
TIMECSTSFT      4   4   4   4   4   4   4   4   0   0   0   0        -481
ZCOLT            10    30    90    30    10    30    30     0     0     0     0     0       -3451
ZCOLR            10    30    93    30    10    39    39     0     0     0     0     0       -3766
ZCOLTJ            1    15    30    15     1    15    15     0     0     0     0     0       -1381
ZCOLRJ            1    12    21    12     1    21    18     0     0     0     0     0       -1291
ZSETIPALM       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETI2ALM       1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   745956978
ZARMIDSET     ON   ON    -1498
COUPFA         0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00          -1          -1
ZKPCF3JT_SW    2  2  2  2  2  2  2  0  0  0  0  0        -211
ZKPCF3JT_FREQ    5.80   5.80   5.80   5.80   5.80   5.80   5.80   5.80  12.00  12.00  12.00  12.00   384827403
.*ZNSXACCELP        1.00 -9999.00 -9999.00     1.00  9999.00  9999.00  1073741823
.*ZNSXDECELP        1.00 -9999.00 -9999.00     1.00  9999.00  9999.00  1073741823
.*ZSETESTOP5      0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000          -1
.*ZCOLVVELSET    0
.*ZCOLESCSET     1  0  0
.*ZCOLESCIMAX        0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLESCIMAX_SYS       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLESCIMAX_CCW       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLESCIMAX_CW       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLDETILIM_SIZE      10     10     10     10     10     10     10     10     10     10     10     10
.*ZCOLDETILIM_TIME       0      4      4      4      0      4      4      0     10     10     10     10
.*ZCOLDETILIM_DIR       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLDETILIM_FLW       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLERRDELAY  15
.*ZXPITCHLIM        0.000     0.000          -1
.*ZSETACFSTOP     1   2         -46
.*Z_PLAN_JA     OFF -6211
.*ZSTP_MENU       3         -46
.*ZSTP_PAGE1   "JT  +Z  - +J4 +     -Z J2 -J4 J2"
.*ZSTP_PAGE1S  "JT +J6     +F   S  -J6     -F   "
.*ZSTP_PAGE2   "BASE +Z   ^          -Z  <-v->  "
.*ZSTP_PAGE2S  "BASE +Z   ^          -Z  <-v->  "
.*ZSTP_PAGE3   "TOOL +r   ^          -r  <-v->  "
.*ZSTP_PAGE3S  "TOOL +r   ^          -r  <-v->  "
.*ZSTP_KEY1        2   -2    4   -4    3   -3          -1
.*ZSTP_KEY1S       0    0    7   -7    6   -6          -1
.*ZSTP_KEY2      101 -101  102 -102  103 -103          -1
.*ZSTP_KEY2S     101 -101  102 -102  103 -103          -1
.*ZSTP_KEY3      121 -121  122 -122  124 -124          -1
.*ZSTP_KEY3S     121 -121  122 -122  125 -125          -1
.*ZYMPRM_VER    57001     -855016
.*ZYMADRS_01    0 10 1 0        -166
.*ZYMPRM_A01    50 100 200 0 150 100 100 120 120 0 0 600 200 200 30 10000 0 2000 2000     -239551
.*ZYMPRM_B01    5243 1092 1500 1500 768 0 300 300 0 0 1500 0 40 200 16 90 0 100     -189736
.*ZYMPRM_C01    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z01    500 50 1000 50 50      -24751
.*ZYMADRS_02    1 10 1 0        -181
.*ZYMPRM_A02    360 528 590 0 150 100 100 250 250 0 0 600 200 200 22 10000 100 80 2000     -232951
.*ZYMPRM_B02    117 0 65535 65535 0 0 200 200 0 0 1500 0 10 200 200 90 65535 100    -2988331
.*ZYMPRM_C02    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z02    500 50 1000 50 50      -24751
.*ZYMADRS_03    2 11 1 0        -211
.*ZYMPRM_A03    120 100 460 0 150 100 100 200 200 0 0 600 200 200 22 10000 100 80 2000     -219481
.*ZYMPRM_B03    377 0 65535 65535 0 -92 200 200 0 0 1500 0 10 200 200 90 65535 100    -2990851
.*ZYMPRM_C03    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z03    500 50 1000 50 50      -24751
.*ZYMADRS_04    3 10 1 0        -211
.*ZYMPRM_A04    315 707 700 0 150 100 100 255 255 0 0 600 200 200 22 10000 100 80 2000     -236761
.*ZYMPRM_B04    143 0 65535 65535 0 0 200 200 0 0 1500 0 10 200 200 90 65535 100    -2988721
.*ZYMPRM_C04    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z04    500 50 1000 50 50      -24751
.*ZYMADRS_06    4 10 1 0        -226
.*ZYMPRM_A06    325 219 450 0 150 100 100 89 89 0 0 600 200 200 22 10000 100 80 2000     -220861
.*ZYMPRM_B06    186 0 65535 65535 0 0 200 200 0 0 1500 0 10 200 200 90 65535 100    -2989366
.*ZYMPRM_C06    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z06    500 50 1000 50 50      -24751
.*ZYMADRS_07    5 36 1 0        -631
.*ZYMPRM_A07    140 366 280 0 150 100 100 84 84 0 0 600 200 200 22 10000 100 80 2000     -217591
.*ZYMPRM_B07    86 0 65535 65535 0 -48 200 200 0 0 1500 0 10 200 200 90 65535 100    -2987146
.*ZYMPRM_C07    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z07    500 50 1000 50 50      -24751
ZYMROBOT      1 0 1 1 0 -1 -1 -1          -1
ZYMEMRDLY     0 0          -1
ZL3LINK       0.0000 0.0000 440.0000 350.0000 24.5000 0.0000    -7233537
ZSPEC02_03        0.000          -1
.*ZSSETHAND         0.000          -1
.*ZSETVKP_1       0   0 100       -1501
.*ZSETVKP_2       0   0 100       -1501
.*ZSETVKP_3       0   0 100       -1501
.*ZSETVKP_4       0   0 100       -1501
.*ZSETVKP_6       0   0 100       -1501
.*ZSETVKP_7       0   0 100       -1501
.*ZSETNS2KVFF   0.0000 0.0000          -1
.*ZSETNSKVFF    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
.*ZSETNS2KAFF   0.0000 0.0000          -1
.*ZSETNSKAFF    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
.*ZSETNS2KJFF   0.0000 0.0000          -1
.*ZSETNSKJFF    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
.*ZSET3L2KVFF   0.0000 0.0000 0.0000 0.0000          -1
.*ZSET3L2KAFF   0.0000 0.0000 0.0000 0.0000          -1
.*ZSET3L2KJFF   0.0000 0.0000 0.0000 0.0000          -1
.*ZL3FLIPAREA    45.000 135.000   267649023
.*ZCPOSCHECK         1.00      0.10      0.15      0.34      0.50      0.14      0.12      1.00      0.00      0.00      0.00      0.00   988807158
.** harish 090601 a++
ZL3SWINGX     -310.000 310.000 -310.000 310.000 310.000 -310.000          -1
ZL3SWINGDIR        1     -1     -1      1     -1      1          -1
.** harish 090601 a--
ZSWITCH ZOVERLMTMSK1    OFF  -515
.*ZSWITCH ZESTOPHOLD3     ON   -511
.*ZSWITCH ZESTOPHOLD4     OFF  -513
.*ZSWITCH ZESTOP5_HCYC    ON   -513
ZSWITCH Z_F47.STOP      OFF  -511
.*ZSWITCH ZNS2VFF         OFF  -505
.*ZSWITCH ZNSVFF          OFF  -503
.*ZSWITCH ZNS2AFF         OFF  -505
.*ZSWITCH ZNSAFF          OFF  -503
.*ZSWITCH ZNS2JFF         OFF  -505
.*ZSWITCH ZNSJFF          OFF  -503
.*ZSWITCH ZCOLDISABLE     OFF  -513
.END
.ROBOTDATA2
.*ZROBOT.TYPE    48 131   4   6       -2836   NT410B-A001  ( 2008-10-24 19:03 )
.*ZLINEAR        -1   0  -1   0  -1   0  -1  -1  -1  -1  -1  -1         134
.*ZZERO                 0 268443735 268439079 268427308         0 268549039 268435456 268435456 268435456 268435456 268435456 268435456 -1612372792
ZWCOMPK           0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000          -1
ZULIMIT         0.00000 172.00000 402.00000 172.00000   0.00000 172.00000  10.00000  10.00000   0.00000   0.00000   0.00000   0.00000   -37584897
ZLLIMIT         0.00000 -172.00000  -3.00000 -172.00000   0.00000 -172.00000 -10.00000 -10.00000   0.00000   0.00000   0.00000   0.00000 -2087010305
ZSETACCEL       1.000   0.800   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   762314748
ZSETDECEL       1.000   0.800   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   762314748
ZSETKVFF        0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   0.970   764831298
ZCONV_CODE      0          -1
.*ZSAVEMR                 0       16427          38       25625           0         116           0           0       32767       32767       32767       32767    -2599111
.*ZRADIUS           1.000  1380.000     1.000   940.000     1.000   615.000     1.000     1.000     1.000     1.000     1.000     1.000  -239599617
.*ZJTSPEED       1143.000   240.000   500.000   480.000     1.000   360.000    10.000    10.000    10.000    10.000    10.000    10.000  -229769217
.*ZACCTIME          0.570     0.300     0.300     0.300     0.384     0.300     0.384     0.384     0.384     0.384     0.384     0.384   933505263
.*ZDEG_BIT      -2.1004820000e-03  4.3083639700e-04 -1.2555803570e-03  8.6167279400e-04  3.6928830000e-04  5.4384958500e-04  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04  3.6928830000e-04 -1380708050  1245327423
.*ZMOTOR_RPM       9999.00    4080.00    2916.67    4080.00    9999.00    4848.25    9999.00    9999.00    9999.00    9999.00    9999.00    9999.00  -872026347
.*COUPFA7        0.0000000000e+00          -1          -1
.*ZSIGWAIT        0          -1
.*ZSETTPNK          1.100     0.900     0.900     0.900
ZGACCEF       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGDECEF       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGFRIST       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGFRID0       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZGFRID1       1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
TIMECONST       2   2   2   2   2   2   2   2   0   0   0   0        -241
TIMECSTSFT      4   4   4   4   4   4   4   4   0   0   0   0        -481
ZCOLT            10    40    50    30    10    30    10    10     0     0     0     0       -2851
ZCOLR            10    66    72   117    10    75    10    10     0     0     0     0       -5551
ZCOLTJ            1    15    15    15     1    15     1     1     0     0     0     0        -961
ZCOLRJ            1    27    66    39     1    72     1     1     0     0     0     0       -3121
ZSETIPALM       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETI2ALM       1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   745956978
ZARMIDSET     ON   ON    -1498
COUPFA         0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00          -1          -1
ZKPCF3JT_SW    2  2  2  2  2  2  0  0  0  0  0  0        -181
ZKPCF3JT_FREQ   10.00  10.00  10.00  10.00  10.00  10.00  10.00  10.00  12.00  12.00  12.00  12.00   318767103
.*ZNSXACCELP        1.00 -9999.00 -9999.00     1.00  9999.00  9999.00  1073741823
.*ZNSXDECELP        1.00 -9999.00 -9999.00     1.00  9999.00  9999.00  1073741823
.*ZSETESTOP5      0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000          -1
.*ZCOLVVELSET    0
.*ZCOLESCSET     1  0  0
.*ZCOLESCIMAX        0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLESCIMAX_SYS       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLESCIMAX_CCW       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLESCIMAX_CW       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLDETILIM_SIZE      10     10     10     10     10     10     10     10     10     10     10     10
.*ZCOLDETILIM_TIME       0      4      4      4      0      4      0      0     10     10     10     10
.*ZCOLDETILIM_DIR       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLDETILIM_FLW       0      0      0      0      0      0      0      0      0      0      0      0
.*ZCOLERRDELAY  15
.*ZXPITCHLIM        0.000     0.000          -1
.*ZSETACFSTOP     1   2         -46
.*Z_PLAN_JA     OFF -6211
.*ZSTP_MENU       3         -46
.*ZSTP_PAGE1   "JT  +Z  - +J4 +     -Z J2 -J4 J2"
.*ZSTP_PAGE1S  "JT +J6          S  -J6          "
.*ZSTP_PAGE2   "BASE +Z   ^          -Z  <-v->  "
.*ZSTP_PAGE2S  "BASE +Z   ^          -Z  <-v->  "
.*ZSTP_PAGE3   "TOOL +r   ^          -r  <-v->  "
.*ZSTP_PAGE3S  "TOOL +r   ^          -r  <-v->  "
.*ZSTP_KEY1        2   -2    4   -4    3   -3          -1
.*ZSTP_KEY1S       0    0    0    0    6   -6          -1
.*ZSTP_KEY2      101 -101  102 -102  103 -103          -1
.*ZSTP_KEY2S     101 -101  102 -102  103 -103          -1
.*ZSTP_KEY3      121 -121  122 -122  124 -124          -1
.*ZSTP_KEY3S     121 -121  122 -122  124 -124          -1
.*ZYMPRM_VER    41001     -615016
.*ZYMADRS_01    0 10 2 0        -181
.*ZYMPRM_A01    50 100 200 0 150 100 100 120 120 0 0 600 200 200 30 10000 0 2000 2000     -239551
.*ZYMPRM_B01    5243 1092 1500 1500 768 0 300 300 0 0 1500 0 40 200 16 90 0 100     -189736
.*ZYMPRM_C01    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z01    500 50 1000 50 50      -24751
.*ZYMADRS_02    7 10 2 0        -286
.*ZYMPRM_A02    163 441 300 0 150 100 100 275 275 0 0 600 200 200 30 10000 100 80 2000     -225211
.*ZYMPRM_B02    296 0 65535 65535 0 0 200 200 0 0 1500 0 10 200 200 90 65535 100    -2991016
.*ZYMPRM_C02    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z02    500 50 1000 50 50      -24751
.*ZYMADRS_03    8 11 2 0        -316
.*ZYMPRM_A03    150 100 500 0 150 100 100 160 160 0 0 600 200 200 30 10000 100 80 2000     -219451
.*ZYMPRM_B03    213 0 65535 65535 0 -83 200 200 0 0 1500 0 10 200 200 90 65535 100    -2988526
.*ZYMPRM_C03    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z03    500 50 1000 50 50      -24751
.*ZYMADRS_04    9 10 2 0        -316
.*ZYMPRM_A04    137 461 300 0 150 100 100 260 260 0 0 600 200 200 30 10000 100 80 2000     -224671
.*ZYMPRM_B04    329 0 65535 65535 0 0 200 200 0 0 1500 0 10 200 200 90 65535 100    -2991511
.*ZYMPRM_C04    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z04    500 50 1000 50 50      -24751
.*ZYMADRS_06    10 6 2 0        -271
.*ZYMPRM_A06    192 187 300 0 150 100 100 140 140 0 0 600 200 200 30 10000 100 80 2000     -217786
.*ZYMPRM_B06    161 0 65535 65535 0 0 200 200 0 0 1500 0 10 200 200 90 65535 100    -2988991
.*ZYMPRM_C06    0.00 0.00 0.00 0.00          -1
.*ZYMPRM_Z06    500 50 1000 50 50      -24751
ZYMROBOT      1 0 1 2 0 -1 -1 -1         -16
ZYMEMRDLY     0 0          -1
ZL3LINK       180.0000 0.0000 440.0000 350.0000 0.0000 0.0000   -45449217
ZSPEC02_03        0.000          -1
.*ZSSETHAND         0.000          -1
.*ZSETVKP_1       0   0 100       -1501
.*ZSETVKP_2       0   0 100       -1501
.*ZSETVKP_3       0   0 100       -1501
.*ZSETVKP_4       0   0 100       -1501
.*ZSETVKP_6       0   0 100       -1501
.*ZSETNS2KVFF   0.0000 0.0000          -1
.*ZSETNSKVFF    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
.*ZSETNS2KAFF   0.0000 0.0000          -1
.*ZSETNSKAFF    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
.*ZSETNS2KJFF   0.0000 0.0000          -1
.*ZSETNSKJFF    0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
.*ZSET3L2KVFF   0.0000 0.0000 0.0000 0.0000          -1
.*ZSET3L2KAFF   0.0000 0.0000 0.0000 0.0000          -1
.*ZSET3L2KJFF   0.0000 0.0000 0.0000 0.0000          -1
.*ZL3FLIPAREA    45.000 135.000   267649023
.*ZCPOSCHECK         1.00      0.10      0.15      0.34      1.00      0.11      1.00      1.00      0.00      0.00      0.00      0.00   925892613
ZSWITCH ZOVERLMTMSK1    OFF  -515
.*ZSWITCH ZESTOPHOLD3     ON   -511
.*ZSWITCH ZESTOPHOLD4     OFF  -513
.*ZSWITCH ZESTOP5_HCYC    ON   -513
ZSWITCH Z_F47.STOP      OFF  -511
.*ZSWITCH ZNS2VFF         OFF  -505
.*ZSWITCH ZNSVFF          OFF  -503
.*ZSWITCH ZNS2AFF         OFF  -505
.*ZSWITCH ZNSAFF          OFF  -503
.*ZSWITCH ZNS2JFF         OFF  -505
.*ZSWITCH ZNSJFF          OFF  -503
.*ZSWITCH ZCOLDISABLE     OFF  -513
.END
.SYSDATA0
REG_POINT       0
ENV_DATA              0   0   0   0   0   0   0   0   0   0
ENV2_DATA       0   0   0   0   0   0   0   0   0   0
TPNL_DSPSEL   255 255   0 255   0   0   0   0   0   0
DEFSIG_I EXT_MTRON       OFF  1032
DEFSIG_I EXT_ERR_RESET   OFF  1031
DEFSIG_I EXT_CYC_START   OFF  1030
DEFSIG_I EXT_PROGRM_RST  OFF     0
DEFSIG_I EXT_JUMP        OFF
         JUMP_ON             0
         JUMP_OFF            0
         JUMP_ST             0
DEFSIG_I EXT_RPS         OFF
         RPS_ON              0
         RPS_ST              0
         RPS_CODE            0     0     0
DEFSIG_I EXT_IT          OFF     0
DEFSIG_I EXT_SLOW_REP.   OFF     0
DEFSIG_I EXT_STANDBY     OFF     0
DEFSIG_I I/F_PAGE1       OFF     0
DEFSIG_I I/F_PAGE2       OFF     0
DEFSIG_I EXT_ROBOT_MOVE_ENABLE  OFF     0
DEFSIG_I EXT_I_INTFER    OFF     0
DEFSIG_I EXT_RESTART     OFF     0
DEFSIG_I EXT2_IT         OFF     0
DEFSIG_I EXT3_IT         OFF     0
DEFSIG_I EXT4_IT         OFF     0
DEFSIG_I EXT_ROBOT2_MOVE_ENABLE  OFF     0
DEFSIG_I EXT_ROBOT3_MOVE_ENABLE  OFF     0
DEFSIG_I EXT_ROBOT4_MOVE_ENABLE  OFF     0
DEFSIG_I RB2EXT_MTRON    OFF     0
DEFSIG_I RB2EXT_ERR_RESET  OFF     0
DEFSIG_I RB2EXT_CYC_START  OFF     0
DEFSIG_I RB2EXT_STANDBY  OFF     0
DEFSIG_I RB2EXT_RESTART  OFF     0
DEFSIG_I BRKCHK_SIGNAL_START  OFF  1104
DEFSIG_O MOTOR_ON        OFF    32
DEFSIG_O ERROR           OFF    31
DEFSIG_O AUTOMATIC       OFF    30
         CND_RUN         OFF
         CND_EHOLD       OFF
         CND_REPEAT      OFF
         CND_CCONT       OFF
         CND_SCONT       OFF
         CND_TLOCK       OFF
         CND_CYCLE       OFF
         CND_RGSO        OFF
         CND_DRYOFF      OFF
DEFSIG_O CYCLE_START     OFF    29
DEFSIG_O TEACH_MODE      OFF    28
DEFSIG_O HOME1           OFF    27
DEFSIG_O HOME2           OFF     0
DEFSIG_O POWER_ON        OFF     0
DEFSIG_O RGSO            OFF     0
DEFSIG_O RPS             OFF     0
DEFSIG_O WORK_SPACE_1    OFF     0
DEFSIG_O WORK_SPACE_2    OFF     0
DEFSIG_O WORK_SPACE_3    OFF     0
DEFSIG_O WORK_SPACE_4    OFF     0
DEFSIG_O WORK_SPACE_5    OFF     0
DEFSIG_O WORK_SPACE_6    OFF     0
DEFSIG_O WORK_SPACE_7    OFF     0
DEFSIG_O WORK_SPACE_8    OFF     0
DEFSIG_O WORK_SPACE_9    OFF     0
DEFSIG_O OUT_DISIG       OFF     4    13  1013
DEFSIG_O RB2AUTOMATIC    OFF     0
DEFSIG_O RB2HOME1        OFF     0
DEFSIG_O RB2HOME2        OFF     0
DEFSIG_O RB2WORK_SPACE_1  OFF     0
DEFSIG_O RB2WORK_SPACE_2  OFF     0
DEFSIG_O RB2WORK_SPACE_3  OFF     0
DEFSIG_O RB2WORK_SPACE_4  OFF     0
DEFSIG_O RB2WORK_SPACE_5  OFF     0
DEFSIG_O RB2WORK_SPACE_6  OFF     0
DEFSIG_O RB2WORK_SPACE_7  OFF     0
DEFSIG_O RB2WORK_SPACE_8  OFF     0
DEFSIG_O RB2WORK_SPACE_9  OFF     0
DEFSIG_O EXT_O_INTFER    OFF     0
DEFSIG_O EMG_STOP        OFF     0
DEFSIG_O MECHA_WARNING   OFF     0
DEFSIG_O ERROR2          OFF     0
DEFSIG_O ERROR3          OFF     0
DEFSIG_O ERROR4          OFF     0
DEFSIG_O RB2MOTOR_ON     OFF     0
DEFSIG_O RB2CYCLE_START  OFF     0
DEFSIG_O BRKCHK_MOVING   OFF   102
DEFSIG_O BRKCHK_ERROR    OFF   108
BRKCHK_SIG_TIMER         40
BRKCHK_START_SIGNAL    1001
BRKCHK_COND_POS pg0
BRKCHK_COND_STEP          1
BRKCHK_COND_START  255 255
TPNL_GRPH       0   0   0   0   0
SETSIO             9600         2
PROTOCOL          5.000     5.000     5.000         1         1         1
SET2SIO            9600         2
DSSETSIO2         19200
DSSETSIO3         19200
DSPROTOCOL2          50        20         3
DSPROTOCOL3          50        20         3
FDD_SPD_BAU         384       384
FDD_SPD_STOP          2         2
FDD_PORT              2
FDD_SWITCH            0
SCSETSIO2         19200         1
SCSETSIO3         19200         1
SCSETSIO7          9600         0         1
SCPROTOCOL2          50        20         3
SCPROTOCOL3          50        20         3
SCPROTOCOL7          50        20         3
SC_TOP_CODE2          0         0         0         0         0         0         0         0         0         0         0         0         0         0         0
SC_END_CODE2          0         0         0         0         0         0         0         0         0         0         0         0         0         0         0
SC_TOP_CODE3          0         0         0         0         0         0         0         0         0         0         0         0         0         0         0
SC_END_CODE3          0         0         0         0         0         0         0         0         0         0         0         0         0         0         0
SC_TOP_CODE7          0         0         0         0         0         0         0         0         0         0         0         0         0         0         0
SC_END_CODE7          0         0         0         0         0         0         0         0         0         0         0         0         0         0         0
SETCOMMODE            0         0
ZSETACF       272  56       -4921
ZMASKIOFUSE1    0          -1
ZMASKIOFUSE2    0          -1
ZARMIDECSET   -31118 -31123 -31124 -31135 -33808 -37500 -37501      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0     3499634
ZMASKIOERR      0   0   0   0
ERRLOG_SIGNO          0         0         0         0         0         0         0         0
OPLOG_SIGNO           0         0         0         0         0         0         0         0
ERRLOG_FLTNO          0         0         0         0         0         0         0         0         0         0
ZSF_UCAPA            16
SWITCH MESSAGES        ON 
SWITCH SCREEN          OFF
SWITCH AUTOSTART.PC    ON 
SWITCH AUTOSTART2.PC   OFF
SWITCH AUTOSTART3.PC   OFF
SWITCH AUTOSTART4.PC   OFF
SWITCH AUTOSTART5.PC   OFF
SWITCH AUTOSTART6.PC   OFF
SWITCH AUTOSTART7.PC   OFF
SWITCH AUTOSTART8.PC   OFF
SWITCH ERRSTART.PC     OFF
SWITCH DISPIO_01       OFF
SWITCH HOLD.STEP       OFF
SWITCH EXTHOLD.MTROFF  OFF
SWITCH SLOW_START      OFF
SWITCH UDP_EMSG        OFF
.END
.SYSDATA
BASE              0.000     0.000     0.000     0.000     0.000     0.000
CHECK_SPEC      1   1   1   1   1   1   0   1   0
ARM_OX_TOP           33
ARM_OX_NUM            8
ARM_WX_TOP         1033
ARM_WX_NUM           16
BRKCHK_BIT            0         0       100         0         0         0         0         0
SYS_BASE          0.000     0.000     0.000     0.000     0.000     0.000
TOOL              0.000     0.000     0.000     0.000     0.000     0.000
DFF              20.000     0.000     0.000     0.000     0.000     0.000     0.000
ARMPROTOCOL        1000         5         3
OTOOL1            0.000     0.000
OTOOL2            0.000     0.000
OBASE             0.000
ZXMTNLIMIT    9999.00 9999.00 9999.00 9999.00 9999.00 9999.00 9999.00 9999.00    0.00    0.00    0.00    0.00
SWITCH CHECK.HOLD      OFF
SWITCH CP              ON 
SWITCH CYCLE.STOP      ON 
SWITCH PREFETCH.SIGINS ON 
SWITCH REP_ONCE        OFF
SWITCH RPS             OFF
SWITCH STP_ONCE        OFF
SWITCH FLEXCOMP        OFF
SWITCH ABS.SPEED       OFF
.END
.SYSDATA2
BASE              0.000     0.000     0.000     0.000     0.000     0.000
CHECK_SPEC      1   1   1   1   1   1   0   1   0
ARM_OX_TOP           49
ARM_OX_NUM            8
ARM_WX_TOP         1049
ARM_WX_NUM           16
BRKCHK_BIT            0         0       100         0         0         0         0         0
SYS_BASE          0.000     0.000     0.000     0.000     0.000     0.000
TOOL              0.000     0.000     0.000     0.000     0.000     0.000
DFF              20.000     0.000     0.000     0.000     0.000     0.000     0.000
ARMPROTOCOL        1000         5         3
OTOOL1            0.000     0.000
OTOOL2            0.000     0.000
OBASE             0.000
ZXMTNLIMIT    9999.00 9999.00 9999.00 9999.00 9999.00 9999.00 9999.00 9999.00    0.00    0.00    0.00    0.00
SWITCH CHECK.HOLD      OFF
SWITCH CP              ON 
SWITCH CYCLE.STOP      ON 
SWITCH PREFETCH.SIGINS ON 
SWITCH REP_ONCE        OFF
SWITCH RPS             OFF
SWITCH STP_ONCE        OFF
SWITCH FLEXCOMP        OFF
SWITCH ABS.SPEED       OFF
.END
.AUXDATA0
SLOW_REPEAT      10.000
.** SCRY10-003-4 m
CHECK_SPEED      10.000    40.000   60.000
TEACH_SPEED      10.000    80.000   250.000     0.016     0.500     1.000
AUX_SELECT      1   1   1   1   1   1   1   1   1   1
AUX_DPASSWD     1
AUX_CHOOSE    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
AUX_DISPLAY     1   1
AUX_INTFER        0.000     0.000         0
EACH_SET             -1        -1        -1        -1        -1        -1        -1        -1        -1        -1        -1        -1
.END
.AUXDATA
UP-LIM          0.00000 140.00000 470.00003 170.00000   0.00000  50.00000 183.00000  10.00000   0.00000   0.00000   0.00000   0.00000
LO-LIM          0.00000 -200.00000   0.00000 -170.00000   0.00000 -290.00000  -3.00000 -10.00000   0.00000   0.00000   0.00000   0.00000
1HOME           0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000    10.000
2HOME           0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000    10.000
WORK_SPACE_1      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_2      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_3      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_4      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_5      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_6      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_7      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_8      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_9      0.000     0.000     0.000     0.000     0.000     0.000         0
CL_APPLI        0   0   0   0   0   0   0   0
CL_HDL[1]        24     0
CL_HDL[2]        24     0
CL_HDL[3]        24     0
CL_HDL[4]        24     0
CL_HDL[5]        24     0
CL_HDL[6]        24     0
CL_HDL[7]        24     0
CL_HDL[8]        24     0
COLT                 10        30        90        30        10        30        30
COLTSW        255
COLTJ                 1        15        30        15         1        15        15
COLTJSW       255
COLR                 10        30        93        30        10        39        39
COLRSW        255
COLRJ                 1        12        21        12         1        21        18
COLRJSW       255
MOVEAREA1     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA2     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA3     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA4     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA5     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA6     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA7     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA8     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA9     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA10    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA11    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA12    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA13    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA14    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA15    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA16    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA17    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA18    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA19    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA20    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
LCCOMT_NUMV0    0
LCCOMT_NUMVN    0
LCCOMT_NUML0    0
LCCOMT_NUMLN    0
.END
.AUXDATA2
UP-LIM          0.00000 170.00000 400.00000 170.00000   0.00000 170.00000  10.00000  10.00000   0.00000   0.00000   0.00000   0.00000
LO-LIM          0.00000 -170.00000   0.00000 -170.00000   0.00000 -170.00000 -10.00000 -10.00000   0.00000   0.00000   0.00000   0.00000
1HOME           0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000    10.000
2HOME           0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000    10.000
WORK_SPACE_1      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_2      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_3      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_4      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_5      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_6      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_7      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_8      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_9      0.000     0.000     0.000     0.000     0.000     0.000         0
CL_APPLI        0   0   0   0   0   0   0   0
CL_HDL[1]        24     0
CL_HDL[2]        24     0
CL_HDL[3]        24     0
CL_HDL[4]        24     0
CL_HDL[5]        24     0
CL_HDL[6]        24     0
CL_HDL[7]        24     0
CL_HDL[8]        24     0
COLT                 10        40        50        30        10        30
COLTSW        255
COLTJ                 1        15        15        15         1        15
COLTJSW       255
COLR                 10        66        72       117        10        75
COLRSW        255
COLRJ                 1        27        66        39         1        72
COLRJSW       255
MOVEAREA1     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA2     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA3     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA4     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA5     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA6     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA7     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA8     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA9     0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA10    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA11    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA12    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA13    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA14    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA15    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA16    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA17    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA18    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA19    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
MOVEAREA20    0       0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000
LCCOMT_NUMV0    0
LCCOMT_NUMVN    0
LCCOMT_NUML0    0
LCCOMT_NUMLN    0
.END
.INTER_PANEL
.END
..MONCMD zloadstart.pc on
.******************************************
.** const.as
.******************************************
.*                                                      /* SCRX11-032 a++ */
.* Robot Global Definitions
.REALS
    R1 = 1                                              ;* Robot 1
    R2 = 2                                              ;* Robot 2
    B1 = 1                                              ;* Blade 1
    B2 = 2                                              ;* Blade 2

    JT1 = 1                                             ;* Robot Joint 1
    JT2 = 2                                             ;* Robot Joint 2
    JT3 = 3                                             ;* Robot Joint 3
    JT4 = 4                                             ;* Robot Joint 4
    JT5 = 5                                             ;* Robot Joint 5
    JT6 = 6                                             ;* Robot Joint 6
    JT7 = 7                                             ;* Robot Joint 7

    HCLU     = 202
    BL_RINSE = 205

    wait_map_start = 0.5
    wait_map_ack   = 0.5
.END
.*                                                      /* SCRX11-032 a-- */
.REALS
    bit[00] = ^H0000
    bit[01] = ^H0001
    bit[02] = ^H0002
    bit[03] = ^H0004
    bit[04] = ^H0008
    bit[05] = ^H0010
    bit[06] = ^H0020
    bit[07] = ^H0040
    bit[08] = ^H0080
    bit[09] = ^H0100
    bit[10] = ^H0200
    bit[11] = ^H0400
    bit[12] = ^H0800
    bit[13] = ^H1000
    bit[14] = ^H2000
    bit[15] = ^H4000
    bit[16] = ^H8000

    rb1 = 1
    rb2 = 2
    rb3 = 3
    rb4 = 4
    pc3 = 5
    pc4 = 6
    tc1 = 7
    tc2 = 8

    cont_d60 = 60
    cont_d61 = 61

    armb_aln  = ^H0001	;*** アライナ（機種有り）
    armb_rob  = ^H0002	;*** ロボット
    armb_trk  = ^H0004	;*** 走行
    armb_dbl  = ^H0008	;*** ダブルハンド
    armb_flp  = ^H0010	;*** スピンハンド
    armb_wet  = ^H0020	;*** WETロボット
    armb_nxx  = ^H0040	;*** NX系ロボット
    armb_3lk  = ^H0080	;*** 3Link
    armb_3mt  = ^H0100	;*** 3Motor N/A
    armb_sxx  = ^H0200	;*** SX
    armb_ntx  = ^H0400	;*** NT
    armb_zzz  = ^H0800	;*** Dual-Z N/A
    armb_twn  = ^H1000	;*** SWING FLIP --> NT570 TWIN HAND

    arm_aln   = ^H0001	;*** Aligner
    arm_a     = ^H0003	;*** A-Type
    arm_at    = ^H0007	;*** A+Track
    arm_ats   = ^H0017	;*** A+Track+Spn
    arm_wet   = ^H0033	;*** WET
    arm_x21   = ^H0043	;*** 2Link 1Hand
    arm_x31   = ^H00c3	;*** A3(3Link1Hand)
    arm_a3t   = ^H00c7	;*** A3(3Link1Hand)+Track
    arm_fnc   = ^H024b	;*** SX920 Furnace
    arm_nt410 = ^H0443
    arm_nt520 = ^H044b
    arm_nt530 = ^H0453
    arm_nt570 = ^H1453

    gbas_none = 0
    gbas_nhom = 1	;*** NEED HOME
    gbas_home = 2

    gbas_escp = 10	;** Escape
    gbas_getp = 11	;** GetWaf
    gbas_putp = 12	;** PutWaf

    gbas_tget = 20	;*** TGetWaf	TGET/TPUT時フラグはこれ以降！
    gbas_tput = 21	;*** TPutWaf
    gbas_tger = 22	;*** TGetWafでError
    gbas_tper = 23	;*** TPutWafでError

    gpos_nchg   = -1
    gpos_home   =  0
    gpos_e_home =  1	;** Entry Home
    gpos_e_prep =  2	;** Entry Prep
    gpos_e_half =  3	;** Entry Half
    gpos_e_near =  4	;** Entry Near
    gpos_e_extd =  5	;** Entry Extd
    gpos_e_slow =  6	;** Entry Slow
    gpos_e_tech =  7	;** Entry Tech
    gpos_r_tech =  8	;** Retract Tech
    gpos_r_slow =  9	;** Retract Slow
    gpos_r_extd = 10	;** Retract Extd
    gpos_r_back = 11	;** Retract Back (BackWD)
    gpos_r_near = 12	;** Retract Near
    gpos_r_half = 13	;** Retract Half
    gpos_r_prep = 14	;** Retract Prep
    gpos_r_home = 15	;** Retract Home

..MONCMD    $gpos_port = "HOME0EHOMEEPREPEHALFENEAREEXTDESLOWETECHRTECHRSLOWREXTDRBACKRNEARRHALFRPREPRHOME"

.** harish 080828 homing a++
    x_ele_x = 1
    x_ele_y = 2
    x_ele_r = 4
    x_ele_s = 5
.** harish 080828 homing a--

    axs_base_x1 = 11	;** TX1
    axs_base_y1 = 12	;** TY1
    axs_base_r1 = 13	;** TR1
    axs_tool_x1 = 14	;** EX1
    axs_tool_y1 = 15	;** EY1
.** harish 090601 a++
    axs_tool_f1r1 = 16	;** F1R1
    axs_tool_f2r1 = 17	;** F2R1    
.** harish 090601 a--
    axs_base_x2 = 21	;** TX2
    axs_base_y2 = 22	;** TY2
    axs_base_r2 = 23	;** TR2
    axs_tool_x2 = 24	;** EX2
    axs_tool_y2 = 25	;** EY2
.** harish 090601 a++
    axs_tool_f1r2 = 26	;** F1R2
    axs_tool_f2r2 = 27	;** F2R2    
.** harish 090601 a--

.** harish 081119 a
    host_all0 = 0
    host_tcp1 = 1
    host_tcp2 = 2
    host_com1 = 3
    host_com2 = 4
    host_stp1 = 5
    host_stp2 = 6
    host_dum1 = 7
    host_dum2 = 8
.** harish 081125 m++
    host_tcp1_sub = 11
    host_tcp2_sub = 12

.** harish 081119 a
..MONCMD $host[host_all0] = "ALL0"
..MONCMD $host[host_tcp1] = "TCP1"
..MONCMD $host[host_tcp2] = "TCP2"
..MONCMD $host[host_com1] = "COM1"
..MONCMD $host[host_com2] = "COM2"
..MONCMD $host[host_stp1] = "STP1"
..MONCMD $host[host_stp2] = "STP2"
..MONCMD $host[host_dum1] = "DUM1"
..MONCMD $host[host_dum2] = "DUM2"
.** harish 081125 a++
..MONCMD $host[host_tcp1_sub] = "TCP1_sub"
..MONCMD $host[host_tcp2_sub] = "TCP2_sub"
.** harish 081125 a--

    inv_normal = 1
    inv_hand   = 1

    d_rob1 = 0
    d_hnd1 = 1
    d_fng1 = 2
    d_prt1 = 3
    d_slt1 = 4
    d_zps1 = 5
    d_llf1 = 6
    d_ulf1 = 7
    d_dat1 = 8
    d_jnt1 = 9
    d_rob2 = 10
    d_hnd2 = 11
    d_fng2 = 12
    d_prt2 = 13
    d_slt2 = 14
    d_zps2 = 15
    d_llf2 = 16
    d_ulf2 = 17
    d_dat2 = 18
    d_jnt2 = 19

    d_divp = 20
    d_max  = 25		;** Last + 1

    mov_non = 0
    mov_jmv = 1
    mov_lmv = 2

    cfgset_nex = 0	;*** NOEXIST_SET_Rのフラグに使用されるので must be 0
    cfgset_def = 1	;*** NOEXIST_SET_Rのフラグに使用されるので must be (not 0)
    cfgset_chk = 2
    cfgset_exe = 3
    cfgqry_chk = 4
    cfgqry_exe = 5
    cfglck_chk = 6
    cfglck_exe = 7
    qrycfg_len = 200
    port_all = -1
    port_non = 0

    pps_get = 1
    pps_gts = 2
    pps_gtx = 3
    pps_gtr = 4
    pps_put = 5
    pps_pts = 6
    pps_ptx = 7
    pps_ptr = 8
    pps_tch = 9
    pps_hom = 10
    pps_map = 11
    pps_imp = 12	;** IMAP
    pps_imr = 13	;** IMAP_R
    pps_gwv = 14
    pps_gwr = 15
.** harish 081210 a
    pps_shp = 16
.** harish 090304-1 a
    pps_aln = 17
.** harish 090304-1 m
..MONCMD $id_pps = "GET,GTS,GTX,GTR,PUT,PTS,PTX,PTR,TAUGHT,HOME,MAP,IMAP,IMAP_R,GWV,GWR,SHIPPING,ALIGN"
    pps_ajp = 16
    pps_pin = 17

    spd_fast   = 1
    spd_mfast  = 2
    spd_medium = 3
    spd_mslow  = 4
    spd_slow   = 5
    spd_escape = 6
..MONCMD $speed_table = "FAST,MEDIUM-FAST,MEDIUM,MEDIUM-SLOW,SLOW"

    teach_respos = 1
    teach_savpos = 2
    teach_rmvpos = 3
    teach_clrpos = 4

    pos_id_actl  = 1
    pos_id_symbl = 2
    pos_id_coord = 3
..MONCMD $id_pos = "ACTL,SYMBL,COORD"

.*********************
.** ++ Port Data ++ **
.*********************
    port_algn = 1
    port_cass = 2
    port_load = 3
    port_util = 4
    port_extr = 5

    port_kind = ^H00FF
    port_form = ^HFF00

    p_hori = ^H0100
    p_vert = ^H0200
    p_down = ^H0400
    p_dive = ^H0800
.*********************
.** -- Port Data -- **
.*********************

    wafer_si = 0
    wafer_qz = 1

    none = -1000
.END

.END
.STRINGS
    $AE_NONE     = "00000"
    $AE_KAWASAKI = " Report to KAWASAKI."

    $ack_ok     = "Ack,0"
    $ack_busy   = "Ack,1"
    $ack_err_cs = "Ack,2"
    $ack_err_fm = "Ack,3"
    $ack_err_pm = "Ack,4"
    $ack_t1tout = "Ack,5"
    $ack_d_trid = "Ack,6"
    $ack_long   = "Ack,3"

    $c_bs = "\b"
    $c_cr = "\r"
    $c_lf = "\n"

    $c_stx = "<"
    $c_mes = ":"
    $c_dat = ","
    $c_end = ">"
    $c_sum = "??"
    $c_etx = "\r\n"

    $res        = "Res"
    $res_ok     = "Res,0"
    $res_ng     = "Res,1"

    $sys_err_idx[0] = ""
    $sys_err_idx[1] = "$sys_err_tbl_p"		;** Pエラー10000
    $sys_err_idx[2] = "$sys_err_tbl_w"		;** Wエラー20000
    $sys_err_idx[3] = "$sys_err_tbl_e"		;** Eエラー30000
    $sys_err_idx[4] = "$sys_err_tbl_d"		;** Dエラー40000
.END
.JOINTS
    #none -1000 -1000 -1000 -1000 -1000 -1000 -1000
.** harish 080828 homing a++
    #preplt[2]  0   -20.389    0    113.691   0     59.693
    #preplb[2]  0     0        0      0       0      0
    #preprt[2]  0    20.383    0   -113.710   0    -48.693
    #preprb[2]  0    30.000    0    -60.000   0      0.000

    #homel[2]   0      0    0     0     0      0
    #homer[2]   0    -10    0   -35     0    -16
    #homec[2]   0      0    0     0     0      0
.** harish 080828 homing a--
.** harish 080922 nt570 homing ++
.** harish 081016 nt570 homing ++
    #homeb[1]    0   13.5    110   -40.30    0    -63.20    0
    #homef[1]    0   13.5    110   -40.30    0    -63.20    180
    #homes[1]    0   13.5    448   -40.30    0    -243.20   90
.*  #homesf[1]   0   13.5    448   -40.30    0    -243.20   90
.*  #homesb[1]   0   13.5    448   -40.30    0    -63.20    90
.** harish 080922 nt570 homing --
.** harish 081016 nt570 homing --
.** harish 081210 a++
.** harish 090415 m++
.** #ref_ship1[1]    0    13.5   440   -40.30  0    -243.20    90 
.** #ref_ship2[1]    0    0      440    0      0    -270       90
    #ref_ship1[1]    0    13.5   470   -40.30  0    -243.20    90 
    #ref_ship2[1]    0    0      470    0      0    -270       90
.** harish 090415 m--
    #ref_ship3[1]    0   -180    440    0      0    -270       180
.** harish 090304-2 m
    #ref_ship1[2]    0    0      0.5    0      0     0
.** harish 081210 a--
.** harish 090304-1 a++
    #ref_algn1[1]    0    13.5    470   -40.30  0    -63.20    90  
    #ref_algn2[1]    0    0       470    0      0    -90       90
    #ref_algn3[1]    0   -180     470    0      0     0        0
.** harish 090304-1 a--
.END
.******************************************
.** sycx_sys.as
.******************************************
..MONCMD $ver_mdl = "Y"
.PROGRAM app_system()
    IF (tsk_arm[0]==2)AND(tsk_arm[1]==arm_nt570)AND(tsk_arm[2]==arm_nt410)THEN
	CALL app_sycx
	app_system = ON
    ELSE
	app_system = OFF
    END
.END
.PROGRAM app_sycx()
    NOEXIST_SET_R vtc.docver[pc3] = 200
    NOEXIST_SET_R vtc.docver[pc4] = 106
;
   cont = cont_d61

   $rob_id[pc3,1] = "R1"
   $rob_id[pc3,2] = ","
   $rob_id[pc3,3] = ","
   $rob_id[pc3,4] = ","
;
   $rob_id[pc4,1] = ","
   $rob_id[pc4,2] = "R1"
   $rob_id[pc4,3] = ""
   $rob_id[pc4,4] = ","
;
    rob_tsk[0] = 2	;** Robot Task数
    rob_tsk[1] = 1	;** Robot1のTask番号
    rob_tsk[2] = 2	;** Robot2のTask番号
    rob_tsk[3] = 0	;** Robot3のTask番号
    rob_tsk[4] = 0	;** Robot4のTask番号
;
    tsk_rob[0] = 2	;** Robot数=RobotTask数
    tsk_rob[1] = 1	;** Task1のRobot番号
    tsk_rob[2] = 2	;** Task2のRobot番号
    tsk_rob[3] = 0	;** Task3のRobot番号
    tsk_rob[4] = 0	;** Task4のRobot番号
    tsk_rob[pc3]  = bit[1]
    tsk_rob[pc4]  = bit[2]
;
    $modl_hnd[1] = "N"
    $modl_hnd[2] = "N"
    $modl_rob[3] = "*"
;
    CALL arm_nt570(1)
    CALL arm_nt410(2)
;
    ZL3BASER  1: 0
    ZL3BASER  2: 180
    ZL3LINKH1 2: 350
    numcyc[1] = 0	;** NT570
    numcyc[2] = 0	;** NT410
;
    can_map[1] = OFF
    can_map[2] = ON
;
    tcp_port[host_tcp1] = 11520
    tcp_port[host_tcp2] = 12800
;
    $net[1,1] = "192.9.200.70"
;
    CALL app_signal
.END
.PROGRAM app_arm_safe(.tsk,.rob,.#loc)
.** Output app_arm_safe[.tsk]
.**
    app_arm_safe[.tsk] = OFF
    IF .rob == 1 THEN
	IF ABS(DEXT(.#loc,4))<5 THEN
	    app_arm_safe[.tsk] = ON
	END
	IF SWITCH("CS",rob_tsk[.rob]) THEN
	    IF g.base[.rob] == gbas_home
		app_arm_safe[.tsk] = ON
	    END
	END
    ELSEIF .rob == 2 THEN
	IF ABS(DEXT(.#loc,4))<5 THEN
	    app_arm_safe[.tsk] = ON
	END
	IF SWITCH("CS",rob_tsk[.rob]) THEN
	    IF g.base[.rob] == gbas_home
		app_arm_safe[.tsk] = ON
	    END
	END
    ELSE
    END
.END
.******************************************
.** app_signal.as
.******************************************
.PROGRAM app_signal()
    so.edg.rels[1,1,2] = 33
    so.edg.hold[1,1,2] = 34
    so.edg.rels[1,1,1] = 37
    so.edg.hold[1,1,1] = 38

    so.edg.rels[2,1,1] = 51
    so.edg.hold[2,1,1] = 52
    so.edg.rels[2,1,2] = 51	;** DUMMY
    so.edg.hold[2,1,2] = 52	;** DUMMY

    si.edg.rels[1,1,2] = 1035
    si.edg.hold[1,1,2] = 1036
    si.edg.rels[1,1,1] = 1039
    si.edg.hold[1,1,1] = 1040

    si.edg.rels[2,1,1] = 1057
    si.edg.hold[2,1,1] = 1058
    si.edg.rels[2,1,2] = 1057	;** DUMMY
    si.edg.hold[2,1,2] = 1058	;** DUMMY

    si.map.find[2,1]   = 1053

    si.fan.rob[1,1]   = 0	;** fan_rob1
    si.fan.rob[1,2]   = 0	;** fan_rob2
    si.fan.rob[1,3]   = 0	;** fan_rob3
    si.fan.rob[1,4]   = 0	;** fan_rob4
    si.fan.rob[1,5]   = 0	;** fan_trkr
    si.fan.rob[1,6]   = 0	;** fan_trkl
    si.fan.rob[2,1]   = 1049	;** fan_rob1
    si.fan.rob[2,2]   = 0	;** fan_rob2
    si.fan.rob[2,3]   = 0	;** fan_rob3
    si.fan.rob[2,4]   = 0	;** fan_rob4
    si.fan.rob[2,5]   = 0	;** fan_trkr
    si.fan.rob[2,6]   = 0	;** fan_trkl
.END
.******************************************
.** app_port.as
.******************************************
.REALS
    p_hori_ns     = 1
    p_hori_wet    = 2
    p_hori_pass   = 3

    p_hori_non    = 4
    p_hori_cs1    = 5
    p_hori_cs2    = 6
    p_hori_cs3    = 7
    p_hori_cs4    = 8
    p_hori_pta    = 9
    p_hori_ptb    = 10
.** harish 080623 m++
    p_hori_vda    = 12
    p_hori_vdb    = 11
.** harish 080623 m--

.** harish 090515-1 a++
    p_hori_imp     = 17
.** harish 090515-1 a--

    p_down_wet    = 21
    p_down_tunl   = 22
    p_down_hclu   = 23
.*                                                      /* SCRX11-032-1 a */
    p_down_rins   = 24

    p_dive_rins   = 41
    p_dive_buff   = 42
.*                                                      /* SCRX12-006-2 a
    p_hori_ws1    = 51
.END
.PROGRAM port_init()
    id_port[pc3,0] = 0
    CALL port.entry(pc3,"PT"  ,"", 201, port_extr BOR p_hori, p_hori_pass, 0)
    CALL port.entry(pc3,"HCLU","", 202, port_extr BOR p_down, p_down_hclu, 0)
    CALL port.entry(pc3,"SHTL","", 203, port_extr BOR p_dive, p_dive_rins, 0)
    CALL port.entry(pc3,"BUF" ,"", 204, port_extr BOR p_dive, p_dive_buff, 0)
.*                                                      /* SCRX11-032-1 a */
    CALL port.entry(pc3,"BL-RINSE" ,"",205, port_extr BOR p_down, p_down_rins, 0)
;
    id_port[pc4,0] = 0
    CALL port.entry(pc4,"P1"  ,"",  11, port_cass BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"P2"  ,"",  12, port_cass BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"P3"  ,"",  13, port_cass BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"P4"  ,"",  14, port_cass BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"LLA" ,"",  21, port_load BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"LLB" ,"",  22, port_load BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"LLC" ,"",  23, port_load BOR p_hori, 0, p_hori_non)
    CALL port.entry(pc4,"LLD" ,"",  24, port_load BOR p_hori, 0, p_hori_non)
    FOR tmp = 1 TO def_uport STEP 1
        CALL port.entry(pc4,"U"+$TRIM_B($ENCODE(tmp)),"",(100+tmp),port_util BOR p_hori, 0, p_hori_non)
    END
.END
.PROGRAM p_hori_app(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE (IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt])) OF
      VALUE  p_hori_pass :
        CALL p_hori_pass(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_cs1 :
        CALL p_hori_cs1(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_cs2 :
        CALL p_hori_cs2(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_cs3 :
        CALL p_hori_cs3(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_cs4 :
        CALL p_hori_cs4(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_pta :
        CALL p_hori_pta(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_ptb :
        CALL p_hori_ptb(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_vda :
        CALL p_hori_vda(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_hori_vdb :
        CALL p_hori_vdb(.tsk,.rob,.hnd,.prt,.slt,.tch)
.** harish 090515 a++
      VALUE  p_hori_imp :
        CALL p_hori_imp(.tsk,.rob,.hnd,.prt,.slt,.tch)
.** harish 090515 a-
.*                                                      /* SCRX12-006-2 a2
      VALUE  p_hori_ws1 :
        CALL p_hori_ws1(.tsk,.rob,.hnd,.prt,.slt,.tch)
      ANY :
        TYPE $RPGNAME()," Parameter Error",(IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt]))
        HALT
    END
.END
.PROGRAM p_down_app(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE (IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt])) OF
      VALUE  p_down_hclu :
        CALL p_down_hclu(.tsk,.rob,.hnd,.prt,.slt,.tch)
.*                                                      /* SCRX11-032-1 a++ */
      VALUE  p_down_rins :
        CALL p_down_rins(.tsk,.rob,.hnd,.prt,.slt,.tch)
.*                                                      /* SCRX11-032-1 a-- */
      ANY :
        TYPE $RPGNAME()," Parameter Error",(IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt]))
        HALT
    END
.END
.PROGRAM p_dive_app(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE (IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt])) OF
      VALUE  p_dive_rins :
        CALL p_dive_rins(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE  p_dive_buff :
        CALL p_dive_buff(.tsk,.rob,.hnd,.prt,.slt,.tch)
      ANY :
        TYPE $RPGNAME()," Parameter Error",(IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt]))
        HALT
    END
.END
.JOINTS
.** harish 081030 m++
    #vx.defpos[1,201,0]  0   73.44   104.4 -139.95 0  -23.49 180.00
    #vx.defpos[1,202,0]  0  -55.43   89.00  130.45 0  -21.20   0.00
    #vx.defpos[1,203,0]  0 -113.21   49.6    94.44 0 -251.24  90.00
    #vx.defpos[1,204,0]  0   48.6    35.00 -109.23 0 -209.36  90.00
    #vx.defpos[1,204,1]  0   48.6    35.00 -109.23 0 -209.36  90.00
    #vx.defpos[1,204,8]  0   58.0    35.00 -126.87 0 -201.07  90.00

    #vx.defpos[2,011,0]  0  -52.77   10.00  165.06 0 -112.28
    #vx.defpos[2,012,0]  0   25.03   10.00   69.45 0  -94.47
    #vx.defpos[2,013,0]  0  -25.03   10.00  -69.45 0   94.47
    #vx.defpos[2,014,0]  0   52.77   10.00 -165.06 0  112.28
    #vx.defpos[2,101,0]  0  -91.98  300.00  119.54 0 -152.56
    #vx.defpos[2,102,0]  0  -72.51  300.00   81.05 0  141.47
    #vx.defpos[2,103,0]  0   -8.22  135.00  -45.28 0 -116.51
    #vx.defpos[2,104,0]  0   54.27  135.00 -133.35 0 -110.91
.** harish 081030 m--
.END
.******************************************
.** app_config.as
.** SycamoreX
.******************************************
.STRINGS
    $vrp.rob_asso[1] = "WR"
    $vrp.rob_asso[2] = "R1"
    $vrp.rob_asso[3] = ""
    $vrp.rob_asso[4] = ""
.END
.PROGRAM cfg_none(.tsk)
    $cfg.old[.tsk] = ""
    CALL cfg_real(.tsk,"T1"         ,vtp.t1[         .tsk] ,vtx.t1[         .tsk] ,  5, 1,   30)
    CALL cfg_real(.tsk,"T2"         ,vtp.t2[         .tsk] ,vtx.t2[         .tsk] ,  5, 1,   30)
    CALL cfg_real(.tsk,"TRES"       ,vtp.tres[       .tsk] ,vtx.tres[       .tsk] ,100, 0, 1000)
    CALL cfg_real(.tsk,"THALF"      ,vtp.thalf[      .tsk] ,vtx.thalf[      .tsk] ,200, 0, 1000)
    CALL cfg_real(.tsk,"TGRIP"      ,vtp.tgrip[      .tsk] ,vtx.tgrip[      .tsk] ,  5, 1,   10)

    CALL cfg_real(.tsk,"EVTNUM"     ,vtp.evtnum[     .tsk] ,vtx.evtnum[     .tsk] ,  3, 1,   10)
    CALL cfg_real(.tsk,"COLDET"     ,vtp.coldet[     .tsk] ,vtx.coldet[     .tsk] ,  1, 0,    1)
    CALL cfg_real(.tsk,"MOVLIM"     ,vtp.movlim[     .tsk] ,vtx.movlim[     .tsk] ,  1, 0,    1)
    CALL cfg_real(.tsk,"AJPLIM"     ,vtp.ajplim[     .tsk] ,vtx.ajplim[     .tsk] ,  1, 0,    1)

    CALL cfg_real(.tsk,"GETRETCHK"  ,vtp.getretchk[  .tsk] ,vtx.getretchk[  .tsk] , def.getretchk[.tsk],   0,    1)
    CALL cfg_real(.tsk,"AFTPUTABS"  ,vtp.aftputabs[  .tsk] ,vtx.aftputabs[  .tsk] , def.aftputabs[.tsk],   0,    1)
    CALL cfg_real(.tsk,"GETPREBCK"  ,vtp.getprebck[  .tsk] ,vtx.getprebck[  .tsk] , def.getprebck[.tsk],   0,    1)

.** harish 080630 a++
    CALL cfg_real(.tsk,"SNIFF"      ,vtp.sniff[.tsk] ,vtx.sniff[.tsk] ,  0, 0,    1 )
.** SCRX10-027-2 m
    CALL cfg_r100(.tsk,"SNFZLIM"   ,vnp.snfzlim ,vnx.snfzlim ,  2         , 0.01,  3 )
.*   made port specific as vpp.snfzup[] - do not use vnp.snfzup anymore                 /* SCRX12-006 d
;    CALL cfg_r100(.tsk,"SNFZUP"    ,vnp.snfzup  ,vnx.snfzup  , 10         , 5   , 15 )
    CALL cfg_real(.tsk,"SNFSPD"    ,vnp.snfspd  ,vnx.snfspd  , def.snfspd , 1   , 20 )
.** harish 080630 a--

    CALL cfg_real(.tsk,"QRYCFGERROR",vtp.qrycfgerror[.tsk] ,vtx.qrycfgerror[.tsk] ,  0, 0,    1)

    CALL cfg_real(.tsk,"WAFGONECHK" ,vtp.wafgonechk[ .tsk] ,vtx.wafgonechk[ .tsk] ,  1, 0,    1)

    CALL cfg_real(.tsk,"FANCTL"     ,vtp.fanctl[     .tsk] ,vtx.fanctl[     .tsk] ,  1, 0,    1)
    CALL cfg_real(.tsk,"FANROB"     ,vtp.fanrob[     .tsk] ,vtx.fanrob[     .tsk] ,  1, 0,    1)
;
    IF .tsk==pc3 THEN
        CALL cfg_none_pc3(.tsk)
    END
    IF .tsk == pc4 THEN
        CALL cfg_none_pc4(.tsk)
    END
.END
.PROGRAM cfg_none_pc3(.tsk)
    CALL cfg_rstr(.tsk,"ROBOTTYPE"      ,vnp.robottype[.tsk]   ,vnx.robottype[.tsk]   ,"NT-TWIN"  , 0, 0.1)
    CALL cfg_real(.tsk,"ROBOTTYPE"      ,vnp.robottype[.tsk]   ,vnx.robottype[.tsk]   , 0,  0   , 0  )
    CALL cfg_real(.tsk,"AXISCNT"        ,vnp.axiscnt[1]        ,vnx.axiscnt[1], id_anum[1,0], id_anum[1,0], id_anum[1,0] )
.** harish 090629-1 a++
.** JOINT 2
    CALL cfg_r100(.tsk,"BLSTEP2"    ,vnp.blstep2[1]    ,vnx.blstep2[1]      , 0.01,    0,   10)
    CALL cfg_real(.tsk,"BLPERRLIM2" ,vnp.blperrlim2[1] ,vnx.blperrlim2[1]   ,  -40,-1000,    0)
    CALL cfg_real(.tsk,"BLCURLIM2"  ,vnp.blcurlim2[1]  ,vnx.blcurlim2[1]    ,  -60,-1000,    0)
    CALL cfg_r100(.tsk,"BLANGLIM2"  ,vnp.blanglim2[1]  ,vnx.blanglim2[1]    ,    2,    0,   10)
    CALL cfg_real(.tsk,"BLCYCLIM2"  ,vnp.blcyclim2[1]  ,vnx.blcyclim2[1]    ,    5,    0,   10)
.** JOINT 4
    CALL cfg_r100(.tsk,"BLSTEP4"    ,vnp.blstep4[1]    ,vnx.blstep4[1]      , 0.01,    0,   10)
    CALL cfg_real(.tsk,"BLPERRLIM4" ,vnp.blperrlim4[1] ,vnx.blperrlim4[1]   ,  -40,-1000,    0)
    CALL cfg_real(.tsk,"BLCURLIM4"  ,vnp.blcurlim4[1]  ,vnx.blcurlim4[1]    ,  -40,-1000,    0)
    CALL cfg_r100(.tsk,"BLANGLIM4"  ,vnp.blanglim4[1]  ,vnx.blanglim4[1]    ,    2,    0,   10)
    CALL cfg_real(.tsk,"BLCYCLIM4"  ,vnp.blcyclim4[1]  ,vnx.blcyclim4[1]    ,    5,    0,   10)
.** JOINT 6
    CALL cfg_r100(.tsk,"BLSTEP6"    ,vnp.blstep6[1]    ,vnx.blstep6[1]      , 0.01,    0,   10)
    CALL cfg_real(.tsk,"BLPERRLIM6" ,vnp.blperrlim6[1] ,vnx.blperrlim6[1]   ,  -40,-1000,    0)
    CALL cfg_real(.tsk,"BLCURLIM6"  ,vnp.blcurlim6[1]  ,vnx.blcurlim6[1]    ,  -20,-1000,    0)
    CALL cfg_r100(.tsk,"BLANGLIM6"  ,vnp.blanglim6[1]  ,vnx.blanglim6[1]    ,    2,    0,   10)
    CALL cfg_real(.tsk,"BLCYCLIM6"  ,vnp.blcyclim6[1]  ,vnx.blcyclim6[1]    ,    5,    0,   10)
.** pass criteria
    CALL cfg_r100(.tsk,"BLPASS"     ,vnp.blpass[1]     ,vnx.blpass[1]       ,  1.2,   0,   10)
.** harish 090629-1 a--
.END
.PROGRAM cfg_none_pc4(.tsk)
    CALL cfg_rstr(.tsk,"ROBOTTYPE"      ,vnp.robottype[.tsk]   ,vnx.robottype[.tsk]   ,"NT-STD"   , 0, 0.1)
    CALL cfg_real(.tsk,"ROBOTTYPE"      ,vnp.robottype[.tsk]   ,vnx.robottype[.tsk]   , 0,  0   , 0  )
    CALL cfg_real(.tsk,"AXISCNT"        ,vnp.axiscnt[2]        ,vnx.axiscnt[2], id_anum[2,0], id_anum[2,0], id_anum[2,0] )
.***    CALL cfg_real(.tsk,"TOUTA1VAC"  ,vnp.toutavac[3]       ,vnx.toutavac[3]       ,  2, 1,    5)
.***    CALL cfg_real(.tsk,"SPDALNWAF"  ,vnp.spdalnwaf         ,vnx.spdalnwaf         , 30,30,   30)
.***    CALL cfg_real(.tsk,"SPDLALGN"   ,vnp.spdlalgn          ,vnx.spdlalgn          , 25,20,   35 )
.***
.***    CALL cfg_rstr(.tsk,"ALNPOSA1"   ,vnp.alnpos[3]         ,vnx.alnpos[3]    ,"0"          ,0    , 0.1)
.***    CALL cfg_rstr(.tsk,"ALNPOSA1"   ,vnp.alnpos[3]         ,vnx.alnpos[3]    ,"90"         ,90   , 0.1)
.***    CALL cfg_rstr(.tsk,"ALNPOSA1"   ,vnp.alnpos[3]         ,vnx.alnpos[3]    ,"180"        ,180  , 0.1)
.***    CALL cfg_rstr(.tsk,"ALNPOSA1"   ,vnp.alnpos[3]         ,vnx.alnpos[3]    ,"270"        ,270  , 0.1)
.***    CALL cfg_real(.tsk,"ALNPOSA1"   ,vnp.alnpos[3]         ,vnx.alnpos[3]    , 0           ,0    , 0  )
.***
.***    CALL cfg_rstr(.tsk,"WAFER"      ,vnp.wafer             ,vnx.wafer             ,"SI"  , wafer_si, 0.1)
.***    CALL cfg_rstr(.tsk,"WAFER"      ,vnp.wafer             ,vnx.wafer             ,"QZ"  , wafer_qz, 0.1)
.***    CALL cfg_real(.tsk,"WAFER"      ,vnp.wafer             ,vnx.wafer             , wafer_si, 0   , 0  )
.***
.*                                                      /* SCRX12-006-4 a++
    CALL cfg_r100(.tsk,"DUCKHGH"    ,vnp.duckhgh[2]        ,vnx.duckhgh[2]   , 310       , 0         , 999 )
    CALL cfg_r100(.tsk,"DUCKHGHLOFST" ,vnp.ducklofst[2]  ,vnx.ducklofst[2], 0         , 0         , 999 )
    CALL cfg_r100(.tsk,"DUCKHGHUOFST" ,vnp.duckuofst[2]  ,vnx.duckuofst[2], 999       , 0         , 999 )
.*                                                      /* SCRX12-006-4 a--
    CALL cfg_real(.tsk,"PORT"       ,vnp.port              ,vnx.port         , def_port  , def_port  , def_port  )
    CALL cfg_real(.tsk,"UPORT"      ,vnp.uport             ,vnx.uport        , def_uport , def_uport , def_uport )
.*                                                      /* SCRX11-066-2 m++
    CALL cfg_r100(.tsk,"MAPABOVE"   ,vnp.mapabove          ,vnx.mapabove     , 5           , 3   , 10 )
    CALL cfg_r100(.tsk,"MAPBELOW"   ,vnp.mapbelow          ,vnx.mapbelow     , 5           , 3   , 10 )
.*                                                      /* SCRX11-066-2 m--
    CALL cfg_r100(.tsk,"MAPCOMB"    ,vnp.mapcomb           ,vnx.mapcomb      , 0.4         , 0   ,  1 )
.** harish 081120 m
    CALL cfg_r100(.tsk,"MAPZLIM"    ,vnp.mapzlim           ,vnx.mapzlim      , 2           , 0.01,  3 )
.*                                                      /* SCRX11-066-2 m
    CALL cfg_r100(.tsk,"WAFTHICK"   ,vnp.wafthick          ,vnx.wafthick     , 0.75        , 0.5 ,  2 )
    CALL cfg_r100(.tsk,"MAPWAF3X"   ,vnp.mapwaf3x          ,vnx.mapwaf3x     , 15          ,15   , 15 )
.*                                                      /* SCRX11-066-2 a++
    CALL cfg_r100(.tsk,"DBLSLOTFACTOR"   ,vnp.dblfactor    ,vnx.dblfactor    , 1.74        , 1   , 10 )
    CALL cfg_r100(.tsk,"CRSSLOTFACTOR"   ,vnp.crsfactor    ,vnx.crsfactor    , 1.74        , 1   , 10 )
.*                                                      /* SCRX11-066-2 a--
    CALL cfg_real(.tsk,"SAFEMAP"    ,vnp.safemap           ,vnx.safemap      , 0           , 0   ,  1 )
.** harish 090515-1 m
    CALL cfg_real(.tsk,"SFMAPSLOT"  ,vnp.sfmapslot         ,vnx.sfmapslot    , 0           , 0   ,  1 )
    CALL cfg_r100(.tsk,"SFMPDIST"   ,vnp.sfmpdist          ,vnx.sfmpdist     , 30          , 0   , 30 )
    CALL cfg_r100(.tsk,"SFMPLAST"   ,vnp.sfmplast          ,vnx.sfmplast     , 10          , 0   , 10 )
    CALL cfg_r100(.tsk,"SFMPPITCH"  ,vnp.sfmppitch         ,vnx.sfmppitch    , 15          ,15   , 15 )
    CALL cfg_r100(.tsk,"MAPRECAL"   ,vnp.maprecal          ,vnx.maprecal     , 0           , 0   ,  2 )
    CALL cfg_real(.tsk,"USEMAPZ"    ,vnp.usemapz           ,vnx.usemapz      , 0           , 0   ,  1 )
    CALL cfg_r100(.tsk,"MTLMAPZ"    ,vnp.mtlmapz           ,vnx.mtlmapz      , 5           , 0   , 10 )
.** harish 090515-1 a++
.** SCRX10-027-2 m
    CALL cfg_r100(.tsk,"MXIN"       ,vnp.mxin           ,vnx.mxin           ,  180,   0,  200)
    CALL cfg_r100(.tsk,"MXINVEL"    ,vnp.mxinvel        ,vnx.mxinvel        ,  6.5,   1,  100)
    CALL cfg_r100(.tsk,"MXOUTVEL"   ,vnp.mxoutvel       ,vnx.mxoutvel       ,  100,   1,  100)
.** SCRX10-027-2 m
    CALL cfg_r100(.tsk,"MOFFSET"    ,vnp.moffset        ,vnx.moffset        ,   12,   0,  100)
.** harish 090515-1 a--
.** harish 090629-1 a++
.** JOINT 2
    CALL cfg_r100(.tsk,"BLSTEP2"    ,vnp.blstep2[2]    ,vnx.blstep2[2]      , 0.01,   0,   10)
    CALL cfg_real(.tsk,"BLPERRLIM2" ,vnp.blperrlim2[2] ,vnx.blperrlim2[2]   ,   40,   0, 1000)
    CALL cfg_real(.tsk,"BLCURLIM2"  ,vnp.blcurlim2[2]  ,vnx.blcurlim2[2]    ,   60,   0, 1000)
    CALL cfg_r100(.tsk,"BLANGLIM2"  ,vnp.blanglim2[2]  ,vnx.blanglim2[2]    ,    2,   0,   10)
    CALL cfg_real(.tsk,"BLCYCLIM2"  ,vnp.blcyclim2[2]  ,vnx.blcyclim2[2]    ,    5,   0,   10)
.** JOINT 4
    CALL cfg_r100(.tsk,"BLSTEP4"    ,vnp.blstep4[2]    ,vnx.blstep4[2]      , 0.01,   0,   10)
    CALL cfg_real(.tsk,"BLPERRLIM4" ,vnp.blperrlim4[2] ,vnx.blperrlim4[2]   ,   40,   0, 1000)
    CALL cfg_real(.tsk,"BLCURLIM4"  ,vnp.blcurlim4[2]  ,vnx.blcurlim4[2]    ,   40,   0, 1000)
    CALL cfg_r100(.tsk,"BLANGLIM4"  ,vnp.blanglim4[2]  ,vnx.blanglim4[2]    ,    2,   0,   10)
    CALL cfg_real(.tsk,"BLCYCLIM4"  ,vnp.blcyclim4[2]  ,vnx.blcyclim4[2]    ,    5,   0,   10)
.** JOINT 6
    CALL cfg_r100(.tsk,"BLSTEP6"    ,vnp.blstep6[2]    ,vnx.blstep6[2]      , 0.01,   0,   10)
    CALL cfg_real(.tsk,"BLPERRLIM6" ,vnp.blperrlim6[2] ,vnx.blperrlim6[2]   ,   40,   0, 1000)
    CALL cfg_real(.tsk,"BLCURLIM6"  ,vnp.blcurlim6[2]  ,vnx.blcurlim6[2]    ,   20,   0, 1000)
    CALL cfg_r100(.tsk,"BLANGLIM6"  ,vnp.blanglim6[2]  ,vnx.blanglim6[2]    ,    2,   0,   10)
    CALL cfg_real(.tsk,"BLCYCLIM6"  ,vnp.blcyclim6[2]  ,vnx.blcyclim6[2]    ,    5,   0,   10)
.** pass criteria
    CALL cfg_r100(.tsk,"BLPASS"     ,vnp.blpass[2]     ,vnx.blpass[2]       ,  1.2,   0,   10)
.** harish 090629-1 a--
.END
.PROGRAM cfg_robot(.tsk,.rob)
    $cfg.old[.tsk] = ""
    CALL cfg_real(.tsk,"AXISCNT",vnp.axiscnt[.rob] ,vnx.axiscnt[.rob], id_anum[.rob,0], id_anum[.rob,0], id_anum[.rob,0] )
    IF .rob==1 THEN
    ELSEIF .rob==2 THEN
        CALL cfg_r100(.tsk,"MAPDEPTH",vnp.mapdepth[.rob] ,vnx.mapdepth[.rob], def.mapdepth[.rob,1], def.mapdepth[.rob,2], def.mapdepth[.rob,3] )
    END
.END
.PROGRAM cfg_port(.tsk,.prt)
    $cfg.old[.tsk] = ""
    IF .tsk == pc3 THEN
        CASE (port_type[0,.prt] BAND port_form ) OF
          VALUE p_hori :
            CALL cfg_p_ho_pass(.tsk,.prt)
          VALUE p_down :
            CALL cfg_p_dn_hclu(.tsk,.prt)
          VALUE p_dive :
            CALL cfg_p_dive(.tsk,.prt)
          ANY :
            TYPE "No Port",$rpgname()
            HALT
        END
    ELSE
        CASE (port_type[0,.prt] BAND port_form ) OF
          VALUE p_hori:
            CALL cfg_p_ho_fi(.tsk,.prt)
          ANY :
            TYPE "No Port",$rpgname()
            HALT
        END
    END
.END
.PROGRAM cfg_p_ho_fi(.tsk,.prt)
   .typ = port_type[0,.prt] BAND port_kind

        CALL cfg_real(.tsk,"SLOT"     ,vpp.slot[     .prt],vpx.slot[     .prt],def_slot[     .typ],   1, 150)
        CALL cfg_r100(.tsk,"PITCH"    ,vpp.pitch[    .prt],vpx.pitch[    .prt],def_pitch[    .typ],   0, 100)

        CALL cfg_r100(.tsk,"LLIFT"    ,vpp.llift[    .prt],vpx.llift[    .prt],def_llift[    .typ],   0,  20)
        CALL cfg_r100(.tsk,"ULIFT"    ,vpp.ulift[    .prt],vpx.ulift[    .prt],def_ulift[    .typ],   0,  20)
        CALL cfg_r100(.tsk,"GFORWD"   ,vpp.gforwd[   .prt],vpx.gforwd[   .prt],def_gforwd[   .typ],   0,  10)
        CALL cfg_r100(.tsk,"PFORWD"   ,vpp.pforwd[   .prt],vpx.pforwd[   .prt],def_pforwd[   .typ],   0,  10)
        CALL cfg_r100(.tsk,"BACKWD"   ,vpp.backwd[   .prt],vpx.backwd[   .prt],def_backwd[   .typ],   0,  10)
        CALL cfg_r100(.tsk,"PUTBACK"  ,vpp.putback[  .prt],vpx.putback[  .prt],def_putback[  .typ],   0,   5)
        CALL cfg_real(.tsk,"GETRETCHK",vpp.getretchk[.prt],vpx.getretchk[.prt],def_getretchk[.typ],   0,    1)
        CALL cfg_real(.tsk,"AFTPUTABS",vpp.aftputabs[.prt],vpx.aftputabs[.prt],def_aftputabs[.typ],   0,    1)
        CALL cfg_real(.tsk,"GETPREBCK",vpp.getprebck[.prt],vpx.getprebck[.prt],def_getprebck[.typ],   0,    1)

        CALL cfg_real(.tsk,"GZSPD"    ,vpp.gzspd[    .prt],vpx.gzspd[    .prt],                100,   1,  100)
        CALL cfg_real(.tsk,"PZSPD"    ,vpp.pzspd[    .prt],vpx.pzspd[    .prt],                100,   1,  100)

        CALL cfg_real(.tsk,"ERRRET"   ,vpp.errret[   .prt],vpx.errret[   .prt], 1, 0, 1)

        CALL cfg_r100(.tsk,"PINTCHZDN",vpp.pintchzdn[.prt],vpx.pintchzdn[.prt],def_pintchzdn[.typ],   2,   5)
        CALL cfg_r100(.tsk,"PINTCHZUP",vpp.pintchzup[.prt],vpx.pintchzup[.prt],def_pintchzup[.typ],   2,  10)
        CALL cfg_real(.tsk,"PINTCHSPD",vpp.pintchspd[.prt],vpx.pintchspd[.prt],def_pintchspd[.typ],   1,   5)
        CALL cfg_r100(.tsk,"PINZOFST" ,vpp.pinzofst[ .prt],vpx.pinzofst[ .prt],0                  ,-100, 100)
        CALL cfg_r100(.tsk,"PINYBACK" ,vpp.pinyback[ .prt],vpx.pinyback[ .prt],def.pinyback[ .prt],   0, 200)
        CALL cfg_r100(.tsk,"ZUPATCHRT",vpp.zupatchrt[.prt],vpx.zupatchrt[.prt],def_zupatchrt[.typ],-100, 350)

        CALL cfg_r100(.tsk,"JIGDIA"   ,vpp.jigdia[   .prt],vpx.jigdia[   .prt],300                , 290, 310)

        CALL cfg_real(.tsk,"SNIFF"    ,vpp.sniff[    .prt],vpx.sniff[    .prt],0                  ,   0,   1)
.***    IF (.typ == port_algn) THEN
.***        CALL cfg_r100(.tsk,"ALNOFFR1" ,vpp.alnoff[ 2,.prt],vpx.alnoff[ 2,.prt],0                  , -45,  45)
.***    END
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"CS1"       , p_hori_cs1, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"CS2"       , p_hori_cs2, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"CS3"       , p_hori_cs3, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"CS4"       , p_hori_cs4, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"PTA"       , p_hori_pta, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"PTB"       , p_hori_ptb, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"VDA"       , p_hori_vda, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"VDB"       , p_hori_vdb, 0.1)
.*                                                      /* SCRX12-006-2 a
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"WS1"       , p_hori_ws1, 0.1)
.** harish 090515-1 a
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"IMAP"      , p_hori_imp, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"UNDEFINED" , 0, 0.1)
        CALL cfg_real(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt], def.portarea[.prt], p_hori_cs1 , p_hori_vdb)
.*                                                      /* SCRX12-006-2 a
        CALL cfg_real(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt], def.portarea[.prt], p_hori_ws1 , p_hori_ws1)
        CALL cfg_r100(.tsk,"PORTZ"    ,vpp.portz[    .prt],vpx.portz[    .prt], def.portz[.prt],  0, 500)
.** harish inverse kinematics 080930 a+
        CALL cfg_r100(.tsk,"PORTANGLE"    ,vpp.portang[    .prt],vpx.portang[    .prt], def.portang[.prt],  -180, 180)
.** harish 090515-1 a++
        IF (.typ == port_util)
            CALL cfg_real(.tsk,"ISIMAP"    ,vpp.isimap[    .prt],vpx.isimap[    .prt],                      0,  0,  1)
        END
.** harish 090515-1 a--
.** SCRX10-027-5 a++
        IF (.typ == port_util)
            CALL cfg_r100(.tsk,"X1OFFSET"    ,vpp.x1offset[    .prt],vpx.x1offset[    .prt],           0,  -350,  350)
        END
.*                                                      /* SCRX12-006 a
        CALL cfg_r100(.tsk,"SNFZUP"    ,vpp.snfzup[    .prt],vpx.snfzup[    .prt], def.snfzup,  5, 25)
.** SCRX10-027-5 a--
.END
.PROGRAM cfg_p_ho_pass(.tsk,.prt)
   .typ = port_type[0,.prt] BAND port_kind

.** harish 081023 m++
        CALL cfg_real(.tsk,"SLOT"     ,vpp.slot[     .prt],vpx.slot[     .prt], 1, 1,150)
        CALL cfg_r100(.tsk,"PITCH"    ,vpp.pitch[    .prt],vpx.pitch[    .prt], 0, 0,100)
.** harish 081023 m--

        CALL cfg_r100(.tsk,"WLLIFT"   ,vpp.llift[    .prt],vpx.llift[    .prt], def.llift[    .prt],   0,   20)
        CALL cfg_r100(.tsk,"WULIFT"   ,vpp.ulift[    .prt],vpx.ulift[    .prt], def.ulift[    .prt],   0,   20)
        CALL cfg_r100(.tsk,"WGFORWD"  ,vpp.gforwd[   .prt],vpx.gforwd[   .prt], def.gforwd[   .prt],   0,   10)
        CALL cfg_r100(.tsk,"WPFORWDY" ,vpp.wpforwdy[ .prt],vpx.wpforwdy[ .prt], def.wpforwdy[ .prt],   1,    5)
        CALL cfg_r100(.tsk,"WPFORWDZ" ,vpp.wpforwdz[ .prt],vpx.wpforwdz[ .prt], def.wpforwdz[ .prt],   1,    5)
        CALL cfg_r100(.tsk,"WBACKWD"  ,vpp.backwd[   .prt],vpx.backwd[   .prt], def.backwd[   .prt],   0,   20)
        CALL cfg_r100(.tsk,"WPUTBACK" ,vpp.putback[  .prt],vpx.putback[  .prt], def.putback[  .prt],   0,    5)
        CALL cfg_real(.tsk,"WGZSPD"   ,vpp.gzspd[    .prt],vpx.gzspd[    .prt], def.gzspd[    .prt],   1,  100)
        CALL cfg_real(.tsk,"WPZSPD"   ,vpp.pzspd[    .prt],vpx.pzspd[    .prt], def.pzspd[    .prt],   1,  100)
        CALL cfg_real(.tsk,"GETRETCHK",vpp.getretchk[.prt],vpx.getretchk[.prt], def.getretchk[.prt],   0,    1)
        CALL cfg_real(.tsk,"AFTPUTABS",vpp.aftputabs[.prt],vpx.aftputabs[.prt], def.aftputabs[.prt],   0,    1)
        CALL cfg_real(.tsk,"GETPREBCK",vpp.getprebck[.prt],vpx.getprebck[.prt], def.getprebck[.prt],   0,    1)
        CALL cfg_real(.tsk,"ERRRET"   ,vpp.errret[   .prt],vpx.errret[   .prt],                   1,   0,    1)

        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"PASS" , p_hori_pass, 0.1)
        CALL cfg_real(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt], def.portarea[.prt], p_hori_pass , p_hori_pass)
        CALL cfg_r100(.tsk,"PORTZ"    ,vpp.portz[    .prt],vpx.portz[    .prt], def.portz[.prt],  0, 500)
.** harish inverse kinematics 080930 a+
        CALL cfg_r100(.tsk,"PORTANGLE"    ,vpp.portang[    .prt],vpx.portang[    .prt], def.portang[.prt],  -180, 180)
.** harish 090703 a
        CALL cfg_r100(.tsk,"PINZOFST" ,vpp.pinzofst[ .prt],vpx.pinzofst[ .prt],0                  ,-400, 400)
.END
.PROGRAM cfg_p_dn_hclu(.tsk,.prt)
   .typ = port_type[0,.prt] BAND port_kind

        CALL cfg_real(.tsk,"SLOT"     ,vpp.slot[     .prt],vpx.slot[     .prt] , 1, 1, 1)
        CALL cfg_r100(.tsk,"PITCH"    ,vpp.pitch[    .prt],vpx.pitch[    .prt] , 0, 0, 0)

        CALL cfg_r100(.tsk,"WZACCESS" ,vpp.wzaccess[ .prt],vpx.wzaccess[ .prt] ,def.wzaccess[ .prt],   5,   100)
        CALL cfg_r100(.tsk,"WZSLOWH"  ,vpp.wzslowh[  .prt],vpx.wzslowh[  .prt] ,def.wzslowh[  .prt],   0,   50)
        CALL cfg_r100(.tsk,"WGFORWD"  ,vpp.gforwd[   .prt],vpx.gforwd[   .prt] ,def.gforwd[   .prt],   0,   10)
        CALL cfg_r100(.tsk,"WPFORWDY" ,vpp.wpforwdy[ .prt],vpx.wpforwdy[ .prt] ,def.wpforwdy[ .prt],   1,    5)
        CALL cfg_r100(.tsk,"WPFORWDZ" ,vpp.wpforwdz[ .prt],vpx.wpforwdz[ .prt] ,def.wpforwdz[ .prt],   1,    5)
        CALL cfg_r100(.tsk,"WPUTBACK" ,vpp.putback[  .prt],vpx.putback[  .prt] ,def.putback[  .prt],   0,    5)

        CALL cfg_real(.tsk,"WZSPDBG"  ,vpp.wzspdbg[  .prt],vpx.wzspdbg[  .prt] ,def.wzspdbg[  .prt],   1,  100)
        CALL cfg_real(.tsk,"WZSPDAG"  ,vpp.wzspdag[  .prt],vpx.wzspdag[  .prt] ,def.wzspdag[  .prt],   1,  100)
        CALL cfg_real(.tsk,"WZSPDBP"  ,vpp.wzspdbp[  .prt],vpx.wzspdbp[  .prt] ,def.wzspdbp[  .prt],   1,  100)
        CALL cfg_real(.tsk,"WZSPDAP"  ,vpp.wzspdap[  .prt],vpx.wzspdap[  .prt] ,def.wzspdap[  .prt],   1,  100)

        CALL cfg_real(.tsk,"WYSPEED"  ,vpp.wyspeed[  .prt],vpx.wyspeed[  .prt] ,def.wyspeed[  .prt],   1,  100)

        CALL cfg_real(.tsk,"GETRETCHK",vpp.getretchk[.prt],vpx.getretchk[.prt] ,def.getretchk[.prt],   0,    1)
        CALL cfg_real(.tsk,"AFTPUTABS",vpp.aftputabs[.prt],vpx.aftputabs[.prt] ,def.aftputabs[.prt],   0,    1)
        CALL cfg_real(.tsk,"ERRRET"   ,vpp.errret[   .prt],vpx.errret[   .prt] ,1, 0, 1)
.*                                                      /* SCRX11-032-1 a++ */
        IF .prt == HCLU THEN
            CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"HCLU" , p_down_hclu, 0.1)
        ELSEIF .prt == BL_RINSE THEN
            CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"BL-RINSE" , p_down_rins, 0.1)
        END
.*                                                      /* SCRX11-032-1 a-- */
        CALL cfg_real(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt], def.portarea[.prt], p_down_hclu , p_down_hclu)
        CALL cfg_r100(.tsk,"PORTZ"    ,vpp.portz[    .prt],vpx.portz[    .prt], def.portz[.prt],  0, 500)
.** harish inverse kinematics 080930 a+
        CALL cfg_r100(.tsk,"PORTANGLE"    ,vpp.portang[    .prt],vpx.portang[    .prt], def.portang[.prt],  -180, 180)
.** harish 090703 a
        CALL cfg_r100(.tsk,"PINZOFST" ,vpp.pinzofst[ .prt],vpx.pinzofst[ .prt],0                  ,-400, 400)
.END
.PROGRAM cfg_p_dive(.tsk,.prt)
   .typ = port_type[0,.prt] BAND port_kind

        CALL cfg_real(.tsk,"SLOT"     ,vpp.slot[     .prt],vpx.slot[     .prt], def.slot[.prt,0], def.slot[.prt,1], def.slot[.prt,2])
.** harish 080724 m+
        CALL cfg_r100(.tsk,"PITCH"    ,vpp.pitch[    .prt],vpx.pitch[    .prt], def.pitch[.prt], 0, 100)

        CALL cfg_r100(.tsk,"WZACCESS" ,vpp.wzaccess[ .prt],vpx.wzaccess[ .prt] ,def.wzaccess[ .prt], 300,  500)
        CALL cfg_r100(.tsk,"WZSLOWH"  ,vpp.wzslowh[  .prt],vpx.wzslowh[  .prt] ,def.wzslowh[  .prt],   0,   50)

        CALL cfg_r100(.tsk,"WFORWDY"  ,vpp.wforwdy[  .prt],vpx.wforwdy[  .prt] ,def.wforwdy[  .prt],   0,   30)
        CALL cfg_r100(.tsk,"WFORWDZ"  ,vpp.wforwdz[  .prt],vpx.wforwdz[  .prt] ,def.wforwdz[  .prt],   0,   10)

        CALL cfg_real(.tsk,"WGZSPD"   ,vpp.gzspd[    .prt],vpx.gzspd[    .prt] ,def.gzspd[    .prt],   1,  100)
        CALL cfg_real(.tsk,"WPZSPD"   ,vpp.pzspd[    .prt],vpx.pzspd[    .prt] ,def.pzspd[    .prt],   1,  100)
        CALL cfg_real(.tsk,"WYSPEED"  ,vpp.wyspeed[  .prt],vpx.wyspeed[  .prt] ,def.wyspeed[  .prt],   1,  100)

        CALL cfg_real(.tsk,"GETRETCHK",vpp.getretchk[.prt],vpx.getretchk[.prt], def.getretchk[.prt],   0,    1)
        CALL cfg_real(.tsk,"AFTPUTABS",vpp.aftputabs[.prt],vpx.aftputabs[.prt], def.aftputabs[.prt],   0,    1)
        CALL cfg_real(.tsk,"ERRRET"   ,vpp.errret[   .prt],vpx.errret[   .prt], 1, 0, 1)

        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"RINS" , p_dive_rins, 0.1)
        CALL cfg_rstr(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt],"BUFF" , p_dive_buff, 0.1)
        CALL cfg_real(.tsk,"PORTAREA" ,vpp.portarea[ .prt],vpx.portarea[ .prt], def.portarea[.prt], p_dive_rins , p_dive_buff)
        CALL cfg_r100(.tsk,"PORTZ"    ,vpp.portz[    .prt],vpx.portz[    .prt], def.portz[.prt],  0, 500)
.** harish 090209-2 a++
        CALL cfg_real(.tsk,"WGZINOUTSPD"   ,vpp.gziospd[    .prt],vpx.gziospd[    .prt] ,def.gziospd[    .prt],   1,  100)
        CALL cfg_real(.tsk,"WPZINOUTSPD"   ,vpp.pziospd[    .prt],vpx.pziospd[    .prt] ,def.pziospd[    .prt],   1,  100)
.** harish 090225-1 m
        CALL cfg_real(.tsk,"WGRIPTIME"           ,vpp.wgriptim[    .prt],vpx.wgriptim[    .prt] ,def.wgriptim[    .prt],   0,  10)
.** harish 090209-2 a--
.** harish 080804 a++
        If .prt == 204
.** harish 090408-2 m++
            CALL cfg_r100(.tsk,"TSZCALERROR",vpp.tszcalerr[.prt],vpx.tszcalerr[.prt],2.0,0.01,5.0)
            CALL cfg_r100(.tsk,"TSRCALERROR",vpp.tsrcalerr[.prt],vpx.tsrcalerr[.prt],2.0,0.01,5.0)
.** harish 090408-2 m--
            CALL cfg_tslots(.tsk,.prt)
        END
.** harish 080804 a--
.** harish inverse kinematics 080930 a+
        CALL cfg_r100(.tsk,"PORTANGLE"    ,vpp.portang[    .prt],vpx.portang[    .prt], def.portang[.prt],  -180, 180)
.** harish 090703 a
        CALL cfg_r100(.tsk,"PINZOFST" ,vpp.pinzofst[ .prt],vpx.pinzofst[ .prt],0                  ,-400, 400)
.END
.******************************************
.** app_config_def.as
.******************************************
.* default for port type have to be def_????
.* default for each port have to be def.????
.*
.PROGRAM cfg_def_set(.prt)
     NOEXIST_SET_R def.portarea[.prt]  = 0
     NOEXIST_SET_R def.portz[   .prt]  = -1000
     NOEXIST_SET_R def.pinyback[.prt]  = 100
.** harish inverse kinematics 080930 a+
     NOEXIST_SET_R def.portang[.prt]  = -1000
.END
.REALS
    def_port  = 4
.*                                                      /* SCRX11-006 m
    def_uport = 7

    def_slot[port_algn] = 1
    def_slot[port_cass] = 25
    def_slot[port_load] = 1
    def_slot[port_util] = 1

    def_pitch[port_algn] = 0
    def_pitch[port_cass] = 10
    def_pitch[port_load] = 0
    def_pitch[port_util] = 0

    def_llift[port_algn] = 2.3
    def_llift[port_cass] = 2.3
    def_llift[port_load] = 2.3
    def_llift[port_util] = 2.3

    def_ulift[port_algn] = 3
    def_ulift[port_cass] = 3
    def_ulift[port_load] = 3
    def_ulift[port_util] = 3

    def_gforwd[port_algn] = 3
    def_gforwd[port_cass] = 3
    def_gforwd[port_load] = 3
    def_gforwd[port_util] = 3

    def_pforwd[port_algn] = 1
    def_pforwd[port_cass] = 1
    def_pforwd[port_load] = 1
    def_pforwd[port_util] = 1

    def_backwd[port_algn] = 7
    def_backwd[port_cass] = 7
    def_backwd[port_load] = 7
    def_backwd[port_util] = 7

    def_putback[port_algn] = 0
    def_putback[port_cass] = 1.5        ;** harish 090629-3 m 0.5
    def_putback[port_load] = 0
    def_putback[port_util] = 0

    def_getretchk[port_algn] = 0
    def_getretchk[port_cass] = 0
    def_getretchk[port_load] = 0
    def_getretchk[port_util] = 0
    def.getretchk[pc3]       = 1
    def.getretchk[pc4]       = 1
    def.getretchk[201]       = 0
    def.getretchk[202]       = 0
    def.getretchk[203]       = 0
    def.getretchk[204]       = 0
.*                                                      /* SCRX11-032-1 a */
    def.getretchk[205]       = 0

    def_aftputabs[port_algn] = 0
    def_aftputabs[port_cass] = 0
    def_aftputabs[port_load] = 1
    def_aftputabs[port_util] = 0
    def.aftputabs[pc3]       = 1
    def.aftputabs[pc4]       = 1
    def.aftputabs[201]       = 0
    def.aftputabs[202]       = 0
    def.aftputabs[203]       = 0
    def.aftputabs[204]       = 0
.*                                                      /* SCRX11-032-1 a */
    def.aftputabs[205]       = 0

    def_getprebck[port_algn] = 0
    def_getprebck[port_cass] = 1
    def_getprebck[port_load] = 0
    def_getprebck[port_util] = 0
    def.getprebck[pc3]       = 0
    def.getprebck[pc4]       = 0
    def.getprebck[201]       = 0
    def.getprebck[202]       = 0
    def.getprebck[203]       = 0
    def.getprebck[204]       = 0
.*                                                      /* SCRX11-032-1 a */
    def.getprebck[205]       = 0

    def_pintchzdn[port_algn] = 5
    def_pintchzdn[port_cass] = 5
    def_pintchzdn[port_load] = 5
    def_pintchzdn[port_util] = 5

    def_pintchzup[port_algn] = 10
    def_pintchzup[port_cass] = 10
    def_pintchzup[port_load] = 10
    def_pintchzup[port_util] = 10

    def_pintchspd[port_algn] = 1
    def_pintchspd[port_cass] = 1
    def_pintchspd[port_load] = 1
    def_pintchspd[port_util] = 1

    def_zupatchrt[port_algn] = 7
    def_zupatchrt[port_cass] = 3
    def_zupatchrt[port_load] = 7
    def_zupatchrt[port_util] = 7

    def.llift[201]      = 7
.** SCRX10-027-2 m
    def.ulift[201]      = 8
    def.gforwd[201]     = 4.5
    def.wpforwdy[201]   = 4.5
    def.wpforwdz[201]   = 3.0
    def.backwd[201]     = 7
    def.putback[201]    = 0
    def.gzspd[201]      = 50
    def.pzspd[201]      = 50

.** harish 080613 m+
.** SCRX10-027-2 m
    def.wzaccess[202]   = 62.5
    def.wzslowh[202]    = 5
    def.gforwd[202]     = 4
    def.wpforwdy[202]   = 4
    def.wpforwdz[202]   = 2
    def.putback[202]    = 0
    def.wzspdag[202]    = 30
    def.wzspdap[202]    = 30
    def.wzspdbg[202]    = 30
    def.wzspdbp[202]    = 30
    def.wyspeed[202]    = 2

    def.slot[203,0]   = 1    ;** Def
    def.slot[203,1]   = 1    ;** Min
    def.slot[203,2]   = 1    ;** Max
.** harish 080724 a+
    def.pitch[203]    = 0
    def.wzaccess[203] = 408
    def.wzslowh[203]  = 5
    def.wforwdy[203]  = 15
    def.wforwdz[203]  = 3
    def.gzspd[203]    = 30
    def.pzspd[203]    = 30
    def.wyspeed[203]  = 2
.** harish 090209-2 a++
    def.gziospd[203]    = 100
    def.pziospd[203]    = 100
    def.wgriptim[203]   =   0
.** harish 090209-2 a--

.** harish 080807 a+
    def.slot[204,0]   = 8    ;** Def
    def.slot[204,1]   = 1    ;** Min
    def.slot[204,2]   = 10    ;** Max
    def.pitch[204]    = 10
.** harish 081201 a
    def.wzaccess[204] = 360
    def.wzslowh[204]  = 5
    def.wforwdz[204]  = 3
    def.wforwdy[204]  = 9   ;** 15 .** harish 090213 per MikeD m
    def.gzspd[204]    = 30
    def.pzspd[204]    = 30
    def.wyspeed[204]  = 2
.** harish 090209-2 a++
    def.gziospd[204]    = 30    ;**100 .** harish 090408-2 per MikeD m
    def.pziospd[204]    = 30    ;**100 .** harish 090408-2 per MikeD m
    def.wgriptim[204]   =  1    ;**  0 .** harish 090408-2 per MikeD m
.** harish 090209-2 a--
.*                                                      /* SCRX11-032-1 a++ */
    def.wzaccess[205]   = 0
    def.wzslowh[205]    = 0
    def.gforwd[205]     = 0
    def.pforwd[205]     = 0
    def.wpforwdy[205]   = 0
    def.wpforwdz[205]   = 0
    def.putback[205]    = 0
    def.wzspdag[205]    = 30
    def.wzspdap[205]    = 30
    def.wzspdbg[205]    = 30
    def.wzspdbp[205]    = 30
    def.wyspeed[205]    = 2
.*                                                      /* SCRX11-032-1 a-- */
    def.portarea[11]  = p_hori_cs1
    def.portarea[12]  = p_hori_cs2
    def.portarea[13]  = p_hori_cs3
    def.portarea[14]  = p_hori_cs4

.** harish 080623 a++
    def.portarea[21]  = p_hori_pta
    def.portarea[22]  = p_hori_ptb
    def.portarea[23]  = p_hori_vdb
    def.portarea[24]  = p_hori_vda
.**harish 080623 a--

    def.portarea[101]  = p_hori_pta
    def.portarea[102]  = p_hori_ptb
    def.portarea[103]  = p_hori_vdb
    def.portarea[104]  = p_hori_vda
.*                                                      /* SCRX11-032-1 a */
    def.portarea[205]  = p_down_rins
.** harish 080624 a++
.** harish 081120 m++
    def.portz[101] = 265
    def.portz[102] = 265
    def.portz[103] = 150
    def.portz[104] = 150

.*                                                  /* SCRX12-006 a
    def.snfzup = 10
.** harish 081120 m--
.** harish 080627 a++  defaults determined using taught positions at tool 801****.
..MONCMD NOEXIST_SET_R    vpp.llift[101]      = 5
..MONCMD NOEXIST_SET_R    vpp.llift[102]      = 5
..MONCMD NOEXIST_SET_R    vpp.llift[103]      = 10
..MONCMD NOEXIST_SET_R    vpp.llift[104]      = 10

..MONCMD NOEXIST_SET_R    vpp.ulift[101]      = 8
..MONCMD NOEXIST_SET_R    vpp.ulift[102]      = 8
..MONCMD NOEXIST_SET_R    vpp.ulift[103]      = 16
..MONCMD NOEXIST_SET_R    vpp.ulift[104]      = 16

.*                                                      /* SCRX12-006-3 a++
..MONCMD NOEXIST_SET_R    vpp.llift[106]      = 0
..MONCMD NOEXIST_SET_R    vpp.ulift[106]      = 0
..MONCMD NOEXIST_SET_R    vpp.pinzofst[106]   = 33.92
..MONCMD NOEXIST_SET_R    vpp.zupatchrt[106]  = -43.14
..MONCMD NOEXIST_SET_R    vpp.portz[106]      = 320
..MONCMD NOEXIST_SET_R    vpp.portang[106]    = 90

..MONCMD NOEXIST_SET_R    vpp.slot[107]      = 7
..MONCMD NOEXIST_SET_R    vpp.pitch[107]      = 12
..MONCMD NOEXIST_SET_R    vpp.llift[107]      = 3.5
..MONCMD NOEXIST_SET_R    vpp.ulift[107]      = 4.6
..MONCMD NOEXIST_SET_R    vpp.getprebck[107]      = 1
..MONCMD NOEXIST_SET_R    vpp.zupatchrt[107]  = 82
..MONCMD NOEXIST_SET_R    vpp.portz[107]      = 297
..MONCMD NOEXIST_SET_R    vpp.portang[107]    = 90

..MONCMD NOEXIST_SET_R    vpp.snfzup[103]    = 17
..MONCMD NOEXIST_SET_R    vpp.snfzup[104]    = 17
.*                                                      /* SCRX12-006-3 a--

.** harish 081120 m++
..MONCMD NOEXIST_SET_R    vpp.zupatchrt[101]  = 16
..MONCMD NOEXIST_SET_R    vpp.zupatchrt[102]  = 16
..MONCMD NOEXIST_SET_R    vpp.zupatchrt[103]  = 16
..MONCMD NOEXIST_SET_R    vpp.zupatchrt[104]  = 16
.** harish 081120 m--
.** harish 080627 a--  defaults determined using taught positions at tool 801****.
.** hairsh 080624 a--
.** harish 081007 a+
..MONCMD NOEXIST_SET_R    vpp.portarea[0]  = 0

    def.portarea[201] = p_hori_pass
    def.portarea[202] = p_down_hclu
    def.portarea[203] = p_dive_rins
    def.portarea[204] = p_dive_buff

.* harish 080613 m++
.** harish 081030 m++
    def.portz[201] = 120        ;** 78 .** harish 090506 m
    def.portz[202] = 152
    def.portz[203] = 450
    def.portz[204] = 450
.*                                                      /* SCRX11-032-1 m */
    def.portz[205] = 400
    def.portz[206] = 152
.** harish 081030 m--
.** harish 081120 m++
    def.portz[11] = 25
    def.portz[12] = 25
    def.portz[13] = 25
    def.portz[14] = 25
    def.portz[21] = 265
    def.portz[22] = 265
    def.portz[23] = 150
    def.portz[24] = 150
.** harish 081120 m--
.* harish 080613 m--

.* harish 080613 m++
    def.pinyback[23]  = 10
    def.pinyback[24]  = 10
.* harish 080613 m--

.* harish 080624 m++
    def.pinyback[103]  = 10
    def.pinyback[104]  = 10
.* harish 080624 m--

.** harish 080630 a+
    def.snfspd    = 10
.** harish inverse kinematics 080930 a++
.** harish cfgid's new from dave 081010 m++
    def.portang[11] = 180
    def.portang[12] = 180
    def.portang[13] = 180
    def.portang[14] = 180
.** SCRX10-027-2 m
    def.portang[21] = 49.4
.** harish 081023 m+
    def.portang[22] = -25
    def.portang[23] = 0
.** harish 081023 m+
    def.portang[24] = -2.5
.** SCRX10-027-2 m
    def.portang[101] = 49.4
.** harish 081023 m+
    def.portang[102] = -25
    def.portang[103] = 0
.** harish 081023 m+
    def.portang[104] = -2.5
.** harish cfgid's new from dave 081010 m--
.** harish inverse kinematics 080930 a--
.END
.******************************************
.** app_move.as
.******************************************
.*                                                      /* SCRX12-006 2 am++
.PROGRAM pmove(.rob,.hnd,.pat,.pos,.#dst,.$inf,.err)
    .err = 0
.*                                                      /* SCRX12-006 2 am--
    IF .rob <= 4 THEN
    IF .rob == 1 THEN
        CALL nt570pmove(.rob,.hnd,.pat,.pos,.#dst,.$inf)
    ELSEIF .rob == 2 THEN
.*                                                      /* SCRX12-006 2 m
        CALL nt410pmove(.rob,.hnd,.pat,.pos,.#dst,.$inf,.err)
    ELSE
        TYPE "PMOVE .rob Error:",.rob
        HALT
    END
.*                                                      /* SCRX12-006 2 a++
    IF .err THEN
        RETURN
    END
.*                                                      /* SCRX12-006 2 a--
.****** Event
    IF .pos == gpos_e_home THEN
        IF evt_pos[.rob] THEN
        CALL event_rob(.rob,$ev_arm_ent[.rob],$ev2_arm_ent)
        END
    END
    ELSE
    qtm_cnt[.rob] = qtm_cnt[.rob]+1
    END
.END
.PROGRAM nt570pmove(.rob,.hnd,.pat,.pos,.#dst,.$inf)
    ZARRAYSET accuracy[1]=999,6
    POINT .#cur = #DEST:rob_tsk[.rob]
    IF ABS(DEXT(.#dst-.#cur,7))>5 THEN
.***    現在位置と目標位置のJT7が違う場合だけ特別な動作を行う
    IF ABS(DEXT(.#dst,7)-90)<5 THEN
.*******    目標JT7が90の場合
.*******    JT7を目標位置へJT3をFLIP高さへ移動する
        POINT .#cur = #PPMIX(.#dst,.#cur,bit[7])
        POINT .#cur = #PPJSET(.#cur,3,MAXVAL(DEXT(.#cur,3),DEXT(.#dst,3),flip_ulm[.rob]))
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(1)")
    ELSEIF ABS(DEXT(.#cur,7)-90)<5 THEN
.*******    現在JT7が90の場合
.*******    JT7以外を目標へ、位置JT3はFLIP高さへ移動する
        POINT .#cur = #PPMIX(.#cur,.#dst,bit[7] BOR bit[3])
        POINT .#cur = #PPJSET(.#cur,3,MAXVAL(DEXT(.#cur,3),DEXT(.#dst,3),flip_ulm[.rob]))
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(2)")
    ELSEIF ABS(DEXT(.#dst-.#cur,6))>5 THEN
.*******    JT7が0<-->180でJT6が回転する場合
        POINT .#cur = #PPJSET(.#cur,3,MAXVAL(DEXT(.#cur,3),flip_ulm[.rob]))
        POINT .#cur = #PPJSET(.#cur,7,90)
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(31)")

        POINT .#cur = #PPJSET(.#dst,3,MAXVAL(DEXT(.#dst,3),flip_ulm[.rob]))
        POINT .#cur = #PPJSET(.#cur,7,90)
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(32)")
    ELSE
.*******    JT7が0<-->180でJT6が回転しない場合
        DECOMPOSE .d[1] = .#dst-.#cur
        POINT .#cur = .#cur + #PPOINT(.d[1]/2,.d[2]/2,.d[3]/2,.d[4]/2,.d[5]/2,.d[6]/2,.d[7]/2)
        POINT .#cur = #PPJSET(.#cur,3,MAXVAL(DEXT(.#cur,3),DEXT(.#dst,3),flip_ulm[.rob]))
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(4)")
    END
    ELSEIF ABS(DEXT(.#dst-.#cur,6))>5 THEN
.*******    JT7は移動しないがJT6が回転する場合
        POINT .#cur = #PPJSET(.#cur,3,MAXVAL(DEXT(.#cur,3),flip_ulm[.rob]))
        POINT .#cur = #PPJSET(.#cur,7,90)
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(51)")

        POINT .#cur = #PPJSET(.#dst,3,MAXVAL(DEXT(.#dst,3),flip_ulm[.rob]))
        POINT .#cur = #PPJSET(.#cur,7,90)
        ZL3ACCURACY accuracy[1]
        CALL jmove(.rob,.hnd,.pos,.#cur,.$inf+":PMOVE(52)")
    END

    IF .pat AND .pos THEN
    ZARRAYSET accuracy[1]=path_acur[.pat,.pos],6
    ZL3ACCURACY accuracy[1]
    END
    CALL jmove(.rob,.hnd,.pos,.#dst,.$inf+":PMOVE(E) LAST")
.END
.*                                                      /* SCRX12-006-4 am++
.PROGRAM nt410pmove(.rob,.hnd,.pat,.pos,.#dst,.$inf,.err)
    CALL duckhghmov(.rob,.hnd,.pat,.pos,.#dst,$inf,.duck_err)
    IF .duck_err THEN
        .err = ON
        RETURN
    END
.*                                                      /* SCRX12-006-4 am--
.** No need special motion
    IF .pat AND .pos THEN
    ZARRAYSET accuracy[1]=path_acur[.pat,.pos],6
    ZL3ACCURACY accuracy[1]
    END
    CALL jmove(.rob,.hnd,.pos,.#dst,.$inf)
.END
.*                                                      /* SCRX12-006-2&6 a++
.REALS
    duckzoutaccr = 999
    toavdxyaccr = 100
    todstxyaccr = 999
    Duck_Above = 1
    Duck_Between = 2
    Duck_Below = 4
.END

.PROGRAM duckhghmov(.rob,.hnd,.pat,.pos,.#dst,.$inf,.duck_err)
    .duck_err = 0
    .ducktop = IFELSE(vnp.duckhgh[.rob],vnp.duckhgh[.rob]+vnp.duckuofst[.rob],0)
    .duckbot = IFELSE(vnp.duckhgh[.rob],vnp.duckhgh[.rob]-vnp.ducklofst[.rob],0)
    IF (.ducktop >= DEXT(#lim_user_u[.rob],3)) AND (.duckbot <= DEXT(#lim_user_l[.rob],3)) THEN
        CALL alarm_str(.rob,.rob,0,$ae_duckrngz)
        .duck_err = ON
        RETURN
    END
    POINT .#cur = #DEST:rob_tsk[.rob]

.*  Same destination XY
    .samedstxy = DEXT(.#cur,2)==DEXT(.#dst,2)    AND DEXT(.#cur,4)==DEXT(.#dst,4) AND DEXT(.#cur,6)==DEXT(.#dst,6)
    .zzz_from = DEXT(.#cur,3)
.*  Determine Starting Zone
    IF .zzz_from>=.duckbot AND .zzz_from<=.ducktop
        .from = Duck_Between
    ELSEIF .zzz_from>.ducktop
        .from = Duck_Above
    ELSEIF .zzz_from<.duckbot
        .from = Duck_Below
    END
    
.*  Determine Ending Zone
    .zzz_dest = DEXT(.#dst,3)
    IF .zzz_dest>=.duckbot AND .zzz_dest<=.ducktop
        .dest = Duck_Between
    ELSEIF .zzz_dest>.ducktop
        .dest = Duck_Above
    ELSEIF .zzz_dest<.duckbot
        .dest = Duck_Below
    END

.*  Move out of current Zone
    CALL duckzout(.rob,.hnd,.pat,.pos,.#dst,.$inf,.samedstxy,.from,.dest,.ducktop,.duckbot)
.*  Check for obstacles if obstacles move to link over link
    CALL checkavoid(.rob,.hnd,.pat,.pos,.#dst,.$inf,.samedstxy,.from,.dest,.ducktop,.duckbot)
.*  Move to Final position
    CALL todstxy(.rob,.hnd,.pat,.pos,.#dst,.$inf,.samedstxy,.from,.dest,.ducktop,.duckbot)
.END

.PROGRAM duckzout(.rob,.hnd,.pat,.pos,.#dst,.$inf,.samedstxy,.from,.dest,.ducktop,.duckbot)
    .inupperzone = 0
    .inlowerzone = 0
    ZARRAYSET accuracy[1] = duckzoutaccr,6
    ZL3ACCURACY accuracy[1]
    POINT .#cur = #DEST:rob_tsk[.rob]
    IF (vnp.duckhgh[.rob] AND (vnp.duckuofst[.rob] OR vnp.ducklofst[.rob])) AND NOT .samedstxy THEN
        CASE .from OF
            VALUE Duck_Between:
            POINT .#srcabv = #PPJSET(.#cur,3,MAXVAL(.ducktop,DEXT(.#cur,3)))
            POINT .#srcblw = #PPJSET(.#cur,3,MINVAL(.duckbot,DEXT(.#cur,3)))
            CASE .dest
                VALUE Duck_Above:
                    CALL jmove(.rob,.hnd,gpos_nchg,.#srcabv,"InDuckHGH->DuckHGHClearAbove")
                VALUE Duck_Between:
.*                  Determine Zone
                    .inupperzone = (DEXT(.#cur,3)-(.duckbot)) > ((.ducktop) - DEXT(.#cur,3))
                    .inlowerzone = (DEXT(.#cur,3)-(.duckbot)) <=((.ducktop) - DEXT(.#cur,3))
                    IF .duckbot <= DEXT(#lim_user_l[.rob],3) THEN
                        .inupperzone = ON
                        .inlowerzone = OFF
                    ELSEIF .ducktop >= DEXT(#lim_user_u[.rob],3) THEN
                        .inlowerzone = ON
                        .inupperzone = OFF
                    END
                    IF .inlowerzone THEN
                        CALL jmove(.rob,.hnd,gpos_nchg,.#srcblw,"InDuckHGH->DuckHGHClearBelow")
                    ELSEIF .inupperzone THEN
                        CALL jmove(.rob,.hnd,gpos_nchg,.#srcabv,"InDuckHGH->DuckHGHClearAbove")
                    END
                VALUE Duck_Below:
                    CALL jmove(.rob,.hnd,gpos_nchg,.#srcblw,"InDuckHGH->DuckHGHClearBelow")
            END
        END
    END
.END

.PROGRAM checkavoid(.rob,.hnd,.pat,.pos,.#dst,.$inf,.samedstxy,.from,.dest,.ducktop,.duckbot)
    .samejt2 = DEXT(#DEST:rob_tsk[.rob],2)==DEXT(#ref_home[p_hori_ws1],2)
    .samejt4 = DEXT(#DEST:rob_tsk[.rob],4)==DEXT(#ref_home[p_hori_ws1],4)
    .samejt6 = DEXT(#DEST:rob_tsk[.rob],6)==DEXT(#ref_home[p_hori_ws1],6)
    .isin[p_hori_ws1] = .samejt2 AND .samejt4 AND .samejt6
    IF (.pat == p_hori_ws1 and (.pos == gpos_e_home OR .pos == gpos_r_home) OR .isin[p_hori_ws1]) AND NOT .samedstxy THEN
        CALL toavdxy(.rob,.hnd,.pos,.#dst,.$inf,.from, .dest,.ducktop,.duckbot)
    END
.END

.PROGRAM toavdxy(.rob,.hnd,.pos,.#dst,.$inf,.from,.dest,.ducktop,.duckbot)
    ZARRAYSET accuracy[1] = toavdxyaccr,6
    ZL3ACCURACY accuracy[1]
    POINT .#cur = #DEST:rob_tsk[.rob]
    POINT .#avd = #PPOINT(0,0,0,0,0,0)
    POINT .#avdabv = #PPJSET(.#avd,3,MAXVAL(.ducktop,DEXT(.#dst,3)))
    POINT .#avdblw = #PPJSET(.#avd,3,MINVAL(.duckbot,DEXT(.#dst,3)))
    CASE .from OF
        VALUE Duck_ABOVE:
            CALL jmove(.rob,.hnd,.pos,.#avdabv,"avoid exclusion")
        VALUE Duck_BETWEEN:
            CASE .dest OF
                VALUE Duck_Above:
                    CALL jmove(.rob,.hnd,.pos,.#avdabv,"avoid exclusion")

                VALUE Duck_Between:
                    .inupperzone = (DEXT(.#cur,3)-(.duckbot)) > ((.ducktop) - DEXT(.#cur,3))
                    .inlowerzone = (DEXT(.#cur,3)-(.duckbot)) <=((.ducktop) - DEXT(.#cur,3))
                    IF .duckbot <= DEXT(#lim_user_l[.rob],3) THEN
                        .inupperzone = ON
                        .inlowerzone = OFF
                    ELSEIF .ducktop >= DEXT(#lim_user_u[.rob],3) THEN
                        .inlowerzone = ON
                        .inupperzone = OFF
                    END
                    IF .inupperzone THEN
                        CALL jmove(.rob,.hnd,.pos,.#avdabv,"avoid exclusion")
                    ELSEIF .inlowerzone THEN
                        CALL jmove(.rob,.hnd,.pos,.#avdblw,"avoid exclusion")
                    END

                VALUE Duck_Below:
                    CALL jmove(.rob,.hnd,.pos,.#avdblw,"avoid exclusion")
            END
        VALUE Duck_Below:
            CALL jmove(.rob,.hnd,.pos,.#avdblw,"avoid exclusion")
    END
.END


.PROGRAM todstxy(.rob,.hnd,.pat,.pos,.#dst,.$inf,.samedstxy,.from,.dest,.ducktop,.duckbot)
    ZARRAYSET accuracy[1] = todstxyaccr,6
    ZL3ACCURACY accuracy[1]
    POINT .#cur = #DEST:rob_tsk[.rob]
    POINT .#dstabv = #PPJSET(.#dst,3,MAXVAL(.ducktop,DEXT(.#dst,3)))
    POINT .#dstblw = #PPJSET(.#dst,3,MINVAL(.duckbot,DEXT(.#dst,3)))
    IF (vnp.duckhgh[.rob] AND (vnp.duckuofst[.rob] OR vnp.ducklofst[.rob])) AND NOT .samedstxy THEN
        CASE .from
            VALUE Duck_Above:
                IF .dest <> Duck_Above THEN
                    call jmove(.rob,.hnd,gpos_nchg,.#dstabv,"AboveDuckHGH->DuckHGHClearAbove")
                END
            VALUE Duck_Between:
                IF .dest == Duck_Between THEN
                    .inupperzone = (DEXT(.#cur,3)-(.duckbot)) > ((.ducktop) - DEXT(.#cur,3))
                    .inlowerzone = (DEXT(.#cur,3)-(.duckbot)) <=((.ducktop) - DEXT(.#cur,3))
                    IF .duckbot <= DEXT(#lim_user_l[.rob],3) THEN
                        .inupperzone = ON
                        .inlowerzone = OFF
                    ELSEIF .ducktop >= DEXT(#lim_user_u[.rob],3) THEN
                        .inlowerzone = ON
                        .inupperzone = OFF
                    END
                    IF .inupperzone THEN
                        CALL jmove(.rob,.hnd,gpos_nchg,.#dstabv,"InDuckHGH->DuckHGHClearanceAbove")
                    ELSEIF .inlowerzone THEN
                        CALL jmove(.rob,.hnd,gpos_nchg,.#dstblw,"InDuckHGH->DuckHGHClearBelow")
                    END
                END
            VALUE Duck_Below:
                IF .dest <> Duck_Below THEN
                    CALL jmove(.rob,.hnd,gpos_nchg,.#dstblw,"BelowDuckHGH->DuckHGHClearBelow")
                END
        END
    END
.END
.*                                                      /* SCRX12-006-2&6 a--
.******************************************
.** app_teach.as
.******************************************
.PROGRAM t_hori_app(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    CASE  (port_type[.rob,.prt]) OF
      VALUE  p_hori_pass :
        CALL t_hori_pass(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE  p_hori_non :
;
        CALL t_hori_cs1(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_cs2(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_cs3(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_cs4(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_pta(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_ptb(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_vda(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
;
        CALL t_hori_vdb(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set

.** harish 090515-1 a++
        CALL t_hori_imp(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
.** harish 090515-1 a--
.*                                                      /* SCRX12-006-2 a2
        CALL t_hori_ws1(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
        IF tchpos[.tsk]    GOTO set
      ANY :
        TYPE $RPGNAME()," Parameter Error",(port_type[.rob,.prt])
        HALT
    END
;
    IF NOT tchpos[.tsk] THEN
        $alm_put[.tsk] = $ae_outof_area
    END
    RETURN
set:
    RETURN
.END
.PROGRAM t_down_app(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    CASE (port_type[.rob,.prt]) OF
      VALUE  p_down_hclu :
        CALL t_down_hclu(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
.*                                                      /* SCRX11-032-1 a++ */
      VALUE  p_down_rins :
        CALL t_down_rins(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
.*                                                      /* SCRX11-032-1 a-- */
      ANY :
        TYPE $RPGNAME()," Parameter Error",(port_type[.rob,.prt])
        HALT
    END
;
    IF NOT tchpos[.tsk] THEN
        $alm_put[.tsk] = $ae_outof_area
    END
.END
.PROGRAM t_dive_app(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    CASE (port_type[.rob,.prt]) OF
      VALUE  p_dive_rins :
        CALL t_dive_rins(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE  p_dive_buff :
        CALL t_dive_buff(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      ANY :
    TYPE $RPGNAME()," Parameter Error",(port_type[.rob,.prt])
    HALT
    END
;
    IF NOT tchpos[.tsk] THEN
    $alm_put[.tsk] = $ae_outof_area
    END
.END
.******************************************
.** sycx_interpolation.as
.******************************************
.** harish 080701 a++ *** INTERPOLATION TEACHING ***
.*
.* Cfg,TrainingSlots:PortID
.*
.PROGRAM cfg_tslots(.tsk,.prt)
    .$key = "TRAININGSLOTS"
    IF ($cfg.key[.tsk]<>.$key)AND($cfg.key[.tsk]<>"ALL")	RETURN
    IF ($cfg.old[.tsk]==.$key)					RETURN
.***L6607-1 a
    cfg.err[.tsk] = OFF
    CASE cfg.mod[.tsk] OF
      VALUE cfgset_nex
	CALL cfg_key_table(.tsk,.$key)
	NOEXIST_SET_R vpx.tslots[.prt] = 1,,cfg.mod[.tsk]
	NOEXIST_SET_S $vpp.tslots[.prt] = "1;8"
      VALUE cfgset_def:
	NOEXIST_SET_R vpx.tslots[.prt] = 1,,cfg.mod[.tsk]
	$vpp.tslots[.prt] = "1;8"
      VALUE cfgset_chk,cfgset_exe :
	IF (vpx.tslots[.prt]) AND (lockcfg[.tsk]) AND (tsk_hst[.tsk]<>host_dum1) THEN
	    cfg.lck[.tsk] = ON
	END
	IF cfg.mod[.tsk] == cfgset_exe THEN
	    $vpp.tslots[.prt] = $cfg.dat[.tsk]
	END
.******L6607-1 d
      VALUE cfgqry_chk,cfgqry_exe :
	$str[.tsk] = .$key + ":" + $cfg.prt[.tsk] + ":" + $vpp.tslots[.prt]
	CALL qrycfg.entry(.tsk,$str[.tsk])
.******L6607-1 d
      VALUE cfglck_chk :
;****** Do Nothing **
      VALUE cfglck_exe :
	IF $cfg.dat[.tsk] == "0" THEN
	    vpx.tslots[.prt] = 0
	ELSE
	    vpx.tslots[.prt] = 1
	END
    END
    $cfg.old[.tsk]=.$key
    RETURN
err:
    cfg.err[.tsk] = ON
.END

.*
.* TrainingSlots機能
.*
.* 補間エリアを探す
.PROGRAM tslots_area(.tsk,.rob,.prt,.ps0)
;    .tsk = tsk_tsk[.rob]
;    .tsk = .rob
    tslots.ps[.tsk,0] = .ps0
    tslots.ps[.tsk,1] = 0
    tslots.ps[.tsk,2] = 0
    tslots.mn[.tsk,1] = 0
    tslots.mn[.tsk,2] = 0
    tslots.mx[.tsk,1] = 0
    tslots.mx[.tsk,2] = 0
    IF GETSTRBLK($vpp.tslots[.prt],";",$TRIM_B($ENCODE(tslots.ps[.tsk,0])))==0	GOTO ret
    FOR .slt = 1 TO vpp.slot[.prt] STEP 1
	IF GETSTRBLK($vpp.tslots[.prt],";",$TRIM_B($ENCODE(.slt))) THEN
	    IF .slt < tslots.ps[.tsk,0] THEN
		tslots.mn[.tsk,1] = IFELSE(tslots.ps[.tsk,1]==0,1,.slt)
		tslots.ps[.tsk,1] = .slt
	    ELSEIF .slt == tslots.ps[.tsk,0] THEN
		tslots.mx[.tsk,1] = IFELSE(tslots.ps[.tsk,1],vpp.slot[.prt],tslots.mx[.tsk,1])
		tslots.mn[.tsk,2] = IFELSE(tslots.ps[.tsk,1],.slt,1)
	    ELSEIF tslots.ps[.tsk,0] < .slt THEN
		tslots.mx[.tsk,1] = IFELSE(tslots.mx[.tsk,1]==vpp.slot[.prt],tslots.ps[.tsk,0],tslots.mx[.tsk,1])
		tslots.mx[.tsk,2] = IFELSE(tslots.ps[.tsk,2]==0,vpp.slot[.prt],tslots.ps[.tsk,2])
		tslots.ps[.tsk,2] = IFELSE(tslots.ps[.tsk,2]==0,.slt,tslots.ps[.tsk,2])
	    END
	END
    END
ret:
    IF pz.tslots THEN
	TYPE /X4,$vpp.tslots[.prt]
	TYPE /X4," MN1 PS1 PS0 MX1"
	TYPE /X4,/I4,tslots.mn[.tsk,1],tslots.ps[.tsk,1],tslots.ps[.tsk,0],tslots.mx[.tsk,1]
	TYPE /X4," MN2 PS0 PS2 MX2"
	TYPE /X4,/I4,tslots.mn[.tsk,2],tslots.ps[.tsk,0],tslots.ps[.tsk,2],tslots.mx[.tsk,2]
    END
.END
.PROGRAM tslots_chk(.tsk,.rob,.prt,.zzz,.pos,.#loc)
;    .tsk = tsk_tsk[.rob]
;    .tsk = .rob
    .slt = tslots.ps[.tsk,0]-tslots.ps[.tsk,.pos]
    POINT .#ref = #vc.tchpos[.rob,.prt,tslots.ps[.tsk,.pos]]
    IF (tsk_arm[.rob] BAND arm_nt570) THEN
	ZL3TRN .loc[1] = .#loc,inv_hand
	ZL3TRN .ref[1] = .#ref,inv_hand
.** harish 080922 training slots m+
	tslots.xx[.tsk,.pos] = (.loc[1]-.ref[1])/.slt  
	tslots.yy[.tsk,.pos] = (.loc[2]-.ref[2])/.slt
	tslots.zz[.tsk,.pos] = (.loc[3]-.ref[3])/.slt
        tslots.r1[.tsk,.pos] = (.loc[4]-.ref[4])/.slt
	tslots.r2[.tsk,.pos] = (.loc[5]-.ref[5])/.slt
	.xxx = tslots.xx[.tsk,.pos] + vpp.pitch[.prt] 
.** harish 080922 error m++
	IF vpp.tsrcalerr[.prt]<ABS(.xxx) THEN
	    CALL alarm_str(.tsk,.rob,0,$AE_TSLOTS_RERR)
	    GOTO exit
	END
	IF vpp.tsrcalerr[.prt]<ABS(tslots.yy[.tsk,.pos]) THEN
	    CALL alarm_str(.tsk,.rob,0,$AE_TSLOTS_RERR)
	    GOTO exit
	END
	IF vpp.tszcalerr[.prt]<ABS(tslots.zz[.tsk,.pos]) THEN
	    CALL alarm_str(.tsk,.rob,0,$AE_TSLOTS_ZERR)
	    GOTO exit
	END
	IF vpp.tsrcalerr[.prt]<ABS(tslots.r1[.tsk,.pos]) THEN
	    CALL alarm_str(.tsk,.rob,0,$AE_TSLOTS_RERR)
	    GOTO exit
	END
	IF vpp.tsrcalerr[.prt]<ABS(tslots.r2[.tsk,.pos]) THEN
	    CALL alarm_str(.tsk,.rob,0,$AE_TSLOTS_RERR)
	    GOTO exit
	END
exit:
.** harish 080922 error m--
	IF pz.tslots THEN
	    TYPE "Area:",.pos,/X1,"XXX:",tslots.xx[.tsk,.pos]
	    TYPE "Area:",.pos,/X1,"YYY:",tslots.yy[.tsk,.pos]
	    TYPE "Area:",.pos,/X1,"ZZZ:",tslots.zz[.tsk,.pos]
	    TYPE "Area:",.pos,/X1,"RR1:",tslots.r1[.tsk,.pos]
	    TYPE "Area:",.pos,/X1,"RR2:",tslots.r2[.tsk,.pos]
	END
     END
.END
.PROGRAM tslots_do(.tsk,.rob,.prt,.zzz,.pos,.#loc)
;    .tsk = tsk_tsk[.rob]
;    .tsk = .rob
    FOR .slt = tslots.mn[.tsk,.pos] TO tslots.mx[.tsk,.pos] STEP 1
        IF (tsk_arm[.rob] BAND arm_nt570)    THEN
	    IF (.slt==tslots.mn[.tsk,.pos] ) THEN
		ZL3TRN .loc[1] = .#loc,inv_hand
		ZL3TRN .add[1] = .#loc,inv_hand
	    END
	    .add[1] = .loc[1]+(.slt-tslots.ps[.tsk,0])*(tslots.xx[.tsk,.pos])
	    .add[2] = .loc[2]+(.slt-tslots.ps[.tsk,0])*(tslots.yy[.tsk,.pos])
	    .add[3] = .loc[3]+(.slt-tslots.ps[.tsk,0])*(tslots.zz[.tsk,.pos])
            .add[4] = .loc[4]+(.slt-tslots.ps[.tsk,0])*(tslots.r1[.tsk,.pos])
	    .add[5] = .loc[5]+(.slt-tslots.ps[.tsk,0])*(tslots.r2[.tsk,.pos])
	    ZL3JNT #vc.tchpos[.rob,.prt,.slt] = .add[1],.#loc,inv_normal,inv_hand
	    IF pz.tslots THEN
		TYPE "XYZR1R2:",.slt,.add[1],.add[2],.add[3],.add[4],.add[5]
	    END
	END
    END
.END
.PROGRAM tslots(.tsk,.rob,.hnd,.prt,.ps0,.#loc,.zzz,.don)
;    .tsk = tsk_tsk[.rob]
;    .tsk = .rob
    IF pz.tslots THEN
	TYPE "TrainingSlots"
    END
    .don = OFF
.***未教示時は補間なし
    IF (DEXT(#vc.tchpos[.rob,.prt,0],IFELSE(.zzz,3,2)) < -999)	GOTO don
    CALL tslots_area(.tsk,.rob,.prt,.ps0)
.***補間量をチェックする
    FOR .pos = 1 TO 2 STEP 1
	IF (tslots.ps[.tsk,.pos]) THEN
	    CALL tslots_chk(.tsk,.rob,.prt,.zzz,.pos,.#loc)
	END
    END
;*    IF $t.alm[.tsk]<>""		GOTO don
    IF tsk_err[.rob]		GOTO don
.***補間を実施する
    FOR .pos = 1 TO 2 STEP 1
	IF (tslots.ps[.tsk,.pos]) THEN
	    CALL tslots_do(.tsk,.rob,.prt,.zzz,.pos,.#loc)
	    .don = ON
	END
    END
    
don:
    IF pz.tslots THEN
	TYPE /X4,$IFELSE(.don,"++DONE++",$IFELSE(tsk_err[.rob],"ERROR","--NONE--"))
    END
.END
.** harish 080701 a--
.******************************************
.** app_hori_pass.as
.**   SycamoreX NT570 PASS THRU
.**   20080421
.**     300mmは直線動作するように変更
.******************************************
.REALS
    mapp_path[p_hori_pass] = 0
    mapp_move[p_hori_pass] = mov_non

    path_move[p_hori_pass,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_pass,gpos_e_prep] = mov_non
    path_move[p_hori_pass,gpos_e_half] = mov_jmv
    path_move[p_hori_pass,gpos_e_near] = mov_non
    path_move[p_hori_pass,gpos_e_extd] = mov_lmv
    path_move[p_hori_pass,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_pass,gpos_e_tech] = mov_jmv
    path_move[p_hori_pass,gpos_r_tech] = mov_lmv
    path_move[p_hori_pass,gpos_r_slow] = mov_lmv
    path_move[p_hori_pass,gpos_r_extd] = mov_lmv
    path_move[p_hori_pass,gpos_r_back] = mov_lmv
    path_move[p_hori_pass,gpos_r_near] = mov_non
    path_move[p_hori_pass,gpos_r_half] = mov_lmv
    path_move[p_hori_pass,gpos_r_prep] = mov_non
    path_move[p_hori_pass,gpos_r_home] = mov_jmv

    path_sped[p_hori_pass,gpos_e_home] = 100
    path_sped[p_hori_pass,gpos_e_prep] = 100
    path_sped[p_hori_pass,gpos_e_half] = 100
    path_sped[p_hori_pass,gpos_e_near] = 100
    path_sped[p_hori_pass,gpos_e_extd] = 100
    path_sped[p_hori_pass,gpos_e_slow] = 100
    path_sped[p_hori_pass,gpos_e_tech] =  -1
    path_sped[p_hori_pass,gpos_r_tech] =  -1
    path_sped[p_hori_pass,gpos_r_slow] =  -1
    path_sped[p_hori_pass,gpos_r_extd] =  -1
    path_sped[p_hori_pass,gpos_r_back] = 100
    path_sped[p_hori_pass,gpos_r_near] = 100
    path_sped[p_hori_pass,gpos_r_half] = 100
    path_sped[p_hori_pass,gpos_r_prep] = 100
    path_sped[p_hori_pass,gpos_r_home] = 100

    path_acce[p_hori_pass,gpos_e_home] = 100
    path_acce[p_hori_pass,gpos_e_prep] = 100
    path_acce[p_hori_pass,gpos_e_half] = 100
    path_acce[p_hori_pass,gpos_e_near] = 100
    path_acce[p_hori_pass,gpos_e_extd] = 200
    path_acce[p_hori_pass,gpos_e_slow] = 100
    path_acce[p_hori_pass,gpos_e_tech] = 100
    path_acce[p_hori_pass,gpos_r_tech] = 100
    path_acce[p_hori_pass,gpos_r_slow] = 100
    path_acce[p_hori_pass,gpos_r_extd] = 100
    path_acce[p_hori_pass,gpos_r_back] = 100
    path_acce[p_hori_pass,gpos_r_near] = 100
    path_acce[p_hori_pass,gpos_r_half] = 100
    path_acce[p_hori_pass,gpos_r_prep] = 100
    path_acce[p_hori_pass,gpos_r_home] = 200

    path_dece[p_hori_pass,gpos_e_home] = 100
    path_dece[p_hori_pass,gpos_e_prep] = 100
    path_dece[p_hori_pass,gpos_e_half] = 200
    path_dece[p_hori_pass,gpos_e_near] = 100
    path_dece[p_hori_pass,gpos_e_extd] = 100
    path_dece[p_hori_pass,gpos_e_slow] = 100
    path_dece[p_hori_pass,gpos_e_tech] = 100
    path_dece[p_hori_pass,gpos_r_tech] = 100
    path_dece[p_hori_pass,gpos_r_slow] = 100
    path_dece[p_hori_pass,gpos_r_extd] = 100
    path_dece[p_hori_pass,gpos_r_back] = 100
    path_dece[p_hori_pass,gpos_r_near] = 100
    path_dece[p_hori_pass,gpos_r_half] = 200
    path_dece[p_hori_pass,gpos_r_prep] = 100
    path_dece[p_hori_pass,gpos_r_home] = 100

    path_acur[p_hori_pass,gpos_e_home] =  99
    path_acur[p_hori_pass,gpos_e_prep] = 999
    path_acur[p_hori_pass,gpos_e_half] = 999
    path_acur[p_hori_pass,gpos_e_near] = 999
    path_acur[p_hori_pass,gpos_e_extd] =   3
    path_acur[p_hori_pass,gpos_e_slow] =   1
    path_acur[p_hori_pass,gpos_e_tech] =   1
    path_acur[p_hori_pass,gpos_r_tech] =   1
    path_acur[p_hori_pass,gpos_r_slow] =   1
    path_acur[p_hori_pass,gpos_r_extd] =   3
    path_acur[p_hori_pass,gpos_r_back] =   1
    path_acur[p_hori_pass,gpos_r_near] = 999
    path_acur[p_hori_pass,gpos_r_half] = 999
    path_acur[p_hori_pass,gpos_r_prep] = 999
    path_acur[p_hori_pass,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_pass]   0   73.44   95.8  -139.95   0    -23.49   180
    #ref_home[p_hori_pass]   0   13.5    95.8   -40.3     0   -63.2    180
.** harish 090506 m++
.*  #ref_mpr1[p_hori_pass]   0   42.276  95.8   -73.684   0   -58.592  180
.*  #ref_mpr2[p_hori_pass]   0   42.276  95.8   -73.684   0   -58.592  180
.*  #ref_mpr3[p_hori_pass]   0   42.276  95.8   -73.684   0   -58.592  180
    #ref_mpr1[p_hori_pass]   0   39.13   95.8   -66.33    0   -62.80   180
    #ref_mpr2[p_hori_pass]   0   39.13   95.8   -66.33    0   -62.80   180
    #ref_mpr3[p_hori_pass]   0   39.13   95.8   -66.33    0   -62.80   180
.** harish 090506 m--
.END
.PROGRAM p_hori_pass(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.wpforwdy[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk] + #PPOINT(0,0,  -vpp.wpforwdz[ .prt])
;** #gp_near
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,300
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] SUB add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_hori_pass],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END
..MONCMD    area[p_hori_pass,1,1] = -1150-100	;** X_MIN
..MONCMD    area[p_hori_pass,1,2] = -1150	;** X_BAS
..MONCMD    area[p_hori_pass,1,3] = -1150+100	;** X_MAX

..MONCMD    area[p_hori_pass,2,1] = -48-100	;** Y_MIN
..MONCMD    area[p_hori_pass,2,2] = -48		;** Y_BAS
..MONCMD    area[p_hori_pass,2,3] = -48+100	;** Y_MAX

..MONCMD    area[p_hori_pass,3,1] = 75-100	;** Z_MIN
..MONCMD    area[p_hori_pass,3,2] = 75		;** Z_BAS
..MONCMD    area[p_hori_pass,3,3] = 75+100	;** Z_MAX

..MONCMD    area[p_hori_pass,4,1] =  90-30	;** R1_MIN
..MONCMD    area[p_hori_pass,4,2] =  90		;** R1_BAS
..MONCMD    area[p_hori_pass,4,3] =  90+30	;** R1_MAX

..MONCMD    area[p_hori_pass,5,1] = 180-10	;** R2_MIN
..MONCMD    area[p_hori_pass,5,2] = 180		;** R2_BAS
..MONCMD    area[p_hori_pass,5,3] = 180+10	;** R2_MAX
.*********************************
.** Teach Position Pass Through **
.*********************************
.PROGRAM t_hori_pass(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_pass
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_down_hclu.as
.**   Face Down for SycamoreX:NT570
.**     HCLU
.******************************************
.REALS
    mapp_path[p_down_hclu] = 0
    mapp_move[p_down_hclu] = mov_non

    path_move[p_down_hclu,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_down_hclu,gpos_e_prep] = mov_non
    path_move[p_down_hclu,gpos_e_half] = mov_jmv
    path_move[p_down_hclu,gpos_e_near] = mov_non
    path_move[p_down_hclu,gpos_e_extd] = mov_jmv
    path_move[p_down_hclu,gpos_e_slow] = mov_jmv
    path_move[p_down_hclu,gpos_e_tech] = mov_jmv
    path_move[p_down_hclu,gpos_r_tech] = mov_jmv
    path_move[p_down_hclu,gpos_r_slow] = mov_jmv
    path_move[p_down_hclu,gpos_r_extd] = mov_jmv
    path_move[p_down_hclu,gpos_r_back] = mov_jmv	;**N/A
    path_move[p_down_hclu,gpos_r_near] = mov_non
    path_move[p_down_hclu,gpos_r_half] = mov_jmv
    path_move[p_down_hclu,gpos_r_prep] = mov_non
    path_move[p_down_hclu,gpos_r_home] = mov_jmv

    path_sped[p_down_hclu,gpos_e_home] = 100
    path_sped[p_down_hclu,gpos_e_prep] = 100
    path_sped[p_down_hclu,gpos_e_half] = 100
    path_sped[p_down_hclu,gpos_e_near] = 100
    path_sped[p_down_hclu,gpos_e_extd] = 100
    path_sped[p_down_hclu,gpos_e_slow] = 100
    path_sped[p_down_hclu,gpos_e_tech] =  -1
    path_sped[p_down_hclu,gpos_r_tech] =  -1
    path_sped[p_down_hclu,gpos_r_slow] =  -1
    path_sped[p_down_hclu,gpos_r_extd] = 100
    path_sped[p_down_hclu,gpos_r_back] = 100
    path_sped[p_down_hclu,gpos_r_near] = 100
    path_sped[p_down_hclu,gpos_r_half] = 100
    path_sped[p_down_hclu,gpos_r_prep] = 100
    path_sped[p_down_hclu,gpos_r_home] = 100

    path_acce[p_down_hclu,gpos_e_home] = 100
    path_acce[p_down_hclu,gpos_e_prep] = 100
.*                                                      /* SCRX11-032-1 m */
    path_acce[p_down_hclu,gpos_e_half] =  75    ;100	;; 70
    path_acce[p_down_hclu,gpos_e_near] = 100
    path_acce[p_down_hclu,gpos_e_extd] =  90	;; 70
    path_acce[p_down_hclu,gpos_e_slow] = 100
    path_acce[p_down_hclu,gpos_e_tech] = 100
    path_acce[p_down_hclu,gpos_r_tech] = 100
    path_acce[p_down_hclu,gpos_r_slow] = 100
    path_acce[p_down_hclu,gpos_r_extd] = 100
    path_acce[p_down_hclu,gpos_r_back] = 100
    path_acce[p_down_hclu,gpos_r_near] = 100
.*                                                      /* SCRX11-032-1 m */
    path_acce[p_down_hclu,gpos_r_half] =  75    ;90	;; 70
    path_acce[p_down_hclu,gpos_r_prep] = 100
.*                                                      /* SCRX11-032-1 m */
    path_acce[p_down_hclu,gpos_r_home] =  75    ;100	;; 70

    path_dece[p_down_hclu,gpos_e_home] = 100
    path_dece[p_down_hclu,gpos_e_prep] = 100
.*                                                      /* SCRX11-032-1 m */
    path_dece[p_down_hclu,gpos_e_half] =  75    ;100	;; 70
    path_dece[p_down_hclu,gpos_e_near] = 100
    path_dece[p_down_hclu,gpos_e_extd] =  90	;; 70
    path_dece[p_down_hclu,gpos_e_slow] = 100
    path_dece[p_down_hclu,gpos_e_tech] = 100
    path_dece[p_down_hclu,gpos_r_tech] = 100
    path_dece[p_down_hclu,gpos_r_slow] = 100
    path_dece[p_down_hclu,gpos_r_extd] = 100
    path_dece[p_down_hclu,gpos_r_back] = 100
    path_dece[p_down_hclu,gpos_r_near] = 100
.*                                                      /* SCRX11-032-1 m */
    path_dece[p_down_hclu,gpos_r_half] =  75    ;90	;; 70
    path_dece[p_down_hclu,gpos_r_prep] = 100
.*                                                      /* SCRX11-032-1 m */
    path_dece[p_down_hclu,gpos_r_home] =  75    ;100	;; 70

    path_acur[p_down_hclu,gpos_e_home] =  10
    path_acur[p_down_hclu,gpos_e_prep] = 999
    path_acur[p_down_hclu,gpos_e_half] = 999
    path_acur[p_down_hclu,gpos_e_near] = 999
    path_acur[p_down_hclu,gpos_e_extd] =  10
    path_acur[p_down_hclu,gpos_e_slow] = 999
    path_acur[p_down_hclu,gpos_e_tech] =   1
    path_acur[p_down_hclu,gpos_r_tech] =   1
    path_acur[p_down_hclu,gpos_r_slow] = 999
    path_acur[p_down_hclu,gpos_r_extd] =  10
    path_acur[p_down_hclu,gpos_r_back] = 999
    path_acur[p_down_hclu,gpos_r_near] = 999
    path_acur[p_down_hclu,gpos_r_half] = 999
    path_acur[p_down_hclu,gpos_r_prep] = 999
    path_acur[p_down_hclu,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_down_hclu]   0  -55.43   80.06   130.45   0   -212.02    0
    #ref_home[p_down_hclu]   0   13.5     0      -40.3    0    -63.2     0
    #ref_half[p_down_hclu]   0    0       0        0      0   -125       0
.** harish 081030 m++
    #ref_mpr1[p_down_hclu]   0    0       0        0      0   -135       0
    #ref_mpr2[p_down_hclu]   0    0       0        0      0   -135       0
.** harish new prepos from Dave 081010 a++
    #ref_mpr3[p_down_hclu]   0    0       0        0      0   -135       0
.** harish 081030 m--
.END
.PROGRAM p_down_hclu(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back N/A
    POINT #gp_back[.tsk] = #gp_goff[.tsk]
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.wpforwdy[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk] + #PPOINT(0,0,  vpp.wpforwdz[ .prt])
;** #gp_near
    POINT #gp_near[.tsk] = #PPJSET(#ref_half[p_down_hclu],3,trn[.tsk,3])
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_down_hclu],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END


..MONCMD    area[p_down_hclu,1,1] = 1045-100	;** X_MIN
..MONCMD    area[p_down_hclu,1,2] = 1045	;** X_BAS
..MONCMD    area[p_down_hclu,1,3] = 1045+100	;** X_MAX

..MONCMD    area[p_down_hclu,2,1] = -138-100	;** Y_MIN
..MONCMD    area[p_down_hclu,2,2] = -138	;** Y_BAS
..MONCMD    area[p_down_hclu,2,3] = -138+100	;** Y_MAX

..MONCMD    area[p_down_hclu,3,1] =  102-100	;** Z_MIN
..MONCMD    area[p_down_hclu,3,2] =  102	;** Z_BAS
..MONCMD    area[p_down_hclu,3,3] =  102+100	;** Z_MAX

..MONCMD    area[p_down_hclu,4,1] = -137-30	;** R1_MIN
..MONCMD    area[p_down_hclu,4,2] = -137	;** R1_BAS
..MONCMD    area[p_down_hclu,4,3] = -137+30	;** R1_MAX

..MONCMD    area[p_down_hclu,5,1] =    0-10	;** R2_MIN
..MONCMD    area[p_down_hclu,5,2] =    0	;** R2_BAS
..MONCMD    area[p_down_hclu,5,3] =    0+10	;** R2_MAX
.*********************************
.** Teach Position HCLU         **
.*********************************
.PROGRAM t_down_hclu(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_down_hclu
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4],trn[.tsk,4]-360)
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_down_rins.as
.**   Face Down for SycamoreX:NT570
.**     BL-RINSE
.******************************************
.REALS
    mapp_path[p_down_rins] = 0
    mapp_move[p_down_rins] = mov_non

    path_move[p_down_rins,gpos_e_home] = mov_jmv        ;**N/A
    path_move[p_down_rins,gpos_e_prep] = mov_non
    path_move[p_down_rins,gpos_e_half] = mov_jmv
    path_move[p_down_rins,gpos_e_near] = mov_non
    path_move[p_down_rins,gpos_e_extd] = mov_jmv
    path_move[p_down_rins,gpos_e_slow] = mov_jmv
    path_move[p_down_rins,gpos_e_tech] = mov_jmv
    path_move[p_down_rins,gpos_r_tech] = mov_jmv
    path_move[p_down_rins,gpos_r_slow] = mov_jmv
    path_move[p_down_rins,gpos_r_extd] = mov_jmv
    path_move[p_down_rins,gpos_r_back] = mov_jmv        ;**N/A
    path_move[p_down_rins,gpos_r_near] = mov_non
    path_move[p_down_rins,gpos_r_half] = mov_jmv
    path_move[p_down_rins,gpos_r_prep] = mov_non
    path_move[p_down_rins,gpos_r_home] = mov_jmv

    path_sped[p_down_rins,gpos_e_home] = 100
    path_sped[p_down_rins,gpos_e_prep] = 100
    path_sped[p_down_rins,gpos_e_half] = 100
    path_sped[p_down_rins,gpos_e_near] = 100
    path_sped[p_down_rins,gpos_e_extd] = 100
    path_sped[p_down_rins,gpos_e_slow] = 100
    path_sped[p_down_rins,gpos_e_tech] =  -1
    path_sped[p_down_rins,gpos_r_tech] =  -1
    path_sped[p_down_rins,gpos_r_slow] =  -1
    path_sped[p_down_rins,gpos_r_extd] = 100
    path_sped[p_down_rins,gpos_r_back] = 100
    path_sped[p_down_rins,gpos_r_near] = 100
    path_sped[p_down_rins,gpos_r_half] = 100
    path_sped[p_down_rins,gpos_r_prep] = 100
    path_sped[p_down_rins,gpos_r_home] = 100

    path_acce[p_down_rins,gpos_e_home] = 100
    path_acce[p_down_rins,gpos_e_prep] = 100
    path_acce[p_down_rins,gpos_e_half] = 100        ;; 70
    path_acce[p_down_rins,gpos_e_near] = 100
    path_acce[p_down_rins,gpos_e_extd] =  90        ;; 70
    path_acce[p_down_rins,gpos_e_slow] = 100
    path_acce[p_down_rins,gpos_e_tech] = 100
    path_acce[p_down_rins,gpos_r_tech] = 100
    path_acce[p_down_rins,gpos_r_slow] = 100
    path_acce[p_down_rins,gpos_r_extd] = 100
    path_acce[p_down_rins,gpos_r_back] = 100
    path_acce[p_down_rins,gpos_r_near] = 100
    path_acce[p_down_rins,gpos_r_half] =  90        ;; 70
    path_acce[p_down_rins,gpos_r_prep] = 100
    path_acce[p_down_rins,gpos_r_home] = 100        ;; 70

    path_dece[p_down_rins,gpos_e_home] = 100
    path_dece[p_down_rins,gpos_e_prep] = 100
    path_dece[p_down_rins,gpos_e_half] = 100        ;; 70
    path_dece[p_down_rins,gpos_e_near] = 100
    path_dece[p_down_rins,gpos_e_extd] =  90        ;; 70
    path_dece[p_down_rins,gpos_e_slow] = 100
    path_dece[p_down_rins,gpos_e_tech] = 100
    path_dece[p_down_rins,gpos_r_tech] = 100
    path_dece[p_down_rins,gpos_r_slow] = 100
    path_dece[p_down_rins,gpos_r_extd] = 100
    path_dece[p_down_rins,gpos_r_back] = 100
    path_dece[p_down_rins,gpos_r_near] = 100
    path_dece[p_down_rins,gpos_r_half] =  90        ;; 70
    path_dece[p_down_rins,gpos_r_prep] = 100
    path_dece[p_down_rins,gpos_r_home] = 100        ;; 70

    path_acur[p_down_rins,gpos_e_home] =  10
    path_acur[p_down_rins,gpos_e_prep] = 999
    path_acur[p_down_rins,gpos_e_half] = 999
    path_acur[p_down_rins,gpos_e_near] = 999
    path_acur[p_down_rins,gpos_e_extd] =  10
    path_acur[p_down_rins,gpos_e_slow] = 999
    path_acur[p_down_rins,gpos_e_tech] =   1
    path_acur[p_down_rins,gpos_r_tech] =   1
    path_acur[p_down_rins,gpos_r_slow] = 999
    path_acur[p_down_rins,gpos_r_extd] =  10
    path_acur[p_down_rins,gpos_r_back] = 999
    path_acur[p_down_rins,gpos_r_near] = 999
    path_acur[p_down_rins,gpos_r_half] = 999
    path_acur[p_down_rins,gpos_r_prep] = 999
    path_acur[p_down_rins,gpos_r_home] = 999
.END

.JOINTS
    #ref_tech[p_down_rins]   0   -6    365   -15    0    -250   60
    #ref_home[p_down_rins]   0   -6    365   -15    0    -250   60
    #ref_half[p_down_rins]   0   -6    365   -15    0    -250   60
    #ref_mpr1[p_down_rins]   0   -6    365   -15    0    -250   60
    #ref_mpr2[p_down_rins]   0   -6    365   -15    0    -250   60
    #ref_mpr3[p_down_rins]   0   -6    365   -15    0    -250   60
.END

.PROGRAM p_down_rins(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back N/A
    POINT #gp_back[.tsk] = #gp_goff[.tsk]
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.wpforwdy[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk] + #PPOINT(0,0,  vpp.wpforwdz[ .prt])
;** #gp_near
    POINT #gp_near[.tsk] = #PPJSET(#ref_half[p_down_rins],3,trn[.tsk,3])
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_down_rins],3,trn[.tsk,3])
;** #gp_home
     POINT #gp_home[.tsk] = #gp_prep[.tsk]
;     POINT #gp_home[.tsk] = #ref_home[p_down_rins]
.END

..MONCMD    area[p_down_rins,1,1] = -136-100            ;** X_MIN
..MONCMD    area[p_down_rins,1,2] = -136                ;** X_BAS
..MONCMD    area[p_down_rins,1,3] = -136+100            ;** X_MAX

..MONCMD    area[p_down_rins,2,1] =  27-100             ;** Y_MIN
..MONCMD    area[p_down_rins,2,2] =  27                 ;** Y_BAS
..MONCMD    area[p_down_rins,2,3] =  27+100             ;** Y_MAX

..MONCMD    area[p_down_rins,3,1] =  365-100            ;** Z_MIN
..MONCMD    area[p_down_rins,3,2] =  365                ;** Z_BAS
..MONCMD    area[p_down_rins,3,3] =  365+100            ;** Z_MAX
.* Joint angle internal calculation may be set to area[p_down_rins,4,2] - 360 = -270
..MONCMD    area[p_down_rins,4,1] =  -300   ;90-30      ;** R1_MIN
..MONCMD    area[p_down_rins,4,2] =  90                 ;** R1_BAS
..MONCMD    area[p_down_rins,4,3] =  90+30              ;** R1_MAX
.* Joint angle internal calculation may be set to area[p_down_rins,5,2] - 360 = -300
..MONCMD    area[p_down_rins,5,1] =  -310   ;60-10      ;** R2_MIN
..MONCMD    area[p_down_rins,5,2] =  60                 ;** R2_BAS
..MONCMD    area[p_down_rins,5,3] =  60+10              ;** R2_MAX

.PROGRAM t_down_rins(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_down_rins
    IF .trn THEN
        ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
        IF .err                                        GOTO err
    ELSE
        ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4],trn[.tsk,4]-360)
    FOR .ele = 1 TO 5 STEP 1
        IF trn[.tsk,.ele] < area[.pas,.ele,1]        GOTO err
        IF trn[.tsk,.ele] > area[.pas,.ele,3]        GOTO err
    END
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_dive_rins.as
.******************************************
.REALS
    mapp_path[p_dive_rins] = 0
    mapp_move[p_dive_rins] = mov_non

    path_move[p_dive_rins,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_dive_rins,gpos_e_prep] = mov_non
    path_move[p_dive_rins,gpos_e_half] = mov_non
    path_move[p_dive_rins,gpos_e_near] = mov_non
    path_move[p_dive_rins,gpos_e_extd] = mov_jmv
    path_move[p_dive_rins,gpos_e_slow] = mov_jmv
    path_move[p_dive_rins,gpos_e_tech] = mov_jmv
    path_move[p_dive_rins,gpos_r_tech] = mov_jmv
    path_move[p_dive_rins,gpos_r_slow] = mov_jmv
    path_move[p_dive_rins,gpos_r_extd] = mov_jmv
    path_move[p_dive_rins,gpos_r_back] = mov_jmv
    path_move[p_dive_rins,gpos_r_near] = mov_non
    path_move[p_dive_rins,gpos_r_half] = mov_non
    path_move[p_dive_rins,gpos_r_prep] = mov_non
    path_move[p_dive_rins,gpos_r_home] = mov_jmv

    path_sped[p_dive_rins,gpos_e_home] = 100
    path_sped[p_dive_rins,gpos_e_prep] = 100
    path_sped[p_dive_rins,gpos_e_half] = 100
    path_sped[p_dive_rins,gpos_e_near] = 100
    path_sped[p_dive_rins,gpos_e_extd] = 100
    path_sped[p_dive_rins,gpos_e_slow] =  -1    ;** 100 .** harish 090209-2 m
    path_sped[p_dive_rins,gpos_e_tech] =  -1
    path_sped[p_dive_rins,gpos_r_tech] =  -1
    path_sped[p_dive_rins,gpos_r_slow] =  -1
    path_sped[p_dive_rins,gpos_r_extd] =  -1    ;** 100 .** harish 090209-2 m
    path_sped[p_dive_rins,gpos_r_back] = 100
    path_sped[p_dive_rins,gpos_r_near] = 100
    path_sped[p_dive_rins,gpos_r_half] = 100
    path_sped[p_dive_rins,gpos_r_prep] = 100
    path_sped[p_dive_rins,gpos_r_home] = 100

    path_acce[p_dive_rins,gpos_e_home] = 100
    path_acce[p_dive_rins,gpos_e_prep] = 100
    path_acce[p_dive_rins,gpos_e_half] = 100
    path_acce[p_dive_rins,gpos_e_near] = 100
    path_acce[p_dive_rins,gpos_e_extd] = 100
    path_acce[p_dive_rins,gpos_e_slow] = 100
    path_acce[p_dive_rins,gpos_e_tech] = 100
    path_acce[p_dive_rins,gpos_r_tech] = 100
    path_acce[p_dive_rins,gpos_r_slow] = 100
    path_acce[p_dive_rins,gpos_r_extd] = 100
    path_acce[p_dive_rins,gpos_r_back] = 100
    path_acce[p_dive_rins,gpos_r_near] = 100
    path_acce[p_dive_rins,gpos_r_half] = 100
    path_acce[p_dive_rins,gpos_r_prep] = 100
    path_acce[p_dive_rins,gpos_r_home] = 100

    path_dece[p_dive_rins,gpos_e_home] = 100
    path_dece[p_dive_rins,gpos_e_prep] = 100
    path_dece[p_dive_rins,gpos_e_half] = 100
    path_dece[p_dive_rins,gpos_e_near] = 100
    path_dece[p_dive_rins,gpos_e_extd] = 100
    path_dece[p_dive_rins,gpos_e_slow] = 100
    path_dece[p_dive_rins,gpos_e_tech] = 100
    path_dece[p_dive_rins,gpos_r_tech] = 100
    path_dece[p_dive_rins,gpos_r_slow] = 100
    path_dece[p_dive_rins,gpos_r_extd] = 100
    path_dece[p_dive_rins,gpos_r_back] = 100
    path_dece[p_dive_rins,gpos_r_near] = 100
    path_dece[p_dive_rins,gpos_r_half] = 100
    path_dece[p_dive_rins,gpos_r_prep] = 100
    path_dece[p_dive_rins,gpos_r_home] = 100

    path_acur[p_dive_rins,gpos_e_home] = 999
    path_acur[p_dive_rins,gpos_e_prep] = 999
    path_acur[p_dive_rins,gpos_e_half] = 999
    path_acur[p_dive_rins,gpos_e_near] = 999
    path_acur[p_dive_rins,gpos_e_extd] =  10
    path_acur[p_dive_rins,gpos_e_slow] = 999
    path_acur[p_dive_rins,gpos_e_tech] =   1
    path_acur[p_dive_rins,gpos_r_tech] =   1
    path_acur[p_dive_rins,gpos_r_slow] =   1
    path_acur[p_dive_rins,gpos_r_extd] =  10
    path_acur[p_dive_rins,gpos_r_back] = 999
    path_acur[p_dive_rins,gpos_r_near] = 999
    path_acur[p_dive_rins,gpos_r_half] = 999
    path_acur[p_dive_rins,gpos_r_prep] = 999
    path_acur[p_dive_rins,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_dive_rins]   0 -113.13   20.000   94.58    0  -251.44    90
    #ref_home[p_dive_rins]   0   13.5     0      -40.3     0  -243.2     90
.** harish 081030 m++
    #ref_mpr1[p_dive_rins]   0 -113.21   20.000   94.44    0  -251.24    90
    #ref_mpr2[p_dive_rins]   0 -113.21   20.000   94.44    0  -251.24    90
.** harish new prepos from Dave 081010 a++
    #ref_mpr3[p_dive_rins]   0 -113.21   20.000   94.44    0  -251.24    90
.** harish 081030 m--
.END
.PROGRAM p_dive_rins(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.wforwdy[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
;** #gp_putt
    POINT #gp_putt[.tsk] = #gp_tech[.tsk]
;** #gp_poff
    POINT #gp_poff[.tsk] = #gp_goff[.tsk]
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_home
;** #gp_prep
;** #gp_half
;** #gp_near
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_dive_rins],3,DEXT(#gp_tech[.tsk],3))
    POINT #gp_prep[.tsk] = #gp_home[.tsk]
    POINT #gp_half[.tsk] = #gp_home[.tsk]
    POINT #gp_near[.tsk] = #gp_home[.tsk]
.END
..MONCMD    area[p_dive_rins,1,1] =  240-100	;** X_MIN
..MONCMD    area[p_dive_rins,1,2] =  240	;** X_BAS
..MONCMD    area[p_dive_rins,1,3] =  240+100	;** X_MAX

..MONCMD    area[p_dive_rins,2,1] = -595-100	;** Y_MIN
..MONCMD    area[p_dive_rins,2,2] = -595	;** Y_BAS
..MONCMD    area[p_dive_rins,2,3] = -595+100	;** Y_MAX
.** harish 090223-2 m++
..MONCMD    area[p_dive_rins,3,1] =  50-20	;** Z_MIN
..MONCMD    area[p_dive_rins,3,2] =  50		;** Z_BAS
..MONCMD    area[p_dive_rins,3,3] =  50+20	;** Z_MAX
.** harish 090223-2 m--
..MONCMD    area[p_dive_rins,4,1] =  90-30	;** R1_MIN
..MONCMD    area[p_dive_rins,4,2] =  90		;** R1_BAS
..MONCMD    area[p_dive_rins,4,3] =  90+30	;** R1_MAX

..MONCMD    area[p_dive_rins,5,1] =  90-10	;** R2_MIN
..MONCMD    area[p_dive_rins,5,2] =  90		;** R2_BAS
..MONCMD    area[p_dive_rins,5,3] =  90+10	;** R2_MAX
.*********************************
.** Teach Position Pass Through **
.*********************************
.PROGRAM t_dive_rins(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_dive_rins
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_dive_buff.as
.******************************************
.REALS
    mapp_path[p_dive_buff] = 0
    mapp_move[p_dive_buff] = mov_non

    path_move[p_dive_buff,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_dive_buff,gpos_e_prep] = mov_non
    path_move[p_dive_buff,gpos_e_half] = mov_non
    path_move[p_dive_buff,gpos_e_near] = mov_non
    path_move[p_dive_buff,gpos_e_extd] = mov_jmv
    path_move[p_dive_buff,gpos_e_slow] = mov_jmv
    path_move[p_dive_buff,gpos_e_tech] = mov_jmv
    path_move[p_dive_buff,gpos_r_tech] = mov_jmv
    path_move[p_dive_buff,gpos_r_slow] = mov_jmv
    path_move[p_dive_buff,gpos_r_extd] = mov_jmv
    path_move[p_dive_buff,gpos_r_back] = mov_jmv
    path_move[p_dive_buff,gpos_r_near] = mov_non
    path_move[p_dive_buff,gpos_r_half] = mov_non
    path_move[p_dive_buff,gpos_r_prep] = mov_non
    path_move[p_dive_buff,gpos_r_home] = mov_jmv

    path_sped[p_dive_buff,gpos_e_home] = 100
    path_sped[p_dive_buff,gpos_e_prep] = 100
    path_sped[p_dive_buff,gpos_e_half] = 100
    path_sped[p_dive_buff,gpos_e_near] = 100
    path_sped[p_dive_buff,gpos_e_extd] = 100
    path_sped[p_dive_buff,gpos_e_slow] =  -1    ;** 100 .** harish 090209-2 m 
    path_sped[p_dive_buff,gpos_e_tech] =  -1
    path_sped[p_dive_buff,gpos_r_tech] =  -1
    path_sped[p_dive_buff,gpos_r_slow] =  -1
    path_sped[p_dive_buff,gpos_r_extd] =  -1	;** 100 .** harish 090209-2 m
    path_sped[p_dive_buff,gpos_r_back] = 100
    path_sped[p_dive_buff,gpos_r_near] = 100
    path_sped[p_dive_buff,gpos_r_half] = 100
    path_sped[p_dive_buff,gpos_r_prep] = 100
    path_sped[p_dive_buff,gpos_r_home] = 100

    path_acce[p_dive_buff,gpos_e_home] = 100
    path_acce[p_dive_buff,gpos_e_prep] = 100
    path_acce[p_dive_buff,gpos_e_half] = 100
    path_acce[p_dive_buff,gpos_e_near] = 100
    path_acce[p_dive_buff,gpos_e_extd] = 100
    path_acce[p_dive_buff,gpos_e_slow] = 100
    path_acce[p_dive_buff,gpos_e_tech] = 100
    path_acce[p_dive_buff,gpos_r_tech] = 100
    path_acce[p_dive_buff,gpos_r_slow] = 100
    path_acce[p_dive_buff,gpos_r_extd] = 100
    path_acce[p_dive_buff,gpos_r_back] = 100
    path_acce[p_dive_buff,gpos_r_near] = 100
    path_acce[p_dive_buff,gpos_r_half] = 100
    path_acce[p_dive_buff,gpos_r_prep] = 100
    path_acce[p_dive_buff,gpos_r_home] = 100

    path_dece[p_dive_buff,gpos_e_home] = 100
    path_dece[p_dive_buff,gpos_e_prep] = 100
    path_dece[p_dive_buff,gpos_e_half] = 100
    path_dece[p_dive_buff,gpos_e_near] = 100
    path_dece[p_dive_buff,gpos_e_extd] = 100
    path_dece[p_dive_buff,gpos_e_slow] = 100
    path_dece[p_dive_buff,gpos_e_tech] = 100
    path_dece[p_dive_buff,gpos_r_tech] = 100
    path_dece[p_dive_buff,gpos_r_slow] = 100
    path_dece[p_dive_buff,gpos_r_extd] = 100
    path_dece[p_dive_buff,gpos_r_back] = 100
    path_dece[p_dive_buff,gpos_r_near] = 100
    path_dece[p_dive_buff,gpos_r_half] = 100
    path_dece[p_dive_buff,gpos_r_prep] = 100
    path_dece[p_dive_buff,gpos_r_home] = 100

    path_acur[p_dive_buff,gpos_e_home] = 999
    path_acur[p_dive_buff,gpos_e_prep] = 999
    path_acur[p_dive_buff,gpos_e_half] = 999
    path_acur[p_dive_buff,gpos_e_near] = 999
    path_acur[p_dive_buff,gpos_e_extd] =  10
    path_acur[p_dive_buff,gpos_e_slow] = 999
    path_acur[p_dive_buff,gpos_e_tech] =   1
    path_acur[p_dive_buff,gpos_r_tech] =   1
    path_acur[p_dive_buff,gpos_r_slow] =   1
    path_acur[p_dive_buff,gpos_r_extd] =  10
    path_acur[p_dive_buff,gpos_r_back] = 999
    path_acur[p_dive_buff,gpos_r_near] = 999
    path_acur[p_dive_buff,gpos_r_half] = 999
    path_acur[p_dive_buff,gpos_r_prep] = 999
    path_acur[p_dive_buff,gpos_r_home] = 999
.END
.JOINTS
.** harish a++
    #ref_tech[p_dive_buff]   0    48.6      145     -109.23   0       -209.36    90
    #ref_home[p_dive_buff]   0    13.50     448     -40.3     0       -243.20    90
    #ref_mpr1[p_dive_buff]   0    48.6      465     -109.23   0       -209.36    90
    #ref_mpr2[p_dive_buff]   0    48.6      465     -109.23   0       -209.36    90
.** harish a--
.** harish new prepos from Dave 081010 a++
    #ref_mpr3[p_dive_buff]   0    48.6      465     -109.23   0       -209.36    90
.END
.PROGRAM p_dive_buff(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.wforwdy[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
;** #gp_putt
    POINT #gp_putt[.tsk] = #gp_tech[.tsk]
;** #gp_poff
    POINT #gp_poff[.tsk] = #gp_goff[.tsk]
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_home
;** #gp_prep
;** #gp_half
;** #gp_near
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_dive_buff],3,DEXT(#gp_tech[.tsk],3))
    POINT #gp_prep[.tsk] = #gp_home[.tsk]
    POINT #gp_half[.tsk] = #gp_home[.tsk]
    POINT #gp_near[.tsk] = #gp_home[.tsk]
.END
.** harish 080801 a++ 
.** harish 090530 m++
..MONCMD    area[p_dive_buff,1,1] = -740-200	;** X_MIN
..MONCMD    area[p_dive_buff,1,2] = -740	;** X_BAS
..MONCMD    area[p_dive_buff,1,3] = -740+100	;** X_MAX

..MONCMD    area[p_dive_buff,2,1] =   30-100	;** Y_MIN
..MONCMD    area[p_dive_buff,2,2] =   30	;** Y_BAS
..MONCMD    area[p_dive_buff,2,3] =   30+100	;** Y_MAX

..MONCMD    area[p_dive_buff,3,1] =   35-15	;** Z_MIN
..MONCMD    area[p_dive_buff,3,2] =   35	;** Z_BAS
..MONCMD    area[p_dive_buff,3,3] =   35+55	;** Z_MAX
.** harish 090530 m--
..MONCMD    area[p_dive_buff,4,1] =   -270-10	;** R1_MIN
..MONCMD    area[p_dive_buff,4,2] =   -270	;** R1_BAS
..MONCMD    area[p_dive_buff,4,3] =   -270+10	;** R1_MAX

..MONCMD    area[p_dive_buff,5,1] =   90-10	;** R2_MIN
..MONCMD    area[p_dive_buff,5,2] =   90	;** R2_BAS
..MONCMD    area[p_dive_buff,5,3] =   90+10	;** R2_MAX
.** harish 080801 a--
.*********************************
.** Teach Position Pass Through **
.*********************************
.PROGRAM t_dive_buff(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_dive_buff
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4],trn[.tsk,4]-360)
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_cs1.as
.**   SycamoreX NT410 CS1
.******************************************
.REALS
    mapp_path[p_hori_cs1] = gpos_e_half+0.5
    mapp_move[p_hori_cs1] = mov_lmv
    mapp_acce[p_hori_cs1] = 30
    mapp_dece[p_hori_cs1] = 30

    path_move[p_hori_cs1,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_cs1,gpos_e_prep] = mov_non
    path_move[p_hori_cs1,gpos_e_half] = mov_jmv
    path_move[p_hori_cs1,gpos_e_near] = mov_non
    path_move[p_hori_cs1,gpos_e_extd] = mov_lmv
    path_move[p_hori_cs1,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_cs1,gpos_e_tech] = mov_jmv
    path_move[p_hori_cs1,gpos_r_tech] = mov_lmv
    path_move[p_hori_cs1,gpos_r_slow] = mov_non
    path_move[p_hori_cs1,gpos_r_extd] = mov_lmv
    path_move[p_hori_cs1,gpos_r_back] = mov_lmv
    path_move[p_hori_cs1,gpos_r_near] = mov_non
    path_move[p_hori_cs1,gpos_r_half] = mov_lmv
    path_move[p_hori_cs1,gpos_r_prep] = mov_non
    path_move[p_hori_cs1,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_cs1,gpos_e_home] = 100
    path_sped[p_hori_cs1,gpos_e_prep] = 100
    path_sped[p_hori_cs1,gpos_e_half] = 100
    path_sped[p_hori_cs1,gpos_e_near] = 100
    path_sped[p_hori_cs1,gpos_e_extd] = 100
    path_sped[p_hori_cs1,gpos_e_slow] = 100
    path_sped[p_hori_cs1,gpos_e_tech] =  -1
    path_sped[p_hori_cs1,gpos_r_tech] =  -1
    path_sped[p_hori_cs1,gpos_r_slow] =  -1
    path_sped[p_hori_cs1,gpos_r_extd] =  -1
    path_sped[p_hori_cs1,gpos_r_back] = 100
    path_sped[p_hori_cs1,gpos_r_near] = 100
    path_sped[p_hori_cs1,gpos_r_half] = 100
    path_sped[p_hori_cs1,gpos_r_prep] = 100
    path_sped[p_hori_cs1,gpos_r_home] = 100
    
    path_acce[p_hori_cs1,gpos_e_home] = 80
    path_acce[p_hori_cs1,gpos_e_prep] = 80
.*                                                      /* SCRX11-032-1 m */
    path_acce[p_hori_cs1,gpos_e_half] = 75              ;60
    path_acce[p_hori_cs1,gpos_e_near] = 50
    path_acce[p_hori_cs1,gpos_e_extd] = 50
    path_acce[p_hori_cs1,gpos_e_slow] = 100
    path_acce[p_hori_cs1,gpos_e_tech] = 100
    path_acce[p_hori_cs1,gpos_r_tech] = 30
    path_acce[p_hori_cs1,gpos_r_slow] = 100
    path_acce[p_hori_cs1,gpos_r_extd] = 50
    path_acce[p_hori_cs1,gpos_r_back] = 30
    path_acce[p_hori_cs1,gpos_r_near] = 50
    path_acce[p_hori_cs1,gpos_r_half] = 60
    path_acce[p_hori_cs1,gpos_r_prep] = 80
.*                                                      /* SCRX11-032-1 m */
    path_acce[p_hori_cs1,gpos_r_home] = 60              ;80

    path_dece[p_hori_cs1,gpos_e_home] = 80
    path_dece[p_hori_cs1,gpos_e_prep] = 80
.*                                                      /* SCRX11-032-1 m */
    path_dece[p_hori_cs1,gpos_e_half] = 75              ;60
    path_dece[p_hori_cs1,gpos_e_near] = 50
    path_dece[p_hori_cs1,gpos_e_extd] = 50
    path_dece[p_hori_cs1,gpos_e_slow] = 100
    path_dece[p_hori_cs1,gpos_e_tech] = 100
    path_dece[p_hori_cs1,gpos_r_tech] = 30
    path_dece[p_hori_cs1,gpos_r_slow] = 100
    path_dece[p_hori_cs1,gpos_r_extd] = 50
    path_dece[p_hori_cs1,gpos_r_back] = 30
    path_dece[p_hori_cs1,gpos_r_near] = 50
    path_dece[p_hori_cs1,gpos_r_half] = 60
    path_dece[p_hori_cs1,gpos_r_prep] = 80
.*                                                      /* SCRX11-032-1 m */
    path_dece[p_hori_cs1,gpos_r_home] = 60              ;80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_cs1,gpos_e_home] =  10
    path_acur[p_hori_cs1,gpos_e_prep] = 999
    path_acur[p_hori_cs1,gpos_e_half] = 999
    path_acur[p_hori_cs1,gpos_e_near] = 999
    path_acur[p_hori_cs1,gpos_e_extd] =   3
    path_acur[p_hori_cs1,gpos_e_slow] =   1
    path_acur[p_hori_cs1,gpos_e_tech] =   1
    path_acur[p_hori_cs1,gpos_r_tech] =   1
    path_acur[p_hori_cs1,gpos_r_slow] =   1
    path_acur[p_hori_cs1,gpos_r_extd] =   3
    path_acur[p_hori_cs1,gpos_r_back] =   1
    path_acur[p_hori_cs1,gpos_r_near] = 999
    path_acur[p_hori_cs1,gpos_r_half] = 999
    path_acur[p_hori_cs1,gpos_r_prep] = 999
    path_acur[p_hori_cs1,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_cs1]   0   -52.772   10    165.051   0   -112.278
.** harish new prepos from Dave 081010 a++
    #ref_home[p_hori_cs1]   0   -18.956   10     33.395   0    -14.426
.*  #ref_home[p_hori_cs1]   0   -17.729   10     29.679   0     -8.948
.*  #ref_mpr1[p_hori_cs1]   0   -49.096   10     98.522   0    -64.428
.*  #ref_mpr2[p_hori_cs1]   0   -49.096   10     98.522   0    -64.428
.** harish 080917 mprepos closer to stations a++
    #ref_mpr1[p_hori_cs1]   0   -60.15   10     118.79   0    -58.64
.*  #ref_mpr2[p_hori_cs1]   0   -60.15   10     118.79   0    -58.64
.** harish new prepos from Dave 081010 a++
.** harish 081111 m++
.*  #ref_mpr2[p_hori_cs1]   0   -51.543  10     123.44   0    -71.903
.*  #ref_mpr3[p_hori_cs1]   0   -51.543  10     123.44   0    -71.903
.** SCRX10-027-3 m++
    #ref_mpr2[p_hori_cs1]   0   -54.194  10     119.773  0    -65.583
    #ref_mpr3[p_hori_cs1]   0   -54.194  10     119.773  0    -65.583
.** SCRX10-027-3 m--
.** harish 081111 m--
.** harish 080917 mprepos closer to stations a--     
.END
.PROGRAM p_hori_cs1(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
;** #gp_half
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = #gp_tech[.tsk],inv_hand
    add[.tsk,2] = -340
;** 090318 for NOVA m+
    add[.tsk,4] =  180			;**m  165
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
;** #gp_home
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_hori_cs1],3,trn[.tsk,3])
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END
..MONCMD    area[p_hori_cs1,1,1] = -758-100	;** X_MIN
..MONCMD    area[p_hori_cs1,1,2] = -758		;** X_BAS
..MONCMD    area[p_hori_cs1,1,3] = -758+100	;** X_MAX

..MONCMD    area[p_hori_cs1,2,1] = -783-100	;** Y_MIN
..MONCMD    area[p_hori_cs1,2,2] = -783		;** Y_BAS
..MONCMD    area[p_hori_cs1,2,3] = -783+100	;** Y_MAX

..MONCMD    area[p_hori_cs1,3,1] =    5		;** Z_MIN
..MONCMD    area[p_hori_cs1,3,2] =   10		;** Z_BAS
..MONCMD    area[p_hori_cs1,3,3] =   270	;** Z_MAX	50 .** harish 090317 m

..MONCMD    area[p_hori_cs1,4,1] = 180-10	;** R1_MIN
..MONCMD    area[p_hori_cs1,4,2] = 180		;** R1_BAS
..MONCMD    area[p_hori_cs1,4,3] = 180+10	;** R1_MAX

..MONCMD    area[p_hori_cs1,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_cs1,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_cs1,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position CS1 **
.*********************************
.PROGRAM t_hori_cs1(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_cs1
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_cs2.as
.**   SycamoreX NT410 cs2
.******************************************
.REALS
    mapp_path[p_hori_cs2] = gpos_e_half+0.5
    mapp_move[p_hori_cs2] = mov_lmv
    mapp_acce[p_hori_cs2] = 30
    mapp_dece[p_hori_cs2] = 30

    path_move[p_hori_cs2,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_cs2,gpos_e_prep] = mov_non
    path_move[p_hori_cs2,gpos_e_half] = mov_jmv
    path_move[p_hori_cs2,gpos_e_near] = mov_non
    path_move[p_hori_cs2,gpos_e_extd] = mov_lmv
    path_move[p_hori_cs2,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_cs2,gpos_e_tech] = mov_jmv
    path_move[p_hori_cs2,gpos_r_tech] = mov_lmv
    path_move[p_hori_cs2,gpos_r_slow] = mov_non
    path_move[p_hori_cs2,gpos_r_extd] = mov_lmv
    path_move[p_hori_cs2,gpos_r_back] = mov_lmv
    path_move[p_hori_cs2,gpos_r_near] = mov_non
    path_move[p_hori_cs2,gpos_r_half] = mov_lmv
    path_move[p_hori_cs2,gpos_r_prep] = mov_non
    path_move[p_hori_cs2,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_cs2,gpos_e_home] = 100
    path_sped[p_hori_cs2,gpos_e_prep] = 100
    path_sped[p_hori_cs2,gpos_e_half] = 100
    path_sped[p_hori_cs2,gpos_e_near] = 100
    path_sped[p_hori_cs2,gpos_e_extd] = 100
    path_sped[p_hori_cs2,gpos_e_slow] = 100
    path_sped[p_hori_cs2,gpos_e_tech] =  -1
    path_sped[p_hori_cs2,gpos_r_tech] =  -1
    path_sped[p_hori_cs2,gpos_r_slow] =  -1
    path_sped[p_hori_cs2,gpos_r_extd] =  -1
    path_sped[p_hori_cs2,gpos_r_back] = 100
    path_sped[p_hori_cs2,gpos_r_near] = 100
    path_sped[p_hori_cs2,gpos_r_half] = 100
    path_sped[p_hori_cs2,gpos_r_prep] = 100
    path_sped[p_hori_cs2,gpos_r_home] = 100

    path_acce[p_hori_cs2,gpos_e_home] = 80
    path_acce[p_hori_cs2,gpos_e_prep] = 80
    path_acce[p_hori_cs2,gpos_e_half] = 60
    path_acce[p_hori_cs2,gpos_e_near] = 50
    path_acce[p_hori_cs2,gpos_e_extd] = 50
    path_acce[p_hori_cs2,gpos_e_slow] = 100
    path_acce[p_hori_cs2,gpos_e_tech] = 100
    path_acce[p_hori_cs2,gpos_r_tech] = 30
    path_acce[p_hori_cs2,gpos_r_slow] = 100
    path_acce[p_hori_cs2,gpos_r_extd] = 50
    path_acce[p_hori_cs2,gpos_r_back] = 30
    path_acce[p_hori_cs2,gpos_r_near] = 50
    path_acce[p_hori_cs2,gpos_r_half] = 60
    path_acce[p_hori_cs2,gpos_r_prep] = 80
    path_acce[p_hori_cs2,gpos_r_home] = 80

    path_dece[p_hori_cs2,gpos_e_home] = 80
    path_dece[p_hori_cs2,gpos_e_prep] = 80
    path_dece[p_hori_cs2,gpos_e_half] = 60
    path_dece[p_hori_cs2,gpos_e_near] = 50
    path_dece[p_hori_cs2,gpos_e_extd] = 50
    path_dece[p_hori_cs2,gpos_e_slow] = 100
    path_dece[p_hori_cs2,gpos_e_tech] = 100
    path_dece[p_hori_cs2,gpos_r_tech] = 30
    path_dece[p_hori_cs2,gpos_r_slow] = 100
    path_dece[p_hori_cs2,gpos_r_extd] = 50
    path_dece[p_hori_cs2,gpos_r_back] = 30
    path_dece[p_hori_cs2,gpos_r_near] = 50
    path_dece[p_hori_cs2,gpos_r_half] = 60
    path_dece[p_hori_cs2,gpos_r_prep] = 80
    path_dece[p_hori_cs2,gpos_r_home] = 80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_cs2,gpos_e_home] =  10
    path_acur[p_hori_cs2,gpos_e_prep] = 999
    path_acur[p_hori_cs2,gpos_e_half] = 999
    path_acur[p_hori_cs2,gpos_e_near] = 999
    path_acur[p_hori_cs2,gpos_e_extd] =   3
    path_acur[p_hori_cs2,gpos_e_slow] =   1
    path_acur[p_hori_cs2,gpos_e_tech] =   1
    path_acur[p_hori_cs2,gpos_r_tech] =   1
    path_acur[p_hori_cs2,gpos_r_slow] =   1
    path_acur[p_hori_cs2,gpos_r_extd] =   3
    path_acur[p_hori_cs2,gpos_r_back] =   1
    path_acur[p_hori_cs2,gpos_r_near] = 999
    path_acur[p_hori_cs2,gpos_r_half] = 999
    path_acur[p_hori_cs2,gpos_r_prep] = 999
    path_acur[p_hori_cs2,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_cs2]   0    25.030   10     69.444   0    -94.474
.*  #ref_home[p_hori_cs2]   0   -17.729   10     29.679   0     -8.948
    #ref_mpr1[p_hori_cs2]   0   -18.956   10     33.395   0    -14.426
.*  #ref_mpr2[p_hori_cs2]   0   -18.956   10     33.395   0    -14.426
.** harish new prepos from Dave 081010 a++
    #ref_home[p_hori_cs2]   0   -18.956   10     33.395   0    -14.426
.** harish 081111 m++
.*  #ref_mpr2[p_hori_cs2]   0     9.070   10     37.963   0    -47.034
.*  #ref_mpr3[p_hori_cs2]   0     9.070   10     37.963   0    -47.034
.** SCRX10-027-3 m++
    #ref_mpr2[p_hori_cs2]   0    -0.775   10     34.849   0    -34.079
    #ref_mpr3[p_hori_cs2]   0    -0.775   10     34.849   0    -34.079
.** SCRX10-027-3 m--
.** harish 081111 m--
.END
.PROGRAM p_hori_cs2(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = #gp_tech[.tsk],inv_hand
    add[.tsk,2] = -340
;** 090318 for NOVA m+
    add[.tsk,4] =  180			;**m  180
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_hori_cs2],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END
..MONCMD    area[p_hori_cs2,1,1] = -253-100	;** X_MIN
..MONCMD    area[p_hori_cs2,1,2] = -253		;** X_BAS
..MONCMD    area[p_hori_cs2,1,3] = -253+100	;** X_MAX

..MONCMD    area[p_hori_cs2,2,1] = -783-100	;** Y_MIN
..MONCMD    area[p_hori_cs2,2,2] = -783		;** Y_BAS
..MONCMD    area[p_hori_cs2,2,3] = -783+100	;** Y_MAX

..MONCMD    area[p_hori_cs2,3,1] =    5		;** Z_MIN
..MONCMD    area[p_hori_cs2,3,2] =   10		;** Z_BAS
..MONCMD    area[p_hori_cs2,3,3] =   270	;** Z_MAX	50 .** harish 090317 m

..MONCMD    area[p_hori_cs2,4,1] = 180-10	;** R1_MIN
..MONCMD    area[p_hori_cs2,4,2] = 180		;** R1_BAS
..MONCMD    area[p_hori_cs2,4,3] = 180+10	;** R1_MAX

..MONCMD    area[p_hori_cs2,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_cs2,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_cs2,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position cs2 **
.*********************************
.PROGRAM t_hori_cs2(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_cs2
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_cs3.as
.**   SycamoreX NT410 cs3
.******************************************
.REALS
    mapp_path[p_hori_cs3] = gpos_e_half+0.5
    mapp_move[p_hori_cs3] = mov_lmv
    mapp_acce[p_hori_cs3] = 30
    mapp_dece[p_hori_cs3] = 30

    path_move[p_hori_cs3,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_cs3,gpos_e_prep] = mov_jmv
    path_move[p_hori_cs3,gpos_e_half] = mov_jmv
    path_move[p_hori_cs3,gpos_e_near] = mov_non
    path_move[p_hori_cs3,gpos_e_extd] = mov_lmv
    path_move[p_hori_cs3,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_cs3,gpos_e_tech] = mov_jmv
    path_move[p_hori_cs3,gpos_r_tech] = mov_lmv
    path_move[p_hori_cs3,gpos_r_slow] = mov_non
    path_move[p_hori_cs3,gpos_r_extd] = mov_lmv
    path_move[p_hori_cs3,gpos_r_back] = mov_lmv
    path_move[p_hori_cs3,gpos_r_near] = mov_non
    path_move[p_hori_cs3,gpos_r_half] = mov_lmv
    path_move[p_hori_cs3,gpos_r_prep] = mov_jmv
    path_move[p_hori_cs3,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_cs3,gpos_e_home] = 100
    path_sped[p_hori_cs3,gpos_e_prep] = 100
    path_sped[p_hori_cs3,gpos_e_half] = 100
    path_sped[p_hori_cs3,gpos_e_near] = 100
    path_sped[p_hori_cs3,gpos_e_extd] = 100
    path_sped[p_hori_cs3,gpos_e_slow] = 100
    path_sped[p_hori_cs3,gpos_e_tech] =  -1
    path_sped[p_hori_cs3,gpos_r_tech] =  -1
    path_sped[p_hori_cs3,gpos_r_slow] =  -1
    path_sped[p_hori_cs3,gpos_r_extd] =  -1
    path_sped[p_hori_cs3,gpos_r_back] = 100
    path_sped[p_hori_cs3,gpos_r_near] = 100
    path_sped[p_hori_cs3,gpos_r_half] = 100
    path_sped[p_hori_cs3,gpos_r_prep] = 100
    path_sped[p_hori_cs3,gpos_r_home] = 100

    path_acce[p_hori_cs3,gpos_e_home] = 80
    path_acce[p_hori_cs3,gpos_e_prep] = 80
    path_acce[p_hori_cs3,gpos_e_half] = 60
    path_acce[p_hori_cs3,gpos_e_near] = 50
    path_acce[p_hori_cs3,gpos_e_extd] = 50
    path_acce[p_hori_cs3,gpos_e_slow] = 100
    path_acce[p_hori_cs3,gpos_e_tech] = 100
    path_acce[p_hori_cs3,gpos_r_tech] = 30
    path_acce[p_hori_cs3,gpos_r_slow] = 100
    path_acce[p_hori_cs3,gpos_r_extd] = 50
    path_acce[p_hori_cs3,gpos_r_back] = 30
    path_acce[p_hori_cs3,gpos_r_near] = 50
    path_acce[p_hori_cs3,gpos_r_half] = 60
    path_acce[p_hori_cs3,gpos_r_prep] = 80
    path_acce[p_hori_cs3,gpos_r_home] = 80

    path_dece[p_hori_cs3,gpos_e_home] = 80
    path_dece[p_hori_cs3,gpos_e_prep] = 80
    path_dece[p_hori_cs3,gpos_e_half] = 60
    path_dece[p_hori_cs3,gpos_e_near] = 50
    path_dece[p_hori_cs3,gpos_e_extd] = 50
    path_dece[p_hori_cs3,gpos_e_slow] = 100
    path_dece[p_hori_cs3,gpos_e_tech] = 100
    path_dece[p_hori_cs3,gpos_r_tech] = 30
    path_dece[p_hori_cs3,gpos_r_slow] = 100
    path_dece[p_hori_cs3,gpos_r_extd] = 50
    path_dece[p_hori_cs3,gpos_r_back] = 30
    path_dece[p_hori_cs3,gpos_r_near] = 50
    path_dece[p_hori_cs3,gpos_r_half] = 60
    path_dece[p_hori_cs3,gpos_r_prep] = 80
    path_dece[p_hori_cs3,gpos_r_home] = 80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_cs3,gpos_e_home] =  10
    path_acur[p_hori_cs3,gpos_e_prep] = 999
    path_acur[p_hori_cs3,gpos_e_half] = 999
    path_acur[p_hori_cs3,gpos_e_near] = 999
    path_acur[p_hori_cs3,gpos_e_extd] =   3
    path_acur[p_hori_cs3,gpos_e_slow] =   1
    path_acur[p_hori_cs3,gpos_e_tech] =   1
    path_acur[p_hori_cs3,gpos_r_tech] =   1
    path_acur[p_hori_cs3,gpos_r_slow] =   1
    path_acur[p_hori_cs3,gpos_r_extd] =   3
    path_acur[p_hori_cs3,gpos_r_back] =   1
    path_acur[p_hori_cs3,gpos_r_near] = 999
    path_acur[p_hori_cs3,gpos_r_half] = 999
    path_acur[p_hori_cs3,gpos_r_prep] = 999
    path_acur[p_hori_cs3,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_cs3]   0   -25.030   10    -69.444   0     94.474
    #ref_prep[p_hori_cs3]   0    30.000   10    -60.000   0      0.000
    #ref_home[p_hori_cs3]   0   -10.000   10    -35.000   0     -16.000
    #ref_mpr1[p_hori_cs3]   0    30.000   10    -60.000   0      0.000
.*  #ref_mpr2[p_hori_cs3]   0    23.867   10    -54.250   0      5.374
.** harish new prepos from Dave 081010 a++
.** harish 081111 m++
.*  #ref_mpr2[p_hori_cs3]   0   -10.888   10    -37.393   0     48.270
.*  #ref_mpr3[p_hori_cs3]   0   -10.888   10    -37.393   0     48.270
.** SCRX10-027-3 m++
    #ref_mpr2[p_hori_cs3]   0     0.784   10    -34.858   0     34.069
    #ref_mpr3[p_hori_cs3]   0     0.784   10    -34.858   0     34.069
.** SCRX10-027-3 m--
.** harish 081111 m--
.END
.PROGRAM p_hori_cs3(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = #gp_tech[.tsk],inv_hand
;** 090318 for NOVA m++
    add[.tsk,2] = -470			;**m -340
    add[.tsk,4] =  180			;**m  155
;** 090318 for NOVA m--
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_prep[p_hori_cs3],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_hori_cs3],3,trn[.tsk,3])
.END
..MONCMD    area[p_hori_cs3,1,1] =  253-100	;** X_MIN
..MONCMD    area[p_hori_cs3,1,2] =  253		;** X_BAS
..MONCMD    area[p_hori_cs3,1,3] =  253+100	;** X_MAX

..MONCMD    area[p_hori_cs3,2,1] = -783-100	;** Y_MIN
..MONCMD    area[p_hori_cs3,2,2] = -783		;** Y_BAS
..MONCMD    area[p_hori_cs3,2,3] = -783+100	;** Y_MAX

..MONCMD    area[p_hori_cs3,3,1] =    5		;** Z_MIN
..MONCMD    area[p_hori_cs3,3,2] =   10		;** Z_BAS
..MONCMD    area[p_hori_cs3,3,3] =   270	;** Z_MAX	50 .** harish 090317 m

..MONCMD    area[p_hori_cs3,4,1] = 180-10	;** R1_MIN
..MONCMD    area[p_hori_cs3,4,2] = 180		;** R1_BAS
..MONCMD    area[p_hori_cs3,4,3] = 180+10	;** R1_MAX

..MONCMD    area[p_hori_cs3,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_cs3,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_cs3,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position cs3 **
.*********************************
.PROGRAM t_hori_cs3(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_cs3
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_cs4.as
.**   SycamoreX NT410 cs4
.******************************************
.REALS
    mapp_path[p_hori_cs4] = gpos_e_half+0.5
    mapp_move[p_hori_cs4] = mov_lmv
    mapp_acce[p_hori_cs4] = 30
    mapp_dece[p_hori_cs4] = 30

    path_move[p_hori_cs4,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_cs4,gpos_e_prep] = mov_jmv
    path_move[p_hori_cs4,gpos_e_half] = mov_jmv
    path_move[p_hori_cs4,gpos_e_near] = mov_non
    path_move[p_hori_cs4,gpos_e_extd] = mov_lmv
    path_move[p_hori_cs4,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_cs4,gpos_e_tech] = mov_jmv
    path_move[p_hori_cs4,gpos_r_tech] = mov_lmv
    path_move[p_hori_cs4,gpos_r_slow] = mov_non
    path_move[p_hori_cs4,gpos_r_extd] = mov_lmv
    path_move[p_hori_cs4,gpos_r_back] = mov_lmv
    path_move[p_hori_cs4,gpos_r_near] = mov_non
    path_move[p_hori_cs4,gpos_r_half] = mov_lmv
    path_move[p_hori_cs4,gpos_r_prep] = mov_jmv
    path_move[p_hori_cs4,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_cs4,gpos_e_home] = 100
    path_sped[p_hori_cs4,gpos_e_prep] = 100
    path_sped[p_hori_cs4,gpos_e_half] = 100
    path_sped[p_hori_cs4,gpos_e_near] = 100
    path_sped[p_hori_cs4,gpos_e_extd] = 100
    path_sped[p_hori_cs4,gpos_e_slow] = 100
    path_sped[p_hori_cs4,gpos_e_tech] =  -1
    path_sped[p_hori_cs4,gpos_r_tech] =  -1
    path_sped[p_hori_cs4,gpos_r_slow] =  -1
    path_sped[p_hori_cs4,gpos_r_extd] =  -1
    path_sped[p_hori_cs4,gpos_r_back] = 100
    path_sped[p_hori_cs4,gpos_r_near] = 100
    path_sped[p_hori_cs4,gpos_r_half] = 100
    path_sped[p_hori_cs4,gpos_r_prep] = 100
    path_sped[p_hori_cs4,gpos_r_home] = 100

    path_acce[p_hori_cs4,gpos_e_home] = 80
    path_acce[p_hori_cs4,gpos_e_prep] = 80
    path_acce[p_hori_cs4,gpos_e_half] = 60
    path_acce[p_hori_cs4,gpos_e_near] = 50
    path_acce[p_hori_cs4,gpos_e_extd] = 50
    path_acce[p_hori_cs4,gpos_e_slow] = 100
    path_acce[p_hori_cs4,gpos_e_tech] = 100
    path_acce[p_hori_cs4,gpos_r_tech] = 30
    path_acce[p_hori_cs4,gpos_r_slow] = 100
    path_acce[p_hori_cs4,gpos_r_extd] = 50
    path_acce[p_hori_cs4,gpos_r_back] = 30
    path_acce[p_hori_cs4,gpos_r_near] = 50
    path_acce[p_hori_cs4,gpos_r_half] = 60
    path_acce[p_hori_cs4,gpos_r_prep] = 80
    path_acce[p_hori_cs4,gpos_r_home] = 80

    path_dece[p_hori_cs4,gpos_e_home] = 80
    path_dece[p_hori_cs4,gpos_e_prep] = 80
    path_dece[p_hori_cs4,gpos_e_half] = 60
    path_dece[p_hori_cs4,gpos_e_near] = 50
    path_dece[p_hori_cs4,gpos_e_extd] = 50
    path_dece[p_hori_cs4,gpos_e_slow] = 100
    path_dece[p_hori_cs4,gpos_e_tech] = 100
    path_dece[p_hori_cs4,gpos_r_tech] = 30
    path_dece[p_hori_cs4,gpos_r_slow] = 100
    path_dece[p_hori_cs4,gpos_r_extd] = 50
    path_dece[p_hori_cs4,gpos_r_back] = 30
    path_dece[p_hori_cs4,gpos_r_near] = 50
    path_dece[p_hori_cs4,gpos_r_half] = 60
    path_dece[p_hori_cs4,gpos_r_prep] = 80
    path_dece[p_hori_cs4,gpos_r_home] = 80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_cs4,gpos_e_home] = 999
    path_acur[p_hori_cs4,gpos_e_prep] = 999
    path_acur[p_hori_cs4,gpos_e_half] = 999
    path_acur[p_hori_cs4,gpos_e_near] = 999
    path_acur[p_hori_cs4,gpos_e_extd] =   3
    path_acur[p_hori_cs4,gpos_e_slow] =   1
    path_acur[p_hori_cs4,gpos_e_tech] =   1
    path_acur[p_hori_cs4,gpos_r_tech] =   1
    path_acur[p_hori_cs4,gpos_r_slow] =   1
    path_acur[p_hori_cs4,gpos_r_extd] =   3
    path_acur[p_hori_cs4,gpos_r_back] =   1
    path_acur[p_hori_cs4,gpos_r_near] = 999
    path_acur[p_hori_cs4,gpos_r_half] = 999
    path_acur[p_hori_cs4,gpos_r_prep] = 999
    path_acur[p_hori_cs4,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_cs4]   0    52.767   10   -165.061   0    112.278
    #ref_prep[p_hori_cs4]   0    30.000   10    -60.000   0      0.000
    #ref_home[p_hori_cs4]   0   -10.000   10    -35.000   0    -16.002
    #ref_mpr1[p_hori_cs4]   0    30.000   10    -60.000   0      0.000
.*  #ref_mpr2[p_hori_cs4]   0    49.093   10    -98.517   0     64.426
.** harish 080917 mprepos closer to stations a++
.*  #ref_mpr2[p_hori_cs4]   0    49.093   10    -98.517   0     54.426
.** harish 080917 mprepos closer to stations a--
.** harish new prepos from Dave 081010 a++
    #ref_mpr2[p_hori_cs4]   0    49.889   10    -99.636   0     64.733
.** harish 081111 m+
.*  #ref_mpr3[p_hori_cs4]   0    51.542   10   -123.473   0     71.930
.** SCRX10-027-3 m
    #ref_mpr3[p_hori_cs4]   0    54.197   10   -119.779   0     65.580
.END
.PROGRAM p_hori_cs4(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
;** #gp_half
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = #gp_tech[.tsk],inv_hand
;** 090318 for NOVA m++
    add[.tsk,2] = -390			;**m  -340			
    add[.tsk,4] = 180			;**m   195
;** 090318 for NOVA m--
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_prep[p_hori_cs4],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_hori_cs4],3,trn[.tsk,3])
.END
..MONCMD    area[p_hori_cs4,1,1] =  758-100	;** X_MIN
..MONCMD    area[p_hori_cs4,1,2] =  758		;** X_BAS
..MONCMD    area[p_hori_cs4,1,3] =  758+100	;** X_MAX

..MONCMD    area[p_hori_cs4,2,1] = -783-100	;** Y_MIN
..MONCMD    area[p_hori_cs4,2,2] = -783		;** Y_BAS
..MONCMD    area[p_hori_cs4,2,3] = -783+100	;** Y_MAX

..MONCMD    area[p_hori_cs4,3,1] =    5		;** Z_MIN
..MONCMD    area[p_hori_cs4,3,2] =   10		;** Z_BAS
..MONCMD    area[p_hori_cs4,3,3] =   270	;** Z_MAX	50 .** harish 090317 m

..MONCMD    area[p_hori_cs4,4,1] = 180-10	;** R1_MIN
..MONCMD    area[p_hori_cs4,4,2] = 180		;** R1_BAS
..MONCMD    area[p_hori_cs4,4,3] = 180+10	;** R1_MAX

..MONCMD    area[p_hori_cs4,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_cs4,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_cs4,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position cs4 **
.*********************************
.PROGRAM t_hori_cs4(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_cs4
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.** harish 090515-1 a++
.******************************************
.** app_hori_imp.as
.**   SycamoreX NT410 IMAP
.******************************************
.REALS
    mapp_path[p_hori_imp] = gpos_e_half+0.5
    mapp_move[p_hori_imp] = mov_lmv
    mapp_acce[p_hori_imp] = 30
    mapp_dece[p_hori_imp] = 30

    path_move[p_hori_imp,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_imp,gpos_e_prep] = mov_jmv
    path_move[p_hori_imp,gpos_e_half] = mov_lmv
    path_move[p_hori_imp,gpos_e_near] = mov_non
    path_move[p_hori_imp,gpos_e_extd] = mov_non
    path_move[p_hori_imp,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_imp,gpos_e_tech] = mov_lmv
.** SCRX10-027-1 m
    path_move[p_hori_imp,gpos_r_tech] = mov_lmv
    path_move[p_hori_imp,gpos_r_slow] = mov_non
    path_move[p_hori_imp,gpos_r_extd] = mov_non
    path_move[p_hori_imp,gpos_r_back] = mov_non
    path_move[p_hori_imp,gpos_r_near] = mov_non
    path_move[p_hori_imp,gpos_r_half] = mov_lmv
    path_move[p_hori_imp,gpos_r_prep] = mov_lmv
    path_move[p_hori_imp,gpos_r_home] = mov_jmv

    path_sped[p_hori_imp,gpos_e_home] = 100
    path_sped[p_hori_imp,gpos_e_prep] = 100
    path_sped[p_hori_imp,gpos_e_half] = 100
    path_sped[p_hori_imp,gpos_e_near] = 100
    path_sped[p_hori_imp,gpos_e_extd] = 100
    path_sped[p_hori_imp,gpos_e_slow] = 100
    path_sped[p_hori_imp,gpos_e_tech] =  -1
    path_sped[p_hori_imp,gpos_r_tech] =  -1
    path_sped[p_hori_imp,gpos_r_slow] =  -1
    path_sped[p_hori_imp,gpos_r_extd] =  -1
    path_sped[p_hori_imp,gpos_r_back] = 100
    path_sped[p_hori_imp,gpos_r_near] = 100
    path_sped[p_hori_imp,gpos_r_half] = 100
    path_sped[p_hori_imp,gpos_r_prep] = 100
    path_sped[p_hori_imp,gpos_r_home] = 100

    path_acce[p_hori_imp,gpos_e_home] = 80
    path_acce[p_hori_imp,gpos_e_prep] = 80
    path_acce[p_hori_imp,gpos_e_half] = 70
    path_acce[p_hori_imp,gpos_e_near] = 60
    path_acce[p_hori_imp,gpos_e_extd] = 60
    path_acce[p_hori_imp,gpos_e_slow] = 100
    path_acce[p_hori_imp,gpos_e_tech] = 100
    path_acce[p_hori_imp,gpos_r_tech] = 30
    path_acce[p_hori_imp,gpos_r_slow] = 100
    path_acce[p_hori_imp,gpos_r_extd] = 60
    path_acce[p_hori_imp,gpos_r_back] = 30
    path_acce[p_hori_imp,gpos_r_near] = 60
    path_acce[p_hori_imp,gpos_r_half] = 70
    path_acce[p_hori_imp,gpos_r_prep] = 80
    path_acce[p_hori_imp,gpos_r_home] = 80

    path_dece[p_hori_imp,gpos_e_home] = 80
    path_dece[p_hori_imp,gpos_e_prep] = 80
    path_dece[p_hori_imp,gpos_e_half] = 70
    path_dece[p_hori_imp,gpos_e_near] = 60
    path_dece[p_hori_imp,gpos_e_extd] = 60
    path_dece[p_hori_imp,gpos_e_slow] = 100
    path_dece[p_hori_imp,gpos_e_tech] = 100
    path_dece[p_hori_imp,gpos_r_tech] = 30
    path_dece[p_hori_imp,gpos_r_slow] = 100
    path_dece[p_hori_imp,gpos_r_extd] = 60
    path_dece[p_hori_imp,gpos_r_back] = 30
    path_dece[p_hori_imp,gpos_r_near] = 60
    path_dece[p_hori_imp,gpos_r_half] = 70
    path_dece[p_hori_imp,gpos_r_prep] = 80
    path_dece[p_hori_imp,gpos_r_home] = 80

    path_acur[p_hori_imp,gpos_e_home] =  10
    path_acur[p_hori_imp,gpos_e_prep] = 999
    path_acur[p_hori_imp,gpos_e_half] = 999
    path_acur[p_hori_imp,gpos_e_near] = 999
    path_acur[p_hori_imp,gpos_e_extd] =   3
    path_acur[p_hori_imp,gpos_e_slow] =   1
    path_acur[p_hori_imp,gpos_e_tech] =   1
    path_acur[p_hori_imp,gpos_r_tech] =   1
    path_acur[p_hori_imp,gpos_r_slow] =   1
    path_acur[p_hori_imp,gpos_r_extd] =   3
    path_acur[p_hori_imp,gpos_r_back] =   1
    path_acur[p_hori_imp,gpos_r_near] = 999
    path_acur[p_hori_imp,gpos_r_half] = 999
    path_acur[p_hori_imp,gpos_r_prep] = 999
    path_acur[p_hori_imp,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_imp]   0    24.350   304    38.48    0   -152.557
    #ref_prep[p_hori_imp]   0    10.001   304    10.00    0    -70.000
    #ref_home[p_hori_imp]   0     0.000   304     0.00    0      0.000
    #ref_mpr1[p_hori_imp]   0    10.001   304    10.00    0    -70.000
    #ref_mpr2[p_hori_imp]   0    30.419   304    49.18    0   -169.600
    #ref_mpr3[p_hori_imp]   0    30.419   304    49.18    0   -169.600
.END
.PROGRAM p_hori_imp(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
.*  ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
.*  ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
.*  ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vnp.moffset
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
.*  ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 00,vnp.moffset
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
.*  ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-400
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vnp.mxin
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_prep[p_hori_imp],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_hori_imp],3,trn[.tsk,3])
.END
..MONCMD    area[p_hori_imp,1,1] = -750-100	;** X_MIN
..MONCMD    area[p_hori_imp,1,2] = -750		;** X_BAS
..MONCMD    area[p_hori_imp,1,3] = -750+100	;** X_MAX

..MONCMD    area[p_hori_imp,2,1] = -300-100	;** Y_MIN
..MONCMD    area[p_hori_imp,2,2] = -300		;** Y_BAS
..MONCMD    area[p_hori_imp,2,3] = -300+100	;** Y_MAX

..MONCMD    area[p_hori_imp,3,1] =  200		;** Z_MIN
..MONCMD    area[p_hori_imp,3,2] =  350		;** Z_BAS
..MONCMD    area[p_hori_imp,3,3] =  500		;** Z_MAX

..MONCMD    area[p_hori_imp,4,1] =  90-30	;** R1_MIN
..MONCMD    area[p_hori_imp,4,2] =  90		;** R1_BAS
..MONCMD    area[p_hori_imp,4,3] =  90+30	;** R1_MAX

..MONCMD    area[p_hori_imp,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_imp,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_imp,5,3] =   0		;** R2_MAX
.************************
.** Teach Position IMAP **
.************************
.PROGRAM t_hori_imp(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_imp
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]> 180,trn[.tsk,4]-360,trn[.tsk,4])
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<-180,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish 090515 m++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
    ELSE
       trn[.tsk,4] = 90
    END
    ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
    IF .err					GOTO err
.** harish 090515 m--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.** harish 090515-1 a--
.******************************************
.** app_hori_pta.as
.**   SycamoreX NT410 PassThru A
.******************************************
.REALS
    mapp_path[p_hori_pta] = gpos_e_half+0.5
    mapp_move[p_hori_pta] = mov_lmv
    mapp_acce[p_hori_pta] = 30
    mapp_dece[p_hori_pta] = 30

    path_move[p_hori_pta,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_pta,gpos_e_prep] = mov_non
    path_move[p_hori_pta,gpos_e_half] = mov_jmv
    path_move[p_hori_pta,gpos_e_near] = mov_non
    path_move[p_hori_pta,gpos_e_extd] = mov_lmv
    path_move[p_hori_pta,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_pta,gpos_e_tech] = mov_jmv
    path_move[p_hori_pta,gpos_r_tech] = mov_lmv
    path_move[p_hori_pta,gpos_r_slow] = mov_non
    path_move[p_hori_pta,gpos_r_extd] = mov_lmv
    path_move[p_hori_pta,gpos_r_back] = mov_lmv
    path_move[p_hori_pta,gpos_r_near] = mov_non
    path_move[p_hori_pta,gpos_r_half] = mov_lmv
    path_move[p_hori_pta,gpos_r_prep] = mov_non
    path_move[p_hori_pta,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_pta,gpos_e_home] = 100
    path_sped[p_hori_pta,gpos_e_prep] = 100
    path_sped[p_hori_pta,gpos_e_half] = 100
    path_sped[p_hori_pta,gpos_e_near] = 100
    path_sped[p_hori_pta,gpos_e_extd] = 100
    path_sped[p_hori_pta,gpos_e_slow] = 100
    path_sped[p_hori_pta,gpos_e_tech] =  -1
    path_sped[p_hori_pta,gpos_r_tech] =  -1
    path_sped[p_hori_pta,gpos_r_slow] =  -1
    path_sped[p_hori_pta,gpos_r_extd] =  -1
    path_sped[p_hori_pta,gpos_r_back] = 100
    path_sped[p_hori_pta,gpos_r_near] = 100
    path_sped[p_hori_pta,gpos_r_half] = 100
    path_sped[p_hori_pta,gpos_r_prep] = 100
    path_sped[p_hori_pta,gpos_r_home] = 100

    path_acce[p_hori_pta,gpos_e_home] = 80
    path_acce[p_hori_pta,gpos_e_prep] = 80
    path_acce[p_hori_pta,gpos_e_half] = 70
    path_acce[p_hori_pta,gpos_e_near] = 60
    path_acce[p_hori_pta,gpos_e_extd] = 60
    path_acce[p_hori_pta,gpos_e_slow] = 100
    path_acce[p_hori_pta,gpos_e_tech] = 100
    path_acce[p_hori_pta,gpos_r_tech] = 30
    path_acce[p_hori_pta,gpos_r_slow] = 100
    path_acce[p_hori_pta,gpos_r_extd] = 60
    path_acce[p_hori_pta,gpos_r_back] = 30
    path_acce[p_hori_pta,gpos_r_near] = 60
    path_acce[p_hori_pta,gpos_r_half] = 70
    path_acce[p_hori_pta,gpos_r_prep] = 80
    path_acce[p_hori_pta,gpos_r_home] = 80

    path_dece[p_hori_pta,gpos_e_home] = 80
    path_dece[p_hori_pta,gpos_e_prep] = 80
    path_dece[p_hori_pta,gpos_e_half] = 70
    path_dece[p_hori_pta,gpos_e_near] = 60
    path_dece[p_hori_pta,gpos_e_extd] = 60
    path_dece[p_hori_pta,gpos_e_slow] = 100
    path_dece[p_hori_pta,gpos_e_tech] = 100
    path_dece[p_hori_pta,gpos_r_tech] = 30
    path_dece[p_hori_pta,gpos_r_slow] = 100
    path_dece[p_hori_pta,gpos_r_extd] = 60
    path_dece[p_hori_pta,gpos_r_back] = 30
    path_dece[p_hori_pta,gpos_r_near] = 60
    path_dece[p_hori_pta,gpos_r_half] = 70
    path_dece[p_hori_pta,gpos_r_prep] = 80
    path_dece[p_hori_pta,gpos_r_home] = 80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_pta,gpos_e_home] =  10
    path_acur[p_hori_pta,gpos_e_prep] = 999
    path_acur[p_hori_pta,gpos_e_half] = 999
    path_acur[p_hori_pta,gpos_e_near] = 999
    path_acur[p_hori_pta,gpos_e_extd] =   3
    path_acur[p_hori_pta,gpos_e_slow] =   1
    path_acur[p_hori_pta,gpos_e_tech] =   1
    path_acur[p_hori_pta,gpos_r_tech] =   1
    path_acur[p_hori_pta,gpos_r_slow] =   1
    path_acur[p_hori_pta,gpos_r_extd] =   3
    path_acur[p_hori_pta,gpos_r_back] =   1
    path_acur[p_hori_pta,gpos_r_near] = 999
    path_acur[p_hori_pta,gpos_r_half] = 999
    path_acur[p_hori_pta,gpos_r_prep] = 999
    path_acur[p_hori_pta,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_pta]   0   -91.975   304   119.532   0   -152.557
    #ref_home[p_hori_pta]   0     0.000   304     0.000   0      0.000
.*  #ref_mpr1[p_hori_pta]   0   -32.654   304    21.425   0   -113.767
.*  #ref_mpr2[p_hori_pta]   0   -32.654   304    21.425   0   -113.767
.** harish 080917 mprepos closer to stations a++
    #ref_mpr1[p_hori_pta]   0   -53.36    304    48.49    0   -120.12
.*  #ref_mpr2[p_hori_pta]   0   -53.36    304    48.49    0   -120.12
.** harish new prepos from Dave 081010 a++
    #ref_mpr2[p_hori_pta]   0   -64.550   304    62.24    0   -120.793
    #ref_mpr3[p_hori_pta]   0   -64.550   304    62.24    0   -120.793
.** harish 080917 mprepos closer to stations a--
.END
.PROGRAM p_hori_pta(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-400
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_hori_pta],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END
..MONCMD    area[p_hori_pta,1,1] = -930-100	;** X_MIN
..MONCMD    area[p_hori_pta,1,2] = -930		;** X_BAS
..MONCMD    area[p_hori_pta,1,3] = -930+100	;** X_MAX

..MONCMD    area[p_hori_pta,2,1] =  606-100	;** Y_MIN
..MONCMD    area[p_hori_pta,2,2] =  606		;** Y_BAS
..MONCMD    area[p_hori_pta,2,3] =  606+100	;** Y_MAX

..MONCMD    area[p_hori_pta,3,1] =  100		;** Z_MIN
..MONCMD    area[p_hori_pta,3,2] =  250		;** Z_BAS
..MONCMD    area[p_hori_pta,3,3] =  400		;** Z_MAX

..MONCMD    area[p_hori_pta,4,1] =  55-10	;** R1_MIN
..MONCMD    area[p_hori_pta,4,2] =  55		;** R1_BAS
..MONCMD    area[p_hori_pta,4,3] =  55+10	;** R1_MAX

..MONCMD    area[p_hori_pta,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_pta,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_pta,5,3] =   0		;** R2_MAX
.************************
.** Teach Position PTA **
.************************
.PROGRAM t_hori_pta(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_pta
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]> 180,trn[.tsk,4]-360,trn[.tsk,4])
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<-180,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_ptb.as
.**   SycamoreX NT410 PassThru-B
.******************************************
.REALS
    mapp_path[p_hori_ptb] = gpos_e_half+0.5
    mapp_move[p_hori_ptb] = mov_lmv
    mapp_acce[p_hori_ptb] = 30
    mapp_dece[p_hori_ptb] = 30

    path_move[p_hori_ptb,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_ptb,gpos_e_prep] = mov_non
    path_move[p_hori_ptb,gpos_e_half] = mov_jmv
    path_move[p_hori_ptb,gpos_e_near] = mov_non
    path_move[p_hori_ptb,gpos_e_extd] = mov_lmv
    path_move[p_hori_ptb,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_ptb,gpos_e_tech] = mov_jmv
    path_move[p_hori_ptb,gpos_r_tech] = mov_lmv
    path_move[p_hori_ptb,gpos_r_slow] = mov_non
    path_move[p_hori_ptb,gpos_r_extd] = mov_lmv
    path_move[p_hori_ptb,gpos_r_back] = mov_lmv
    path_move[p_hori_ptb,gpos_r_near] = mov_non
    path_move[p_hori_ptb,gpos_r_half] = mov_lmv
    path_move[p_hori_ptb,gpos_r_prep] = mov_non
    path_move[p_hori_ptb,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_ptb,gpos_e_home] = 100
    path_sped[p_hori_ptb,gpos_e_prep] = 100
    path_sped[p_hori_ptb,gpos_e_half] = 100
    path_sped[p_hori_ptb,gpos_e_near] = 100
    path_sped[p_hori_ptb,gpos_e_extd] = 100
    path_sped[p_hori_ptb,gpos_e_slow] = 100
    path_sped[p_hori_ptb,gpos_e_tech] =  -1
    path_sped[p_hori_ptb,gpos_r_tech] =  -1
    path_sped[p_hori_ptb,gpos_r_slow] =  -1
    path_sped[p_hori_ptb,gpos_r_extd] =  -1
    path_sped[p_hori_ptb,gpos_r_back] = 100
    path_sped[p_hori_ptb,gpos_r_near] = 100
    path_sped[p_hori_ptb,gpos_r_half] = 100
    path_sped[p_hori_ptb,gpos_r_prep] = 100
    path_sped[p_hori_ptb,gpos_r_home] = 100

    path_acce[p_hori_ptb,gpos_e_home] = 80
    path_acce[p_hori_ptb,gpos_e_prep] = 80
    path_acce[p_hori_ptb,gpos_e_half] = 70
    path_acce[p_hori_ptb,gpos_e_near] = 60
    path_acce[p_hori_ptb,gpos_e_extd] = 60
    path_acce[p_hori_ptb,gpos_e_slow] = 100
    path_acce[p_hori_ptb,gpos_e_tech] = 100
    path_acce[p_hori_ptb,gpos_r_tech] = 30
    path_acce[p_hori_ptb,gpos_r_slow] = 100
    path_acce[p_hori_ptb,gpos_r_extd] = 60
    path_acce[p_hori_ptb,gpos_r_back] = 30
    path_acce[p_hori_ptb,gpos_r_near] = 60
    path_acce[p_hori_ptb,gpos_r_half] = 70
    path_acce[p_hori_ptb,gpos_r_prep] = 80
    path_acce[p_hori_ptb,gpos_r_home] = 80

    path_dece[p_hori_ptb,gpos_e_home] = 80
    path_dece[p_hori_ptb,gpos_e_prep] = 80
    path_dece[p_hori_ptb,gpos_e_half] = 70
    path_dece[p_hori_ptb,gpos_e_near] = 60
    path_dece[p_hori_ptb,gpos_e_extd] = 60
    path_dece[p_hori_ptb,gpos_e_slow] = 100
    path_dece[p_hori_ptb,gpos_e_tech] = 100
    path_dece[p_hori_ptb,gpos_r_tech] = 30
    path_dece[p_hori_ptb,gpos_r_slow] = 100
    path_dece[p_hori_ptb,gpos_r_extd] = 60
    path_dece[p_hori_ptb,gpos_r_back] = 30
    path_dece[p_hori_ptb,gpos_r_near] = 60
    path_dece[p_hori_ptb,gpos_r_half] = 70
    path_dece[p_hori_ptb,gpos_r_prep] = 80
    path_dece[p_hori_ptb,gpos_r_home] = 80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_ptb,gpos_e_home] =  10
    path_acur[p_hori_ptb,gpos_e_prep] = 999
    path_acur[p_hori_ptb,gpos_e_half] = 999
    path_acur[p_hori_ptb,gpos_e_near] = 999
    path_acur[p_hori_ptb,gpos_e_extd] =   3
    path_acur[p_hori_ptb,gpos_e_slow] =   1
    path_acur[p_hori_ptb,gpos_e_tech] =   1
    path_acur[p_hori_ptb,gpos_r_tech] =   1
    path_acur[p_hori_ptb,gpos_r_slow] =   1
    path_acur[p_hori_ptb,gpos_r_extd] =   3
    path_acur[p_hori_ptb,gpos_r_back] =   1
    path_acur[p_hori_ptb,gpos_r_near] = 999
    path_acur[p_hori_ptb,gpos_r_half] = 999
    path_acur[p_hori_ptb,gpos_r_prep] = 999
    path_acur[p_hori_ptb,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_ptb]   0   -72.551   304    81.051   0    141.460
    #ref_home[p_hori_ptb]   0   -20.389   304   113.691   0     59.693
.* harish 080613 m++
.*  #ref_mpr1[p_hori_ptb]   0   -49.480   304   127.708   0     74.820
.*  #ref_mpr2[p_hori_ptb]   0   -49.480   304   127.708   0     74.820
.** harish 080828 homing a++
    #ref_mpr1[p_hori_ptb]   0   -47.61    304   102.50    0     95.12   
.*  #ref_mpr2[p_hori_ptb]   0   -47.61    304   102.50    0     95.12
.** harish new prepos from Dave 081010 a++
    #ref_mpr2[p_hori_ptb]   0   -47.64    304    88.96    0    110.08
    #ref_mpr3[p_hori_ptb]   0   -47.64    304    88.96    0    110.08
.* harish 080613 m--
.END
.PROGRAM p_hori_ptb(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
.** harish 081023 m+
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = -80,-400
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_hori_ptb],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END
..MONCMD    area[p_hori_ptb,1,1] = -310-100	;** X_MIN
..MONCMD    area[p_hori_ptb,1,2] = -310		;** X_BAS
..MONCMD    area[p_hori_ptb,1,3] = -310+100	;** X_MAX

..MONCMD    area[p_hori_ptb,2,1] =  606-100	;** Y_MIN
..MONCMD    area[p_hori_ptb,2,2] =  606		;** Y_BAS
..MONCMD    area[p_hori_ptb,2,3] =  606+100	;** Y_MAX

..MONCMD    area[p_hori_ptb,3,1] =  100		;** Z_MIN
..MONCMD    area[p_hori_ptb,3,2] =  250		;** Z_BAS
..MONCMD    area[p_hori_ptb,3,3] =  400		;** Z_MAX

..MONCMD    area[p_hori_ptb,4,1] = -30-10	;** R1_MIN
..MONCMD    area[p_hori_ptb,4,2] = -30		;** R1_BAS
..MONCMD    area[p_hori_ptb,4,3] = -30+10	;** R1_MAX

..MONCMD    area[p_hori_ptb,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_ptb,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_ptb,5,3] =   0		;** R2_MAX
.*******************************
.** Teach Position PassThru-B **
.*******************************
.PROGRAM t_hori_ptb(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_ptb
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]> 180,trn[.tsk,4]-360,trn[.tsk,4])
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<-180,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_vda.as
.**   SycamoreX NT410 VDA
.******************************************
.REALS
.** harish 090526 m
.*  mapp_path[p_hori_vda] = gpos_e_prep+0.5
    mapp_path[p_hori_vda] = gpos_e_half+0.5
    mapp_move[p_hori_vda] = mov_lmv
    mapp_acce[p_hori_vda] = 30
    mapp_dece[p_hori_vda] = 30

    path_move[p_hori_vda,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_vda,gpos_e_prep] = mov_jmv
    path_move[p_hori_vda,gpos_e_half] = mov_jmv
.** harish 081106 m+
.*  path_move[p_hori_vda,gpos_e_near] = mov_non
    path_move[p_hori_vda,gpos_e_near] = mov_jmv
    path_move[p_hori_vda,gpos_e_extd] = mov_lmv
    path_move[p_hori_vda,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_vda,gpos_e_tech] = mov_jmv
    path_move[p_hori_vda,gpos_r_tech] = mov_lmv
    path_move[p_hori_vda,gpos_r_slow] = mov_non
    path_move[p_hori_vda,gpos_r_extd] = mov_lmv
    path_move[p_hori_vda,gpos_r_back] = mov_lmv
.** harish 081106 m+
.*  path_move[p_hori_vda,gpos_r_near] = mov_non
    path_move[p_hori_vda,gpos_r_near] = mov_lmv
    path_move[p_hori_vda,gpos_r_half] = mov_jmv
    path_move[p_hori_vda,gpos_r_prep] = mov_jmv
    path_move[p_hori_vda,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_vda,gpos_e_home] = 100
    path_sped[p_hori_vda,gpos_e_prep] = 100
    path_sped[p_hori_vda,gpos_e_half] = 100
    path_sped[p_hori_vda,gpos_e_near] = 100
    path_sped[p_hori_vda,gpos_e_extd] = 100
    path_sped[p_hori_vda,gpos_e_slow] = 100
    path_sped[p_hori_vda,gpos_e_tech] =  -1
    path_sped[p_hori_vda,gpos_r_tech] =  -1
    path_sped[p_hori_vda,gpos_r_slow] =  -1
    path_sped[p_hori_vda,gpos_r_extd] =  -1
    path_sped[p_hori_vda,gpos_r_back] = 100
    path_sped[p_hori_vda,gpos_r_near] = 100
    path_sped[p_hori_vda,gpos_r_half] = 100
    path_sped[p_hori_vda,gpos_r_prep] = 100
    path_sped[p_hori_vda,gpos_r_home] = 100

    path_acce[p_hori_vda,gpos_e_home] = 80
    path_acce[p_hori_vda,gpos_e_prep] = 80
    path_acce[p_hori_vda,gpos_e_half] = 60
    path_acce[p_hori_vda,gpos_e_near] = 60
    path_acce[p_hori_vda,gpos_e_extd] = 60
    path_acce[p_hori_vda,gpos_e_slow] = 100
    path_acce[p_hori_vda,gpos_e_tech] = 100
    path_acce[p_hori_vda,gpos_r_tech] = 30
    path_acce[p_hori_vda,gpos_r_slow] = 100
    path_acce[p_hori_vda,gpos_r_extd] = 60
    path_acce[p_hori_vda,gpos_r_back] = 30
    path_acce[p_hori_vda,gpos_r_near] = 60
    path_acce[p_hori_vda,gpos_r_half] = 60
    path_acce[p_hori_vda,gpos_r_prep] = 80
    path_acce[p_hori_vda,gpos_r_home] = 80

    path_dece[p_hori_vda,gpos_e_home] = 80
    path_dece[p_hori_vda,gpos_e_prep] = 80
    path_dece[p_hori_vda,gpos_e_half] = 60
    path_dece[p_hori_vda,gpos_e_near] = 60
    path_dece[p_hori_vda,gpos_e_extd] = 60
    path_dece[p_hori_vda,gpos_e_slow] = 100
    path_dece[p_hori_vda,gpos_e_tech] = 100
    path_dece[p_hori_vda,gpos_r_tech] = 30
    path_dece[p_hori_vda,gpos_r_slow] = 100
    path_dece[p_hori_vda,gpos_r_extd] = 60
    path_dece[p_hori_vda,gpos_r_back] = 30
    path_dece[p_hori_vda,gpos_r_near] = 60
    path_dece[p_hori_vda,gpos_r_half] = 60
    path_dece[p_hori_vda,gpos_r_prep] = 80
    path_dece[p_hori_vda,gpos_r_home] = 80
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_vda,gpos_e_home] =  10
    path_acur[p_hori_vda,gpos_e_prep] =  99
.** harish 081106 m+
.*  path_acur[p_hori_vda,gpos_e_half] =  10
    path_acur[p_hori_vda,gpos_e_half] = 999
    path_acur[p_hori_vda,gpos_e_near] = 999
    path_acur[p_hori_vda,gpos_e_extd] =   3
    path_acur[p_hori_vda,gpos_e_slow] =   1
    path_acur[p_hori_vda,gpos_e_tech] =   1
    path_acur[p_hori_vda,gpos_r_tech] =   1
    path_acur[p_hori_vda,gpos_r_slow] =   1
    path_acur[p_hori_vda,gpos_r_extd] =   3
    path_acur[p_hori_vda,gpos_r_back] =   1
    path_acur[p_hori_vda,gpos_r_near] = 999
.** harish 081106 m+
.*  path_acur[p_hori_vda,gpos_r_half] =  10
    path_acur[p_hori_vda,gpos_r_half] = 999
    path_acur[p_hori_vda,gpos_r_prep] =  99
    path_acur[p_hori_vda,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_vda]   0    54.268   135   -133.359  0    -110.9092
    #ref_prep[p_hori_vda]   0    20.389   135   -113.691  0    -48.693
    #ref_home[p_hori_vda]   0   -10.000   135    -35.000  0    -16.000
.** harish 080724 m++
    #ref_mpr1[p_hori_vda]   0    20.383   135   -113.710  0    -48.693
.*  #ref_mpr2[p_hori_vda]   0    24.364   135   -119.888  0    -104.051
.** harish 080724 m--
.** harish 080917 mprepos closer to stations a++
.*  #ref_mpr2[p_hori_vda]   0    34.18    135   -136.76   0    -96.99
.** harish 080917 mprepos closer to stations a--
.** harish new prepos from Dave 081010 a++
.** harish 081023 m+
    #ref_mpr2[p_hori_vda]   0    18.29    135   -110.04   0    -124.75 
    #ref_mpr3[p_hori_vda]   0    42.796   135   -149.066  0    -85.889
.END
.PROGRAM p_hori_vda(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
.** harish 080613 m+
.** harish inverse kinematics 080930 a+
.** harish 081023 m+
.** harish 081106 m+
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = -55,-300,0,-33
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
.** harish 081106 m++   
.*  POINT #gp_half[.tsk] = #gp_near[.tsk]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = #gp_near[.tsk],inv_hand
    add[.tsk,2] = -146.25
    ZL3JNT rob_tsk[.rob]: #gp_half[.tsk] = add[.tsk,1],#gp_near[.tsk], inv_normal, inv_hand
.** harish 081106 m--
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_prep[p_hori_vda],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_hori_vda],3,trn[.tsk,3])
.END
..MONCMD    area[p_hori_vda,1,1] =  850-100	;** X_MIN
..MONCMD    area[p_hori_vda,1,2] =  850		;** X_BAS
..MONCMD    area[p_hori_vda,1,3] =  850+100	;** X_MAX

..MONCMD    area[p_hori_vda,2,1] =  171-100	;** Y_MIN
..MONCMD    area[p_hori_vda,2,2] =  171		;** Y_BAS
..MONCMD    area[p_hori_vda,2,3] =  171+100	;** Y_MAX

..MONCMD    area[p_hori_vda,3,1] =   50		;** Z_MIN
..MONCMD    area[p_hori_vda,3,2] =  135		;** Z_BAS
..MONCMD    area[p_hori_vda,3,3] =  200		;** Z_MAX

.** harish inverse kinematics 080930 a++
..MONCMD    area[p_hori_vda,4,1] = -10		;** R1_MIN
..MONCMD    area[p_hori_vda,4,2] =  0		;** R1_BAS
..MONCMD    area[p_hori_vda,4,3] =  10		;** R1_MAX
.** harish inverse kinematics 080930 a--

..MONCMD    area[p_hori_vda,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_vda,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_vda,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position VD-A **
.*********************************
.PROGRAM t_hori_vda(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_vda
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]> 180,trn[.tsk,4]-360,trn[.tsk,4])
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<-180,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_vdb.as
.**   SycamoreX NT410 VDB
.******************************************
.REALS
    mapp_path[p_hori_vdb] = gpos_e_prep+0.5
    mapp_move[p_hori_vdb] = mov_lmv
    mapp_acce[p_hori_vdb] = 30
    mapp_dece[p_hori_vdb] = 30

    path_move[p_hori_vdb,gpos_e_home] = mov_jmv	;**N/A
    path_move[p_hori_vdb,gpos_e_prep] = mov_jmv
    path_move[p_hori_vdb,gpos_e_half] = mov_jmv
    path_move[p_hori_vdb,gpos_e_near] = mov_non
    path_move[p_hori_vdb,gpos_e_extd] = mov_lmv
    path_move[p_hori_vdb,gpos_e_slow] = mov_non	;**N/A
    path_move[p_hori_vdb,gpos_e_tech] = mov_jmv
    path_move[p_hori_vdb,gpos_r_tech] = mov_lmv
    path_move[p_hori_vdb,gpos_r_slow] = mov_non
    path_move[p_hori_vdb,gpos_r_extd] = mov_lmv
    path_move[p_hori_vdb,gpos_r_back] = mov_lmv
    path_move[p_hori_vdb,gpos_r_near] = mov_non
    path_move[p_hori_vdb,gpos_r_half] = mov_lmv
    path_move[p_hori_vdb,gpos_r_prep] = mov_jmv
    path_move[p_hori_vdb,gpos_r_home] = mov_jmv

.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
    path_sped[p_hori_vdb,gpos_e_home] = 100
    path_sped[p_hori_vdb,gpos_e_prep] = 100
    path_sped[p_hori_vdb,gpos_e_half] = 100
    path_sped[p_hori_vdb,gpos_e_near] = 100
    path_sped[p_hori_vdb,gpos_e_extd] = 100
    path_sped[p_hori_vdb,gpos_e_slow] = 100
    path_sped[p_hori_vdb,gpos_e_tech] =  -1
    path_sped[p_hori_vdb,gpos_r_tech] =  -1
    path_sped[p_hori_vdb,gpos_r_slow] =  -1
    path_sped[p_hori_vdb,gpos_r_extd] =  -1
    path_sped[p_hori_vdb,gpos_r_back] = 100
    path_sped[p_hori_vdb,gpos_r_near] = 100
    path_sped[p_hori_vdb,gpos_r_half] = 100
    path_sped[p_hori_vdb,gpos_r_prep] = 100
    path_sped[p_hori_vdb,gpos_r_home] = 100

    path_acce[p_hori_vdb,gpos_e_home] = 70
    path_acce[p_hori_vdb,gpos_e_prep] = 50
    path_acce[p_hori_vdb,gpos_e_half] = 50
    path_acce[p_hori_vdb,gpos_e_near] = 50
    path_acce[p_hori_vdb,gpos_e_extd] = 50
    path_acce[p_hori_vdb,gpos_e_slow] = 100
    path_acce[p_hori_vdb,gpos_e_tech] = 100
    path_acce[p_hori_vdb,gpos_r_tech] = 30
    path_acce[p_hori_vdb,gpos_r_slow] = 100
    path_acce[p_hori_vdb,gpos_r_extd] = 50
    path_acce[p_hori_vdb,gpos_r_back] = 30
    path_acce[p_hori_vdb,gpos_r_near] = 50
    path_acce[p_hori_vdb,gpos_r_half] = 50
    path_acce[p_hori_vdb,gpos_r_prep] = 50
    path_acce[p_hori_vdb,gpos_r_home] = 70

    path_dece[p_hori_vdb,gpos_e_home] = 70
    path_dece[p_hori_vdb,gpos_e_prep] = 50
    path_dece[p_hori_vdb,gpos_e_half] = 50
    path_dece[p_hori_vdb,gpos_e_near] = 50
    path_dece[p_hori_vdb,gpos_e_extd] = 50
    path_dece[p_hori_vdb,gpos_e_slow] = 100
    path_dece[p_hori_vdb,gpos_e_tech] = 100
    path_dece[p_hori_vdb,gpos_r_tech] = 30
    path_dece[p_hori_vdb,gpos_r_slow] = 100
    path_dece[p_hori_vdb,gpos_r_extd] = 50
    path_dece[p_hori_vdb,gpos_r_back] = 30
    path_dece[p_hori_vdb,gpos_r_near] = 50
    path_dece[p_hori_vdb,gpos_r_half] = 50
    path_dece[p_hori_vdb,gpos_r_prep] = 50
    path_dece[p_hori_vdb,gpos_r_home] = 70
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.

    path_acur[p_hori_vdb,gpos_e_home] =  10
    path_acur[p_hori_vdb,gpos_e_prep] = 999
    path_acur[p_hori_vdb,gpos_e_half] = 999
    path_acur[p_hori_vdb,gpos_e_near] = 999
    path_acur[p_hori_vdb,gpos_e_extd] =   3
    path_acur[p_hori_vdb,gpos_e_slow] =   1
    path_acur[p_hori_vdb,gpos_e_tech] =   1
    path_acur[p_hori_vdb,gpos_r_tech] =   1
    path_acur[p_hori_vdb,gpos_r_slow] =   1
    path_acur[p_hori_vdb,gpos_r_extd] =   3
    path_acur[p_hori_vdb,gpos_r_back] =   1
    path_acur[p_hori_vdb,gpos_r_near] = 999
    path_acur[p_hori_vdb,gpos_r_half] = 999
    path_acur[p_hori_vdb,gpos_r_prep] = 999
    path_acur[p_hori_vdb,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_vdb]   0   -8.214    135    -45.273   0    -116.513
    #ref_prep[p_hori_vdb]   0    20.389   135    -113.691  0    -48.693
    #ref_home[p_hori_vdb]   0   -10.000   135    -35.000   0    -16.000
.*  #ref_mpr1[p_hori_vdb]   0    20.383   135    -113.710  0    -48.693
.*  #ref_mpr2[p_hori_vdb]   0    14.79    135    -103.89   0    -61.35
.** harish 080917 mprepos closer to stations a++
.** harish new prepos from Dave 081010 a++
    #ref_mpr1[p_hori_vdb]   0   -65.00    135    -59.997   0    -15.99
.** harish 081110 m++
.*  #ref_mpr2[p_hori_vdb]   0   -59.999   135    -61.002   0    -82.00
.*  #ref_mpr3[p_hori_vdb]   0   -59.999   135    -61.002   0    -82.00
.** harish 081111 m++
.*  #ref_mpr2[p_hori_vdb]   0   -57.061   135    -62.218   0    -83.72
.*  #ref_mpr3[p_hori_vdb]   0   -57.061   135    -62.218   0    -83.72
    #ref_mpr2[p_hori_vdb]   0   -54.53    135    -62.12    0    -74.62
    #ref_mpr3[p_hori_vdb]   0   -45.08    135    -63.75    0    -82.44
.** harish 081111 m++
.** harish 081110 m--
.** harish 080917 mprepos closer to stations a--
.END
.PROGRAM p_hori_vdb(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
.** harish 080708 m+
.** harish inverse kinematics 080930 a+
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 65,-280,0,40
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_half
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
    POINT #gp_prep[.tsk] = #PPJSET(#ref_prep[p_hori_vdb],3,trn[.tsk,3])
;** #gp_home
    POINT #gp_home[.tsk] = #PPJSET(#ref_home[p_hori_vdb],3,trn[.tsk,3])
.END
..MONCMD    area[p_hori_vdb,1,1] =  230-100	;** X_MIN
..MONCMD    area[p_hori_vdb,1,2] =  230		;** X_BAS
..MONCMD    area[p_hori_vdb,1,3] =  230+100	;** X_MAX

..MONCMD    area[p_hori_vdb,2,1] =  171-100	;** Y_MIN
..MONCMD    area[p_hori_vdb,2,2] =  171		;** Y_BAS
..MONCMD    area[p_hori_vdb,2,3] =  171+100	;** Y_MAX

..MONCMD    area[p_hori_vdb,3,1] =   50		;** Z_MIN
..MONCMD    area[p_hori_vdb,3,2] =  135		;** Z_BAS
..MONCMD    area[p_hori_vdb,3,3] =  200		;** Z_MAX

.** harish inverse kinematics 080930 a++
..MONCMD    area[p_hori_vdb,4,1] =  -10		;** R1_MIN
..MONCMD    area[p_hori_vdb,4,2] =   0		;** R1_BAS
..MONCMD    area[p_hori_vdb,4,3] =   10		;** R1_MAX
.** harish inverse kinematics 080930 a--

..MONCMD    area[p_hori_vdb,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_vdb,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_vdb,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position VD-B **
.*********************************
.PROGRAM t_hori_vdb(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_vdb
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]> 180,trn[.tsk,4]-360,trn[.tsk,4])
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<-180,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
.** harish inverse kinematics 080930 a++
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
.** harish inverse kinematics 080930 a--
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** app_hori_ws1.as
.**   SycamoreX NT410 ws1
.******************************************
.*                                                      /* SCRX12-006-2 a++
.REALS
    mapp_path[p_hori_ws1] = gpos_e_half+0.5
    mapp_move[p_hori_ws1] = mov_lmv
    mapp_acce[p_hori_ws1] = 30
    mapp_dece[p_hori_ws1] = 30

    path_move[p_hori_ws1,gpos_e_home] = mov_jmv
    path_move[p_hori_ws1,gpos_e_prep] = mov_non
    path_move[p_hori_ws1,gpos_e_half] = mov_jmv
    path_move[p_hori_ws1,gpos_e_near] = mov_jmv
    path_move[p_hori_ws1,gpos_e_extd] = mov_lmv
    path_move[p_hori_ws1,gpos_e_slow] = mov_non
    path_move[p_hori_ws1,gpos_e_tech] = mov_jmv
    path_move[p_hori_ws1,gpos_r_tech] = mov_lmv
    path_move[p_hori_ws1,gpos_r_slow] = mov_non
    path_move[p_hori_ws1,gpos_r_extd] = mov_lmv
    path_move[p_hori_ws1,gpos_r_back] = mov_lmv
    path_move[p_hori_ws1,gpos_r_near] = mov_non
    path_move[p_hori_ws1,gpos_r_half] = mov_lmv
    path_move[p_hori_ws1,gpos_r_prep] = mov_non
    path_move[p_hori_ws1,gpos_r_home] = mov_jmv

    path_sped[p_hori_ws1,gpos_e_home] = 100
    path_sped[p_hori_ws1,gpos_e_prep] = 100
    path_sped[p_hori_ws1,gpos_e_half] = 100
    path_sped[p_hori_ws1,gpos_e_near] = 100
    path_sped[p_hori_ws1,gpos_e_extd] = 80
    path_sped[p_hori_ws1,gpos_e_slow] = 100
    path_sped[p_hori_ws1,gpos_e_tech] = -1
    path_sped[p_hori_ws1,gpos_r_tech] = -1
    path_sped[p_hori_ws1,gpos_r_slow] = -1
    path_sped[p_hori_ws1,gpos_r_extd] = -1
    path_sped[p_hori_ws1,gpos_r_back] = 100
    path_sped[p_hori_ws1,gpos_r_near] = 100
    path_sped[p_hori_ws1,gpos_r_half] = 80
    path_sped[p_hori_ws1,gpos_r_prep] = 100
    path_sped[p_hori_ws1,gpos_r_home] = 80
    
    path_acce[p_hori_ws1,gpos_e_home] = 100
    path_acce[p_hori_ws1,gpos_e_prep] = 100
    path_acce[p_hori_ws1,gpos_e_half] = 45
    path_acce[p_hori_ws1,gpos_e_near] = 45
    path_acce[p_hori_ws1,gpos_e_extd] = 30
    path_acce[p_hori_ws1,gpos_e_slow] = 100
    path_acce[p_hori_ws1,gpos_e_tech] = 100
    path_acce[p_hori_ws1,gpos_r_tech] = 100
    path_acce[p_hori_ws1,gpos_r_slow] = 100
    path_acce[p_hori_ws1,gpos_r_extd] = 100
    path_acce[p_hori_ws1,gpos_r_back] = 100
    path_acce[p_hori_ws1,gpos_r_near] = 100
    path_acce[p_hori_ws1,gpos_r_half] = 30
    path_acce[p_hori_ws1,gpos_r_prep] = 100
    path_acce[p_hori_ws1,gpos_r_home] = 45

    path_dece[p_hori_ws1,gpos_e_home] = 100
    path_dece[p_hori_ws1,gpos_e_prep] = 100
    path_dece[p_hori_ws1,gpos_e_half] = 45
    path_dece[p_hori_ws1,gpos_e_near] = 45
    path_dece[p_hori_ws1,gpos_e_extd] = 30
    path_dece[p_hori_ws1,gpos_e_slow] = 100
    path_dece[p_hori_ws1,gpos_e_tech] = 100
    path_dece[p_hori_ws1,gpos_r_tech] = 100
    path_dece[p_hori_ws1,gpos_r_slow] = 100
    path_dece[p_hori_ws1,gpos_r_extd] = 100
    path_dece[p_hori_ws1,gpos_r_back] = 100
    path_dece[p_hori_ws1,gpos_r_near] = 100
    path_dece[p_hori_ws1,gpos_r_half] = 30
    path_dece[p_hori_ws1,gpos_r_prep] = 100
    path_dece[p_hori_ws1,gpos_r_home] = 45

    path_acur[p_hori_ws1,gpos_e_home] = 999
    path_acur[p_hori_ws1,gpos_e_prep] = 999
    path_acur[p_hori_ws1,gpos_e_half] = 30
    path_acur[p_hori_ws1,gpos_e_near] = 30
    path_acur[p_hori_ws1,gpos_e_extd] = 3
    path_acur[p_hori_ws1,gpos_e_slow] = 1
    path_acur[p_hori_ws1,gpos_e_tech] = 1
    path_acur[p_hori_ws1,gpos_r_tech] = 1
    path_acur[p_hori_ws1,gpos_r_slow] = 1
    path_acur[p_hori_ws1,gpos_r_extd] = 3
    path_acur[p_hori_ws1,gpos_r_back] = 1
    path_acur[p_hori_ws1,gpos_r_near] = 999
    path_acur[p_hori_ws1,gpos_r_half] = 50
    path_acur[p_hori_ws1,gpos_r_prep] = 999
    path_acur[p_hori_ws1,gpos_r_home] = 999
.END
.JOINTS
    #ref_tech[p_hori_ws1]   0 -13.5791 0 88.07503 0 -164.50798
    #ref_home[p_hori_ws1]   0 8 0 15 0 -55.5 0 ; 0 0 32.99665 17 0 -45
.*    #ref_home[p_hori_ws1]   0 9.6 32.99665 14.7 0 -54
.*  #ref_home[p_hori_ws1]   0   -17.729   10      29.679  0     -8.948
.*  #ref_mpr1[p_hori_ws1]   0   -49.096   10      98.522  0    -64.428
.*  #ref_mpr2[p_hori_ws1]   0   -49.096   10      98.522  0    -64.428
    #ref_mpr1[p_hori_ws1]   0 49.18945 317.70953 35.82146 0 -165.01321 0
.*  #ref_mpr2[p_hori_ws1]   0   -60.15    10     118.79   0    -58.64
.*  #ref_mpr2[p_hori_ws1]   0   -51.543   10     123.44   0    -71.903
.*  #ref_mpr3[p_hori_ws1]   0   -51.543   10     123.44   0    -71.903
    #ref_mpr2[p_hori_ws1]   0 49.18945 317.70953 35.82146 0 -165.01321 0
    #ref_mpr3[p_hori_ws1]   0 55.34007 317.88031 23.84593 0 -159.17227 0
    #ref_map1[p_hori_ws1]   0 20.6823 399.87973 39.89028 0 -140.56337 0
    #ref_half[p_hori_ws1]   0 49.18644 317.63922 35.81974 0 -165.01158 0
.END
.PROGRAM p_hori_ws1(.tsk,.rob,.hnd,.prt,.slt,.tch)
;** #gp_tech
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
	POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
;** #gp_goff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.gforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_goff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_back
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.backwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_back[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_putt
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_putt[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_poff
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.putback[.prt]+vpp.pforwd[.prt]
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
    ZL3JNT rob_tsk[.rob]: #gp_poff[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
;** #gp_pfng
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk]
;** #gp_near
;** #gp_half
    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = #gp_tech[.tsk],inv_hand
    add[.tsk,1] = -450
    add[.tsk,4] = 100
    ;**m  165
    ZL3JNT rob_tsk[.rob]: #gp_near[.tsk] = add[.tsk,1],#gp_tech[.tsk], inv_normal, inv_hand
    POINT #gp_half[.tsk] = #gp_near[.tsk]
;** #gp_prep
;** #gp_home
    POINT #gp_prep[.tsk] = #PPJSET(#ref_home[p_hori_ws1],3,trn[.tsk,3])
    POINT #gp_home[.tsk] = #gp_prep[.tsk]
.END
..MONCMD    area[p_hori_ws1,1,1] = -877-50	;** X_MIN
..MONCMD    area[p_hori_ws1,1,2] = -877		;** X_BAS
..MONCMD    area[p_hori_ws1,1,3] = -877+25	;** X_MAX

..MONCMD    area[p_hori_ws1,2,1] = -310-40	;** Y_MIN
..MONCMD    area[p_hori_ws1,2,2] = -310		;** Y_BAS
..MONCMD    area[p_hori_ws1,2,3] = -310+100	;** Y_MAX

..MONCMD    area[p_hori_ws1,3,1] =    5		;** Z_MIN
..MONCMD    area[p_hori_ws1,3,2] =   10		;** Z_BAS
..MONCMD    area[p_hori_ws1,3,3] =   400	;** Z_MAX

..MONCMD    area[p_hori_ws1,4,1] = 90-10	;** R1_MIN
..MONCMD    area[p_hori_ws1,4,2] = 90		;** R1_BAS
..MONCMD    area[p_hori_ws1,4,3] = 90+10	;** R1_MAX

..MONCMD    area[p_hori_ws1,5,1] =   0		;** R2_MIN
..MONCMD    area[p_hori_ws1,5,2] =   0		;** R2_BAS
..MONCMD    area[p_hori_ws1,5,3] =   0		;** R2_MAX
.*********************************
.** Teach Position ws1 **
.*********************************
.PROGRAM t_hori_ws1(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    .pas = p_hori_ws1
    IF .trn THEN
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    ELSE
	ZL3TRN rob_tsk[.rob] : trn[.tsk,1] = #jnt[.tsk], inv_hand
    END
    trn[.tsk,4] = IFELSE(trn[.tsk,4]<0,trn[.tsk,4]+360,trn[.tsk,4])
    FOR .ele = 1 TO 5 STEP 1
	IF trn[.tsk,.ele] < area[.pas,.ele,1]	GOTO err
	IF trn[.tsk,.ele] > area[.pas,.ele,3]	GOTO err
    END
    IF vpp.portang[.prt] <> -1000
	trn[.tsk,4] = vpp.portang[.prt]
	ZL3JNT/ERR .err rob_tsk[.rob]: #jnt[.tsk] = trn[.tsk,1],#ref_tech[.pas],inv_normal,inv_hand
	IF .err					GOTO err
    END
    tchpos[.tsk] = .pas
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.*                                                      /* SCRX12-006-2 a--
.******************************************
.** arm_nt410.as
.******************************************
.PROGRAM arm_nt410(.rob)
;** Axis Information
    $id_axis[.rob]   = ",R,Z,L1,,B1"
     id_anum[.rob,1] = 2	;** R 
     id_anum[.rob,2] = 4	;** L1
     id_anum[.rob,3] = 6	;** B1
     id_anum[.rob,4] = 3	;** Z
     id_anum[.rob,0] = 4
;** Speed Data
    spd_table[.rob,spd_fast]   = 100	;** FAST
    spd_table[.rob,spd_mfast]  =  70	;** MEDIUM-FAST
.** harish 090629-2 m
    spd_table[.rob,spd_medium] =  30	;** MEDIUM
    spd_table[.rob,spd_mslow]  =  30	;** MEDIUM-SLOW
    spd_table[.rob,spd_slow]   =  10	;** SLOW
    spd_table[.rob,spd_escape] =   5	;** ESCAPE ;** 10 .** harish 090408-1 m
    zspd_rate[.rob] = 0.2
;** Limit Data
    POINT #lim_user_l[.rob] = #PPOINT(0,-170,   0, -170, 0, -170 )
    POINT #lim_user_u[.rob] = #PPOINT(0, 170, 400,  170, 0,  170 )
    POINT #movlim[.rob]     = #PPOINT(0,   5,  10,    5, 0,    5 )
;** Coldet
.*                                                      /* SCRX11-032-1 m++ */
    COLR  rob_tsk[.rob]: 0,140,72,117,0,96
    COLRJ rob_tsk[.rob]: 0,40,66,39,0,99
.*                                                      /* SCRX11-032-1 m-- */
;**
    finger_rob[.rob,1] = bit[1]
    grip_stat[.rob,grip_pres,1] = ^B00
    grip_stat[.rob,grip_abst,1] = ^B01
    grip_stat[.rob,grip_rels,1] = ^B10
    grip_stat[.rob,grip_mask,1] = ^B11

.** harish 081120 a++
    grip_stat1[.rob,grip_pres,1] = ^B00
    grip_stat1[.rob,grip_abst,1] = ^B01
    grip_stat1[.rob,grip_rels,1] = ^B10
    grip_stat1[.rob,grip_mask,1] = ^B11
.** harish 081120 a--

;** MADA BKM Blade Data
    def.mapxoff[.rob] = 275.5
    def.mapzoff[.rob] = 2.88	;** Peek-Angle 4 Degree
.** harish 080630 a+
    def.maproffvdb[.rob] = 10      ;** Blade angle for VDB/U3	;**25 harish 090616 m 
    def.maproffvda[.rob] = 10      ;** Blade angle for VDA/U4	
.*                                                      /* SCRX12-006-2 a
    def.maproffws1[.rob] = 10
    maps_pos.z[.rob] = -def.mapzoff[.rob]
    maps_pos.x[.rob] =  def.mapxoff[.rob]
.** harish 080630 a+
    maps_pos.rvdb[.rob] =  def.maproffvdb[.rob]
    maps_pos.rvda[.rob] =  def.maproffvda[.rob]
.*                                                      /* SCRX12-006-2 a
    maps_pos.rws1[.rob] =  def.maproffws1[.rob]


    def.mapdepth[.rob,1] = 6	;** Default
    def.mapdepth[.rob,2] = 2	;** Min
    def.mapdepth[.rob,3] = 8	;** Max
    spd_map_safe[.rob] = 30
    spd_map_nrml[.rob] = 11
    spd_map_slow[.rob] =  4

    ZSETACCEL .rob: 1,0.8,1,1,1,1
    ZSETDECEL .rob: 1,0.8,1,1,1,1

    ZSET3L2KVFF  .rob: 0,0,0,0
    ZSET3L2KAFF  .rob: 0,0,0,0
    ZSET3L2KJFF  .rob: 0,0,0,0

.**harish 080828 homing a++
    can_homec[.rob,x_ele_x,0]  = -100   ;** center x min 
    can_homec[.rob,x_ele_x,1]  =  100   ;** center x max
    can_homec[.rob,x_ele_y,0]  = -360   ;** center y min
    can_homec[.rob,x_ele_y,1]  = -175   ;** center y max
 
    can_homerc[.rob,x_ele_x,0]  =  100   ;** right center x min 
    can_homerc[.rob,x_ele_x,1]  =  400   ;** right center x max
    can_homerc[.rob,x_ele_y,0]  = -360   ;** right center y min
    can_homerc[.rob,x_ele_y,1]  = -175   ;** right center y max
   
    can_homel[.rob,x_ele_x,0]  = -800   ;** left x min
    can_homel[.rob,x_ele_x,1]  = -100   ;** left x max
    can_homel[.rob,x_ele_y,0]  = -360   ;** left y min
    can_homel[.rob,x_ele_y,1]  =  0     ;** left y max
   
    can_homer[.rob,x_ele_x,0]  =  100   ;** right x min
    can_homer[.rob,x_ele_x,1]  =  800   ;** right x min
    can_homer[.rob,x_ele_y,0]  = -360   ;** right y max
    can_homer[.rob,x_ele_y,1]  = -120   ;** right y max

.** harish homing from U4 collision  081007 m++
.** harish 090127 m++
    can_homea1[.rob,6,0]       = -100	;** angle a1 min
    can_homea1[.rob,6,1]       = -0     ;** angle a1 max
    can_homea2[.rob,6,0]       =  0     ;** angle a2 min
    can_homea2[.rob,6,1]       =  60    ;** angle a2 max

    can_homej2[.rob,2,0]       = -20	;** angle j2 min
    can_homej2[.rob,2,1]       =  20    ;** angle j2 max
    can_homej4[.rob,4,0]       = -20    ;** angle j4 min
    can_homej4[.rob,4,1]       =  20    ;** angle j4 max
.** harish 090403-1 a++
    can_homej2r[.rob,2,0]      =  -15	;** angle j2 min
    can_homej2r[.rob,2,1]      =   60   ;** angle j2 max
    can_homej4r[.rob,4,0]      = -145   ;** angle j4 min
    can_homej4r[.rob,4,1]      =    0   ;** angle j4 max

    can_homej2l[.rob,2,0]       = -66	;** angle j2 min
    can_homej2l[.rob,2,1]       =   0   ;** angle j2 max
    can_homej4l[.rob,4,0]       = -61   ;** angle j4 min
    can_homej4l[.rob,4,1]       = 145   ;** angle j4 max
.** harish 090403-1 a--
.** harish 090211 a++
    can_homej6[.rob,6,0]       = -45	;** angle j6 min
    can_homej6[.rob,6,1]       =  45    ;** angle j6 max
.** harish 090211 a--
.** harish 090127 m--
.** harish homing from U4 collision  081007 m--
.**harish 080828 homing a--
.** harish 090309 a
    POINT #j_zero[.rob]     = #PPOINT(0,   0,   0,   0,   0,   0,   0 )
.END
.******************************************
.** arm_nt570.as
.******************************************
.PROGRAM arm_nt570(.rob)
;** Axis Information
    $id_axis[.rob]   = ",R,Z,L1,,B1,S1"
     id_anum[.rob,1] = 2	;** R 
     id_anum[.rob,2] = 4	;** L1
     id_anum[.rob,3] = 6	;** B1
     id_anum[.rob,4] = 7	;** S1
     id_anum[.rob,5] = 3	;** Z
     id_anum[.rob,0] = 5
;** Speed Data
    spd_table[.rob,spd_fast]   = 100	;** FAST
    spd_table[.rob,spd_mfast]  =  70	;** MEDIUM-FAST
.** harish 090629-2 m
    spd_table[.rob,spd_medium] =  30	;** MEDIUM
    spd_table[.rob,spd_mslow]  =  30	;** MEDIUM-SLOW
    spd_table[.rob,spd_slow]   =  10	;** SLOW
    spd_table[.rob,spd_escape] =   5	;** ESCAPE ;** 10 .** harish 090408-1 m
    zspd_rate[.rob] = 0.2
;** Limit Data
    POINT #lim_user_l[.rob] = #PPOINT(0,-200,  0, -170, 0, -290,  -3 )
    POINT #lim_user_u[.rob] = #PPOINT(0, 140, 470,  170, 0,   50, 183 )
    POINT #movlim[.rob]     = #PPOINT(0,   5,  10,    5, 0,    5,   5 )
    flip_ulm[.rob] = 450
;** Coldet
.*                                                      /* SCRX11-032-1 m++ */
    COLR  rob_tsk[.rob]: 0,37,93,30,0,39,39
    COLRJ rob_tsk[.rob]: 0,12,21,12,0,21,21
.*                                                      /* SCRX11-032-1 m-- */
.** harish 081031 JT7 direction a+
    ZAXIS 1: 7 = ,,,,0
.** harish 090223-1 a
    ZAXIS 1:7 = ,,,,,,,,1000
;**
    finger_rob[.rob,1] = bit[1] BOR bit[2]

    grip_stat[.rob,grip_pres,1] = ^B0100	;**F1-Present F2-Absent
    grip_stat[.rob,grip_pres,2] = ^B0001	;**F1-Absent  F2-Present
    grip_stat[.rob,grip_pres,3] = ^B0000
    grip_stat[.rob,grip_abst,1] = ^B0101
    grip_stat[.rob,grip_abst,2] = ^B0101
    grip_stat[.rob,grip_abst,3] = ^B0101
    grip_stat[.rob,grip_rels,1] = ^B1010
    grip_stat[.rob,grip_rels,2] = ^B1010
    grip_stat[.rob,grip_rels,3] = ^B1010
    grip_stat[.rob,grip_mask,1] = ^B0011
    grip_stat[.rob,grip_mask,2] = ^B1100
    grip_stat[.rob,grip_mask,3] = ^B1111

.** harish 081120 a++
    grip_stat1[.rob,grip_pres,1] = ^B0100	;**F1-Present F2-Absent
    grip_stat1[.rob,grip_pres,2] = ^B0001	;**F1-Absent  F2-Present
    grip_stat1[.rob,grip_pres,3] = ^B0000
    grip_stat1[.rob,grip_abst,1] = ^B0001
    grip_stat1[.rob,grip_abst,2] = ^B0001
    grip_stat1[.rob,grip_abst,3] = ^B0101
    grip_stat1[.rob,grip_rels,1] = ^B0010
    grip_stat1[.rob,grip_rels,2] = ^B0010
    grip_stat1[.rob,grip_rels,3] = ^B1010
    grip_stat1[.rob,grip_mask,1] = ^B0011
    grip_stat1[.rob,grip_mask,2] = ^B1100
    grip_stat1[.rob,grip_mask,3] = ^B1111
.** harish 081120 a--

    ZSET3L2KVFF  .rob: 0,0,0,0
    ZSET3L2KAFF  .rob: 0,0,0,0
    ZSET3L2KJFF  .rob: 0,0,0,0

.** harish homing NT570 081016 m++
.** harish 080922 nt570 homing ++
    can_homeb[.rob,x_ele_x,0]  =  0     ;** top x min
    can_homeb[.rob,x_ele_x,1]  =  300   ;** top x max
    can_homeb[.rob,x_ele_y,0]  =  10    ;** top y min
    can_homeb[.rob,x_ele_y,1]  =  80    ;** top y max
   
    can_homef[.rob,x_ele_x,0]  = -830   ;** bottom x min
    can_homef[.rob,x_ele_x,1]  =  0     ;** bottom x max
    can_homef[.rob,x_ele_y,0]  =  10    ;** bottom y min
    can_homef[.rob,x_ele_y,1]  =  80    ;** bottom y max

    can_homes1[.rob,x_ele_s,0]  = -3     ;** R2 min
    can_homes1[.rob,x_ele_s,1]  =  3     ;** R2 max
    can_homes2[.rob,x_ele_s,0]  = 177    ;** R2 min
    can_homes2[.rob,x_ele_s,1]  = 183    ;** R2 max
.** harish homing NT570 081016 m++
    can_homer2[.rob,x_ele_r,0]  =  80     ;** R1 min
    can_homer2[.rob,x_ele_r,1]  =  100    ;** R1 max
    can_homer1[.rob,x_ele_r,0]  = -100    ;** R1 min
    can_homer1[.rob,x_ele_r,1]  = -80     ;** R1 max
.** harish 090209-1 a++
    can_homej2[.rob,2,0]       = -10	;** angle j2 min
    can_homej2[.rob,2,1]       =  65    ;** angle j2 max
.** harish 090209-1 a--
.** harish homing NT570 081016 m-- 
.** harish 080922 nt570 homing --
.** harish 090309 a
    POINT #j_zero[.rob]     = #PPOINT(0,   0,   0,   0,   0,   0,   0 )
.END
.******************************************
.** srv_nt410.as
.******************************************
.REALS
.** Servo Data for NT410 20080424
    srv_nt410 = 2	;** This must be robot number

    servo[srv_nt410] = 1
.** harish 080627 m++
    srvprm[srv_nt410,2,01] =   163     	;**KVP		
    srvprm[srv_nt410,2,02] =   441     	;**JRAT		
    srvprm[srv_nt410,2,03] =   300     	;**TVI		
    srvprm[srv_nt410,2,04] =     0     	;**VR_TVI1	
    srvprm[srv_nt410,2,05] =   150     	;**VR_TVI2	
    srvprm[srv_nt410,2,06] =   100     	;**VR_V1	
    srvprm[srv_nt410,2,07] =   100     	;**VR_V2	
.*                                                      /* SCRX11-066-3 m2
    srvprm[srv_nt410,2,08] =   305     	;**SQTCLM	
    srvprm[srv_nt410,2,09] =   305     	;**SYSILM	
    srvprm[srv_nt410,2,10] =     0     	;**CCWTLM	
    srvprm[srv_nt410,2,11] =     0     	;**CWTLM	
    srvprm[srv_nt410,2,12] =   600     	;**TCFIL	
    srvprm[srv_nt410,2,13] =   200     	;**TCNFILA	
    srvprm[srv_nt410,2,14] =   200     	;**TCNFILB	
    srvprm[srv_nt410,2,15] =    30     	;**KP		
    srvprm[srv_nt410,2,16] = 10000     	;**TPI		
    srvprm[srv_nt410,2,17] =   100     	;**FFGN		
    srvprm[srv_nt410,2,18] =    80     	;**FFFIL	
    srvprm[srv_nt410,2,19] =  2000     	;**VCFIL	
    srvprm[srv_nt410,2,20] =     0     	;**NOTCH_f1	
    srvprm[srv_nt410,2,21] =     0     	;**NOTCH_f2	
    srvprm[srv_nt410,2,22] =     0     	;**NOTCH_z1	
    srvprm[srv_nt410,2,23] =     0     	;**NOTCH_z2
.*                                                      /* SCRX11-032-1 m */
    srvprm[srv_nt410,2,24] =   370      ;296     	;**AS_PERLM	
    srvprm[srv_nt410,2,25] =     0     	;**AS_ENVERRKP	
    srvprm[srv_nt410,2,26] = 65535     	;**OFLV		
    srvprm[srv_nt410,2,27] = 65535     	;**OFWLV	
    srvprm[srv_nt410,2,28] =     0     	;**PCOMER	
    srvprm[srv_nt410,2,29] =     0     	;**GRAV		
    srvprm[srv_nt410,2,30] =   200     	;**BONDLY	
    srvprm[srv_nt410,2,31] =   200     	;**BOFFDLY	
    srvprm[srv_nt410,2,32] =     0     	;**BONBGN	
    srvprm[srv_nt410,2,33] =     0     	;**AFBK		
    srvprm[srv_nt410,2,34] =  1500     	;**AFBFIL	
    srvprm[srv_nt410,2,35] =     0     	;**OBG		
    srvprm[srv_nt410,2,36] =    10     	;**ANRES	
    srvprm[srv_nt410,2,37] =   200     	;**OBLPF1	
    srvprm[srv_nt410,2,38] =   200     	;**OBLPF2	
    srvprm[srv_nt410,2,39] =    90     	;**OLWLV	
    srvprm[srv_nt410,2,40] = 65535     	;**OSDATA	
    srvprm[srv_nt410,2,41] =   100     	;**INP		

    srvprm[srv_nt410,3,01] =   150     	;**KVP		
    srvprm[srv_nt410,3,02] =   100     	;**JRAT		
    srvprm[srv_nt410,3,03] =   500     	;**TVI		
    srvprm[srv_nt410,3,04] =     0     	;**VR_TVI1	
    srvprm[srv_nt410,3,05] =   150     	;**VR_TVI2	
    srvprm[srv_nt410,3,06] =   100     	;**VR_V1	
    srvprm[srv_nt410,3,07] =   100     	;**VR_V2	
    srvprm[srv_nt410,3,08] =   160     	;**SQTCLM	
    srvprm[srv_nt410,3,09] =   160     	;**SYSILM	
    srvprm[srv_nt410,3,10] =     0     	;**CCWTLM	
    srvprm[srv_nt410,3,11] =     0     	;**CWTLM	
    srvprm[srv_nt410,3,12] =   600     	;**TCFIL	
    srvprm[srv_nt410,3,13] =   200     	;**TCNFILA	
    srvprm[srv_nt410,3,14] =   200     	;**TCNFILB	
    srvprm[srv_nt410,3,15] =    30     	;**KP		
    srvprm[srv_nt410,3,16] = 10000     	;**TPI		
    srvprm[srv_nt410,3,17] =   100     	;**FFGN		
    srvprm[srv_nt410,3,18] =    80     	;**FFFIL	
    srvprm[srv_nt410,3,19] =  2000     	;**VCFIL	
    srvprm[srv_nt410,3,20] =     0     	;**NOTCH_f1	
    srvprm[srv_nt410,3,21] =     0     	;**NOTCH_f2	
    srvprm[srv_nt410,3,22] =     0     	;**NOTCH_z1	
    srvprm[srv_nt410,3,23] =     0     	;**NOTCH_z2	
    srvprm[srv_nt410,3,24] =   213     	;**AS_PERLM	
    srvprm[srv_nt410,3,25] =     0     	;**AS_ENVERRKP	
    srvprm[srv_nt410,3,26] = 65535     	;**OFLV		
    srvprm[srv_nt410,3,27] = 65535     	;**OFWLV	
    srvprm[srv_nt410,3,28] =     0     	;**PCOMER	
    srvprm[srv_nt410,3,29] =   -83     	;**GRAV		
    srvprm[srv_nt410,3,30] =   200     	;**BONDLY	
    srvprm[srv_nt410,3,31] =   200     	;**BOFFDLY	
    srvprm[srv_nt410,3,32] =     0     	;**BONBGN	
    srvprm[srv_nt410,3,33] =     0     	;**AFBK		
    srvprm[srv_nt410,3,34] =  1500     	;**AFBFIL	
    srvprm[srv_nt410,3,35] =     0     	;**OBG		
    srvprm[srv_nt410,3,36] =    10     	;**ANRES	
    srvprm[srv_nt410,3,37] =   200     	;**OBLPF1	
    srvprm[srv_nt410,3,38] =   200     	;**OBLPF2	
    srvprm[srv_nt410,3,39] =    90     	;**OLWLV	
    srvprm[srv_nt410,3,40] = 65535     	;**OSDATA	
    srvprm[srv_nt410,3,41] =   100     	;**INP		

    srvprm[srv_nt410,4,01] =   137     	;**KVP		
    srvprm[srv_nt410,4,02] =   461     	;**JRAT		
    srvprm[srv_nt410,4,03] =   300     	;**TVI		
    srvprm[srv_nt410,4,04] =     0     	;**VR_TVI1	
    srvprm[srv_nt410,4,05] =   150     	;**VR_TVI2	
    srvprm[srv_nt410,4,06] =   100     	;**VR_V1	
    srvprm[srv_nt410,4,07] =   100     	;**VR_V2	
.*                                                      /* SCRX11-066-3 m2
    srvprm[srv_nt410,4,08] =   290     	;**SQTCLM	
    srvprm[srv_nt410,4,09] =   290     	;**SYSILM	
    srvprm[srv_nt410,4,10] =     0     	;**CCWTLM	
    srvprm[srv_nt410,4,11] =     0     	;**CWTLM	
    srvprm[srv_nt410,4,12] =   600     	;**TCFIL	
    srvprm[srv_nt410,4,13] =   200     	;**TCNFILA	
    srvprm[srv_nt410,4,14] =   200     	;**TCNFILB	
    srvprm[srv_nt410,4,15] =    30     	;**KP		
    srvprm[srv_nt410,4,16] = 10000     	;**TPI		
    srvprm[srv_nt410,4,17] =   100     	;**FFGN		
    srvprm[srv_nt410,4,18] =    80     	;**FFFIL	
    srvprm[srv_nt410,4,19] =  2000     	;**VCFIL	
    srvprm[srv_nt410,4,20] =     0     	;**NOTCH_f1	
    srvprm[srv_nt410,4,21] =     0     	;**NOTCH_f2	
    srvprm[srv_nt410,4,22] =     0     	;**NOTCH_z1	
    srvprm[srv_nt410,4,23] =     0     	;**NOTCH_z2	
    srvprm[srv_nt410,4,24] =   329     	;**AS_PERLM	
    srvprm[srv_nt410,4,25] =     0     	;**AS_ENVERRKP	
    srvprm[srv_nt410,4,26] = 65535     	;**OFLV		
    srvprm[srv_nt410,4,27] = 65535     	;**OFWLV	
    srvprm[srv_nt410,4,28] =     0     	;**PCOMER	
    srvprm[srv_nt410,4,29] =     0     	;**GRAV		
    srvprm[srv_nt410,4,30] =   200     	;**BONDLY	
    srvprm[srv_nt410,4,31] =   200     	;**BOFFDLY	
    srvprm[srv_nt410,4,32] =     0     	;**BONBGN	
    srvprm[srv_nt410,4,33] =     0     	;**AFBK		
    srvprm[srv_nt410,4,34] =  1500     	;**AFBFIL	
    srvprm[srv_nt410,4,35] =     0     	;**OBG		
    srvprm[srv_nt410,4,36] =    10     	;**ANRES	
    srvprm[srv_nt410,4,37] =   200     	;**OBLPF1	
    srvprm[srv_nt410,4,38] =   200     	;**OBLPF2	
    srvprm[srv_nt410,4,39] =    90     	;**OLWLV	
    srvprm[srv_nt410,4,40] = 65535     	;**OSDATA	
    srvprm[srv_nt410,4,41] =   100     	;**INP		

    srvprm[srv_nt410,6,01] =   192     	;**KVP		
    srvprm[srv_nt410,6,02] =   187     	;**JRAT		
    srvprm[srv_nt410,6,03] =   300     	;**TVI		
    srvprm[srv_nt410,6,04] =     0     	;**VR_TVI1	
    srvprm[srv_nt410,6,05] =   150     	;**VR_TVI2	
    srvprm[srv_nt410,6,06] =   100     	;**VR_V1	
    srvprm[srv_nt410,6,07] =   100     	;**VR_V2	
.*                                                      /* SCRX11-066-3 m2
    srvprm[srv_nt410,6,08] =   160     	;**SQTCLM	
    srvprm[srv_nt410,6,09] =   160     	;**SYSILM	
    srvprm[srv_nt410,6,10] =     0     	;**CCWTLM	
    srvprm[srv_nt410,6,11] =     0     	;**CWTLM	
    srvprm[srv_nt410,6,12] =   600     	;**TCFIL	
    srvprm[srv_nt410,6,13] =   200     	;**TCNFILA	
    srvprm[srv_nt410,6,14] =   200     	;**TCNFILB	
    srvprm[srv_nt410,6,15] =    30     	;**KP		
    srvprm[srv_nt410,6,16] = 10000     	;**TPI		
    srvprm[srv_nt410,6,17] =   100     	;**FFGN		
    srvprm[srv_nt410,6,18] =    80     	;**FFFIL	
    srvprm[srv_nt410,6,19] =  2000     	;**VCFIL	
    srvprm[srv_nt410,6,20] =     0     	;**NOTCH_f1	
    srvprm[srv_nt410,6,21] =     0     	;**NOTCH_f2	
    srvprm[srv_nt410,6,22] =     0     	;**NOTCH_z1	
    srvprm[srv_nt410,6,23] =     0     	;**NOTCH_z2	
.*                                                      /* SCRX11-032-1 m */
    srvprm[srv_nt410,6,24] =   175      ;161     	;**AS_PERLM	
    srvprm[srv_nt410,6,25] =     0     	;**AS_ENVERRKP	
    srvprm[srv_nt410,6,26] = 65535     	;**OFLV		
    srvprm[srv_nt410,6,27] = 65535     	;**OFWLV	
    srvprm[srv_nt410,6,28] =     0     	;**PCOMER	
    srvprm[srv_nt410,6,29] =     0     	;**GRAV		
    srvprm[srv_nt410,6,30] =   200     	;**BONDLY	
    srvprm[srv_nt410,6,31] =   200     	;**BOFFDLY	
    srvprm[srv_nt410,6,32] =     0     	;**BONBGN	
    srvprm[srv_nt410,6,33] =     0     	;**AFBK		
    srvprm[srv_nt410,6,34] =  1500     	;**AFBFIL	
    srvprm[srv_nt410,6,35] =     0     	;**OBG		
    srvprm[srv_nt410,6,36] =    10     	;**ANRES	
    srvprm[srv_nt410,6,37] =   200     	;**OBLPF1	
    srvprm[srv_nt410,6,38] =   200     	;**OBLPF2	
    srvprm[srv_nt410,6,39] =    90     	;**OLWLV	
    srvprm[srv_nt410,6,40] = 65535     	;**OSDATA	
    srvprm[srv_nt410,6,41] =   100     	;**INP	
.** harish 080627 m--
.END
.******************************************
.** srv_nt570.as
.******************************************
.REALS
.** Servo Data for NT570 20080424
    srv_nt570 = 1	;** This must be robot number

    servo[srv_nt570] = 1

    srvprm[srv_nt570,2,01] =   360	;**KVP		
    srvprm[srv_nt570,2,02] =   528	;**JRAT		
    srvprm[srv_nt570,2,03] =   590	;**TVI		
    srvprm[srv_nt570,2,04] =     0	;**VR_TVI1	
    srvprm[srv_nt570,2,05] =   150	;**VR_TVI2	
    srvprm[srv_nt570,2,06] =   100	;**VR_V1	
    srvprm[srv_nt570,2,07] =   100	;**VR_V2	
    srvprm[srv_nt570,2,08] =   250	;**SQTCLM	
    srvprm[srv_nt570,2,09] =   250	;**SYSILM	
    srvprm[srv_nt570,2,10] =     0	;**CCWTLM	
    srvprm[srv_nt570,2,11] =     0	;**CWTLM
    srvprm[srv_nt570,2,12] =   600	;**TCFIL	
    srvprm[srv_nt570,2,13] =   200	;**TCNFILA	
    srvprm[srv_nt570,2,14] =   200	;**TCNFILB	
    srvprm[srv_nt570,2,15] =    22	;**KP		
    srvprm[srv_nt570,2,16] = 10000	;**TPI		
    srvprm[srv_nt570,2,17] =   100	;**FFGN		
    srvprm[srv_nt570,2,18] =    80	;**FFFIL	
    srvprm[srv_nt570,2,19] =  2000	;**VCFIL	
    srvprm[srv_nt570,2,20] =     0.0	;**NOTCH_f1	
    srvprm[srv_nt570,2,21] =     0.0	;**NOTCH_f2	
    srvprm[srv_nt570,2,22] =     0.0	;**NOTCH_z1	
    srvprm[srv_nt570,2,23] =     0.0	;**NOTCH_z2
.*                                                      /* SCRX11-032-1 m */	
    srvprm[srv_nt570,2,24] =  130   ;117	;**AS_PERLM	
    srvprm[srv_nt570,2,25] =     0	;**AS_ENVERRKP	
    srvprm[srv_nt570,2,26] = 65535	;**OFLV		
    srvprm[srv_nt570,2,27] = 65535	;**OFWLV	
    srvprm[srv_nt570,2,28] =     0	;**PCOMER	
    srvprm[srv_nt570,2,29] =     0	;**GRAV		
    srvprm[srv_nt570,2,30] =   200	;**BONDLY	
    srvprm[srv_nt570,2,31] =   200	;**BOFFDLY	
    srvprm[srv_nt570,2,32] =     0	;**BONBGN	
    srvprm[srv_nt570,2,33] =     0	;**AFBK		
    srvprm[srv_nt570,2,34] =  1500	;**AFBFIL	
    srvprm[srv_nt570,2,35] =     0	;**OBG		
    srvprm[srv_nt570,2,36] =    10	;**ANRES	
    srvprm[srv_nt570,2,37] =   200	;**OBLPF1	
    srvprm[srv_nt570,2,38] =   200	;**OBLPF2	
    srvprm[srv_nt570,2,39] =    90	;**OLWLV	
    srvprm[srv_nt570,2,40] = 65535	;**OSDATA	
    srvprm[srv_nt570,2,41] =   100	;**INP		

    srvprm[srv_nt570,3,01] =   120	;**KVP		
    srvprm[srv_nt570,3,02] =   100	;**JRAT		
    srvprm[srv_nt570,3,03] =   460	;**TVI		
    srvprm[srv_nt570,3,04] =     0	;**VR_TVI1	
    srvprm[srv_nt570,3,05] =   150	;**VR_TVI2	
    srvprm[srv_nt570,3,06] =   100	;**VR_V1	
    srvprm[srv_nt570,3,07] =   100	;**VR_V2	
    srvprm[srv_nt570,3,08] =   200	;**SQTCLM	
    srvprm[srv_nt570,3,09] =   200	;**SYSILM	
    srvprm[srv_nt570,3,10] =     0	;**CCWTLM	
    srvprm[srv_nt570,3,11] =     0	;**CWTLM	
    srvprm[srv_nt570,3,12] =   600	;**TCFIL	
    srvprm[srv_nt570,3,13] =   200	;**TCNFILA	
    srvprm[srv_nt570,3,14] =   200	;**TCNFILB	
    srvprm[srv_nt570,3,15] =    22	;**KP		
    srvprm[srv_nt570,3,16] = 10000	;**TPI		
    srvprm[srv_nt570,3,17] =   100	;**FFGN		
    srvprm[srv_nt570,3,18] =    80	;**FFFIL	
    srvprm[srv_nt570,3,19] =  2000	;**VCFIL	
    srvprm[srv_nt570,3,20] =     0.0	;**NOTCH_f1	
    srvprm[srv_nt570,3,21] =     0.0	;**NOTCH_f2	
    srvprm[srv_nt570,3,22] =     0.0	;**NOTCH_z1	
    srvprm[srv_nt570,3,23] =     0.0	;**NOTCH_z2	
    srvprm[srv_nt570,3,24] = 377	;**AS_PERLM	
    srvprm[srv_nt570,3,25] =     0	;**AS_ENVERRKP	
    srvprm[srv_nt570,3,26] = 65535	;**OFLV		
    srvprm[srv_nt570,3,27] = 65535	;**OFWLV	
    srvprm[srv_nt570,3,28] =     0	;**PCOMER	
    srvprm[srv_nt570,3,29] =   -92	;**GRAV		
    srvprm[srv_nt570,3,30] =   200	;**BONDLY	
    srvprm[srv_nt570,3,31] =   200	;**BOFFDLY	
    srvprm[srv_nt570,3,32] =     0	;**BONBGN	
    srvprm[srv_nt570,3,33] =     0	;**AFBK		
    srvprm[srv_nt570,3,34] =  1500	;**AFBFIL	
    srvprm[srv_nt570,3,35] =     0	;**OBG		
    srvprm[srv_nt570,3,36] =    10	;**ANRES	
    srvprm[srv_nt570,3,37] =   200	;**OBLPF1	
    srvprm[srv_nt570,3,38] =   200	;**OBLPF2	
    srvprm[srv_nt570,3,39] =    90	;**OLWLV	
    srvprm[srv_nt570,3,40] = 65535	;**OSDATA	
    srvprm[srv_nt570,3,41] =   100	;**INP		

    srvprm[srv_nt570,4,01] =   315	;**KVP		
    srvprm[srv_nt570,4,02] =   707	;**JRAT		
    srvprm[srv_nt570,4,03] =   700	;**TVI		
    srvprm[srv_nt570,4,04] =     0	;**VR_TVI1	
    srvprm[srv_nt570,4,05] =   150	;**VR_TVI2	
    srvprm[srv_nt570,4,06] =   100	;**VR_V1	
    srvprm[srv_nt570,4,07] =   100	;**VR_V2	
    srvprm[srv_nt570,4,08] =   255	;**SQTCLM	
    srvprm[srv_nt570,4,09] =   255	;**SYSILM	
    srvprm[srv_nt570,4,10] =     0	;**CCWTLM	
    srvprm[srv_nt570,4,11] =     0	;**CWTLM	
    srvprm[srv_nt570,4,12] =   600	;**TCFIL	
    srvprm[srv_nt570,4,13] =   200	;**TCNFILA	
    srvprm[srv_nt570,4,14] =   200	;**TCNFILB	
    srvprm[srv_nt570,4,15] =    22	;**KP		
    srvprm[srv_nt570,4,16] = 10000	;**TPI		
    srvprm[srv_nt570,4,17] =   100	;**FFGN		
    srvprm[srv_nt570,4,18] =    80	;**FFFIL	
    srvprm[srv_nt570,4,19] =  2000	;**VCFIL	
    srvprm[srv_nt570,4,20] =     0.0	;**NOTCH_f1	
    srvprm[srv_nt570,4,21] =     0.0	;**NOTCH_f2	
    srvprm[srv_nt570,4,22] =     0.0	;**NOTCH_z1	
    srvprm[srv_nt570,4,23] =     0.0	;**NOTCH_z2	
    srvprm[srv_nt570,4,24] = 143	;**AS_PERLM	
    srvprm[srv_nt570,4,25] =     0	;**AS_ENVERRKP	
    srvprm[srv_nt570,4,26] = 65535	;**OFLV		
    srvprm[srv_nt570,4,27] = 65535	;**OFWLV	
    srvprm[srv_nt570,4,28] =     0	;**PCOMER	
    srvprm[srv_nt570,4,29] =     0	;**GRAV		
    srvprm[srv_nt570,4,30] =   200	;**BONDLY	
    srvprm[srv_nt570,4,31] =   200	;**BOFFDLY	
    srvprm[srv_nt570,4,32] =     0	;**BONBGN	
    srvprm[srv_nt570,4,33] =     0	;**AFBK		
    srvprm[srv_nt570,4,34] =  1500	;**AFBFIL	
    srvprm[srv_nt570,4,35] =     0	;**OBG		
    srvprm[srv_nt570,4,36] =    10	;**ANRES	
    srvprm[srv_nt570,4,37] =   200	;**OBLPF1	
    srvprm[srv_nt570,4,38] =   200	;**OBLPF2	
    srvprm[srv_nt570,4,39] =    90	;**OLWLV	
    srvprm[srv_nt570,4,40] = 65535	;**OSDATA	
    srvprm[srv_nt570,4,41] =   100	;**INP		

    srvprm[srv_nt570,6,01] =   325	;**KVP		
    srvprm[srv_nt570,6,02] =   219	;**JRAT		
    srvprm[srv_nt570,6,03] =   450	;**TVI		
    srvprm[srv_nt570,6,04] =     0	;**VR_TVI1	
    srvprm[srv_nt570,6,05] =   150	;**VR_TVI2	
    srvprm[srv_nt570,6,06] =   100	;**VR_V1	
    srvprm[srv_nt570,6,07] =   100	;**VR_V2	
    srvprm[srv_nt570,6,08] =   120	;**SQTCLM	
    srvprm[srv_nt570,6,09] =   120	;**SYSILM	
    srvprm[srv_nt570,6,10] =     0	;**CCWTLM	
    srvprm[srv_nt570,6,11] =     0	;**CWTLM	
    srvprm[srv_nt570,6,12] =   600	;**TCFIL	
    srvprm[srv_nt570,6,13] =   200	;**TCNFILA	
    srvprm[srv_nt570,6,14] =   200	;**TCNFILB	
    srvprm[srv_nt570,6,15] =    22	;**KP		
    srvprm[srv_nt570,6,16] = 10000	;**TPI		
    srvprm[srv_nt570,6,17] =   100	;**FFGN		
    srvprm[srv_nt570,6,18] =    80	;**FFFIL	
    srvprm[srv_nt570,6,19] =  2000	;**VCFIL	
    srvprm[srv_nt570,6,20] =     0.0	;**NOTCH_f1	
    srvprm[srv_nt570,6,21] =     0.0	;**NOTCH_f2	
    srvprm[srv_nt570,6,22] =     0.0	;**NOTCH_z1	
    srvprm[srv_nt570,6,23] =     0.0	;**NOTCH_z2	
    srvprm[srv_nt570,6,24] = 186	;**AS_PERLM	
    srvprm[srv_nt570,6,25] =     0	;**AS_ENVERRKP	
    srvprm[srv_nt570,6,26] = 65535	;**OFLV		
    srvprm[srv_nt570,6,27] = 65535	;**OFWLV	
    srvprm[srv_nt570,6,28] =     0	;**PCOMER	
    srvprm[srv_nt570,6,29] =     0	;**GRAV		
    srvprm[srv_nt570,6,30] =   200	;**BONDLY	
    srvprm[srv_nt570,6,31] =   200	;**BOFFDLY	
    srvprm[srv_nt570,6,32] =     0	;**BONBGN	
    srvprm[srv_nt570,6,33] =     0	;**AFBK		
    srvprm[srv_nt570,6,34] =  1500	;**AFBFIL	
    srvprm[srv_nt570,6,35] =     0	;**OBG		
    srvprm[srv_nt570,6,36] =    10	;**ANRES	
    srvprm[srv_nt570,6,37] =   200	;**OBLPF1	
    srvprm[srv_nt570,6,38] =   200	;**OBLPF2	
    srvprm[srv_nt570,6,39] =    90	;**OLWLV	
    srvprm[srv_nt570,6,40] = 65535	;**OSDATA	
    srvprm[srv_nt570,6,41] =   100	;**INP		

    srvprm[srv_nt570,7,01] =   140	;**KVP		
    srvprm[srv_nt570,7,02] =   366	;**JRAT		
    srvprm[srv_nt570,7,03] =   280	;**TVI		
    srvprm[srv_nt570,7,04] =     0	;**VR_TVI1	
    srvprm[srv_nt570,7,05] =   150	;**VR_TVI2	
    srvprm[srv_nt570,7,06] =   100	;**VR_V1	
    srvprm[srv_nt570,7,07] =   100	;**VR_V2	
    srvprm[srv_nt570,7,08] =    84	;**SQTCLM	
    srvprm[srv_nt570,7,09] =    84	;**SYSILM	
    srvprm[srv_nt570,7,10] =     0	;**CCWTLM	
    srvprm[srv_nt570,7,11] =     0	;**CWTLM	
    srvprm[srv_nt570,7,12] =   600	;**TCFIL	
    srvprm[srv_nt570,7,13] =   200	;**TCNFILA	
    srvprm[srv_nt570,7,14] =   200	;**TCNFILB	
    srvprm[srv_nt570,7,15] =    22	;**KP		
    srvprm[srv_nt570,7,16] = 10000	;**TPI		
    srvprm[srv_nt570,7,17] =   100	;**FFGN		
    srvprm[srv_nt570,7,18] =    80	;**FFFIL	
    srvprm[srv_nt570,7,19] =  2000	;**VCFIL	
    srvprm[srv_nt570,7,20] =     0.0	;**NOTCH_f1	
    srvprm[srv_nt570,7,21] =     0.0	;**NOTCH_f2	
    srvprm[srv_nt570,7,22] =     0.0	;**NOTCH_z1	
    srvprm[srv_nt570,7,23] =     0.0	;**NOTCH_z2	
    srvprm[srv_nt570,7,24] =  86	;**AS_PERLM	
    srvprm[srv_nt570,7,25] =     0	;**AS_ENVERRKP	
    srvprm[srv_nt570,7,26] = 65535	;**OFLV		
    srvprm[srv_nt570,7,27] = 65535	;**OFWLV	
    srvprm[srv_nt570,7,28] =     0	;**PCOMER	
    srvprm[srv_nt570,7,29] =   -48	;**GRAV		
    srvprm[srv_nt570,7,30] =   200	;**BONDLY	
    srvprm[srv_nt570,7,31] =   200	;**BOFFDLY	
    srvprm[srv_nt570,7,32] =     0	;**BONBGN	
    srvprm[srv_nt570,7,33] =     0	;**AFBK		
    srvprm[srv_nt570,7,34] =  1500	;**AFBFIL	
    srvprm[srv_nt570,7,35] =     0	;**OBG		
    srvprm[srv_nt570,7,36] =    10	;**ANRES	
    srvprm[srv_nt570,7,37] =   200	;**OBLPF1	
    srvprm[srv_nt570,7,38] =   200	;**OBLPF2	
    srvprm[srv_nt570,7,39] =    90	;**OLWLV	
    srvprm[srv_nt570,7,40] = 65535	;**OSDATA	
    srvprm[srv_nt570,7,41] =   100	;**INP		
.END
.*************************************************
.*	NT410 Accel/Decel/Speed Parameters
.*************************************************
.** harish 080619  .** changes accel/decel to avoid over current on JT4 (adviced by Tet-san) **.
.PROGRAM nt410newprm()
.** NT410 NEW PARAMETERS
    p_hori_lla = p_hori_pta
    p_hori_llb = p_hori_ptb
    p_hori_llc = p_hori_vdb
    p_hori_lld = p_hori_vda
.** ACCEL/DECEL
.* CS1
    path_acce[p_hori_cs1,gpos_e_home] = 60
    path_acce[p_hori_cs1,gpos_e_prep] = 60
    path_acce[p_hori_cs1,gpos_e_half] =  60
    path_acce[p_hori_cs1,gpos_e_near] = 60
    path_acce[p_hori_cs1,gpos_e_extd] =  60
    path_acce[p_hori_cs1,gpos_e_slow] = 100
    path_acce[p_hori_cs1,gpos_e_tech] = 100
    path_acce[p_hori_cs1,gpos_r_tech] =  30
    path_acce[p_hori_cs1,gpos_r_slow] = 100
    path_acce[p_hori_cs1,gpos_r_extd] = 100
    path_acce[p_hori_cs1,gpos_r_back] =  30
    path_acce[p_hori_cs1,gpos_r_near] = 60
    path_acce[p_hori_cs1,gpos_r_half] =  60
    path_acce[p_hori_cs1,gpos_r_prep] = 60
    path_acce[p_hori_cs1,gpos_r_home] =  60

    path_dece[p_hori_cs1,gpos_e_home] = 60
    path_dece[p_hori_cs1,gpos_e_prep] = 60
    path_dece[p_hori_cs1,gpos_e_half] =  60
    path_dece[p_hori_cs1,gpos_e_near] = 60
    path_dece[p_hori_cs1,gpos_e_extd] =  60
    path_dece[p_hori_cs1,gpos_e_slow] = 100
    path_dece[p_hori_cs1,gpos_e_tech] = 100
    path_dece[p_hori_cs1,gpos_r_tech] =  30
    path_dece[p_hori_cs1,gpos_r_slow] = 100
    path_dece[p_hori_cs1,gpos_r_extd] = 100
    path_dece[p_hori_cs1,gpos_r_back] =  30
    path_dece[p_hori_cs1,gpos_r_near] = 60
    path_dece[p_hori_cs1,gpos_r_half] =  60
    path_dece[p_hori_cs1,gpos_r_prep] = 60
    path_dece[p_hori_cs1,gpos_r_home] =  60

.* CS2
    path_acce[p_hori_cs2,gpos_e_home] = 60
    path_acce[p_hori_cs2,gpos_e_prep] = 60
    path_acce[p_hori_cs2,gpos_e_half] =  60
    path_acce[p_hori_cs2,gpos_e_near] =  60
    path_acce[p_hori_cs2,gpos_e_extd] =  60
    path_acce[p_hori_cs2,gpos_e_slow] = 100
    path_acce[p_hori_cs2,gpos_e_tech] = 100
    path_acce[p_hori_cs2,gpos_r_tech] =  30
    path_acce[p_hori_cs2,gpos_r_slow] = 100
    path_acce[p_hori_cs2,gpos_r_extd] = 100
    path_acce[p_hori_cs2,gpos_r_back] = 30
    path_acce[p_hori_cs2,gpos_r_near] = 60
    path_acce[p_hori_cs2,gpos_r_half] =  60
    path_acce[p_hori_cs2,gpos_r_prep] =  60
    path_acce[p_hori_cs2,gpos_r_home] =  60

    path_dece[p_hori_cs2,gpos_e_home] = 60
    path_dece[p_hori_cs2,gpos_e_prep] = 60
    path_dece[p_hori_cs2,gpos_e_half] =  60
    path_dece[p_hori_cs2,gpos_e_near] =  60
    path_dece[p_hori_cs2,gpos_e_extd] =  60
    path_dece[p_hori_cs2,gpos_e_slow] = 100
    path_dece[p_hori_cs2,gpos_e_tech] = 100
    path_dece[p_hori_cs2,gpos_r_tech] =  30
    path_dece[p_hori_cs2,gpos_r_slow] = 100
    path_dece[p_hori_cs2,gpos_r_extd] = 100
    path_dece[p_hori_cs2,gpos_r_back] = 30
    path_dece[p_hori_cs2,gpos_r_near] = 60
    path_dece[p_hori_cs2,gpos_r_half] =  60
    path_dece[p_hori_cs2,gpos_r_prep] =  60
    path_dece[p_hori_cs2,gpos_r_home] =  60

.* CS3
    path_acce[p_hori_cs3,gpos_e_home] = 60
    path_acce[p_hori_cs3,gpos_e_prep] =  60
    path_acce[p_hori_cs3,gpos_e_half] =  60
    path_acce[p_hori_cs3,gpos_e_near] =  60
    path_acce[p_hori_cs3,gpos_e_extd] =  60
    path_acce[p_hori_cs3,gpos_e_slow] = 100
    path_acce[p_hori_cs3,gpos_e_tech] = 100
    path_acce[p_hori_cs3,gpos_r_tech] =  30
    path_acce[p_hori_cs3,gpos_r_slow] = 100
    path_acce[p_hori_cs3,gpos_r_extd] = 100
    path_acce[p_hori_cs3,gpos_r_back] = 30
    path_acce[p_hori_cs3,gpos_r_near] = 60
    path_acce[p_hori_cs3,gpos_r_half] =  60
    path_acce[p_hori_cs3,gpos_r_prep] =  60
    path_acce[p_hori_cs3,gpos_r_home] =  60

    path_dece[p_hori_cs3,gpos_e_home] = 60
    path_dece[p_hori_cs3,gpos_e_prep] =  60
    path_dece[p_hori_cs3,gpos_e_half] =  60
    path_dece[p_hori_cs3,gpos_e_near] =  60
    path_dece[p_hori_cs3,gpos_e_extd] =  60
    path_dece[p_hori_cs3,gpos_e_slow] = 100
    path_dece[p_hori_cs3,gpos_e_tech] = 100
    path_dece[p_hori_cs3,gpos_r_tech] =  30
    path_dece[p_hori_cs3,gpos_r_slow] = 100
    path_dece[p_hori_cs3,gpos_r_extd] = 100
    path_dece[p_hori_cs3,gpos_r_back] = 30
    path_dece[p_hori_cs3,gpos_r_near] = 60
    path_dece[p_hori_cs3,gpos_r_half] =  60
    path_dece[p_hori_cs3,gpos_r_prep] =  60
    path_dece[p_hori_cs3,gpos_r_home] =  60

.* CS4
    path_acce[p_hori_cs4,gpos_e_home] = 60
    path_acce[p_hori_cs4,gpos_e_prep] = 60
    path_acce[p_hori_cs4,gpos_e_half] =  60
    path_acce[p_hori_cs4,gpos_e_near] = 60
    path_acce[p_hori_cs4,gpos_e_extd] =  60
    path_acce[p_hori_cs4,gpos_e_slow] = 100
    path_acce[p_hori_cs4,gpos_e_tech] = 100
    path_acce[p_hori_cs4,gpos_r_tech] =  30
    path_acce[p_hori_cs4,gpos_r_slow] = 100
    path_acce[p_hori_cs4,gpos_r_extd] = 100
    path_acce[p_hori_cs4,gpos_r_back] =  30
    path_acce[p_hori_cs4,gpos_r_near] = 60
    path_acce[p_hori_cs4,gpos_r_half] =  60
    path_acce[p_hori_cs4,gpos_r_prep] =  60
    path_acce[p_hori_cs4,gpos_r_home] = 60

    path_dece[p_hori_cs4,gpos_e_home] = 60
    path_dece[p_hori_cs4,gpos_e_prep] = 60
    path_dece[p_hori_cs4,gpos_e_half] =  60
    path_dece[p_hori_cs4,gpos_e_near] = 60
    path_dece[p_hori_cs4,gpos_e_extd] =  60
    path_dece[p_hori_cs4,gpos_e_slow] = 100
    path_dece[p_hori_cs4,gpos_e_tech] = 100
    path_dece[p_hori_cs4,gpos_r_tech] =  30
    path_dece[p_hori_cs4,gpos_r_slow] = 100
    path_dece[p_hori_cs4,gpos_r_extd] = 100
    path_dece[p_hori_cs4,gpos_r_back] =  30
    path_dece[p_hori_cs4,gpos_r_near] = 60
    path_dece[p_hori_cs4,gpos_r_half] =  60
    path_dece[p_hori_cs4,gpos_r_prep] =  60
    path_dece[p_hori_cs4,gpos_r_home] = 60

.* LLA
    path_acce[p_hori_lla,gpos_e_home] = 60
    path_acce[p_hori_lla,gpos_e_prep] = 60
    path_acce[p_hori_lla,gpos_e_half] = 60
    path_acce[p_hori_lla,gpos_e_near] = 100
    path_acce[p_hori_lla,gpos_e_extd] = 100
    path_acce[p_hori_lla,gpos_e_slow] = 100
    path_acce[p_hori_lla,gpos_e_tech] = 100
    path_acce[p_hori_lla,gpos_r_tech] =  30
    path_acce[p_hori_lla,gpos_r_slow] = 100
    path_acce[p_hori_lla,gpos_r_extd] = 100
    path_acce[p_hori_lla,gpos_r_back] = 30
    path_acce[p_hori_lla,gpos_r_near] = 100
    path_acce[p_hori_lla,gpos_r_half] = 60
    path_acce[p_hori_lla,gpos_r_prep] = 60
    path_acce[p_hori_lla,gpos_r_home] = 60

    path_dece[p_hori_lla,gpos_e_home] = 60
    path_dece[p_hori_lla,gpos_e_prep] = 60
    path_dece[p_hori_lla,gpos_e_half] = 60
    path_dece[p_hori_lla,gpos_e_near] = 100
    path_dece[p_hori_lla,gpos_e_extd] = 100
    path_dece[p_hori_lla,gpos_e_slow] = 100
    path_dece[p_hori_lla,gpos_e_tech] = 100
    path_dece[p_hori_lla,gpos_r_tech] =  30
    path_dece[p_hori_lla,gpos_r_slow] = 100
    path_dece[p_hori_lla,gpos_r_extd] = 30
    path_dece[p_hori_lla,gpos_r_back] = 100
    path_dece[p_hori_lla,gpos_r_near] = 100
    path_dece[p_hori_lla,gpos_r_half] = 60
    path_dece[p_hori_lla,gpos_r_prep] = 60
    path_dece[p_hori_lla,gpos_r_home] = 60

.* LLB
    path_acce[p_hori_llb,gpos_e_home] = 60
    path_acce[p_hori_llb,gpos_e_prep] = 60
    path_acce[p_hori_llb,gpos_e_half] =  60
    path_acce[p_hori_llb,gpos_e_near] = 100
    path_acce[p_hori_llb,gpos_e_extd] = 100
    path_acce[p_hori_llb,gpos_e_slow] = 100
    path_acce[p_hori_llb,gpos_e_tech] = 100
    path_acce[p_hori_llb,gpos_r_tech] =  30
    path_acce[p_hori_llb,gpos_r_slow] = 100
    path_acce[p_hori_llb,gpos_r_extd] = 100
    path_acce[p_hori_llb,gpos_r_back] = 30
    path_acce[p_hori_llb,gpos_r_near] = 100
    path_acce[p_hori_llb,gpos_r_half] =  60
    path_acce[p_hori_llb,gpos_r_prep] = 60
    path_acce[p_hori_llb,gpos_r_home] =  60

    path_dece[p_hori_llb,gpos_e_home] = 60
    path_dece[p_hori_llb,gpos_e_prep] = 60
    path_dece[p_hori_llb,gpos_e_half] =  60
    path_dece[p_hori_llb,gpos_e_near] = 100
    path_dece[p_hori_llb,gpos_e_extd] = 100
    path_dece[p_hori_llb,gpos_e_slow] = 100
    path_dece[p_hori_llb,gpos_e_tech] = 100
    path_dece[p_hori_llb,gpos_r_tech] =  30
    path_dece[p_hori_llb,gpos_r_slow] = 100
    path_dece[p_hori_llb,gpos_r_extd] = 100
    path_dece[p_hori_llb,gpos_r_back] = 30
    path_dece[p_hori_llb,gpos_r_near] = 100
    path_dece[p_hori_llb,gpos_r_half] =  60
    path_dece[p_hori_llb,gpos_r_prep] = 60
    path_dece[p_hori_llb,gpos_r_home] =  60

.* LLC
    path_acce[p_hori_llc,gpos_e_home] = 60
    path_acce[p_hori_llc,gpos_e_prep] =  60
    path_acce[p_hori_llc,gpos_e_half] =  60
    path_acce[p_hori_llc,gpos_e_near] =  100
    path_acce[p_hori_llc,gpos_e_extd] =  100
    path_acce[p_hori_llc,gpos_e_slow] = 100
    path_acce[p_hori_llc,gpos_e_tech] = 100
    path_acce[p_hori_llc,gpos_r_tech] =  30
    path_acce[p_hori_llc,gpos_r_slow] = 100
    path_acce[p_hori_llc,gpos_r_extd] = 100
    path_acce[p_hori_llc,gpos_r_back] = 30
    path_acce[p_hori_llc,gpos_r_near] = 100
    path_acce[p_hori_llc,gpos_r_half] =  60
    path_acce[p_hori_llc,gpos_r_prep] =  60
    path_acce[p_hori_llc,gpos_r_home] =  60

    path_dece[p_hori_llc,gpos_e_home] = 60
    path_dece[p_hori_llc,gpos_e_prep] =  60
    path_dece[p_hori_llc,gpos_e_half] =  60
    path_dece[p_hori_llc,gpos_e_near] =  100
    path_dece[p_hori_llc,gpos_e_extd] =  100
    path_dece[p_hori_llc,gpos_e_slow] = 100
    path_dece[p_hori_llc,gpos_e_tech] = 100
    path_dece[p_hori_llc,gpos_r_tech] =  30
    path_dece[p_hori_llc,gpos_r_slow] = 100
    path_dece[p_hori_llc,gpos_r_extd] = 100
    path_dece[p_hori_llc,gpos_r_back] = 30
    path_dece[p_hori_llc,gpos_r_near] = 100
    path_dece[p_hori_llc,gpos_r_half] =  60
    path_dece[p_hori_llc,gpos_r_prep] =  60
    path_dece[p_hori_llc,gpos_r_home] =  60

.* LLD
    path_acce[p_hori_lld,gpos_e_home] = 60
    path_acce[p_hori_lld,gpos_e_prep] =  60
    path_acce[p_hori_lld,gpos_e_half] =  60
    path_acce[p_hori_lld,gpos_e_near] = 100
    path_acce[p_hori_lld,gpos_e_extd] = 100
    path_acce[p_hori_lld,gpos_e_slow] = 100
    path_acce[p_hori_lld,gpos_e_tech] = 100
    path_acce[p_hori_lld,gpos_r_tech] =  30
    path_acce[p_hori_lld,gpos_r_slow] = 100
    path_acce[p_hori_lld,gpos_r_extd] = 100
    path_acce[p_hori_lld,gpos_r_back] = 30
    path_acce[p_hori_lld,gpos_r_near] = 100
    path_acce[p_hori_lld,gpos_r_half] =  60
    path_acce[p_hori_lld,gpos_r_prep] =  60
    path_acce[p_hori_lld,gpos_r_home] =  60

    path_dece[p_hori_lld,gpos_e_home] = 60
    path_dece[p_hori_lld,gpos_e_prep] =  60
    path_dece[p_hori_lld,gpos_e_half] =  60
    path_dece[p_hori_lld,gpos_e_near] = 100
    path_dece[p_hori_lld,gpos_e_extd] = 100
    path_dece[p_hori_lld,gpos_e_slow] = 100
    path_dece[p_hori_lld,gpos_e_tech] = 100
    path_dece[p_hori_lld,gpos_r_tech] =  30
    path_dece[p_hori_lld,gpos_r_slow] = 100
    path_dece[p_hori_lld,gpos_r_extd] = 100
    path_dece[p_hori_lld,gpos_r_back] = 30
    path_dece[p_hori_lld,gpos_r_near] = 100
    path_dece[p_hori_lld,gpos_r_half] =  60
    path_dece[p_hori_lld,gpos_r_prep] =  60
    path_dece[p_hori_lld,gpos_r_home] =  60

.** SPEED **.
.* CS1
    path_sped[p_hori_cs1,gpos_e_home] = 100
    path_sped[p_hori_cs1,gpos_e_prep] = 100
    path_sped[p_hori_cs1,gpos_e_half] = 100
    path_sped[p_hori_cs1,gpos_e_near] = 100
    path_sped[p_hori_cs1,gpos_e_extd] = 100
    path_sped[p_hori_cs1,gpos_e_slow] = 100
    path_sped[p_hori_cs1,gpos_e_tech] =  -1
    path_sped[p_hori_cs1,gpos_r_tech] =  -1
    path_sped[p_hori_cs1,gpos_r_slow] =  -1
    path_sped[p_hori_cs1,gpos_r_extd] =  -1
    path_sped[p_hori_cs1,gpos_r_back] = 100
    path_sped[p_hori_cs1,gpos_r_near] = 100
    path_sped[p_hori_cs1,gpos_r_half] = 100
    path_sped[p_hori_cs1,gpos_r_prep] = 100
    path_sped[p_hori_cs1,gpos_r_home] = 100

.* CS2
    path_sped[p_hori_cs2,gpos_e_home] = 100
    path_sped[p_hori_cs2,gpos_e_prep] = 100
    path_sped[p_hori_cs2,gpos_e_half] = 100
    path_sped[p_hori_cs2,gpos_e_near] = 100
    path_sped[p_hori_cs2,gpos_e_extd] = 100
    path_sped[p_hori_cs2,gpos_e_slow] = 100
    path_sped[p_hori_cs2,gpos_e_tech] =  -1
    path_sped[p_hori_cs2,gpos_r_tech] =  -1
    path_sped[p_hori_cs2,gpos_r_slow] =  -1
    path_sped[p_hori_cs2,gpos_r_extd] =  -1
    path_sped[p_hori_cs2,gpos_r_back] = 100
    path_sped[p_hori_cs2,gpos_r_near] = 100
    path_sped[p_hori_cs2,gpos_r_half] = 100
    path_sped[p_hori_cs2,gpos_r_prep] = 100
    path_sped[p_hori_cs2,gpos_r_home] = 100

.* CS3
    path_sped[p_hori_cs3,gpos_e_home] = 100
    path_sped[p_hori_cs3,gpos_e_prep] = 100
    path_sped[p_hori_cs3,gpos_e_half] = 100
    path_sped[p_hori_cs3,gpos_e_near] = 100
    path_sped[p_hori_cs3,gpos_e_extd] = 100
    path_sped[p_hori_cs3,gpos_e_slow] = 100
    path_sped[p_hori_cs3,gpos_e_tech] =  -1
    path_sped[p_hori_cs3,gpos_r_tech] =  -1
    path_sped[p_hori_cs3,gpos_r_slow] =  -1
    path_sped[p_hori_cs3,gpos_r_extd] =  -1
    path_sped[p_hori_cs3,gpos_r_back] = 100
    path_sped[p_hori_cs3,gpos_r_near] = 100
    path_sped[p_hori_cs3,gpos_r_half] = 100
    path_sped[p_hori_cs3,gpos_r_prep] = 100
    path_sped[p_hori_cs3,gpos_r_home] = 100

.* CS4
    path_sped[p_hori_cs4,gpos_e_home] = 100
    path_sped[p_hori_cs4,gpos_e_prep] = 100
    path_sped[p_hori_cs4,gpos_e_half] = 100
    path_sped[p_hori_cs4,gpos_e_near] = 100
    path_sped[p_hori_cs4,gpos_e_extd] = 100
    path_sped[p_hori_cs4,gpos_e_slow] = 100
    path_sped[p_hori_cs4,gpos_e_tech] =  -1
    path_sped[p_hori_cs4,gpos_r_tech] =  -1
    path_sped[p_hori_cs4,gpos_r_slow] =  -1
    path_sped[p_hori_cs4,gpos_r_extd] =  -1
    path_sped[p_hori_cs4,gpos_r_back] = 100
    path_sped[p_hori_cs4,gpos_r_near] = 100
    path_sped[p_hori_cs4,gpos_r_half] = 100
    path_sped[p_hori_cs4,gpos_r_prep] = 100
    path_sped[p_hori_cs4,gpos_r_home] = 100

.* LLA
    path_sped[p_hori_lla,gpos_e_home] = 100
    path_sped[p_hori_lla,gpos_e_prep] = 100
    path_sped[p_hori_lla,gpos_e_half] = 100
    path_sped[p_hori_lla,gpos_e_near] = 100
    path_sped[p_hori_lla,gpos_e_extd] = 100
    path_sped[p_hori_lla,gpos_e_slow] = 100
    path_sped[p_hori_lla,gpos_e_tech] =  -1
    path_sped[p_hori_lla,gpos_r_tech] =  -1
    path_sped[p_hori_lla,gpos_r_slow] =  -1
    path_sped[p_hori_lla,gpos_r_extd] =  -1
    path_sped[p_hori_lla,gpos_r_back] = 100
    path_sped[p_hori_lla,gpos_r_near] = 100
    path_sped[p_hori_lla,gpos_r_half] = 100
    path_sped[p_hori_lla,gpos_r_prep] = 100
    path_sped[p_hori_lla,gpos_r_home] = 100

.* LLB
    path_sped[p_hori_llb,gpos_e_home] = 100
    path_sped[p_hori_llb,gpos_e_prep] = 100
    path_sped[p_hori_llb,gpos_e_half] = 100
    path_sped[p_hori_llb,gpos_e_near] = 100
    path_sped[p_hori_llb,gpos_e_extd] = 100
    path_sped[p_hori_llb,gpos_e_slow] = 100
    path_sped[p_hori_llb,gpos_e_tech] =  -1
    path_sped[p_hori_llb,gpos_r_tech] =  -1
    path_sped[p_hori_llb,gpos_r_slow] =  -1
    path_sped[p_hori_llb,gpos_r_extd] =  -1
    path_sped[p_hori_llb,gpos_r_back] = 100
    path_sped[p_hori_llb,gpos_r_near] = 100
    path_sped[p_hori_llb,gpos_r_half] = 100
    path_sped[p_hori_llb,gpos_r_prep] = 100
    path_sped[p_hori_llb,gpos_r_home] = 100

.* LLC
    path_sped[p_hori_llc,gpos_e_home] = 100
    path_sped[p_hori_llc,gpos_e_prep] = 100
    path_sped[p_hori_llc,gpos_e_half] = 100
    path_sped[p_hori_llc,gpos_e_near] = 100
    path_sped[p_hori_llc,gpos_e_extd] = 100
    path_sped[p_hori_llc,gpos_e_slow] = 100
    path_sped[p_hori_llc,gpos_e_tech] =  -1
    path_sped[p_hori_llc,gpos_r_tech] =  -1
    path_sped[p_hori_llc,gpos_r_slow] =  -1
    path_sped[p_hori_llc,gpos_r_extd] =  -1
    path_sped[p_hori_llc,gpos_r_back] = 100
    path_sped[p_hori_llc,gpos_r_near] = 100
    path_sped[p_hori_llc,gpos_r_half] = 100
    path_sped[p_hori_llc,gpos_r_prep] = 100
    path_sped[p_hori_llc,gpos_r_home] = 100

.* LLD
    path_sped[p_hori_lld,gpos_e_home] = 100
    path_sped[p_hori_lld,gpos_e_prep] = 100
    path_sped[p_hori_lld,gpos_e_half] = 100
    path_sped[p_hori_lld,gpos_e_near] = 100
    path_sped[p_hori_lld,gpos_e_extd] = 100
    path_sped[p_hori_lld,gpos_e_slow] = 100
    path_sped[p_hori_lld,gpos_e_tech] =  -1
    path_sped[p_hori_lld,gpos_r_tech] =  -1
    path_sped[p_hori_lld,gpos_r_slow] =  -1
    path_sped[p_hori_lld,gpos_r_extd] =  -1
    path_sped[p_hori_lld,gpos_r_back] = 100
    path_sped[p_hori_lld,gpos_r_near] = 100
    path_sped[p_hori_lld,gpos_r_half] = 100
    path_sped[p_hori_lld,gpos_r_prep] = 100
    path_sped[p_hori_lld,gpos_r_home] = 100

.END


.PROGRAM nt410oldprm()
.** OLD NT410 PARAMETERS **.
    p_hori_lla = p_hori_pta
    p_hori_llb = p_hori_ptb
    p_hori_llc = p_hori_vdb
    p_hori_lld = p_hori_vda
.**SPEED and ACCEl/DECEL
.* CS1
    path_sped[p_hori_cs1,gpos_e_home] = 100
    path_sped[p_hori_cs1,gpos_e_prep] = 100
    path_sped[p_hori_cs1,gpos_e_half] = 100
    path_sped[p_hori_cs1,gpos_e_near] = 100
    path_sped[p_hori_cs1,gpos_e_extd] = 100
    path_sped[p_hori_cs1,gpos_e_slow] = 100
    path_sped[p_hori_cs1,gpos_e_tech] =  -1
    path_sped[p_hori_cs1,gpos_r_tech] =  -1
    path_sped[p_hori_cs1,gpos_r_slow] =  -1
    path_sped[p_hori_cs1,gpos_r_extd] =  -1
    path_sped[p_hori_cs1,gpos_r_back] = 100
    path_sped[p_hori_cs1,gpos_r_near] = 100
    path_sped[p_hori_cs1,gpos_r_half] = 100
    path_sped[p_hori_cs1,gpos_r_prep] = 100
    path_sped[p_hori_cs1,gpos_r_home] = 100

    path_acce[p_hori_cs1,gpos_e_home] = 100
    path_acce[p_hori_cs1,gpos_e_prep] = 100
    path_acce[p_hori_cs1,gpos_e_half] =  60
    path_acce[p_hori_cs1,gpos_e_near] = 100
    path_acce[p_hori_cs1,gpos_e_extd] =  60
    path_acce[p_hori_cs1,gpos_e_slow] = 100
    path_acce[p_hori_cs1,gpos_e_tech] = 100
    path_acce[p_hori_cs1,gpos_r_tech] =  30
    path_acce[p_hori_cs1,gpos_r_slow] = 100
    path_acce[p_hori_cs1,gpos_r_extd] = 100
    path_acce[p_hori_cs1,gpos_r_back] =  30
    path_acce[p_hori_cs1,gpos_r_near] = 100
    path_acce[p_hori_cs1,gpos_r_half] =  60
    path_acce[p_hori_cs1,gpos_r_prep] = 100
    path_acce[p_hori_cs1,gpos_r_home] =  60

    path_dece[p_hori_cs1,gpos_e_home] = 100
    path_dece[p_hori_cs1,gpos_e_prep] = 100
    path_dece[p_hori_cs1,gpos_e_half] =  60
    path_dece[p_hori_cs1,gpos_e_near] = 100
    path_dece[p_hori_cs1,gpos_e_extd] =  50
    path_dece[p_hori_cs1,gpos_e_slow] = 100
    path_dece[p_hori_cs1,gpos_e_tech] = 100
    path_dece[p_hori_cs1,gpos_r_tech] =  30
    path_dece[p_hori_cs1,gpos_r_slow] = 100
    path_dece[p_hori_cs1,gpos_r_extd] = 100
    path_dece[p_hori_cs1,gpos_r_back] =  30
    path_dece[p_hori_cs1,gpos_r_near] = 100
    path_dece[p_hori_cs1,gpos_r_half] =  60
    path_dece[p_hori_cs1,gpos_r_prep] = 100
    path_dece[p_hori_cs1,gpos_r_home] =  60


.* CS2
    path_sped[p_hori_cs2,gpos_e_home] = 100
    path_sped[p_hori_cs2,gpos_e_prep] = 100
    path_sped[p_hori_cs2,gpos_e_half] = 100
    path_sped[p_hori_cs2,gpos_e_near] = 100
    path_sped[p_hori_cs2,gpos_e_extd] = 100
    path_sped[p_hori_cs2,gpos_e_slow] = 100
    path_sped[p_hori_cs2,gpos_e_tech] =  -1
    path_sped[p_hori_cs2,gpos_r_tech] =  -1
    path_sped[p_hori_cs2,gpos_r_slow] =  -1
    path_sped[p_hori_cs2,gpos_r_extd] =  -1
    path_sped[p_hori_cs2,gpos_r_back] = 100
    path_sped[p_hori_cs2,gpos_r_near] = 100
    path_sped[p_hori_cs2,gpos_r_half] = 100
    path_sped[p_hori_cs2,gpos_r_prep] = 100
    path_sped[p_hori_cs2,gpos_r_home] = 100

    path_acce[p_hori_cs2,gpos_e_home] = 100
    path_acce[p_hori_cs2,gpos_e_prep] = 100
    path_acce[p_hori_cs2,gpos_e_half] =  50
    path_acce[p_hori_cs2,gpos_e_near] =  50
    path_acce[p_hori_cs2,gpos_e_extd] =  50
    path_acce[p_hori_cs2,gpos_e_slow] = 100
    path_acce[p_hori_cs2,gpos_e_tech] = 100
    path_acce[p_hori_cs2,gpos_r_tech] =  30
    path_acce[p_hori_cs2,gpos_r_slow] = 100
    path_acce[p_hori_cs2,gpos_r_extd] = 100
    path_acce[p_hori_cs2,gpos_r_back] = 100
    path_acce[p_hori_cs2,gpos_r_near] = 100
    path_acce[p_hori_cs2,gpos_r_half] =  50
    path_acce[p_hori_cs2,gpos_r_prep] =  50
    path_acce[p_hori_cs2,gpos_r_home] =  50

    path_dece[p_hori_cs2,gpos_e_home] = 100
    path_dece[p_hori_cs2,gpos_e_prep] = 100
    path_dece[p_hori_cs2,gpos_e_half] =  50
    path_dece[p_hori_cs2,gpos_e_near] =  50
    path_dece[p_hori_cs2,gpos_e_extd] =  50
    path_dece[p_hori_cs2,gpos_e_slow] = 100
    path_dece[p_hori_cs2,gpos_e_tech] = 100
    path_dece[p_hori_cs2,gpos_r_tech] =  30
    path_dece[p_hori_cs2,gpos_r_slow] = 100
    path_dece[p_hori_cs2,gpos_r_extd] = 100
    path_dece[p_hori_cs2,gpos_r_back] = 100
    path_dece[p_hori_cs2,gpos_r_near] = 100
    path_dece[p_hori_cs2,gpos_r_half] =  50
    path_dece[p_hori_cs2,gpos_r_prep] =  50
    path_dece[p_hori_cs2,gpos_r_home] =  50


.* CS3
    path_sped[p_hori_cs3,gpos_e_home] = 100
    path_sped[p_hori_cs3,gpos_e_prep] = 100
    path_sped[p_hori_cs3,gpos_e_half] = 100
    path_sped[p_hori_cs3,gpos_e_near] = 100
    path_sped[p_hori_cs3,gpos_e_extd] = 100
    path_sped[p_hori_cs3,gpos_e_slow] = 100
    path_sped[p_hori_cs3,gpos_e_tech] =  -1
    path_sped[p_hori_cs3,gpos_r_tech] =  -1
    path_sped[p_hori_cs3,gpos_r_slow] =  -1
    path_sped[p_hori_cs3,gpos_r_extd] =  -1
    path_sped[p_hori_cs3,gpos_r_back] = 100
    path_sped[p_hori_cs3,gpos_r_near] = 100
    path_sped[p_hori_cs3,gpos_r_half] = 100
    path_sped[p_hori_cs3,gpos_r_prep] = 100
    path_sped[p_hori_cs3,gpos_r_home] = 100

    path_acce[p_hori_cs3,gpos_e_home] = 100
    path_acce[p_hori_cs3,gpos_e_prep] =  60
    path_acce[p_hori_cs3,gpos_e_half] =  60
    path_acce[p_hori_cs3,gpos_e_near] =  60
    path_acce[p_hori_cs3,gpos_e_extd] =  60
    path_acce[p_hori_cs3,gpos_e_slow] = 100
    path_acce[p_hori_cs3,gpos_e_tech] = 100
    path_acce[p_hori_cs3,gpos_r_tech] =  30
    path_acce[p_hori_cs3,gpos_r_slow] = 100
    path_acce[p_hori_cs3,gpos_r_extd] = 100
    path_acce[p_hori_cs3,gpos_r_back] = 100
    path_acce[p_hori_cs3,gpos_r_near] = 100
    path_acce[p_hori_cs3,gpos_r_half] =  50
    path_acce[p_hori_cs3,gpos_r_prep] =  50
    path_acce[p_hori_cs3,gpos_r_home] =  50

    path_dece[p_hori_cs3,gpos_e_home] = 100
    path_dece[p_hori_cs3,gpos_e_prep] =  60
    path_dece[p_hori_cs3,gpos_e_half] =  60
    path_dece[p_hori_cs3,gpos_e_near] =  60
    path_dece[p_hori_cs3,gpos_e_extd] =  60
    path_dece[p_hori_cs3,gpos_e_slow] = 100
    path_dece[p_hori_cs3,gpos_e_tech] = 100
    path_dece[p_hori_cs3,gpos_r_tech] =  30
    path_dece[p_hori_cs3,gpos_r_slow] = 100
    path_dece[p_hori_cs3,gpos_r_extd] = 100
    path_dece[p_hori_cs3,gpos_r_back] = 100
    path_dece[p_hori_cs3,gpos_r_near] = 100
    path_dece[p_hori_cs3,gpos_r_half] =  50
    path_dece[p_hori_cs3,gpos_r_prep] =  50
    path_dece[p_hori_cs3,gpos_r_home] =  50

  
.* CS4
    path_sped[p_hori_cs4,gpos_e_home] = 100
    path_sped[p_hori_cs4,gpos_e_prep] = 100
    path_sped[p_hori_cs4,gpos_e_half] = 100
    path_sped[p_hori_cs4,gpos_e_near] = 100
    path_sped[p_hori_cs4,gpos_e_extd] = 100
    path_sped[p_hori_cs4,gpos_e_slow] = 100
    path_sped[p_hori_cs4,gpos_e_tech] =  -1
    path_sped[p_hori_cs4,gpos_r_tech] =  -1
    path_sped[p_hori_cs4,gpos_r_slow] =  -1
    path_sped[p_hori_cs4,gpos_r_extd] =  -1
    path_sped[p_hori_cs4,gpos_r_back] = 100
    path_sped[p_hori_cs4,gpos_r_near] = 100
    path_sped[p_hori_cs4,gpos_r_half] = 100
    path_sped[p_hori_cs4,gpos_r_prep] = 100
    path_sped[p_hori_cs4,gpos_r_home] = 100

    path_acce[p_hori_cs4,gpos_e_home] = 100
    path_acce[p_hori_cs4,gpos_e_prep] = 100
    path_acce[p_hori_cs4,gpos_e_half] =  60
    path_acce[p_hori_cs4,gpos_e_near] = 100
    path_acce[p_hori_cs4,gpos_e_extd] =  60
    path_acce[p_hori_cs4,gpos_e_slow] = 100
    path_acce[p_hori_cs4,gpos_e_tech] = 100
    path_acce[p_hori_cs4,gpos_r_tech] =  30
    path_acce[p_hori_cs4,gpos_r_slow] = 100
    path_acce[p_hori_cs4,gpos_r_extd] = 100
    path_acce[p_hori_cs4,gpos_r_back] =  30
    path_acce[p_hori_cs4,gpos_r_near] = 100
    path_acce[p_hori_cs4,gpos_r_half] =  60
    path_acce[p_hori_cs4,gpos_r_prep] =  60
    path_acce[p_hori_cs4,gpos_r_home] = 100

    path_dece[p_hori_cs4,gpos_e_home] = 100
    path_dece[p_hori_cs4,gpos_e_prep] = 100
    path_dece[p_hori_cs4,gpos_e_half] =  60
    path_dece[p_hori_cs4,gpos_e_near] = 100
    path_dece[p_hori_cs4,gpos_e_extd] =  50
    path_dece[p_hori_cs4,gpos_e_slow] = 100
    path_dece[p_hori_cs4,gpos_e_tech] = 100
    path_dece[p_hori_cs4,gpos_r_tech] =  30
    path_dece[p_hori_cs4,gpos_r_slow] = 100
    path_dece[p_hori_cs4,gpos_r_extd] = 100
    path_dece[p_hori_cs4,gpos_r_back] =  30
    path_dece[p_hori_cs4,gpos_r_near] = 100
    path_dece[p_hori_cs4,gpos_r_half] =  60
    path_dece[p_hori_cs4,gpos_r_prep] =  60
    path_dece[p_hori_cs4,gpos_r_home] = 100


.* LLA
    path_sped[p_hori_lla,gpos_e_home] = 100
    path_sped[p_hori_lla,gpos_e_prep] = 100
    path_sped[p_hori_lla,gpos_e_half] = 100
    path_sped[p_hori_lla,gpos_e_near] = 100
    path_sped[p_hori_lla,gpos_e_extd] = 100
    path_sped[p_hori_lla,gpos_e_slow] = 100
    path_sped[p_hori_lla,gpos_e_tech] =  -1
    path_sped[p_hori_lla,gpos_r_tech] =  -1
    path_sped[p_hori_lla,gpos_r_slow] =  -1
    path_sped[p_hori_lla,gpos_r_extd] =  -1
    path_sped[p_hori_lla,gpos_r_back] = 100
    path_sped[p_hori_lla,gpos_r_near] = 100
    path_sped[p_hori_lla,gpos_r_half] = 100
    path_sped[p_hori_lla,gpos_r_prep] = 100
    path_sped[p_hori_lla,gpos_r_home] = 100

    path_acce[p_hori_lla,gpos_e_home] = 100
    path_acce[p_hori_lla,gpos_e_prep] = 100
    path_acce[p_hori_lla,gpos_e_half] = 100
    path_acce[p_hori_lla,gpos_e_near] = 100
    path_acce[p_hori_lla,gpos_e_extd] = 100
    path_acce[p_hori_lla,gpos_e_slow] = 100
    path_acce[p_hori_lla,gpos_e_tech] = 100
    path_acce[p_hori_lla,gpos_r_tech] =  30
    path_acce[p_hori_lla,gpos_r_slow] = 100
    path_acce[p_hori_lla,gpos_r_extd] = 100
    path_acce[p_hori_lla,gpos_r_back] = 100
    path_acce[p_hori_lla,gpos_r_near] = 100
    path_acce[p_hori_lla,gpos_r_half] = 100
    path_acce[p_hori_lla,gpos_r_prep] = 100
    path_acce[p_hori_lla,gpos_r_home] = 100

    path_dece[p_hori_lla,gpos_e_home] = 100
    path_dece[p_hori_lla,gpos_e_prep] = 100
    path_dece[p_hori_lla,gpos_e_half] = 100
    path_dece[p_hori_lla,gpos_e_near] = 100
    path_dece[p_hori_lla,gpos_e_extd] = 100
    path_dece[p_hori_lla,gpos_e_slow] = 100
    path_dece[p_hori_lla,gpos_e_tech] = 100
    path_dece[p_hori_lla,gpos_r_tech] =  30
    path_dece[p_hori_lla,gpos_r_slow] = 100
    path_dece[p_hori_lla,gpos_r_extd] = 100
    path_dece[p_hori_lla,gpos_r_back] = 100
    path_dece[p_hori_lla,gpos_r_near] = 100
    path_dece[p_hori_lla,gpos_r_half] = 100
    path_dece[p_hori_lla,gpos_r_prep] = 100
    path_dece[p_hori_lla,gpos_r_home] = 100

.* LLB
    path_sped[p_hori_llb,gpos_e_home] = 100
    path_sped[p_hori_llb,gpos_e_prep] = 100
    path_sped[p_hori_llb,gpos_e_half] = 100
    path_sped[p_hori_llb,gpos_e_near] = 100
    path_sped[p_hori_llb,gpos_e_extd] = 100
    path_sped[p_hori_llb,gpos_e_slow] = 100
    path_sped[p_hori_llb,gpos_e_tech] =  -1
    path_sped[p_hori_llb,gpos_r_tech] =  -1
    path_sped[p_hori_llb,gpos_r_slow] =  -1
    path_sped[p_hori_llb,gpos_r_extd] =  -1
    path_sped[p_hori_llb,gpos_r_back] = 100
    path_sped[p_hori_llb,gpos_r_near] = 100
    path_sped[p_hori_llb,gpos_r_half] = 100
    path_sped[p_hori_llb,gpos_r_prep] = 100
    path_sped[p_hori_llb,gpos_r_home] = 100

    path_acce[p_hori_llb,gpos_e_home] = 100
    path_acce[p_hori_llb,gpos_e_prep] = 100
    path_acce[p_hori_llb,gpos_e_half] =  70
    path_acce[p_hori_llb,gpos_e_near] = 100
    path_acce[p_hori_llb,gpos_e_extd] =  70
    path_acce[p_hori_llb,gpos_e_slow] = 100
    path_acce[p_hori_llb,gpos_e_tech] = 100
    path_acce[p_hori_llb,gpos_r_tech] =  30
    path_acce[p_hori_llb,gpos_r_slow] = 100
    path_acce[p_hori_llb,gpos_r_extd] = 100
    path_acce[p_hori_llb,gpos_r_back] = 100
    path_acce[p_hori_llb,gpos_r_near] = 100
    path_acce[p_hori_llb,gpos_r_half] =  65
    path_acce[p_hori_llb,gpos_r_prep] = 100
    path_acce[p_hori_llb,gpos_r_home] =  65

    path_dece[p_hori_llb,gpos_e_home] = 100
    path_dece[p_hori_llb,gpos_e_prep] = 100
    path_dece[p_hori_llb,gpos_e_half] =  70
    path_dece[p_hori_llb,gpos_e_near] = 100
    path_dece[p_hori_llb,gpos_e_extd] =  70
    path_dece[p_hori_llb,gpos_e_slow] = 100
    path_dece[p_hori_llb,gpos_e_tech] = 100
    path_dece[p_hori_llb,gpos_r_tech] =  30
    path_dece[p_hori_llb,gpos_r_slow] = 100
    path_dece[p_hori_llb,gpos_r_extd] = 100
    path_dece[p_hori_llb,gpos_r_back] = 100
    path_dece[p_hori_llb,gpos_r_near] = 100
    path_dece[p_hori_llb,gpos_r_half] =  65
    path_dece[p_hori_llb,gpos_r_prep] = 100
    path_dece[p_hori_llb,gpos_r_home] =  65
 

.* LLC
    path_sped[p_hori_llc,gpos_e_home] = 100
    path_sped[p_hori_llc,gpos_e_prep] = 100
    path_sped[p_hori_llc,gpos_e_half] = 100
    path_sped[p_hori_llc,gpos_e_near] = 100
    path_sped[p_hori_llc,gpos_e_extd] = 100
    path_sped[p_hori_llc,gpos_e_slow] = 100
    path_sped[p_hori_llc,gpos_e_tech] =  -1
    path_sped[p_hori_llc,gpos_r_tech] =  -1
    path_sped[p_hori_llc,gpos_r_slow] =  -1
    path_sped[p_hori_llc,gpos_r_extd] =  -1
    path_sped[p_hori_llc,gpos_r_back] = 100
    path_sped[p_hori_llc,gpos_r_near] = 100
    path_sped[p_hori_llc,gpos_r_half] = 100
    path_sped[p_hori_llc,gpos_r_prep] = 100
    path_sped[p_hori_llc,gpos_r_home] = 100

    path_acce[p_hori_llc,gpos_e_home] = 100
    path_acce[p_hori_llc,gpos_e_prep] =  70
    path_acce[p_hori_llc,gpos_e_half] =  70
    path_acce[p_hori_llc,gpos_e_near] =  70
    path_acce[p_hori_llc,gpos_e_extd] =  70
    path_acce[p_hori_llc,gpos_e_slow] = 100
    path_acce[p_hori_llc,gpos_e_tech] = 100
    path_acce[p_hori_llc,gpos_r_tech] =  30
    path_acce[p_hori_llc,gpos_r_slow] = 100
    path_acce[p_hori_llc,gpos_r_extd] = 100
    path_acce[p_hori_llc,gpos_r_back] = 100
    path_acce[p_hori_llc,gpos_r_near] = 100
    path_acce[p_hori_llc,gpos_r_half] =  70
    path_acce[p_hori_llc,gpos_r_prep] =  70
    path_acce[p_hori_llc,gpos_r_home] =  70

    path_dece[p_hori_llc,gpos_e_home] = 100
    path_dece[p_hori_llc,gpos_e_prep] =  70
    path_dece[p_hori_llc,gpos_e_half] =  70
    path_dece[p_hori_llc,gpos_e_near] =  70
    path_dece[p_hori_llc,gpos_e_extd] =  70
    path_dece[p_hori_llc,gpos_e_slow] = 100
    path_dece[p_hori_llc,gpos_e_tech] = 100
    path_dece[p_hori_llc,gpos_r_tech] =  30
    path_dece[p_hori_llc,gpos_r_slow] = 100
    path_dece[p_hori_llc,gpos_r_extd] = 100
    path_dece[p_hori_llc,gpos_r_back] = 100
    path_dece[p_hori_llc,gpos_r_near] = 100
    path_dece[p_hori_llc,gpos_r_half] =  70
    path_dece[p_hori_llc,gpos_r_prep] =  70
    path_dece[p_hori_llc,gpos_r_home] =  70


.* LLD
    path_sped[p_hori_lld,gpos_e_home] = 100
    path_sped[p_hori_lld,gpos_e_prep] = 100
    path_sped[p_hori_lld,gpos_e_half] = 100
    path_sped[p_hori_lld,gpos_e_near] = 100
    path_sped[p_hori_lld,gpos_e_extd] = 100
    path_sped[p_hori_lld,gpos_e_slow] = 100
    path_sped[p_hori_lld,gpos_e_tech] =  -1
    path_sped[p_hori_lld,gpos_r_tech] =  -1
    path_sped[p_hori_lld,gpos_r_slow] =  -1
    path_sped[p_hori_lld,gpos_r_extd] =  -1
    path_sped[p_hori_lld,gpos_r_back] = 100
    path_sped[p_hori_lld,gpos_r_near] = 100
    path_sped[p_hori_lld,gpos_r_half] = 100
    path_sped[p_hori_lld,gpos_r_prep] = 100
    path_sped[p_hori_lld,gpos_r_home] = 100

    path_acce[p_hori_lld,gpos_e_home] = 100
    path_acce[p_hori_lld,gpos_e_prep] =  60
    path_acce[p_hori_lld,gpos_e_half] =  60
    path_acce[p_hori_lld,gpos_e_near] = 100
    path_acce[p_hori_lld,gpos_e_extd] =  60
    path_acce[p_hori_lld,gpos_e_slow] = 100
    path_acce[p_hori_lld,gpos_e_tech] = 100
    path_acce[p_hori_lld,gpos_r_tech] =  30
    path_acce[p_hori_lld,gpos_r_slow] = 100
    path_acce[p_hori_lld,gpos_r_extd] = 100
    path_acce[p_hori_lld,gpos_r_back] = 100
    path_acce[p_hori_lld,gpos_r_near] = 100
    path_acce[p_hori_lld,gpos_r_half] =  60
    path_acce[p_hori_lld,gpos_r_prep] =  60
    path_acce[p_hori_lld,gpos_r_home] =  60

    path_dece[p_hori_lld,gpos_e_home] = 100
    path_dece[p_hori_lld,gpos_e_prep] =  60
    path_dece[p_hori_lld,gpos_e_half] =  60
    path_dece[p_hori_lld,gpos_e_near] = 100
    path_dece[p_hori_lld,gpos_e_extd] =  60
    path_dece[p_hori_lld,gpos_e_slow] = 100
    path_dece[p_hori_lld,gpos_e_tech] = 100
    path_dece[p_hori_lld,gpos_r_tech] =  30
    path_dece[p_hori_lld,gpos_r_slow] = 100
    path_dece[p_hori_lld,gpos_r_extd] = 100
    path_dece[p_hori_lld,gpos_r_back] = 100
    path_dece[p_hori_lld,gpos_r_near] = 100
    path_dece[p_hori_lld,gpos_r_half] =  60
    path_dece[p_hori_lld,gpos_r_prep] =  60
    path_dece[p_hori_lld,gpos_r_home] =  60

.END
.*****************************************************************
.******************************************
.** sycx_small_tp.as
.******************************************
.PROGRAM small_tp_menu
;** For NT570
    ZSTP_MENU  1:  3
    ZSTP_PAGE  1:  1, "JT  +Z  - +J4 +     -Z J2 -J4 J2"
    ZSTP_PAGE  1:101, "JT +J6     +F   S  -J6     -F   "
    ZSTP_PAGE  1:  2, "BASE +Z   ^          -Z  <-v->  "
    ZSTP_PAGE  1:102, "BASE +Z   ^          -Z  <-v->  "
    ZSTP_PAGE  1:  3, "TOOL +r   ^          -r  <-v->  "
.** harish 090601 m
.*  ZSTP_PAGE  1:103, "TOOL +r   ^          -r  <-v->  "
    ZSTP_PAGE  1:103, "TOOL +rL  +rR        -rL  -rR   "
    ZSTP_KEY   1:  1,  2,   -2,    4,   -4,    3,   -3
    ZSTP_KEY   1:101,  0,    0,    7,   -7,    6,   -6
    ZSTP_KEY   1:  2,101, -101,  102, -102,  103, -103
    ZSTP_KEY   1:102,101, -101,  102, -102,  103, -103
    ZSTP_KEY   1:  3,121, -121,  122, -122,  124, -124
.** harish 090601 m
.*  ZSTP_KEY   1:103,121, -121,  122, -122,  125, -125
    ZSTP_KEY   1:103,  0,    0,  154, -154,  144, -144
;** For NT410
    ZSTP_MENU  2:  3
    ZSTP_PAGE  2:  1, "JT  +Z  - +J4 +     -Z J2 -J4 J2"
    ZSTP_PAGE  2:101, "JT +J6          S  -J6          "
    ZSTP_PAGE  2:  2, "BASE +Z   ^          -Z  <-v->  "
    ZSTP_PAGE  2:102, "BASE +Z   ^          -Z  <-v->  "
    ZSTP_PAGE  2:  3, "TOOL +r   ^          -r  <-v->  "
    ZSTP_PAGE  2:103, "TOOL +r   ^          -r  <-v->  "
    ZSTP_KEY   2:  1,  2,   -2,    4,   -4,    3,   -3
    ZSTP_KEY   2:101,  0,    0,    0,    0,    6,   -6
    ZSTP_KEY   2:  2,101, -101,  102, -102,  103, -103
    ZSTP_KEY   2:102,101, -101,  102, -102,  103, -103
    ZSTP_KEY   2:  3,121, -121,  122, -122,  124, -124
    ZSTP_KEY   2:103,121, -121,  122, -122,  124, -124
.END
.******************************************
.** debug.as
.******************************************
.REALS
    mz.test = 0

    pz.tcp = 0

    pz.log = 1
    pz.alarm = 0
    pz.move = 0
    tz.move = 0
    pz.gcpos = 0
    pz.speed = 0
    pz.wgone = 0
    pz.gtime = 0
    pz.gstat = 0
    pz.grip = 0
    pz.gout = 0
    tz.gstat = 0
    tz.grip = 0
    pz.limit = 0
    pz.cur_pos = 0
    pz.event = 0
    pz.dev = 0
    pz.algn = 0
    tz.algn = 0
    dz.algn = 0
    pz.pintch = 0
    pz.smalltp = 0
    pz.map_ana = 0
.** harish 080804 a+
    pz.tslots = 0
.END
.******************************************
.** version.as
.******************************************
.STRINGS
.** SCRX10-027-9 m++
    $ver_fic = "2.00"
    $ver_app = "210A"
    $ver_tst = ""
    $ver_rot = "9"
.** SCRX10-027-9 m++  
.END
.******************************************
.** system.as
.******************************************
.PROGRAM system()
    ENDKILL
    TYPE ""
    TYPE	"**************************************************"
    TYPE	"**** Initializing Data ... Please wait        ****"
    TYPE	"**************************************************"
    CALL stop
.** harish 081119 a
    pc.started = OFF
    CALL system_task
    CALL app_system
    CALL netconf_init
    IF app_system THEN
	TYPE	"**************************************************"
	TYPE	"**** Initializing Data Finish Successfully !! ****"
	TYPE	"**************************************************"
    ELSE
	TYPE	"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	TYPE	"!!!! Error Initializing Data Error Error      !!!!"
	TYPE	"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    END
    AUTOSTART.PC ON
    IF SYSDATA(ZSIMENV) OR mz.test THEN
	CALL win
    END
.END
.PROGRAM system_task()
.***    IF SYSDATA(ZSIMENV) OR mz.test		GOTO dum
aut:
    tsk_arm[0] = SYSDATA(NUMROBOT)
    FOR cnt = 1 TO 4 STEP 1
	tsk_arm[cnt] = SYSDATA(SEMIARM,cnt)
    END
    RETURN
dum:
    TYPE   ""
    TYPE   "Sycamore X <1>"
    TYPE   "AA+AL      <2>"
    TYPE   "WET+AT4+AL <3>"
    TYPE   "Sycamore   <9>"
    TYPE   "Auto       <A>"
    PROMPT "Input Key:",$key
    SCASE $key OF
      SVALUE "1"
	tsk_arm[0] = 3
	tsk_arm[1] = arm_nt570
	tsk_arm[2] = arm_at
	tsk_arm[3] = arm_aln
	tsk_arm[4] = 0
      SVALUE "2"
	tsk_arm[0] = 3
	tsk_arm[1] = arm_a
	tsk_arm[2] = arm_a
	tsk_arm[3] = arm_aln
	tsk_arm[4] = 0
      SVALUE "3"
	tsk_arm[0] = 3
	tsk_arm[1] = arm_wet
	tsk_arm[2] = arm_ats
	tsk_arm[3] = arm_aln
	tsk_arm[4] = 0
      SVALUE "4"
	tsk_arm[0] = 2
	tsk_arm[1] = arm_nt530
	tsk_arm[2] = arm_nt520
	tsk_arm[3] = 0
	tsk_arm[4] = 0
      SVALUE "A","a"
	GOTO aut
     ANY :
	GOTO dum
    END
.END
.PROGRAM sys_switch()
    ZTPDISCON ON
    FOR tmp = 1 TO 4 STEP 1
	IF rob_tsk[tmp] THEN
	    PREFETCH.SIGINS rob_tsk[tmp]: ON
	    CYCLE.STOP      rob_tsk[tmp]: ON
	    STP_ONCE        rob_tsk[tmp]: OFF
	END
    END
    IF SYSDATA(ZSIMENV) THEN
	ZINSIG ON
    ELSE
	ZINSIG OFF
    END
.END
.******************************************
.** pc_util.as
.******************************************
.PROGRAM win()
    pz.log = on
.** harish 081119 a
    pc.started = ON
    CALL pc_start
.END
.PROGRAM sim()
    pz.log = on
.** harish 081119 a
    pc.started = ON
    CALL pc_start
.END
.PROGRAM start()
    pz.log = off
.** harish 081119 a
    pc.started = ON
    CALL pc_start
.END
.PROGRAM autostart.pc()
    pz.log = off
.** harish 081119 a
    pc.started = ON
    CALL pc_start
.END
.PROGRAM stop()
.** harish 081119 a++
    IF NOT pc.started
;      CALL event_entry(pc3,$ev_refuse,$ev2_refuse)   .** tcp socket error 081205
;      CALL event_entry(pc4,$ev_refuse,$ev2_refuse)   .** tcp socket error 081205
.** harish 081210 a++
       pc_stop_evt[pc3] = 1
       pc_stop_evt[pc4] = 1
       TWAIT 0.5
.** harish 081210 a--
    END
.** harish 081119 a--
    FOR tmp = 1 TO SYSDATA(NUMROBOT) STEP 1
	MC HOLD tmp:
    END
    FOR tmp = 1 TO SYSDATA(NUMPC) STEP 1
	IF (TASKNO<>(1000+tmp)) THEN
	    PCABORT tmp:
	END
    END
    TWAIT 1
    FOR tmp = 1 TO SYSDATA(NUMROBOT) STEP 1
	MC KILL tmp:
	MC ZPOWER tmp: OFF
    END
    FOR tmp = 1 TO SYSDATA(NUMPC) STEP 1
	IF (TASKNO<>(1000+tmp)) THEN
	    MC PCKILL tmp:
	END
    END
.END
.PROGRAM zloadstart.pc()
.** harish 081119 a
    pc.started = ON
    CALL system
.END
.******************************************
.** pc_main.as
.******************************************
.** harish 081119 a++
.PROGRAM evt_intlck(.tsk,.evt)
    IF SWITCH("POWER",tsk_rob[.tsk]) AND (ERROR:tsk_rob[.tsk]==0)THEN
        .evt = ON
    END
    IF NOT SWITCH("POWER",tsk_rob[.tsk]) AND (ERROR:tsk_rob[.tsk]<>0)THEN
	.err = SYSDATA(ERROR.CODE,tsk_rob[.tsk])
	.err = ABS(.err)
	CASE .err MOD 10000
	  VALUE 1027,3808,1135 :
	    IF .evt 
	      CALL event_entry(.tsk,$EV_INTERLOCK,$EV2_INTERLOCK)
              .evt = OFF
	    END
	END
    END
.END
.** harish 081119 a--
.** harish 081203 a++
.PROGRAM evt_tchrpt(.tsk,.evt)
   IF NOT GET_ROBSEL
       IF .evt
           .hst = tsk_hst[.tsk]
           tsk_hst[.tsk] = IFELSE(.tsk==pc3,host_tcp1,host_tcp2)
           CALL event_entry(.tsk,$EV_ACCEPT,$EV2_ACCEPT)
           .evt = OFF
           tsk_hst[.tsk] = .hst
       END
   ELSE
       IF NOT .evt
	   .hst = tsk_hst[.tsk]
           tsk_hst[.tsk] = IFELSE(.tsk==pc3,host_tcp1,host_tcp2)
           CALL event_entry(.tsk,$EV_REFUSE,$EV2_REFUSE)
           .evt = ON
	   tsk_hst[.tsk] = .hst
       END
   END
.END    
.** harish 081203 a--
.PROGRAM pc_start()
.** SCRX10-027-8 a
    ZLANG OFF

    ENDKILL
    CALL stop
.** harish 081119 a
    pc.started = OFF
;
    CALL pc_init
;
.** harish 081210 a
    tcp_init = 0
    IF tsk_rob[pc3] THEN
	PCEXECUTE 3: pc_main
    END
.** harish 090701-1 a++
    IF SYSDATA(ZSIMENV) THEN
        tcp_init = ON
    END
.** harish 090701-1 a--
.** harish 081210 a
    WAIT tcp_init
    IF tsk_rob[pc4] THEN
	PCEXECUTE 4: pc_main
    END
.** harish 090323 a
    PCEXECUTE 6: autostart6.pc
.END
.PROGRAM pc_main()
    .tsk = IFELSE(TASKNO==1003,pc3,pc4)
    CALL pc_main.init(.tsk)
.** harish 081203 a+
    .evt_tchrpt[.tsk] = OFF
.** harish 081119 a++
    .evt_entry[.tsk] = ON
    CALL event_entry(.tsk,$ev_start,$ev2_start)
    CALL event_entry(.tsk,$ev_accept,$ev2_accept)
.** harish 081119 a--
Loop:
.** harish 081119 a+
    CALL evt_intlck(.tsk,.evt_entry[.tsk])
.** harish 081203 a+
    CALL evt_tchrpt(.tsk,.evt_tchrpt[.tsk])
.** harish 081210 a++
    IF pc_stop_evt[.tsk] THEN
      CALL event_entry(.tsk,$ev_refuse,$ev2_refuse)
      pc_stop_evt[.tsk] = 0
    END
.** harish 081210 a--
    CALL pc_recv(.tsk)
    IF tsk_num[.tsk] THEN
	CALL pc_comd(.tsk)
    END
    CALL pc_send(.tsk)
    CALL pc_event(.tsk)
    TWAIT 0.016
    GOTO Loop
.END
.******************************************
.** pc_init.as
.******************************************
.PROGRAM pc_init()
    CALL sys_switch
    CALL port_init
    CALL config_init
    CALL pc_recv_init
    CALL pc_comd_init
    CALL pc_send_init
    CALL mode_init
    CALL alarm_init
    CALL log_init
    CALL speed_init
    CALL servo_init
    CALL fan_init
    CALL event_init
    CALL ver_init
    CALL teach_init
    CALL small_tp_init
.** SCRX10-027-4 a++
    CALL cfg_change.save(5)
    CALL cfg_change.save(6)
.** SCRX10-027-4 a--
.END
.PROGRAM pc_main.init(.tsk)
    CALL host_tcp.init(.tsk)
    CALL host_com.init(.tsk)
;***CALL host_stp.init(.tsk)
    CALL host_dum.init(.tsk)
.END
.******************************************
.** pc_recv.as
.******************************************
.PROGRAM pc_recv_init()
    ZKSCS_SYX $c_stx,$c_mes,$c_dat,$c_end,$c_sum,$c_etx
.** harish 081125=>081216 m++
    ZARRAYSET $hst_get[0]="",16
    ZARRAYSET $hst_sav[0]="",16
    ZARRAYSET $hst_now[0]="",16
    ZARRAYSET $hst_old[0]="",16
.** harish 081125 m--
.*    ZARRAYSET $hst_get[1]="",8
.*    ZARRAYSET $hst_sav[1]="",8
.*    ZARRAYSET $hst_now[1]="",8
.*    ZARRAYSET $hst_old[1]="",8
.END
.PROGRAM pc_recv(.tsk)
    tsk_num[.tsk] = 0
;;;;TCPIP
    CALL host_tcp.proc(.tsk,OFF,.hst)
    IF ($hst_get[.hst] <> "")OR($hst_sav[.hst]<>"")	GOTO ana
;;;;COM Serial
    CALL host_com.proc(.tsk,OFF,.hst)
    IF ($hst_get[.hst] <> "")OR($hst_sav[.hst]<>"")	GOTO ana
;;;;Dummy
    CALL host_dum.proc(.tsk,OFF,.hst)
    IF ($hst_get[.hst] <> "")OR($hst_sav[.hst]<>"")	GOTO ana
;;;;Small TP
;;    CALL host_stp.proc(.tsk,.hst)
    CALL host_stp.proc(.tsk)
    RETURN
ana:
    .$sav = $hst_sav[.hst]
    IF ($hst_get[.hst]<>"") THEN
	UTIMER @hst_tim[.hst] = 0
    END
    tsk_hst[.tsk] = .hst
    ZKSCS_ANA tsk_num[.tsk],$tsk_mes[.tsk],$tsk_dat[.tsk,0],$tsk_sum[.tsk],$tsk_cal[.tsk],$tsk_del[.tsk],$tsk_ana[.tsk],$hst_sav[.hst]=$hst_sav[.hst],$hst_get[.hst]
    IF tsk_num[.tsk] > 0 THEN
	IF $tsk_del[.tsk] <> "" THEN
	    CALL log_cmd(.tsk,0)
	END
	STR_3DIV .$dum[1],.dum = $tsk_ana[.tsk],$c_mes,$c_end
	$hst_now[.hst] = .$dum[2]
;****** .mes = VAL($tsk_mes[.tsk])
;****** MADA MessageID Check **
	$tsk_cmd[.tsk] = $TOUPPER($tsk_dat[.tsk,0])
	CALL log_cmd(.tsk,1)
    ELSEIF tsk_num[.tsk] THEN
	CASE tsk_num[.tsk] OF
	  VALUE -1 :
.********** 入力文字列が２５５文字以上
	    $tsk_ana[.tsk] = .$sav+$LEFT($hst_get[.hst],255-LEN(.$sav))
	    CALL send_nak(.tsk,$ack_long)
	  VALUE -2 :
.********** 削除文字列がある
.********** 削除された文字列がありAck送信はしないがLOGに残す
	    CALL log_cmd(.tsk,0)
	  VALUE -3:
.********** 終端文字列が不完全/LFの前にCRがない
	    $tsk_ana[.tsk] = $tsk_del[.tsk]
	    CALL send_nak(.tsk,$ack_err_fm)
	  VALUE -4:
.********** 開始文字列がない
	    $tsk_ana[.tsk] = $tsk_del[.tsk]
	    CALL send_nak(.tsk,$ack_err_fm)
	  VALUE -5:
.********** 終了文字列がない
	    IF $tsk_del[.tsk]<>"" THEN
		CALL log_cmd(.tsk,0)
	    END
	    CALL send_nak(.tsk,$ack_err_fm)
	  VALUE -6:
.********** チェックサム文字数が違う
	    IF $tsk_del[.tsk]<>"" THEN
		CALL log_cmd(.tsk,0)
	    END
	    CALL send_nak(.tsk,$ack_err_fm)
	  VALUE -7:
.********** チェックサムエラー
	    IF $tsk_del[.tsk]<>"" THEN
		CALL log_cmd(.tsk,0)
	    END
	    CALL send_nak(.tsk,$ack_err_cs)
	  VALUE -8:
.********** メッセージID分割文字がない
	    IF $tsk_del[.tsk]<>"" THEN
		CALL log_cmd(.tsk,0)
	    END
	    CALL send_nak(.tsk,$ack_err_fm)
	END
	tsk_num[.tsk] = 0
    ELSE
	IF UTIMER(@hst_tim[.hst]) > vtp.t1[.tsk] THEN
	    IF vc.mode[.tsk] BAND m_tout THEN
		$tsk_ana[.tsk] = $hst_sav[.hst]
		CALL send_nak(.tsk,$ack_t1tout)
		$hst_sav[.hst] = ""
	    END
	END
    END
    $hst_get[.hst] = ""
.END
.******************************************
.** pc_send.as
.******************************************
.PROGRAM pc_send_init()
    ZARRAYSET  tsk_hst[1] = 0,6

    ZARRAYSET  snd_alm[1] =  0,4
    ZARRAYSET $snd_alm[1] = "",6

    $snd_ack[pc3] = ""
    $snd_ack[pc4] = ""

    ZARRAYSET  snd_res[1] =  0,4
    ZARRAYSET $snd_res[1] = "",6

    $snd_dat[pc3] = ""
    $snd_dat[pc4] = ""

    $snd_snd[pc3] = ""
    $snd_snd[pc4] = ""
.END
.PROGRAM pc_send(.tsk)
    FOR .rob = 1 TO 4 STEP 1
.******* harish 090327-1 m
.** SCRY10-003-3 d
.**	IF (bit[.rob] BAND tsk_rob[.tsk]) OR (prepos.hst[.rob]==tsk_hst[.rob] AND tsk_hst[.tsk] == tsk_hst[.rob]) THEN
.** SCRY10-003-3 a++
	IF (bit[.rob] BAND tsk_rob[.tsk]) THEN
          IF (prepos.hst[.rob] == tsk_hst[.tsk] OR tsk_hst[.rob] == tsk_hst[.tsk]) THEN
.** SCRY10-003-3 a--
	    IF snd_res[.rob] THEN
		 tsk_hst[.tsk] =  tsk_hst[.rob]
		$tsk_mes[.tsk] = $tsk_mes[.rob]
		$snd_res[.tsk] = $snd_res[.rob]
		CALL send_res_data(.tsk)
		 tsk_hst[.rob] = 0
		$tsk_mes[.rob] = ""
		$snd_res[.rob] = ""
		 snd_res[.rob] = OFF
	    END
	    IF snd_alm[.rob] THEN
		 tsk_hst[.tsk] =  tsk_hst[.rob]
		$tsk_mes[.tsk] = $tsk_mes[.rob]
		$alm_put[.tsk] = $alm_put[.rob]
		CALL alarm_put(.tsk,.rob,alm_ext[.rob])
		$alm_put[.rob] = ""
		 snd_alm[.rob] = OFF
	    END
	    IF tsk_cmd[.rob] THEN
		IF SWITCH("CS",rob_tsk[.rob]) THEN
		    .hnd = 1
		    IF (.rob<=2)AND(vtp.wafgonechk[tsk_tsk[.tsk]])AND(wgone.chk[.rob,.hnd]) THEN
			CALL gripper_stat(.rob,.hnd,.out,.sta)
			IF .sta<>wgone.sta[.rob,.hnd] THEN
			    IF wgone.chk[.rob,.hnd] THEN
.*                                                      /* SCRY11-032-7 a */
                                wgone.chk[.rob,.hnd] = OFF
				MC HOLD rob_tsk[.rob]:
				 tsk_hst[.tsk] =  tsk_hst[.rob]
				$tsk_mes[.tsk] = $tsk_mes[.rob]
				CALL alarm_str(.tsk,.rob,.hnd,$ae_wafer_gone)
				CALL send_stop_res(.tsk,.rob)
				CALL dev_free(.rob)
				tsk_cmd[.rob] = OFF
			    ELSE
				TYPE "wgone timing"
			    END
			END
		    END
		ELSEIF (tsk_hst[.rob]==host_stp1)OR(tsk_hst[.rob]==host_stp2) THEN
.****************** Check動作中
		    IF (.rob<>GET_ROBSEL)OR(GET_SMTP_TOP<>3)THEN
.*********************** ロボット選択スイッチが変更またはメニュー画面が変更された場合、コマンドは破棄する
			tsk_cmd[.rob] = OFF
			TYPE "CheckMode robot or menu change"
		    END
		ELSE
		     tsk_hst[.tsk] =  tsk_hst[.rob]
		    $tsk_mes[.tsk] = $tsk_mes[.rob]
		    CALL alarm_sys(.tsk,.rob,0,0)
		    CALL send_stop_res(.tsk,.rob)
		    CALL dev_free(.rob)
		    tsk_cmd[.rob] = OFF
		END
	    END
.******* harish 090327-1 a++
	    IF (.rob<=2) AND (prepos.hst[.rob]==tsk_hst[.rob]) THEN
	      IF ( PrePos.cmd[.rob] ) AND ( NOT SWITCH("CS",rob_tsk[.rob]) ) THEN
		CALL alarm_sys(.tsk,.rob,0,0)
		tsk_err[.rob] = tsk_err[.tsk]
.*		failed system error, send event message.
		CALL prpos.event_rob(.rob, $EV_PRPOS_ERR[.rob], $EV2_PRPOS_ERR)
		CALL prpos.event_out(.rob)
		PrePos.cmd[.rob] = OFF
	      END
	    END
.******* harish 090327-1 a--
	    IF tsk_evt[.rob] THEN
		tsk_hst[.tsk] = tsk_evt[.rob]
.*************** harish 090327-1 m++
              IF (.rob<=2)
		IF ( $event1[.rob,2] <> $EV_PRPOS_SUC[.rob] AND $event2[.rob,2] <> $EV2_PRPOS_SUC ) OR ( NOT tsk_cmd[.rob] ) THEN
		    CALL event_entry(.tsk,$event1[.rob,2],$event2[.rob,2])
		END
.*************** harish 090327-1 m--
.*		CALL event_entry(.tsk,$event1[.rob,2],$event2[.rob,2])
		$event1[.rob,1] = ""
		$event2[.rob,2] = ""
		tsk_evt[.rob] = 0
.*************** harish 090327-1 a
	      END	
	    END
.** SCRY10-003-3 a
	  END
	END
    END
.END


.PROGRAM send_nak(.tsk,.$nak)
    $tsk_mes[.tsk] = $IFELSE($tsk_mes[.tsk]=="","000",$tsk_mes[.tsk])
    IF 123<LEN($tsk_ana[.tsk]) THEN
.*******文字列が長い場合log_cmdのCR/LFのREPLACEで255を超える可能性があるので
.*******分割してLOGGINGしておく。念のため
	.$str = $tsk_ana[.tsk]
	$tsk_ana[.tsk] = $LEFT(.$str,123)
	CALL log_cmd(.tsk,1)
	$tsk_ana[.tsk] = $MID(.$str,124)
	CALL log_cmd(.tsk,1)
	$tsk_ana[.tsk] = .$str
    ELSE
	CALL log_cmd(.tsk,1)
    END
    $snd_ack[.tsk] = .$nak
    CALL send_ack_data(.tsk)
.END
.PROGRAM send_ack(.tsk)
    $snd_ack[.tsk] = $ack_ok
    CALL send_ack_data(.tsk)
.END
.PROGRAM send_ack_data(.tsk)
    $snd_dat[.tsk] = $snd_ack[.tsk]
    IF (tsk_hst[.tsk]==host_stp1)OR(tsk_hst[.tsk]==host_stp2) THEN
	IF $snd_ack[.tsk]==$ack_ok	GOTO exit
    END
    CALL send_data_make(.tsk,$tsk_mes[.tsk])
    IF (tsk_hst[.tsk]==host_dum1)OR(tsk_hst[.tsk]==host_dum2) THEN
	$dum_ack[tsk_hst[.tsk]] = $snd_ack[.tsk]
    END
exit:
    $snd_ack[.tsk] = ""
.END

.PROGRAM send_stop_res(.tsk,.rob)
    CALL make_res_ng(.rob)
    $snd_res[.tsk] = $snd_res[.rob]
    CALL send_res_data(.tsk)
.END


.PROGRAM send_res_data(.tsk)
.** INPUT $snd_res[.tsk]
    $snd_dat[.tsk] = $snd_res[.tsk]
    IF (tsk_hst[.tsk]==host_stp1)OR(tsk_hst[.tsk]==host_stp2) THEN
	IF $snd_dat[.tsk]==$res_ok THEN
	    $snd_dat[.tsk] = "Complete!"
	ELSEIF $snd_dat[.tsk]==$res_ng THEN
	    GOTO ext
	END
    END
    CALL send_data_make(.tsk,$tsk_mes[.tsk])
    IF (tsk_hst[.tsk]==host_dum1)OR(tsk_hst[.tsk]==host_dum2) THEN
	$dum_res[tsk_hst[.tsk]] = $snd_res[.tsk]
    END
ext:
    $snd_res[.tsk] = ""
.END

.PROGRAM send_data_make(.tsk,.$mes)
.** Input  $snd_dat[.tsk]
.**         tsk_hst[.tsk]
.**
    $snd_snd[.tsk] = .$mes + $c_mes + $snd_dat[.tsk]
    .$sum = $REPLACE(($ENCODE(/H2,STR_CSUM($snd_snd[.tsk],^HFF)))," ","0",-1)
    $snd_snd[.tsk] = $c_stx + $snd_snd[.tsk] + $c_end + .$sum + $c_etx
    CASE tsk_hst[.tsk] OF
      VALUE host_tcp1,host_tcp2 :
	CALL host_tcp.send(.tsk,tsk_hst[.tsk])
      VALUE host_com1,host_com2 :
	CALL host_com.send(.tsk,tsk_hst[.tsk])
      VALUE host_stp1,host_stp2 :
	$snd_snd[.tsk] = $snd_dat[.tsk]
	zt_chk_cmd = OFF
	LTPRINT $snd_snd[.tsk],-1
.** harish 081119 a++
      VALUE host_all0 :
        CASE .tsk
	  VALUE PC3:
            CALL host_tcp.send(.tsk,host_tcp1)
	    CALL host_com.send(.tsk,host_com1)
          VALUE PC4
            CALL host_tcp.send(.tsk,host_tcp2) 
            CALL host_com.send(.tsk,host_com2)
	END
.** harish 081119 a--
    END
    CALL log_cmd(.tsk,-1)
    $snd_dat[.tsk] = ""
.END



.PROGRAM make_res_ng(.tsk)
    SCASE $tsk_cmd[.tsk] OF
      SVALUE "MAP","MAPSLOT"
	CALL map_err(.tsk,tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1])
      ANY :
	$snd_res[.tsk] = $res_ng
    END
.END
.******************************************
.** pc_comd.as
.******************************************
.PROGRAM pc_comd_init()
    ZARRAYSET tsk_cmd[1]=0,6
    ZARRAYSET tsk_use[1]=0,4
    ZARRAYSET tsk_num[5]=0,2
    ZARRAYSET tsk_mod[5]=0,2
    ZARRAYSET tsk_tsk[1]=0,4
              tsk_tsk[pc3] = pc3
              tsk_tsk[pc4] = pc4
    ZARRAYSET tsk_err[1]=0,6
    ZARRAYSET tsk_evt[1]=0,4
    FOR tmp = 1 TO 6 STEP 1
    ZARRAYSET tsk_dat[tmp,0] = 0,d_max
    END
    ZARRAYSET tsk_hst[1]=0,6
    ZARRAYSET $tsk_cmd[1]   ="",6
    ZARRAYSET $tsk_dat[5,0] = "",d_max
    ZARRAYSET $tsk_dat[6,0] = "",d_max
    ZARRAYSET $tsk_mes[1]   ="",6
    ZARRAYSET $tsk_sum[5]   ="",2
    ZARRAYSET $tsk_cal[5]   ="",2
    ZARRAYSET $tsk_del[5]   ="",2
    ZARRAYSET $tsk_ana[5]   ="",2
.** harish 090327-1 a++
    ZARRAYSET   PrePos[1]=0,6
    ZARRAYSET   PrePos.cmd[1]=0,6
    ZARRAYSET   prepos.hst[1]=0,6
    ZARRAYSET   $PrePos.host[1]="",6
    ZARRAYSET   $PrePos.mes[1]="",6
.** harish 090327-1 a--
.END
.PROGRAM pc_comd(.tsk)
    tsk_cmd[.tsk] = 0
    tsk_num[.tsk] = tsk_num[.tsk]-1
    tsk_err[.tsk] = 0
    ZARRAYSET tsk_dat[.tsk,0] = 0,d_max
    CALL pc_comd.sel(.tsk)
    IF tsk_cmd[.tsk] THEN
	CALL check_bsy_mode(.tsk,tsk_cmd[.tsk],tsk_mod[.tsk])
	IF ($snd_ack[.tsk]=="")AND($snd_res[.tsk]=="") THEN
	    IF zt_chk_cmd THEN
		CALL chk_prime(.tsk,tsk_cmd[.tsk])
		zt_chk_cmd = OFF
	    ELSE
		CALL is_servo(.tsk,tsk_cmd[.tsk],ON)
		IF NOT is_servo[.tsk]	GOTO snd
	    END
	    FOR .rob = 1 TO 4 STEP 1
		IF (tsk_cmd[.tsk] BAND bit[.rob]) THEN
		              tsk_tsk[.rob]   =  .tsk
		    ZARRAYCPY tsk_dat[.rob,0] =  tsk_dat[.tsk,0],d_max
		              tsk_hst[.rob]   =  tsk_hst[.tsk]
		             $tsk_cmd[.rob]   = $tsk_cmd[.tsk]
		             $tsk_mes[.rob]   = $tsk_mes[.tsk]
		              tsk_cmd[.rob]   =  tsk_cmd[.tsk]
		END
	    END
	END
    END
snd:
    IF $snd_ack[.tsk] <> "" THEN
	CALL send_ack_data(.tsk)
    END
    IF $snd_res[.tsk] <> "" THEN
	CALL send_res_data(.tsk)
    END
    $hst_old[tsk_hst[.tsk]] = $hst_now[tsk_hst[.tsk]]
.END
.PROGRAM pc_comd.sel(.tsk)
    IF VAL($tsk_mes[.tsk])<0 THEN
	IF $tsk_cmd[.tsk] == "ACK" THEN
	    CALL event_ack(.tsk)
	    GOTO Exit
	END
	GOTO efm
    END
    SCASE $tsk_cmd[.tsk] OF
.********************
.** Wafer Handling **
.********************
.** harish 090327-1 a++
      SVALUE "GETWAFPREPOS","PUTWAFPREPOS":
	CALL getputprpos_cmd(.tsk)
      SVALUE "TRWAFPREPOS":
	CALL trwafprpos_cmd(.tsk)
.** harish 090327-1 a--
      SVALUE "GETWAF","PUTWAF" :
	CALL getput_cmd(.tsk)
      SVALUE "GETX","PUTX" :
	CALL getputx_cmd(.tsk)
      SVALUE "TRWAF"
	CALL trwaf_cmd(.tsk)
      SVALUE "MOVTOLOC" :
	CALL movtoloc_cmd(.tsk)
.** harish 090515-1 a++
      SVALUE "SCAN"
        CALL scan_cmd(.tsk)
.** harish 090515-1 a--
.********************************
.** Wafer Handling Test Motion **
.********************************
      SVALUE "TGETWAF","TPUTWAF" :
	CALL tgetput_cmd(.tsk)
      SVALUE "QRYTESTMOTION","QTM":
	CALL qrytestmotion(.tsk)
.*************
.** Mapping **
.*************
      SVALUE "MAP" :
	CALL map_cmd(.tsk,OFF)
      SVALUE "MAPSLOT" :
	CALL map_cmd(.tsk,ON)
      SVALUE "FMAP" :
	CALL qrymap(.tsk,map_res,OFF)
      SVALUE "QRYMAPTHICK":
	CALL qrymap(.tsk,map_ttt,OFF)
      SVALUE "QRYMAPPITCH":
	CALL qrymap(.tsk,map_gap,OFF)
      SVALUE "QRYMAPZPOS":
	CALL qrymap(.tsk,map_acc,OFF)
      SVALUE "QRYMAPSLOTZPOS":
	CALL qrymap(.tsk,map_acc,ON)
.** harish 081120 a++
.**************
.** Diagnose **
.**************
      SVALUE "DIAGNOSE" :
        CALL diagnose_cmd(.tsk)
.** harish 081120 a--
.*************
.** Homing  **
.*************
      SVALUE "HOME" :
	CALL home_cmd(.tsk)
.*************
.** Aligner **
.*************
      SVALUE "ALGN" :
	CALL algn_cmd(.tsk)
.*******************
.** Manual Motion **
.*******************
      SVALUE "MOVREL":
	CALL m_relabs_cmd(.tsk,-1)
      SVALUE "MOVABS":
	CALL m_relabs_cmd(.tsk,+1)


.*********************
.** Program Control **
.*********************
      SVALUE "INIT":
	CALL init_cmd(.tsk)
      SVALUE "STOP":
	CALL stop_cmd(.tsk)
      SVALUE "SETSPD":
	CALL setspd_cmd(.tsk)
      SVALUE "QRYSPD":
	CALL qryspd_cmd(.tsk)
      SVALUE "MODE":
	CALL mode_cmd(.tsk)
      SVALUE "QRYMODE":
	CALL qrymode_cmd(.tsk)
      SVALUE "MOTRPWR" :
	CALL motrpwr_cmd(.tsk)

.************
.** Config **
.************
      SVALUE "SAVECFG":
	CALL savecfg_cmd(.tsk)
      SVALUE "QRYCFG":
	CALL qrycfg_cmd(.tsk)
      SVALUE "SETCFG":
	CALL setcfg_cmd(.tsk)
      SVALUE "LOCKCFG":
	CALL lockcfg_cmd(.tsk)

      SVALUE "MOVLIM":
	CALL movlim_cmd(.tsk)
      SVALUE "AJPLIM":
	CALL ajplim_cmd(.tsk)
      SVALUE "COLDET":
	CALL coldet_cmd(.tsk)

.*********************
.** Teaching Motion **
.*********************
      SVALUE "MOVPREPOS":
	CALL movprepos_cmd(.tsk)
      SVALUE "PINTCH":
	CALL pintch_cmd(.tsk)

.**************
.** Teaching **
.**************
      SVALUE "TCHPOS" :
	CALL tchpos_cmd(.tsk)
      SVALUE "SAVPOS","SAVP_ALL" :
	CALL tchext_cmd(.tsk,teach_savpos)
      SVALUE "CANCELTCH","RESPOS" :
	CALL tchext_cmd(.tsk,teach_respos)
      SVALUE "RMVPOS" :
	CALL tchext_cmd(.tsk,teach_rmvpos)
      SVALUE "CLEARTCH" :
	CALL cleartch_cmd(.tsk)

      SVALUE "ADJUSTPOS","APOS":
	CALL adjustpos_cmd(.tsk)
      SVALUE "SETPOS" :
	CALL setpos_cmd(.tsk)

      SVALUE "SETDEFAULTPOS" :
	CALL setdefaultpos(.tsk)
      SVALUE "QRYDEFAULTPOS" :
	CALL qrydefaultpos(.tsk)


.**************
.** Position **
.**************
      SVALUE "QRYCPOS" :
	CALL qrycpos_cmd(.tsk)
      SVALUE "QRYPOS" :
	CALL qrypos_cmd(.tsk,OFF)
      SVALUE "QRYWPOS","QRYTCHPOS" :
	CALL qrypos_cmd(.tsk,ON)

.*********************
.** Plunger/Gripper **
.*********************
      SVALUE "HLDWAF":
	CALL hldwaf_cmd(.tsk)
      SVALUE "HOLD","RELS":
	CALL hold_rels_cmd(.tsk)
      SVALUE "SENS":
	CALL sens_cmd(.tsk)


.************
.** Status **
.************
      SVALUE "VER":
	CALL ver_cmd(.tsk)
      SVALUE "MODL":
	CALL modl_cmd(.tsk)
      SVALUE "SETCOMPATIBILITY","SETDOCVER":
	CALL setdocver(.tsk)
      SVALUE "EVTENABLE":
	CALL evtenable_cmd(.tsk)
      SVALUE "QRYDOCEVT"
	CALL qrydocevt_cmd(.tsk)
      SVALUE "STAT":
	CALL stat_cmd(.tsk)
      SVALUE "QRYALRM","Q":
	CALL qryalrm_cmd(.tsk,OFF)
      SVALUE "QA":
	CALL qryalrm_cmd(.tsk,ON)
      SVALUE "QRYLIM"
	CALL qrylim_cmd(.tsk)

      SVALUE "SETZERO"
.** harish 090309 a++
        IF vtc.docver[.tsk] >= 200
	    CALL setzero_cmd(.tsk)
	ELSE
            CALL setzero_cmd1(.tsk)
	END
.** harish 090309 a--

      ANY :
efm:
	$snd_ack[.tsk] = $ack_err_fm
    END
Exit:
    RETURN
.END
.******************************************
.** host_tcp.as
.******************************************
.REALS
    tcp_temp[host_tcp1] = 0
    tcp_temp[host_tcp2] = 0
    tcp_temp[host_tcp1_sub] = 0
    tcp_temp[host_tcp2_sub] = 0

    tcp_stat[host_tcp1] = 0
    tcp_stat[host_tcp2] = 0
    tcp_stat[host_tcp1_sub] = 0
    tcp_stat[host_tcp2_sub] = 0

    tcp_client_chk[pc3] = 0
    tcp_client_chk[pc4] = 0

    tcp_sckt[host_tcp1] = -1
    tcp_sckt[host_tcp2] = -1
    tcp_sckt[host_tcp1_sub] = -1
    tcp_sckt[host_tcp2_sub] = -1

    tcp_actv_sckt[host_tcp1] = -1
    tcp_actv_sckt[host_tcp2] = -1

    tcp_recv[host_tcp1] = 0
    tcp_recv[host_tcp2] = 0
    tcp_recv[host_tcp1_sub] = 0
    tcp_recv[host_tcp2_sub] = 0

    tcp_ipad[host_tcp1,1] = 0
    tcp_ipad[host_tcp2,1] = 0
    tcp_ipad[host_tcp1_sub,1] = 0
    tcp_ipad[host_tcp2_sub,1] = 0

    tcp_close_tmo = 2
.** harish 081125 m--
.END
.STRINGS
    $pz.tcp_now[host_tcp1] = ""
    $pz.tcp_now[host_tcp2] = ""
    $pz.tcp_old[host_tcp1] = ""
    $pz.tcp_old[host_tcp2] = ""
    $tcp_recv[host_tcp1,1] = ""
    $tcp_recv[host_tcp2,1] = ""
.** harish 081125 a++
    $pz.tcp_now[host_tcp1_sub] = ""
    $pz.tcp_now[host_tcp2_sub] = ""
    $pz.tcp_old[host_tcp1_sub] = ""
    $pz.tcp_old[host_tcp2_sub] = ""
    $tcp_recv[host_tcp1_sub,1] = ""
    $tcp_recv[host_tcp2_sub,1] = ""
.** harish 081125 a--
.END
.PROGRAM host_tcp.init(.tsk)
    CALL host_tcp.proc(.tsk,ON,.hst)
.END
.** harish 081125 m++
.PROGRAM host_tcp.proc(.tsk,.ini,.hst)
    .hst = IFELSE(.tsk==pc3,host_tcp1,host_tcp2)
    .sub = IFELSE(.tsk==pc3,host_tcp1_sub,host_tcp2_sub)

    IF SYSDATA(ZSIMENV)		RETURN
    IF .ini THEN
        log_tcp_buf[.tsk] = 500			;** Shimomura 12/15 ;** TCP CONNECT LOG
        NOEXIST_SET_R log_tcp.cnt[.tsk] = 0	;** Shimomura 12/15
	NOEXIST_SET_R log_tcp.max[.tsk] = 0	;** Shimomura 12/15
        tcp_send[host_tcp1] = 0		        ;** Shimomura 12/13
        tcp_send[host_tcp2] = 0		        ;** Shimomura 12/13
	tcp_send[host_tcp1_sub] = 0		;** Shimomura 12/13
	tcp_send[host_tcp2_sub] = 0		;** Shimomura 12/13

        TCP_INFO tcpini[host_tcp1],tcp_port[host_tcp1],tcp_ipad[host_tcp1,1]
        TCP_INFO tcpini[host_tcp2],tcp_port[host_tcp2],tcp_ipad[host_tcp2,1]

;;	 TYPE  .tsk,tcp_sckt[.hst], tcp_sckt[.sub]
	$pz.tcp_old[.hst] = ""
	$pz.tcp_old[.sub] = ""
	$pz.tcp_now[.hst] = "TCP_INFO"
	TCP_INFO tcp_temp[.hst],tcp_port[.hst],tcp_ipad[.hst,1]
        IF pz.tcp THEN
          TYPE "TCP_INFO ",.hst,tcp_temp[.hst] 
        END
	IF  tcp_temp[.hst] >= 0 THEN
            IF  tcp_sckt[.sub]  == tcp_temp[.hst] THEN
	        tcp_stat[.sub]  = 0	;** -->Connect
                tcp_sckt[.sub]  = tcp_temp[.hst]
		tcp_actv_sckt[.hst] = tcp_sckt[.sub]
		tcp_stat[.hst]  = -1
		tcp_sckt[.hst]  = -1
                .open = 1
            ELSE
	        tcp_stat[.hst]  = 0	;** -->Connect
                tcp_sckt[.hst]  = tcp_temp[.hst]
		tcp_actv_sckt[.hst] = tcp_sckt[.hst]
		tcp_stat[.sub]  = -1
		tcp_sckt[.sub]  = -1
                .open = 1
            END
	ELSE
	    tcp_sckt[.hst]  = -1
	    tcp_sckt[.sub]  = -1
	    tcp_stat[.hst]  =  tcp_temp[.hst]
	    tcp_stat[.sub]  =  tcp_temp[.hst]
	END
        IF .tsk == pc3 THEN
          TCP_STATUS/FULL tcp_temp[.hst],tcpsta_port[.hst,1],tcpsta_sock[.hst,1],tcpsta_err[.hst,1],tcpsta_sub[.hst,1],$tcpsta_sub[.hst,1]
;;        TYPE tcp_temp[.hst] 
	  FOR .i = 1 TO tcp_temp[.hst]
            IF pz.tcp THEN
                  TYPE "TCP_STATUS",.i,.hst,tcpsta_port[.hst,.i],tcpsta_sock[.hst,.i],tcpsta_err[.hst,.i],tcpsta_sub[.hst,.i],$tcpsta_sub[.hst,.i]
            END
            IF tcpsta_sub[.hst,.i] != -1 THEN
                   IF tcpini[host_tcp1] != tcpsta_sock[.hst,.i] AND tcpini[host_tcp2] != tcpsta_sock[.hst,.i] THEN
		     TCP_CLOSE tcp_temp[.hst+2],tcpsta_sock[.hst,.i],tcp_close_tmo
		     IF pz.tcp THEN
		       TYPE "TCP_CLOSE at ini ",.tsk,tcpsta_sock[.hst,.i]
		     END
                   END
	    END
          END
        END
.** harish 081210 a
	tcp_init = 1
    ELSE
	IF  tcp_stat[.hst] == -2 THEN
	    $pz.tcp_now[.hst] = "TCP_LISTEN"
	    TCP_LISTEN tcp_temp[.hst],tcp_port[.hst]
	    IF  tcp_temp[.hst] == 0 THEN
		tcp_stat[.hst] = -1
                tcp_stat[.sub] = -1
		$pz.tcp_now[.hst] = $pz.tcp_now[.hst] + " Success"
	    ELSE
		$pz.tcp_now[.hst] = $pz.tcp_now[.hst] + " Error"
	    END
	ELSEIF tcp_stat[.hst] == -1 THEN
	    $pz.tcp_now[.hst] = "TCP_ACCEPT"
	    TCP_ACCEPT tcp_temp[.hst],tcp_port[.hst],0,tcp_ipad[.hst,1]
	    IF tcp_temp[.hst] >= 0 THEN
		IF tcp_sckt[.hst] >= 0 THEN
		    TCP_CLOSE tcp_temp[.hst+2],tcp_sckt[.hst],tcp_close_tmo
		    $pz.tcp_now[.hst] = $pz.tcp_now[.hst]+"(TCP_CLOSE)"
		END
		tcp_sckt[.hst] = tcp_temp[.hst]
		tcp_stat[.hst] = 0
		$pz.tcp_now[.hst] = $pz.tcp_now[.hst]+"(CONNECT)"	     ;** Shimomura 12/15
		IF (tcp_sckt[.sub] >= 0)AND(tcp_stat[.sub]==0) THEN
		    $pz.tcp_now[.hst] = $pz.tcp_now[.hst]+"(EVENT START)"    ;** Shimomura 12/15
 		    tcp_client_chk[.tsk] = 1
		    CALL event_entry(.tsk,$ev_client_chk,$ev2_client_chk)
                    UTIMER .@tim = 0
		    tcp_stat[.hst] = 1
                ELSE
                    tcp_actv_sckt[.hst] = tcp_sckt[.hst]
                END
	    END
	ELSEIF tcp_stat[.hst] ==  0 || tcp_stat[.hst] ==  1 THEN
	    $pz.tcp_now[.hst] = "TCP_RECV"
	    TCP_RECV tcp_temp[.hst],tcp_sckt[.hst],$tcp_recv[.hst,1],tcp_recv[.hst],0,254
	    IF tcp_temp[.hst] == 0 THEN
		$pz.tcp_now[.hst] = $pz.tcp_now[.hst] + " Data"
                IF  tcp_actv_sckt[.hst] == tcp_sckt[.hst] THEN
		    $hst_get[.hst] = $tcp_recv[.hst,1]
                END
	    ELSEIF tcp_temp[.hst] == -34025 THEN
		TCP_CLOSE tcp_temp[.hst+2],tcp_sckt[.hst],tcp_close_tmo
		tcp_sckt[.hst] = -1
		tcp_stat[.hst] = -1
		$pz.tcp_now[.hst] = $pz.tcp_now[.hst] + " CLOSED"
.** Shimomura  081215 m++
;;	        IF (tcp_sckt[.sub] >= 0) AND ( tcp_stat[.sub] ==  0 || tcp_stat[.sub] ==  1) THEN
;;                    tcp_actv_sckt[.hst] = tcp_sckt[.sub]
;;	        END
.** Shimomura  081215 m--
	    ELSEIF tcp_temp[.hst] == -34024 THEN
		IF TCP_ISLINK <= 0 THEN
;;		    tcp_stat[.hst] = -1					;** Shimomura 12/13
		    $pz.tcp_now[.hst] = $pz.tcp_now[.hst] + " Disconnected"
		ELSE
		    $pz.tcp_now[.hst] = $pz.tcp_now[.hst] + " No Data"
		END
	    ELSE
		$pz.tcp_now[.hst] = $pz.tcp_now[.hst] + $ENCODE("Error:",tcp_temp[.hst])
	    END
            IF tcp_stat[.hst] == 1 THEN ; sub live check
		IF UTIMER(.@tim) <= vtp.t2[.tsk]*vtp.evtnum[.tsk]+1 THEN
		    IF tcp_send[.sub] != 0 THEN				;** Shimomura 12/13
		       GOTO Change2hst
		    END
	            IF tcp_client_chk[.tsk] == 0 THEN 
;;                         tcp_stat[.hst] = 0
			 IF tcp_sckt[.hst] >= 0 THEN
		    	    TCP_CLOSE tcp_temp[.hst+2],tcp_sckt[.hst],tcp_close_tmo
		    	    tcp_sckt[.hst] = -1
		    	    tcp_stat[.hst] = -1
			    $pz.tcp_now[.hst] = $pz.tcp_now[.hst]+"(EVENT GET CLOSE)"    ;** Shimomura 12/15
                    	    IF pz.tcp THEN
		        	TYPE "TCP_CLOSE hst"
                            END
                         END
                    END
                ELSE
Change2hst:
                  IF pz.tcp THEN
		    TYPE "Change active socket to hst"
		  END
                  tcp_actv_sckt[.hst] = tcp_sckt[.hst]
                  tcp_stat[.hst] = 0
		  IF tcp_stat[.sub] == 0 AND tcp_sckt[.sub] >= 0 THEN
		    TCP_CLOSE tcp_temp[.sub+2],tcp_sckt[.sub],tcp_close_tmo
		    tcp_sckt[.sub] = -1
		    tcp_stat[.sub] = -1
		    $pz.tcp_now[.hst] = $pz.tcp_now[.hst]+"(EVENT NONE CLOSE)"    ;** Shimomura 12/15
                    IF pz.tcp THEN
		        TYPE "TCP_CLOSE sub"
                    END
                  END
                END
            END
	ELSE
	    $pz.tcp_now[.hst] = "TCP_Status Error"
	END
    END
.** shimomura 081215 a++ TCP CONNECT LOG **.
    IF $pz.tcp_now[.hst] <> $pz.tcp_old[.hst] THEN
      IF INSTR($pz.tcp_now[.hst],"CONNECT") OR INSTR($pz.tcp_now[.hst],"CLOSE")
	CALL log_tcp_connect(.tsk,.hst,tcp_stat[.hst],$pz.tcp_now[.hst])
      END
    END
.** shimomura 081215 a--
    IF pz.tcp THEN
	IF $pz.tcp_now[.hst] <> $pz.tcp_old[.hst] THEN
	    TYPE .hst,$pz.tcp_now[.hst],tcp_stat[.hst]
	END
.** shimomura 081215 a
    END
    $pz.tcp_old[.hst] = $pz.tcp_now[.hst]
;   END

;;----SUB----------------------------------------------------------
    tcp_port[host_tcp1_sub] = tcp_port[host_tcp1]
    tcp_port[host_tcp2_sub] = tcp_port[host_tcp2]

    IF .ini THEN
;;	$pz.tcp_old[.sub] = ""
;;	$pz.tcp_now[.sub] = "TCP_INFO"
;;	TCP_INFO tcp_temp[.sub],tcp_port[.sub],tcp_ipad[.sub,1]
;;	IF  tcp_temp[.sub] >= 0 THEN
;;	    tcp_sckt[.sub]  = tcp_temp[.sub]
;;	    tcp_stat[.sub]  = 0	;** -->Connect
;;	ELSE
;;	    tcp_sckt[.sub]  = -1
;;	    tcp_stat[.sub]  = tcp_temp[.sub]
;;	END
    ELSE
	IF  tcp_stat[.sub] == -2 THEN
;;	    $pz.tcp_now[.sub] = "TCP_LISTEN"
;;	    TCP_LISTEN tcp_temp[.sub],tcp_port[.sub]
;;	    IF  tcp_temp[.sub] == 0 THEN
;;		tcp_stat[.sub] = -1
;;		$pz.tcp_now[.sub] = $pz.tcp_now[.sub] + " Success"
;;	    ELSE
;;		$pz.tcp_now[.sub] = $pz.tcp_now[.sub] + " Error"
;;	    END
	ELSEIF tcp_stat[.sub] == -1 THEN
	    $pz.tcp_now[.sub] = "TCP_ACCEPT"
	    TCP_ACCEPT tcp_temp[.sub],tcp_port[.sub],0,tcp_ipad[.sub,1]
	    IF tcp_temp[.sub] >= 0 THEN
		IF tcp_sckt[.sub] >= 0 THEN
		    TCP_CLOSE tcp_temp[.sub+2],tcp_sckt[.sub],tcp_close_tmo
		    $pz.tcp_now[.sub] = $pz.tcp_now[.sub]+"(TCP_CLOSE)"
		END
		tcp_sckt[.sub] = tcp_temp[.sub]
		tcp_stat[.sub] = 0
		$pz.tcp_now[.sub] = $pz.tcp_now[.sub]+"(CONNECT)"    ;** Shimomura 12/15
		IF (tcp_sckt[.hst] >= 0)AND(tcp_stat[.hst]==0) THEN
		$pz.tcp_now[.sub] = $pz.tcp_now[.sub]+"(EVENT START)"    ;** Shimomura 12/15
		    tcp_client_chk[.tsk] = 1
		    CALL event_entry(.tsk,$ev_client_chk,$ev2_client_chk)
                    UTIMER .@tim = 0
                    tcp_stat[.sub] = 1
                ELSE
                    tcp_actv_sckt[.hst] = tcp_sckt[.sub]
                END
	    END
	ELSEIF tcp_stat[.sub] ==  0 || tcp_stat[.sub] ==  1 THEN
	    $pz.tcp_now[.sub] = "TCP_RECV"
	    TCP_RECV tcp_temp[.sub],tcp_sckt[.sub],$tcp_recv[.sub,1],tcp_recv[.sub],0,254
	    IF tcp_temp[.sub] == 0 THEN
		$pz.tcp_now[.sub] = $pz.tcp_now[.sub] + " Data"
                IF  tcp_actv_sckt[.hst] == tcp_sckt[.sub] THEN
                    $hst_get[.hst] = $tcp_recv[.sub,1]
                END
	    ELSEIF tcp_temp[.sub] == -34025 THEN
		TCP_CLOSE tcp_temp[.sub+2],tcp_sckt[.sub],tcp_close_tmo
		tcp_sckt[.sub] = -1
		tcp_stat[.sub] = -1
		$pz.tcp_now[.sub] = $pz.tcp_now[.sub] + " CLOSED"
.** shimomura 081215 m++
;;	        IF (tcp_sckt[.hst] >= 0) AND ( tcp_stat[.hst] ==  0 || tcp_stat[.hst] ==  1) THEN
;;                   tcp_actv_sckt[.hst] = tcp_sckt[.hst]
;;	        END
.** shimomura 081215 m--
	    ELSEIF tcp_temp[.sub] == -34024 THEN
		IF TCP_ISLINK <= 0 THEN
;;		    tcp_stat[.sub] = -1							;** Shimomura 12/13
		    $pz.tcp_now[.sub] = $pz.tcp_now[.sub] + " Disconnected"
		ELSE
		    $pz.tcp_now[.sub] = $pz.tcp_now[.sub] + " No Data"
		END
	    ELSE
		$pz.tcp_now[.sub] = $pz.tcp_now[.sub] + $ENCODE("Error:",tcp_temp[.sub])
	    END
            IF tcp_stat[.sub] == 1 THEN ; hst live check
		IF UTIMER(.@tim) <= vtp.t2[.tsk]*vtp.evtnum[.tsk]+1 THEN
		    IF tcp_send[.hst] != 0 THEN
		       GOTO Change2sub
		    END
	            IF tcp_client_chk[.tsk] == 0 THEN 
;;                        tcp_stat[.sub] = 0
			 IF tcp_sckt[.sub] >= 0 THEN
		    	    TCP_CLOSE tcp_temp[.sub+2],tcp_sckt[.sub],tcp_close_tmo
		    	    tcp_sckt[.sub] = -1
		    	    tcp_stat[.sub] = -1
			    $pz.tcp_now[.sub] = $pz.tcp_now[.sub]+"(EVENT GET CLOSE)"    ;** Shimomura 12/15
                    	    IF pz.tcp THEN
		        	TYPE "TCP_CLOSE sub"
                            END
                         END
                    END
                ELSE
Change2sub:
                  IF pz.tcp THEN
		    TYPE "Change active socket to sub"
		  END
                  tcp_actv_sckt[.hst] = tcp_sckt[.sub]
                  tcp_stat[.sub] = 0
		  IF tcp_stat[.hst] == 0 AND tcp_sckt[.hst] >= 0 THEN
		    TCP_CLOSE tcp_temp[.hst+2],tcp_sckt[.hst],tcp_close_tmo
		    tcp_sckt[.hst] = -1
		    tcp_stat[.hst] = -1
		    $pz.tcp_now[.sub] = $pz.tcp_now[.sub]+"(EVENT NONE CLOSE)"           ;** Shimomura 12/15
                    IF pz.tcp THEN
		        TYPE "TCP_CLOSE hst"
                    END
                  END
                END
            END
	ELSE
	    $pz.tcp_now[.sub] = "TCP_Status Error"
	END
    END
.** shimomura 081215 a++ TCP CONNECT LOG **.
    IF $pz.tcp_now[.sub] <> $pz.tcp_old[.sub] THEN
      IF INSTR($pz.tcp_now[.sub],"CONNECT") OR INSTR($pz.tcp_now[.sub],"CLOSE")
	CALL log_tcp_connect(.tsk,.sub,tcp_stat[.sub],$pz.tcp_now[.sub])
      END
    END
.** shimomura 081215 a--
    IF pz.tcp THEN
	IF $pz.tcp_now[.sub] <> $pz.tcp_old[.sub] THEN
	    TYPE .sub,$pz.tcp_now[.sub],tcp_stat[.sub]
	END
.** shimomura 081215 a
    END
    $pz.tcp_old[.sub] = $pz.tcp_now[.sub]
;   END
;;--------SUB ---------------------------------------------------------------------------
.END
.** shimomura 081215 a++ TCP CONNECT LOG **.
.PROGRAM log_tcp_connect(.tsk,.s,.stat,.$inf)
    log_tcp.cnt[.tsk] = RING(log_tcp.cnt[.tsk],1,log_tcp_buf[.tsk])
    $log_tcp_inf[.tsk] = $DATE(3)+" "+$TIME+$ENCODE(.s)+$ENCODE(.stat)+" "
    $log_tcpstr[.tsk] = .$inf
    $log_tcp.str[.tsk,log_tcp.cnt[.tsk]] = $log_tcp_inf[.tsk]+$log_tcpstr[.tsk]
    IF pz.tcp THEN
	TYPE "LOG ",$log_tcp.str[.tsk,log_tcp.cnt[.tsk]]
    END
    log_tcp.max[.tsk] = MAXVAL(log_tcp.max[.tsk],log_tcp.cnt[.tsk])
.END
.** shimomura 081215 a--
.PROGRAM host_tcp.send(.tsk,.hst)
    IF SYSDATA(ZSIMENV)		RETURN
    .sub = IFELSE(.hst==host_tcp1,host_tcp1_sub,host_tcp2_sub)
;
    IF tcp_actv_sckt[.hst] == tcp_sckt[.hst] THEN
      IF (tcp_sckt[.hst] >= 0)AND(tcp_stat[.hst]==0 || tcp_stat[.hst]==1) THEN
	$tcp_send[.hst,1] = $snd_snd[.tsk]
	TCP_SEND tcp_send[.hst],tcp_sckt[.hst],$tcp_send[.hst,1],1,0
        IF pz.tcp THEN
	    TYPE "TCP_SEND ",.hst,$tcp_send[.hst,1]
        END      
      END
;;      IF pz.tcp THEN
;;          TYPE "TCP_SEND ",.hst,$tcp_send[.hst,1]
;;      END
.** Shimomura  081215 m
    ELSEIF tcp_actv_sckt[.hst] == tcp_sckt[.sub] THEN
      IF (tcp_sckt[.sub] >= 0)AND(tcp_stat[.sub]==0 || tcp_stat[.sub]==1) THEN
	$tcp_send[.sub,1] = $snd_snd[.tsk]
	TCP_SEND tcp_send[.sub],tcp_sckt[.sub],$tcp_send[.sub,1],1,0
        IF pz.tcp THEN
	    TYPE "TCP_SEND ",.sub,$tcp_send[.sub,1]
        END
      END
;;      IF pz.tcp THEN
;;	    TYPE "TCP_SEND ",.sub,$tcp_send[.sub,1]
;;      END
    END
.END
.** harish 081125 m--
.******************************************
.** host_com.as
.******************************************
.REALS
    com_rcv[host_com1] = 0
    com_rcv[host_com2] = 0
    com_ch[host_com1] = 2
    com_ch[host_com2] = 3
.END
.STRINGS
    $com_rcv[host_com1] = ""
    $com_rcv[host_com2] = ""
.END
.PROGRAM host_com.init(.tsk)
    CALL host_com.proc(.tsk,ON,.hst)
.END
.PROGRAM host_com.proc(.tsk,.ini,.hst)
    .hst = IFELSE(.tsk==pc3,host_com1,host_com2)
    IF SYSDATA(ZSIMENV)		RETURN
    IF .ini THEN
	SCSETSIO   com_ch[.hst]: 19200,1
	SCCOMRESET com_ch[.hst]:
    ELSE
	SC2RECEIVE/NP/NW com_ch[.hst]:$com_rcv[.hst],com_rcv[.hst],-1
	IF com_rcv[.hst] == 0 THEN
	    $hst_get[.hst] = $com_rcv[.hst]
	END
    END
.END


.PROGRAM host_com.send(.tsk,.hst)
    IF NOT SYSDATA(ZSIMENV) THEN
retry:
	SC2SEND/NP/NW com_ch[.hst]:$snd_snd[.tsk],.err
	IF .err THEN
	    TWAIT 0.016
	    GOTO retry
	END
    END
.END
.******************************************
.** host_dum.as
.******************************************
.REALS
    dum_mes[host_dum1] = 0
    dum_mes[host_dum2] = 0
.END
.STRINGS
    $dum1 = ""
    $dum2 = ""
.END
.PROGRAM host_dum.init(.tsk)
    CALL host_dum.proc(.tsk,ON,.hst)
.END
.PROGRAM host_dum.proc(.tsk,.ini,.hst)
    .hst = IFELSE(.tsk==pc3,host_dum1,host_dum2)
    IF .ini THEN
	dum1 = OFF
	dum2 = OFF
    ELSE
	IF .hst == host_dum1 THEN
	    IF dum1 THEN
		$hst_get[.hst] = $dum1
		dum1 = OFF
	    END
	ELSE
	    IF dum2 THEN
		$hst_get[.hst] = $dum2
		dum2 = OFF
	    END
	END
    END
.END
.PROGRAM dmake()
    CALL dum1
.END
.PROGRAM dum1()
    CALL dum_inp(dum1,$dum1,host_dum1)
.END
.PROGRAM dum2()
    CALL dum_inp(dum2,$dum2,host_dum2)
.END
.PROGRAM dum_inp(.dum,.$dum,.hst)
    PROMPT "Input Data =",.$inp
    CALL dum_str(.dum,.$dum,.hst,.$inp)
.END
.PROGRAM dum_str(.dum,.$dum,.hst,.$inp)
    IF "\'"==$LEFT(.$inp,1) THEN
	.$dum = .$inp
	.$dum = $MID(.$dum,2,255)
	.$dum = $REPLACE(.$dum,"@CRLF",$c_cr+$c_lf,-1)
	.$dum = $REPLACE(.$dum,"@LF"  ,      $c_lf,-1)
	.$dum = $REPLACE(.$dum,"@CR"  ,$c_cr      ,-1)
	.$dum = $REPLACE(.$dum,"@BS"  ,$c_bs      ,-1)
    ELSEIF .$inp <> "" THEN
	dum_mes[.hst] = RING(dum_mes[.hst],1,999)
	.$dum = $REPLACE($ENCODE(/I3,dum_mes[.hst])," ","0",-1)+$c_mes + .$inp
	.$dum = $c_stx+.$dum+$c_end+$REPLACE($ENCODE(/H2,STR_CSUM(.$dum,^HFF))," ","0",-1)+$c_etx
    ELSE .$inp == "" THEN
	RETURN
    END
    .dum = ON
.END

.PROGRAM dwait1(.$cmd)
    $dum_ack[host_dum1] = ""
    $dum_res[host_dum1] = ""
    CALL dum_str(dum1,$dum1,host_dum1,.$cmd)
    WAIT (dum1==OFF)
    WAIT ($dum_ack[host_dum1]<>"")
    IF $dum_ack[host_dum1] <> $ack_ok THEN
	TYPE $host[host_dum1]," Ack Code Error:",$dum_ack[host_dum1],"<>",$ack_ok
	HALT
    END
    WAIT ($dum_res[host_dum1]<>"")
    IF $dum_res[host_dum1] <> $res_ok THEN
	TYPE $host[host_dum1]," Res Code Error:",$dum_res[host_dum1],"<>",$res_ok
	HALT
    END
.END
.PROGRAM dwait2(.$cmd)
    $dum_ack[host_dum2] = ""
    $dum_res[host_dum2] = ""
    CALL dum_str(dum2,$dum2,host_dum2,.$cmd)
    WAIT (dum2==OFF)
    WAIT ($dum_ack[host_dum2]<>"")
    IF $dum_ack[host_dum2] <> $ack_ok THEN
	TYPE $host[host_dum2]," Ack Code Error:",$dum_ack[host_dum2],"<>",$ack_ok
	HALT
    END
    WAIT ($dum_res[host_dum2]<>"")
    IF $dum_res[host_dum2] <> $res_ok THEN
	TYPE $host[host_dum2]," Res Code Error:",$dum_res[host_dum2],"<>",$res_ok
	HALT
    END
.END
.PROGRAM dwait0(.tsk,.$cmd)
    IF .tsk == pc3 THEN
	CALL dwait1(.$cmd)
    ELSEIF .tsk == pc4 THEN
	CALL dwait2(.$cmd)
    ELSE
	HALT
    END
.END


.PROGRAM dwait1_res(.$cmd,.$res)
    $dum_ack[host_dum1] = ""
    $dum_res[host_dum1] = ""
    CALL dum_str(dum1,$dum1,host_dum1,.$cmd)
    WAIT (dum1==OFF)
    WAIT ($dum_ack[host_dum1]<>"")
    IF $dum_ack[host_dum1] <> $ack_ok THEN
	TYPE $host[host_dum1]," Ack Code Error:",$dum_ack[host_dum1],"<>",$ack_ok
	HALT
    END
    WAIT ($dum_res[host_dum1]<>"")
    .$res = $dum_res[host_dum1]
.END
.PROGRAM dwait2_res(.$cmd,.$res)
    $dum_ack[host_dum2] = ""
    $dum_res[host_dum2] = ""
    CALL dum_str(dum2,$dum2,host_dum2,.$cmd)
    WAIT (dum2==OFF)
    WAIT ($dum_ack[host_dum2]<>"")
    IF $dum_ack[host_dum2] <> $ack_ok THEN
	TYPE $host[host_dum2]," Ack Code Error:",$dum_ack[host_dum2],"<>",$ack_ok
	HALT
    END
    WAIT ($dum_res[host_dum2]<>"")
    .$res = $dum_res[host_dum2]
.END
.PROGRAM dwait0_res(.tsk,.$cmd,.$res)
    IF .tsk == pc3 THEN
	CALL dwait1_res(.$cmd,.$res)
    ELSEIF .tsk == pc4 THEN
	CALL dwait2_res(.$cmd,.$res)
    ELSE
	HALT
    END
.END
.******************************************
.** small_tp.as
.******************************************
.PROGRAM small_tp_init()
   ZT_TCH_CMD = OFF
   ZT_CHK_CMD = OFF
   ZT_FIC_CMD = OFF
  $ZT_TCH_CMD = ""
  $ZT_CHK_CMD = ""
  $ZT_FIC_CMD = ""
   rob_sel = -1
   sim_sel = 0
   CALL small_tp_menu
   CALL small_tp.chg(0)
.END
.PROGRAM small_tp_sel(.get)
    IF SYSDATA(ZSIMENV) THEN
	.get = sim_sel
    ELSE
	.get = GET_ROBSEL
    END
.END
.PROGRAM small_tp.chg(.tsk)
    CALL small_tp_sel(.get)
    IF rob_sel == .get	RETURN
    IF (rob_sel==1)OR(rob_sel==2) THEN
	MC ZPOWER rob_sel : OFF
    END
    .top = IFELSE(GET_SMTP_TOP==2 OR GET_SMTP_TOP==3,ON,OFF)
    IF .top THEN
	SMTP_TOP
    END
    $ZS.TEACH[0] = "ZZZZ"
    $ZS.CHECK[0] = "ZZZZ"
    $tp.port[0]  = "ZZZZ"
    IF NOT .tsk			GOTO exit
    rob_sel = .get
    IF NOT rob_sel		GOTO exit
;** Set Port String
    ZARRAYCPY $tp.port[0] = $id_port[.tsk,1],id_port[.tsk,0]
    $tp.port[id_port[.tsk,0]] = "ZZZZ"
;** Set Slot String
    .max = 1
    FOR .cnt = 1 TO id_port[.tsk,0] STEP 1
	.max = IFELSE(vpp.slot[id_port[.tsk,.cnt]]>.max,vpp.slot[id_port[.tsk,.cnt]],.max)
    END
    TYPE .max
    $tp.slot[0] = ""
    FOR .cnt = 1 TO .max STEP 1
	$tp.slot[.cnt] = $TRIM_B($ENCODE(/I,.cnt))
    END
    $tp.slot[.cnt+1] = "ZZZZ"
;** Set Misc. Strings
    $tp.hldwaf[0] = "Hold"
    $tp.hldwaf[1] = "Rels"
    $tp.hldwaf[2] = "ZZZZ"
;** Set Teach Commnad Strings
    $ZS.TEACH[0] = "TchPos," +$vrp.rob_asso[rob_sel]+":B1,$tp.port:$tp.slot,Taught"
    $ZS.TEACH[1] = "SavPos," +$vrp.rob_asso[rob_sel]+":B1,$tp.port"
    $ZS.TEACH[2] = "HldWaf," +$vrp.rob_asso[rob_sel]+":B1,$tp.hldwaf"
    $ZS.TEACH[3] = "SavP_ALL"
    $ZS.TEACH[4] = "ZZZZ"
;** Set Check Command Strings
    $ZS.CHECK[0] = "TGetWaf,"  +$vrp.rob_asso[rob_sel]+":B1,$tp.port:$tp.slot,1"
    $ZS.CHECK[1] = "TPutWaf,"  +$vrp.rob_asso[rob_sel]+":B1,$tp.port:$tp.slot,1"
    $ZS.CHECK[2] = "GetWaf,"   +$vrp.rob_asso[rob_sel]+":B1,$tp.port:$tp.slot"
    $ZS.CHECK[3] = "PutWaf,"   +$vrp.rob_asso[rob_sel]+":B1,$tp.port:$tp.slot"
    $ZS.CHECK[4] = "Home,"     +$vrp.rob_asso[rob_sel]
    $ZS.CHECK[5] = "MovPrePos,"+$vrp.rob_asso[rob_sel]+":B1,$tp.port:$tp.slot"
    $ZS.CHECK[6] = "ZZZZ"
exit:
    IF pz.smalltp THEN
	FOR .cnt = 0 TO 999 STEP 1
	    TYPE "$ZS.TEACH[",/I2,.cnt,"]=",$ZS.TEACH[.cnt]
	    IF $ZS.TEACH[.cnt]=="ZZZZ" THEN
		.cnt = 1000
	    END
	END
	FOR .cnt = 0 TO 999 STEP 1
	    TYPE "$ZS.CHECK[",/I2,.cnt,"]=",$ZS.CHECK[.cnt]
	    IF $ZS.CHECK[.cnt]=="ZZZZ" THEN
		.cnt = 1000
	    END
	END
	FOR .cnt = 0 TO 999 STEP 1
	    TYPE "$TP.PORT[",/I2,.cnt,"]=",$tp.port[.cnt]
	    IF $tp.port[.cnt]=="ZZZZ" THEN
		.cnt = 1000
	    END
	END
    END
    IF .top THEN
	SMTP_TOP
    END
.END
.PROGRAM host_stp.proc(.tsk)
    CALL small_tp_sel(.get)
    IF .get THEN
	IF tsk_rob[.tsk] BAND bit[.get] THEN
	    IF zt_tch_cmd THEN
		$tsk_ana[.tsk] = $zt_tch_cmd
		$zt_tch_cmd = ""
		 zt_tch_cmd = OFF
	    ELSEIF zt_chk_cmd THEN
		$tsk_ana[.tsk] = $zt_chk_cmd
		$zt_chk_cmd = ""
	    ELSE
		CALL small_tp.chg(.tsk)
		RETURN
	    END
	    $tsk_mes[.tsk] = "000"
	    STR_MDIV $tsk_dat[.tsk,0],tsk_num[.tsk] = $tsk_ana[.tsk],","
	    IF tsk_num[.tsk] == 0 THEN
		$tsk_dat[.tsk,0] = $tsk_ana[.tsk]
		tsk_num[.tsk] = 1
	    END
	    tsk_hst[.tsk] = IFELSE(.tsk==pc3,host_stp1,host_stp2)
	    $tsk_cmd[.tsk] = $TOUPPER($tsk_dat[.tsk,0])
	    CALL log_cmd(.tsk,1)
	END
    END
.END
.PROGRAM sm_tch()
    PROMPT "Input SmallTP Teach Command:",$ZT_TCH_CMD
    IF $ZT_TCH_CMD <> "" THEN
	ZT_TCH_CMD = ON
    END
.END
.PROGRAM sm_chk()
    PROMPT "Input SmallTP Check Command:",$ZT_CHK_CMD
    IF $ZT_CHK_CMD <> "" THEN
	ZT_CHK_CMD = ON
    END
.END
.******************************************
.** netconf.as
.******************************************
.STRINGS
    $net[0,1] = "192.168.0.2"	;** ASSystem default PAddress
.END
.PROGRAM netconf_init()
    GETNETCONF $net[pc3,1],$net[pc3,2],$net[pc3,3],$net[pc3,4],$net[pc3,5],$net[pc3,6],$net[pc3,7],$net[pc3,8]
    IF $net[pc3,1]==$net[0,1] THEN
	NETCONF/ERR tmp $net[1,1],$net[pc3,2],$net[pc3,3],$net[pc3,4],$net[pc3,5],$net[pc3,6],$net[pc3,7]
    END
.END
.******************************************
.** log.as
.******************************************
.** harish 090617-2 a++
.REALS
    grip_chk_retry[1] = 0			;Robot1 retry num for grip check
    grip_chk_retry[2] = 0			;Robot2 retry num for grip check
    grip_retry_wait[1] = 3			;Robot1 wait timer before grip check retry
    grip_retry_wait[2] = 3			;Robot1 wait timer before grip check retry
    gid_dbg_disp = 0				;guide retry debug display sw
    grip_cnt_max = 10				;grip time max store number
    grip_log_cnt_max = 5
.END
.PROGRAM grip.init()
    FOR .rob = 1 TO 2 STEP 1
	NOEXIST_SET_R GRIP_LOG_CNT[.rob] = 0
	FOR .hnd = 1 TO 2 STEP 1
	    NOEXIST_SET_R grip.cnt_rel[.rob,.hnd] = 0
	    NOEXIST_SET_R grip.cnt_hld[.rob,.hnd] = 0
	    FOR .cnt = 1 TO grip_cnt_max STEP 1
		NOEXIST_SET_R grip_tim_rel[.rob,.hnd,.cnt] = 0
		NOEXIST_SET_R grip_tim_hld[.rob,.hnd,.cnt] = 0
	    END
	END
	FOR .cnt = 1 TO grip_log_cnt_max STEP 1
	    NOEXIST_SET_S $GRIP_LOG[.rob,.cnt] = ""
	END
    END
.END
.** harish 090617-2 a--

.PROGRAM log_init()
    log_buf[rb1] = 500	;** Robot1 Motion
    log_buf[rb2] = 500	;** Robot2 Motion
    log_buf[rb3] = 100	;** Robot3 Motion
    log_buf[rb4] =   0	;** Robot4 Motion
    log_buf[pc3] = 500	;** PC3 Communication Log
    log_buf[pc4] = 500	;** PC4 Communication Log
    log_buf[tc1] = 200	;** Robot1 Teaching Log
    log_buf[tc2] = 200	;** Robot2 Teaching Log
    FOR tmp = 1 TO 8 STEP 1
	NOEXIST_SET_R log.cnt[tmp] = 0
	NOEXIST_SET_R log.max[tmp] = 0
	IF SYSDATA(ZSIMENV) THEN
	    FOR tmp2 = 1 TO log_buf[tmp] STEP 1
		    NOEXIST_SET_S $log.str[tmp,tmp2] = ""
.**		IF tmp==pc3 OR tmp==pc4 THEN
.**		    NOEXIST_SET_S $log.inf[tmp,tmp2] = ""
.**		END
	    END
	END
    END
.** harish 090617-2 a
    CALL grip.init
.END
.PROGRAM log_cmd(.tsk,.mod)
.** Input  $tsk_ana[pc#]
.**        $tsk_del[pc#]
.**        $snd_snd[pc#]
.**         tsk_hst[pc#]
    log.cnt[.tsk] = RING(log.cnt[.tsk],1,log_buf[.tsk])
    $log_inf[.tsk] = $DATE(3)+" "+$TIME+"("+$ENCODE(/I1,.tsk)+":"+$host[tsk_hst[.tsk]]+")"
    IF .mod > 0 THEN
	$log_inf[.tsk] = $log_inf[.tsk] + "R->"
	$log_str[.tsk] = $tsk_ana[.tsk]
    ELSEIF .mod < 0 THEN
	$log_inf[.tsk] = $log_inf[.tsk] + "S<-"
	$log_str[.tsk] = $snd_snd[.tsk]
    ELSE
	$log_inf[.tsk] = $log_inf[.tsk] + "Rxx"
	$log_str[.tsk] = $tsk_del[.tsk]
    END
.***$log.inf[.tsk,log.cnt[.tsk]] = $log_inf[.tsk]
.***$log.str[.tsk,log.cnt[.tsk]] = $log_str[.tsk]
    $log.str[.tsk,log.cnt[.tsk]] = $log_inf[.tsk]+$LEFT($log_str[.tsk],255-LEN($log_inf[.tsk]))
    IF (pz.log) THEN
	$log_str[.tsk] = $REPLACE($log_str[.tsk],$c_cr,"\\r",-1)
	$log_str[.tsk] = $REPLACE($log_str[.tsk],$c_lf,"\\n",-1)
	TYPE /S,$log_inf[.tsk]," : "
	TYPE    $log_str[.tsk]
    END
    log.max[.tsk] = MAXVAL(log.max[.tsk],log.cnt[.tsk])
.END
.PROGRAM log_mov(.rob,.$inf)
    log.cnt[.rob] = RING(log.cnt[.rob],1,log_buf[.rob])
    $log_inf[.rob] = $DATE(3)+" "+$TIME+"("+$ENCODE(/I1,.rob)+":"+$host[tsk_hst[.rob]]+")"+$tsk_cmd[.rob]+":"+$rpgname(1)+":"+.$inf+":"
    $log_str[.rob] = $ENCODE(/f9.2,#move_dst[.rob])
    $log.str[.rob,log.cnt[.rob]] = $log_inf[.rob]+$log_str[.rob]
    IF (pz.move)AND(NOT tz.move) THEN
	TYPE $log_inf[.rob]
	TYPE /X8,$log_str[.rob]
    END
    log.max[.rob] = MAXVAL(log.max[.rob],log.cnt[.rob])
.END
.** harish 090617-1 a++
.PROGRAM log_tch(.tsk,.rob,.#loc)
    .buf = .rob + 6
    log.cnt[.buf] = RING(log.cnt[.buf],1,log_buf[.buf])
    .$str = $DATE(3)+" "+$TIME+"("+$ENCODE(/I1,.tsk)+":"+$host[tsk_hst[.tsk]]+")"
    .$str = .$str+$tsk_ana[.tsk]+$ENCODE(" PPOINT ",/f9.3,.#loc)
    $log.str[.buf,log.cnt[.buf]] = .$str
    log.max[.buf] = MAXVAL(log.max[.buf],log.cnt[.buf])
.END
.PROGRAM log()
    CALL log_disp(5)
    CALL log_disp(6)
.END
.PROGRAM log5()
    CALL log_disp(5)
.END
.PROGRAM log6()
    CALL log_disp(6)
.END
.PROGRAM rob()
    CALL log_disp(1)
    CALL log_disp(2)
.END
.PROGRAM rob1()
    CALL log_disp(1)
.END
.PROGRAM rob2()
    CALL log_disp(2)
.END
.PROGRAM log_disp(.tsk)
    ENDKILL
    IF .tsk <= 4 THEN
	TYPE $ENCODE("Robot",/I1,.tsk," Motion Log")
    ELSE
	TYPE $ENCODE("Command Log(",/I1,.tsk-4,")")
    END
    FOR .cnt = log.cnt[.tsk] TO 1 STEP -1
	IF .tsk <= 4 THEN
	    TYPE $log.str[.tsk,.cnt]
	ELSE
	    .$log = $log.str[.tsk,.cnt]
	    .$log = $REPLACE(.$log,$c_cr,"\\r",-1)
	    .$log = $REPLACE(.$log,$c_lf,"\\n",-1)
	    TYPE .$log
	END
    END
    FOR .cnt = log.max[.tsk] TO (log.cnt[.tsk]+1) STEP -1
	IF .tsk <= 4 THEN
	    TYPE $log.str[.tsk,.cnt]
	ELSE
	    .$log = $log.str[.tsk,.cnt]
	    .$log = $REPLACE(.$log,$c_cr,"\\r",-1)
	    .$log = $REPLACE(.$log,$c_lf,"\\n",-1)
	    TYPE $log.inf[.tsk,.cnt],.$log
	END
    END
    IF NOT log.cnt[.tsk] THEN
	TYPE "No Logging Data"
    END
.END
.** harish 090617-1 a--
.******************************************
.** config.as
.******************************************
.PROGRAM config_init()
    IF tsk_rob[pc3] THEN
	lockcfg[pc3] = 1
	CALL cfg_key_table(pc3,"")
	CALL cfg_table(pc3,port_non,cfgset_nex)
	CALL cfg_table(pc3,port_all,cfgset_nex)
    END
    IF tsk_rob[pc4] THEN
	lockcfg[pc4] = 1
	CALL cfg_key_table(pc4,"")
	CALL cfg_table(pc4,port_non,cfgset_nex)
	CALL cfg_table(pc4,port_all,cfgset_nex)
    END
.END
.***************
.*** SAVECFG ***
.***************
.PROGRAM savecfg_cmd(.tsk)
    $snd_ack[.tsk] = $ack_ok
    $snd_res[.tsk] = $res_ok
.END
.***************
.*** LOCKCFG ***
.***************
.PROGRAM lockcfg_cmd(.tsk)
    IF (tsk_num[.tsk]<>2)	GOTO efm
    IF "ALL"==$TOUPPER($tsk_dat[.tsk,1]) THEN
	.all = ON
    ELSE
	.all = OFF
	STR_2DIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,1],":"
	IF tsk_div[.tsk]==0 THEN
	    $cfg.key[.tsk] = $tsk_dat[.tsk,1]
	    .prt = port_non
	ELSEIF tsk_div[.tsk] == 2 tHEN
	    $cfg.key[.tsk] = $tsk_dat[.tsk,d_divp]
	    CALL id_port(.tsk,d_divp+1,d_prt1)
	    .prt = tsk_dat[.tsk,d_prt1]
	    IF NOT .prt THEN
		CALL id_robot(.tsk,d_divp+1,d_rob1)
		.prt = tsk_dat[.tsk,d_rob1]
	    END
	    IF NOT .prt		GOTO epm
	ELSE
	    GOTO efm
	END
    END
    CALL id_0_1(.tsk,2,d_dat1)
    IF id_error[.tsk]	GOTO epm
    $cfg.dat[.tsk] = $tsk_dat[.tsk,2]
    IF NOT .all THEN
	CALL cfg_table(.tsk,.prt,cfglck_chk)
	IF cfg.err[.tsk]	GOTO epm
    END
    CALL check_bsy_mode(.tsk,tsk_rob[.tsk],m_char)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    IF NOT .all THEN
	CALL cfg_table(.tsk,.prt,cfglck_exe)
    ELSE
	lockcfg[.tsk] = tsk_dat[.tsk,d_dat1]
    END
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.**************
.*** SETCFG ***
.**************
.PROGRAM setcfg_cmd(.tsk)
    IF tsk_num[.tsk]==0 THEN
	$snd_ack[.tsk] = $ack_ok
	$snd_res[.tsk] = $res_ok
	RETURN
    END
    CALL setcfg_sub(.tsk,cfgset_chk)
    IF $snd_ack[.tsk]<>""	RETURN
    CALL check_bsy_mode(.tsk,tsk_rob[.tsk],m_char)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    IF cfg.lock[.tsk] THEN
	$alm_put[.tsk] = $ae_cfglock
	CALL alarm_put(.tsk,0,0)
	$snd_res[.tsk] = $res_ng
	RETURN
    END
    CALL setcfg_sub(.tsk,cfgset_exe)
TYPE "MADA Cfg Changed"
.** SCRX10-027-4 a++
    CALL cfg_change(.tsk)
.*                                                      /* SCRX12-006 1 a++
    if $snd_res[.tsk] == $res_ng
        RETURN
    END
.*                                                      /* SCRX12-006 1 a--
.** SCRX10-027-4 a--
    $snd_res[.tsk] = $res_ok
.END
.PROGRAM setcfg_sub(.tsk,.mod)
    cfg.lock[.tsk] = OFF
    FOR .buf = 1 TO tsk_num[.tsk] STEP 1
	STR_MDIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,.buf],":"
	IF tsk_div[.tsk]==2 THEN
	    .prt = port_non
	    $cfg.dat[.tsk] = $tsk_dat[.tsk,d_divp+1]
	ELSEIF tsk_div[.tsk] == 3 tHEN
	    CALL id_port(.tsk,d_divp+1,d_prt1)
	    .prt = tsk_dat[.tsk,d_prt1]
	    IF NOT .prt THEN
		CALL id_robot(.tsk,d_divp+1,d_rob1)
		.prt = tsk_dat[.tsk,d_rob1]
	    END
	    IF NOT .prt		GOTO epm
	    $cfg.dat[.tsk] = $tsk_dat[.tsk,d_divp+2]
	ELSE
	    GOTO efm
	END
	IF $cfg.dat[.tsk] == ""		GOTO epm
	$cfg.key[.tsk] = $tsk_dat[.tsk,d_divp]
	CALL cfg_table(.tsk,.prt,.mod)
	IF cfg.err[.tsk]		GOTO epm
    END
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.***************
.*** QRYCFG  ***
.***************
.PROGRAM qrycfg_cmd(.tsk)
    .all = OFF
    .hst = tsk_hst[.tsk]
    $cfg.dat[.tsk] = ""
    IF $hst_old[.hst] == $hst_now[.hst] THEN
	IF qrycfg.cnt[.hst]	GOTO ack
    END
    CALL qrycfg.entry(.tsk,"")
    .all = ON
    IF (tsk_num[.tsk]==0) THEN
.*TYPE "QryCfg"
	.prt = port_all
	GOTO ack
    ELSEIF (tsk_num[.tsk]==1) THEN
	IF "NULL"==$TOUPPER($tsk_dat[.tsk,1]) THEN
.*TYPE "QryCfg,NULL"
	    .prt = port_non
	    GOTO ack
	ELSE
.*TYPE "QryCfg,Port"
	    CALL id_port(.tsk,1,d_prt1)
	    .prt = tsk_dat[.tsk,d_prt1]
	    IF .prt	GOTO ack
.*TYPE "QryCfg,Robot"
	    CALL id_unit(.tsk,1,d_rob1)
	    .prt = tsk_dat[.tsk,d_rob1]
	    IF .prt	GOTO ack
	END
    END
    .all = OFF
    FOR .buf = 1 TO tsk_num[.tsk] STEP 1
	STR_2DIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,.buf],":"
.*TYPE $tsk_dat[.tsk,.buf]
	IF tsk_div[.tsk]==0 THEN
	    $cfg.key[.tsk] = $tsk_dat[.tsk,.buf]
	    CALL cfg_table(.tsk,port_non,cfgqry_chk)
	ELSEIF tsk_div[.tsk] == 2 tHEN
	    $cfg.key[.tsk] = $tsk_dat[.tsk,d_divp]
	    CALL id_port(.tsk,d_divp+1,d_prt1)
	    .prt = tsk_dat[.tsk,d_prt1]
	    IF NOT .prt THEN
		CALL id_robot(.tsk,d_divp+1,d_rob1)
		.prt = tsk_dat[.tsk,d_rob1]
	    END
	    IF NOT .prt		GOTO epm
	    CALL cfg_table(.tsk,.prt,cfgqry_chk)
	ELSE
	    GOTO efm
	END
	IF cfg.err[.tsk] AND (NOT vtp.qrycfgerror[.tsk]) THEN
	    FOR .tmp = 1 TO cfg_key_table[.tsk] STEP 1
		IF GETSTRBLK($cfg_key_table[.tsk,.tmp],",",$cfg.key[.tsk]) THEN
		    CALL qrycfg.entry(.tsk,$tsk_dat[.tsk,.buf]+":-10000")
		    cfg.err[.tsk] = OFF
		    .tmp = cfg_key_table[.tsk]+1	;** For-Loop end
		END
	    END
	END
	IF cfg.err[.tsk]	GOTO epm
    END
;** 複数指定で一行を超える場合は一行にする
    qrycfg.cnt[.tsk] = 1
    qrycfg.max[.tsk] = 1
ack:
    CALL send_ack(.tsk)
    IF .all THEN
	$cfg.key[.tsk] = "ALL"
	IF (.prt == port_non)OR(.prt==port_all) THEN
	    CALL cfg_table(.tsk,port_non,cfgqry_exe)
	END
	IF (.prt)THEN
	    CALL cfg_table(.tsk,.prt,cfgqry_exe)
	END
    END
    qrycfg.cnt[.hst] = qrycfg.cnt[.hst]-1
    $snd_res[.tsk] = $res+","+$TRIM_B($ENCODE(/I,qrycfg.cnt[.hst]))+","+$qrycfg[.hst,qrycfg.max[.hst]-qrycfg.cnt[.hst]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM qrycfg.entry(.tsk,.$str)
    .hst = tsk_hst[.tsk]
    IF .$str == "" THEN
	qrycfg.max[.hst] = 0
    ELSE
	IF qrycfg.max[.hst] == 0 THEN
	     qrycfg.max[.hst] = 1
	    $qrycfg[.hst,qrycfg.max[.hst]] = ""
	END
	IF ((LEN($qrycfg[.hst,qrycfg.max[.hst]])+LEN(.$str))> qrycfg_len) THEN
	     qrycfg.max[.hst] = qrycfg.max[.hst] + 1
	    $qrycfg[.hst,qrycfg.max[.hst]] = ""
	END
	IF $qrycfg[.hst,qrycfg.max[.hst]]<>"" THEN
	    $qrycfg[.hst,qrycfg.max[.hst]]=$qrycfg[.hst,qrycfg.max[.hst]]+","
	END
	$qrycfg[.hst,qrycfg.max[.hst]]=$qrycfg[.hst,qrycfg.max[.hst]]+.$str
    END
    qrycfg.cnt[.hst] = qrycfg.max[.hst]
.END
.*****************
.*** CFG_TABLE ***
.*****************
.PROGRAM cfg_table(.tsk,.prt,.mod)
    IF (.mod==cfgset_nex)OR(.mod==cfgset_def) THEN
	$cfg.key[.tsk] = "ALL"
	$cfg.dat[.tsk] = ""
    ELSE
	$cfg.key[.tsk] = $TOUPPER($cfg.key[.tsk])
	$cfg.dat[.tsk] = $TOUPPER($cfg.dat[.tsk])
    END
    $cfg.old[.tsk] = ""
    $cfg.prt[.tsk] = ""
     cfg.mod[.tsk] = .mod
     cfg.err[.tsk] = ON
    IF .prt == port_non THEN
	CALL cfg_none(.tsk)
    ELSEIF .prt == port_all THEN
	IF (tsk_rob[.tsk] BAND bit[1]) THEN
	    $cfg.prt[.tsk] = $vrp.rob_asso[1]
	    CALL cfg_robot(.tsk,1)
	END
	IF (tsk_rob[.tsk] BAND bit[2]) THEN
	    $cfg.prt[.tsk] = $vrp.rob_asso[2]
	    CALL cfg_robot(.tsk,2)
	END
.**	IF (tsk_rob[.tsk] BAND bit[3]) THEN
.**	    $cfg.prt[.tsk] = $vrp.rob_asso[3]
.**	    CALL cfg_robot(.tsk,3)
.**	END
	FOR .cnt = 1 TO id_port[.tsk,0] STEP 1
	    $cfg.prt[.tsk] = $vpp.portasso[id_port[.tsk,.cnt]]
	    CALL cfg_port(.tsk,id_port[.tsk,.cnt])
	END
    ELSEIF (.prt==1)OR(.prt==2) THEN
	$cfg.prt[.tsk] = $vrp.rob_asso[.prt]
	CALL cfg_robot(.tsk,.prt)
    ELSE	;** PORT
	$cfg.prt[.tsk] = $vpp.portasso[.prt]
	CALL cfg_port(.tsk,.prt)
    END
.END

.****************
.*** CFG_R100 ***
.****************
.PROGRAM cfg_r100(.tsk,.$key,.key,.lck,.def,.min,.max)
    CALL cfg_real(.tsk,.$key,.key,.lck,.def,.min,.max)
.END
.****************
.*** CFG_REAL ***
.****************
.PROGRAM cfg_real(.tsk,.$key,.key,.lck,.def,.min,.max)
    IF ($cfg.key[.tsk]<>.$key)AND($cfg.key[.tsk]<>"ALL")	RETURN
    IF ($cfg.old[.tsk]==.$key)					RETURN
    cfg.err[.tsk] = OFF
    CASE cfg.mod[.tsk] OF
      VALUE cfgset_nex,cfgset_def :
	CALL cfg_key_table(.tsk,.$key)
	NOEXIST_SET_R .key = .def,,cfg.mod[.tsk]
	NOEXIST_SET_R .lck =    1,,cfg.mod[.tsk]
      VALUE cfgset_chk,cfgset_exe :
	IF NOT IS_INTVAL($cfg.dat[.tsk])	GOTO err
	.dat = VAL($cfg.dat[.tsk])
	IF "cfg_r100"==$RPGNAME(1) THEN
	    .dat = .dat/100
	END
	IF (.dat<.min)OR(.max<.dat)		GOTO err
	IF (.lck) AND (lockcfg[.tsk]) THEN
	    cfg.lock[.tsk] = ON
	END
	IF cfg.mod[.tsk] == cfgset_exe THEN
	    .key = .dat
	END
      VALUE cfgqry_chk,cfgqry_exe :
	IF "cfg_r100"==$RPGNAME(1) THEN
	    CALL int_str(.key,$str[.tsk],100)
	ELSE
	    CALL int_str(.key,$str[.tsk],1)
	END
	IF $cfg.prt[.tsk] == "" THEN
	    $str[.tsk] = .$key + ":" + $str[.tsk]
	ELSE
	    $str[.tsk] = .$key + ":" + $cfg.prt[.tsk] + ":" + $str[.tsk]
	END
	CALL qrycfg.entry(.tsk,$str[.tsk])
      VALUE cfglck_chk :
;****** Do Nothing **
      VALUE cfglck_exe :
	IF $cfg.dat[.tsk] == "0" THEN
	    .lck = 0
	ELSE
	    .lck = 1
	END
    END
    $cfg.old[.tsk]=.$key
    RETURN
err:
    cfg.err[.tsk] = ON
.END
.****************
.*** CFG_RSTR ***
.****************
.PROGRAM cfg_rstr(.tsk,.$key,.key,.lck,.$str,.bas,.off)
    IF ($cfg.key[.tsk]<>.$key)AND($cfg.key[.tsk]<>"ALL")	RETURN
    IF ($cfg.old[.tsk]==.$key)					RETURN
    CASE cfg.mod[.tsk] OF
      VALUE cfgset_nex,cfgset_def,cfglck_chk,cfglck_exe :
.****** Q6426-1 a
	CALL cfg_key_table(.tsk,.$key)
	RETURN
      VALUE cfgset_chk,cfgset_exe :
	IF $cfg.dat[.tsk]<>.$str		GOTO err
	IF (.lck) AND (lockcfg[.tsk]) THEN
	    cfg.lock[.tsk] = ON
	END
	IF cfg.mod[.tsk] == cfgset_exe THEN
	    .key = .bas
	END
	cfg.err[.tsk] = OFF
      VALUE cfgqry_chk,cfgqry_exe :
	IF (.key<(.bas-.off))OR((.bas+.off)<.key)	GOTO err
	IF $cfg.prt[.tsk] == "" THEN
	    $str[.tsk] = .$key + ":" + .$str
	ELSE
	    $str[.tsk] = .$key + ":" + $cfg.prt[.tsk] + ":" + .$str
	END
	CALL qrycfg.entry(.tsk,$str[.tsk])
	cfg.err[.tsk] = OFF
    END
    $cfg.old[.tsk]=.$key
    RETURN
err:
    cfg.err[.tsk] = ON
.END

.PROGRAM cfg_key_table(.tsk,.$key)
    IF .$key=="" THEN
	cfg_key_table[.tsk] = 1
	$cfg_key_table[.tsk,cfg_key_table[.tsk]] = ","
    ELSE
	.ari = OFF
	FOR .tbl = 1 TO cfg_key_table[.tsk] STEP 1
	    IF GETSTRBLK($cfg_key_table[.tsk,.tbl],",",.$key) THEN
		.ari = ON
	    END
	END
	IF NOT .ari THEN
	    IF (LEN($cfg_key_table[.tsk,cfg_key_table[.tsk]])+LEN(.$key)+1)>255 THEN
		cfg_key_table[.tsk] = cfg_key_table[.tsk]+1
		$cfg_key_table[.tsk,cfg_key_table[.tsk]] = ","
	    END
	    $cfg_key_table[.tsk,cfg_key_table[.tsk]] = $cfg_key_table[.tsk,cfg_key_table[.tsk]] + .$key + ","
	END
    END
.END
.** SCRX10-027-4 a++
.PROGRAM cfg_change.save(.tsk)
    FOR .cnt = 1 TO id_port[.tsk,0] STEP 1
        .prt = id_port[.tsk,.cnt]
        vpo.slot[ .prt] = vpp.slot[ .prt]
        vpo.pitch[.prt] = vpp.pitch[.prt]
.*                                                      /* SCRX12-006 1 a++
        if (port_type[0,.prt] BAND port_util) AND tsk_rob[.tsk] == 2 then
            vpo.x1offset[.prt] = vpp.x1offset[.prt]
        end
.*                                                      /* SCRX12-006 1 a--
    END
.END

.PROGRAM cfg_change(.tsk)
    .tp = OFF
    FOR .cnt = 1 TO id_port[.tsk,0] STEP 1
        .prt = id_port[.tsk,.cnt]
        IF (vpo.slot[.prt]<>vpp.slot[.prt])THEN
            CALL port_slot.chg(.tsk,.prt)
            vpo.slot[ .prt] = vpp.slot[ .prt]
            .tp = ON
        ELSEIF (vpo.pitch[.prt]<>vpp.pitch[.prt])THEN
            CALL port_slot.chg(.tsk,.prt)
            vpo.pitch[.prt] = vpp.pitch[.prt]
.*                                                      /* SCRX12-006 1 a++
        ELSEIF (port_type[0,.prt] BAND port_util) AND tsk_rob[.tsk] == 2 THEN
            IF (vpo.x1offset[.prt]<>vpp.x1offset[.prt]) THEN
                CALL port_slot.chg(.tsk,.prt)
                if $snd_res[.tsk]==$res_ng then
                    vpp.x1offset[.prt] = vpo.x1offset[.prt]
                    CALL port_slot.chg(.tsk,.prt)
                else
                    vpo.x1offset[.prt] = vpp.x1offset[.prt]
                end
            END
.*                                                      /* SCRX12-006 1 a--
        END
    END
    IF .tp THEN
	CALL small_tp.chg(.tsk)
    END
.END

.PROGRAM port_slot.chg(.tsk,.prt)
    FOR .rob = 1 TO 2 STEP 1
        IF .rob == tsk_rob[.tsk] THEN
            .zzz = DEXT(#vc.tchpos[.rob,.prt,0],3)
            .dis = IFELSE(.zzz<-999,0,vpp.pitch[.prt])
            CASE (port_type[0,.prt] BAND port_form ) OF
              VALUE p_dive:
                .zzz = DEXT(#vc.tchpos[.rob,.prt,0],2)
                .dis = IFELSE(.zzz<-999,0,vpp.pitch[.prt])
                IF port_type[.rob,.prt]==p_dive_buff THEN
                    IF ($vpp.tslots[.prt]<>"1" AND $vpp.tslots[.prt]<>"1;0") THEN
                        FOR .slt = 0 TO vpp.slot[.prt] STEP 1
                            POINT #vc.tchpos[.rob,.prt,.slt] = #none
                        END
                        CALL event_replace(.tsk,.rob,0,.prt,0,.$ev1,$EV_RETEACH)
                        CALL event_replace(.tsk,.rob,0,.prt,0,.$ev2,$EV2_RETEACH)
                        CALL event_entry(.tsk,.$ev1,.$ev2)
                        GOTO next
                    END
                END
                FOR .i = 1 TO vpp.slot[.prt] STEP 1
                    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #vc.tchpos[.rob,.prt,0],inv_hand
                    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,(.i-1)*.dis
                    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
                    ZL3JNT rob_tsk[.rob]: #vc.tchpos[.rob,.prt,.i] = add[.tsk,1],#vc.tchpos[.rob,.prt,0], inv_normal, inv_hand
                END
              ANY :
                FOR .slt = 1 TO vpp.slot[.prt] STEP 1
                    POINT #vc.tchpos[.rob,.prt,.slt] = #PPJSET(#vc.tchpos[.rob,.prt,0],3,.zzz+(.slt-1)*.dis)
                END
            END
next:
.*                                                      /* SCRX11-066-1 a++
.*          Check if position has been saved previously.
            .is_tch = TRUE
            FOR .j = JT1 TO vnp.axiscnt[.rob]+2 STEP 1
                .jt[.j] = DEXT(#vc.tchpos[.rob,.prt,0],.j)
                .is_tch = IFELSE(.jt[.j]<none+1,FALSE,.is_tch)
            END
            IF .is_tch THEN
                FOR .slt = 1 TO vpp.slot[.prt] STEP 1
                    IF (.slt > 1) AND (port_type[0,.prt] BAND port_util) AND (tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
                        POINT .#tch = #vc.tchpos[.rob,.prt,0]
                        ZL3TRN rob_tsk[.rob]: .tch[1] = .#tch,inv_hand
                        ZL3TRN rob_tsk[.rob]: .add[1] = 0,vpp.x1offset[.prt]
                        ZL3TRN rob_tsk[.rob]: .add[1] = .tch[1] ADD .add[1]
                        ZL3JNT/ERR .err rob_tsk[.rob]: .#tch = .add[1],.#tch, inv_normal, inv_hand
                        IF .err THEN
                            CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_X1off_TchErr)
                            $snd_res[.tsk] = $res_ng
                            GOTO err1
                        END
                        POINT #vc.tchpos[.rob,.prt,.slt] = #PPJSET(.#tch,3,.zzz+(.slt-1)*.dis)
                    END
                END
            END
err1:
            .zzz = DEXT(#vc.savpos[.rob,.prt,0],3)
            .dis = IFELSE(.zzz<-999,0,vpp.pitch[.prt])
            CASE (port_type[0,.prt] BAND port_form ) OF
              VALUE p_dive:
                .zzz = DEXT(#vc.savpos[.rob,.prt,0],2)
                .dis = IFELSE(.zzz<-999,0,vpp.pitch[.prt])
                IF port_type[.rob,.prt]==p_dive_buff THEN
                    IF ($vpp.tslots[.prt]<>"1" AND $vpp.tslots[.prt]<>"1;0") THEN
                        FOR .slt = 0 TO vpp.slot[.prt] STEP 1
                            POINT #vc.savpos[.rob,.prt,.slt] = #none
                        END
                        CALL event_replace(.tsk,.rob,0,.prt,0,.$ev1,$EV_RETEACH)
                        CALL event_replace(.tsk,.rob,0,.prt,0,.$ev2,$EV2_RETEACH)
                        CALL event_entry(.tsk,.$ev1,.$ev2)
                        GOTO next2
                    END
                END
                FOR .i = 1 TO vpp.slot[.prt] STEP 1
                    ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #vc.savpos[.rob,.prt,0],inv_hand
                    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,(.i-1)*.dis
                    ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
                    ZL3JNT rob_tsk[.rob]: #vc.savpos[.rob,.prt,.i] = add[.tsk,1],#vc.savpos[.rob,.prt,0], inv_normal, inv_hand
                END
              ANY :
                FOR .slt = 1 TO vpp.slot[.prt] STEP 1
                    POINT #vc.savpos[.rob,.prt,.slt] = #PPJSET(#vc.savpos[.rob,.prt,0],3,.zzz+(.slt-1)*.dis)
                END
            END
next2:
            .is_tch = TRUE
            FOR .j = JT1 TO vnp.axiscnt[.rob]+2 STEP 1
                .jt[.j] = DEXT(#vc.savpos[.rob,.prt,0],.j)
                .is_tch = IFELSE(.jt[.j]<none+1,FALSE,.is_tch)
            END
            IF .is_tch THEN
                FOR .slt = 1 TO vpp.slot[.prt] STEP 1
                    IF (.slt > 1) AND (port_type[0,.prt] BAND port_util) AND (tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
                        POINT .#sav = #vc.savpos[.rob,.prt,0]
                        ZL3TRN rob_tsk[.rob]: .sav[1] = .#sav,inv_hand
                        ZL3TRN rob_tsk[.rob]: .add[1] = 0,vpp.x1offset[.prt]
                        ZL3TRN rob_tsk[.rob]: .add[1] = .sav[1] ADD .add[1]
                        ZL3JNT/ERR .err rob_tsk[.rob]: .#sav = .add[1],.#sav, inv_normal, inv_hand
                        IF .err THEN
                            CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_X1off_SavErr)
                            $snd_res[.tsk] = $res_ng
                            GOTO err2
                        END
                        POINT #vc.savpos[.rob,.prt,.slt] = #PPJSET(.#sav,3,.zzz+(.slt-1)*.dis)
                    END
                END
            END
err2:
.*                                                      /* SCRX11-066-1 a--
.*                                                  /* SCRX11-032-8 a++ */

.*                                                      /* SCRX11-066-1 d++
.*          Auto save only if position has been saved previously.
.*            IF .is_tch THEN
.*                FOR .slt = 0 TO vpp.slot[.prt] STEP 1
.*                    POINT #vc.savpos[.rob,.prt,.slt] = #vc.tchpos[.rob,.prt,.slt]
.*                END
.*            END
.*                                                      /* SCRX11-066-1 d--
.*          Reset unused slots to -1000 (none)
            IF vpo.slot[.prt] > vpp.slot[.prt] THEN
                FOR .slt = vpp.slot[.prt]+1 TO vpo.slot[.prt]
                    POINT #vc.tchpos[.rob,.prt,.slt] = #none
.*                                                  /* SCRX11-066-1 m */
                    POINT #vc.savpos[.rob,.prt,.slt] = #none
                END
            END
.*                                                  /* SCRX11-032-8 a-- */
            FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
                IF DEXT(#vc.tchpos[.rob,.prt,0],id_anum[.rob,.cnt])<-999  RETURN
            END
            POINT #jnt[.tsk] = #vc.tchpos[.rob,.prt,0]
            CALL tchpos_exe(.tsk,.rob,0,.prt,1,pps_tch,OFF)
        END
    END
.END

.PROGRAM event_replace(.tsk,.rob,.hnd,.prt,.slt,.$event_out,.$event_in)
    .$event_out = .$event_in
    IF .rob THEN
        .$event_out = $REPLACE(.$event_out,"%R",$vrp.rob_asso[.rob],-1)
    END
    IF .hnd THEN
        .$event_out = $REPLACE(.$event_out,"%B",$ENCODE("B",/I1,.hnd),-1)
    END
    IF .prt THEN
        .$event_out = $REPLACE(.$event_out,"%P",$vpp.portasso[.prt],-1) 
    END
    IF .slt THEN
        .$event_out = $REPLACE(.$event_out,"%S",$ENCODE(.slt),-1)
    END
    IF .tsk THEN
        .$event_out = $REPLACE(.$event_out,"%DATA",$cfg.key[.tsk],-1) 
    END
.END
.** SCRX10-027-4 a--
.******************************************
.** port.as
.******************************************
.PROGRAM port.entry(.tsk,.$id,.$pa,.id,.typ0,.typ1,.typ2)
     id_port[.tsk,0] = id_port[.tsk,0] + 1
     id_port[.tsk,id_port[.tsk,0]] =  .id
    $id_port[.tsk,id_port[.tsk,0]] = .$id
     port_type[0,.id] = .typ0
     port_type[1,.id] = .typ1
     port_type[2,.id] = .typ2
     NOEXIST_SET_S $vpp.portasso[.id] = $IFELSE(.$pa<>"",.$pa,.$id)
     CALL cfg_def_set(.id)
.END
.******************************************
.** mode.as
.******************************************
.REALS
    m_init = ^H001	;** Initialization and Low Level Motion Commands
    m_tech = ^H002	;** Teaching Commands
    m_auto = ^H004	;** Automatic Run Commands
    m_char = ^H008	;** Characterize Commands
    m_stat = ^H010	;** Status Request Commands
    m_phtm = ^H020	;** Phantom (No Real Wafer) Mode
    m_tout = ^H040	;** T1 Time out Mode
    m_bksp = ^H080	;** Bask Space OK Mode (N/A)
    m_pcyc = ^H0100	;** Phantom Cycle Mode
.** harish 090527 a
    m_soft = ^H0200     ;** Soft touch mode
.END
.PROGRAM mode_init()
    mode_auto = m_init          + m_auto          + m_stat          + m_tout
    mode_phtm = m_init          + m_auto          + m_stat + m_phtm + m_tout
    mode_tech = m_init + m_tech          + m_char + m_stat          + m_tout
    mode_term = m_init + m_tech + m_auto + m_char + m_stat                   + m_bksp
    mode_ptrm = m_init + m_tech + m_auto + m_char + m_stat + m_phtm + m_tout
    mode_pcyc = m_init + m_tech + m_auto + m_char + m_stat          + m_tout           + m_pcyc
;
.** harish 090527 a
    mode_soft = m_init + m_tech          + m_char + m_stat          + m_tout			+ m_soft
;
    vc.mode[pc3] = mode_auto
    vc.mode[pc4] = mode_auto
.END
.** harish 090527 a++
.PROGRAM softparam1()
    ZYMSRVPRM 1:2 AS_PERLM = 40*1.5
    ZYMSRVPRM 1:4 AS_PERLM = 40*1.5
    ZYMSRVPRM 1:6 AS_PERLM = 20*1.5
    ZYMSRVPRM 1:7 AS_PERLM = 60*1.5
;
    ZILIMCHG  1:2 CCWILIM = 50*1.5	
    ZILIMCHG  1:2 CWILIM  = 50*1.5
    ZILIMCHG  1:4 CCWILIM = 50*1.5
    ZILIMCHG  1:4 CWILIM  = 50*1.5
    ZILIMCHG  1:6 CCWILIM = 25*1.5
    ZILIMCHG  1:6 CWILIM  = 25*1.5
    ZILIMCHG  1:7 CCWILIM = 50*1.5
    ZILIMCHG  1:7 CWILIM  = 50*1.5
;   ZAXIS 1:7 = ,,,,,,,,1500
.END
.PROGRAM reptparam1()
    ZYMSRVPRM 1:2 AS_PERLM = srvprm[srv_nt570,2,24]
    ZYMSRVPRM 1:4 AS_PERLM = srvprm[srv_nt570,4,24]
    ZYMSRVPRM 1:6 AS_PERLM = srvprm[srv_nt570,6,24]
    ZYMSRVPRM 1:7 AS_PERLM = srvprm[srv_nt570,7,24]
;
    ZILIMCHG  1:2 CCWILIM = 0
    ZILIMCHG  1:2 CWILIM  = 0
    ZILIMCHG  1:4 CCWILIM = 0
    ZILIMCHG  1:4 CWILIM  = 0
    ZILIMCHG  1:6 CCWILIM = 0
    ZILIMCHG  1:6 CWILIM  = 0
    ZILIMCHG  1:7 CCWILIM = 0
    ZILIMCHG  1:7 CWILIM  = 0
;   ZAXIS 1:7 = ,,,,,,,,1000
.END
.PROGRAM softparam2()
    ZYMSRVPRM 2:2 AS_PERLM = 30*1.5
    ZYMSRVPRM 2:4 AS_PERLM = 30*1.5
    ZYMSRVPRM 2:6 AS_PERLM = 60*1.5
;
    ZILIMCHG  2:2 CCWILIM = 40*1.5
    ZILIMCHG  2:2 CWILIM  = 40*1.5
    ZILIMCHG  2:4 CCWILIM = 30*1.5
    ZILIMCHG  2:4 CWILIM  = 30*1.5
    ZILIMCHG  2:6 CCWILIM = 30*1.5
    ZILIMCHG  2:6 CWILIM  = 30*1.5
.END
.PROGRAM reptparam2()
    ZYMSRVPRM 2:2 AS_PERLM = srvprm[srv_nt410,2,24]
    ZYMSRVPRM 2:4 AS_PERLM = srvprm[srv_nt410,4,24]
    ZYMSRVPRM 2:6 AS_PERLM = srvprm[srv_nt410,6,24]
;
    ZILIMCHG  2:2 CCWILIM = 0
    ZILIMCHG  2:2 CWILIM  = 0
    ZILIMCHG  2:4 CCWILIM = 0
    ZILIMCHG  2:4 CWILIM  = 0
    ZILIMCHG  2:6 CCWILIM = 0
    ZILIMCHG  2:6 CWILIM  = 0
.END
.PROGRAM soft_param(.tsk,.unt,.chg)

    IF .unt == tsk_rob[.tsk] AND tsk_arm[.unt]  == arm_nt570
	IF .chg 
          CALL softparam1
	  type "NT570 SERVO PARAMETER CHANGE TO SOFT"
        ELSE
          CALL reptparam1
	  type "NT570 SERVO PARAMETER CHANGE TO NORMAL"
        END
    ELSEIF .unt == tsk_rob[.tsk] AND tsk_arm[.unt]  == arm_nt410
        IF .chg 
          CALL softparam2
	  type "NT410 SERVO PARAMETER CHANGE TO SOFT"
        ELSE
          CALL reptparam2
	  type "NT410 SERVO PARAMETER CHANGE TO NORMAL"
	END
    END
.END
.PROGRAM change_soft(.tsk,.unt,.chg)
    CALL is_servo(.tsk,.unt,OFF)
    CALL power_off(.tsk,.unt)
    TWAIT 0.5
    CALL soft_param(.tsk,.unt,.chg)
    TWAIT 0.5
    IF is_servo[.tsk]
	CALL power_on(.tsk,.unt)
	CALL servo_on(.tsk,.unt)
	IF NOT servo_on[.tsk] THEN
.** SCRY10-003-1 a++
	    IF vc.mode[.tsk] == mode_soft THEN 
		CALL soft_param(.tsk,.unt,ON)
	    ELSE
		CALL soft_param(.tsk,.unt,OFF)
	    END
.** SCRY10-003-1 a--
	    $snd_res[.tsk] = $res_ng
	    RETURN
	END
    END	
.END
.** harish 090527 a--
.PROGRAM mode_cmd(.tsk)
    IF tsk_num[.tsk]<>1		GOTO efm
    SCASE $TOUPPER($tsk_dat[.tsk,1]) OF
      SVALUE "AUTO" :
	.mod = mode_auto
      SVALUE "PHTM" :
	.mod = mode_phtm
      SVALUE "TCH" :
	.mod = mode_tech
      SVALUE "TERM" :
	.mod = mode_term
      SVALUE "PTERM" :
	.mod = mode_ptrm
      SVALUE "PCYC" :
	.mod = mode_pcyc
.** harish 090527 a++
      SVALUE "SOFT" :
	.mod = mode_soft
.** harish 090527 a--
      ANY :
	GOTO epm
    END
    IF vc.mode[.tsk]==.mod THEN
	CALL send_ack(.tsk)
.** harish 090527 a
        GOTO nchg
    ELSE
	CALL check_bsy_mode(.tsk,tsk_rob[.tsk],m_init)
	IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    END
.** harish 090527 a++
    IF .mod == mode_soft
	CALL change_soft(.tsk,tsk_rob[.tsk],ON)
	vc.speed[tsk_rob[.tsk]]=spd_table[tsk_rob[.tsk],spd_escape]
    ELSEIF vc.mode[.tsk] == mode_soft AND SWITCH("REPEAT")
	CALL change_soft(.tsk,tsk_rob[.tsk],OFF)
    END
    IF $snd_res[.tsk]<>"" RETURN
nchg:
.** harish 090527 a--
    vc.mode[.tsk]=.mod
    $snd_res[.tsk] = $res_ok
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.PROGRAM qrymode_cmd(.tsk)
    IF tsk_num[.tsk]<>0		GOTO efm
    CALL send_ack(.tsk)
    CASE vc.mode[.tsk] OF
      VALUE mode_auto :
	$snd_res[.tsk] = "AUTO"
      VALUE mode_phtm :
	$snd_res[.tsk] = "PHTM"
      VALUE mode_tech :
	$snd_res[.tsk] = "TCH"
      VALUE mode_term :
	$snd_res[.tsk] = "TERM"
      VALUE mode_ptrm :
	$snd_res[.tsk] = "PTERM"
      VALUE mode_pcyc :
	$snd_res[.tsk] = "PCYC"
.** harish 090527 a++
      VALUE mode_soft :
	$snd_res[.tsk] = "SOFT"
.** harish 090527 a--
      ANY :
	$snd_res[.tsk] = "ERR("+$TRIM_B($ENCODE(/H,vc.mode[.tsk]))+")"
    END
    $snd_res[.tsk] = $res + "," + $snd_res[.tsk]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END

.PROGRAM check_bsy_mode(.tsk,.unt,.mod)
.** Input  .unt must be BIT!!!
;** Check Busy or NOT
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF (tsk_cmd[.rob])OR(tsk_use[.rob])OR(snd_res[.rob]) THEN
		IF NOT zt_chk_cmd THEN
		    $snd_ack[.tsk] = $ack_busy
		    RETURN
		END
	    END
	END
    END
    CALL send_ack(.tsk)
.***小型TPコマンドはモードチェックしない
    IF (tsk_hst[.tsk]==host_stp1)OR(tsk_hst[.tsk]==host_stp2)	GOTO exit
;** Check Command Mode
    IF NOT (vc.mode[.tsk] BAND .mod) THEN
	IF (.mod==m_auto)AND(vc.mode[.tsk]==mode_tech) THEN
	    $alm_put[.tsk] = $ae_md_auto_tech
	ELSEIF (.mod==m_tech)AND(vc.mode[.tsk]==mode_auto) THEN
	    $alm_put[.tsk] = $ae_md_tech_auto
	ELSEIF (.mod==m_tech)AND(vc.mode[.tsk]==mode_phtm) THEN
	    $alm_put[.tsk] = $ae_md_tech_phtm
	ELSEIF (.mod==m_char)AND(vc.mode[.tsk]==mode_auto) THEN
	    $alm_put[.tsk] = $ae_md_char_auto
	ELSEIF (.mod==m_char)AND(vc.mode[.tsk]==mode_phtm) THEN
	    $alm_put[.tsk] = $ae_md_char_phtm
.** harish 090527 a++
	ELSEIF (.mod==m_auto)AND(vc.mode[.tsk]==mode_soft) THEN
	    $alm_put[.tsk] = $ae_md_auto_soft
.** harish 090527 a--
	ELSE
	    $alm_put[.tsk] = $ae_md_error_any
	END
	CALL alarm_put(.tsk,0,0)
	CALL make_res_ng(.tsk)
    END
exit:
    RETURN
.END
.******************************************
.** id_check.as
.******************************************
.PROGRAM id_unit(.tsk,.buf,.rob)
    IF $tsk_dat[.tsk,.buf] == ""	GOTO err
    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
      SVALUE $rob_id[.tsk,1],$vrp.rob_asso[1] :
	tsk_dat[.tsk,.rob] = 1
      SVALUE $rob_id[.tsk,2],$vrp.rob_asso[2] :
	tsk_dat[.tsk,.rob] = 2
      SVALUE $rob_id[.tsk,3],$vrp.rob_asso[3] :
	tsk_dat[.tsk,.rob] = 3
      SVALUE $rob_id[.tsk,4],$vrp.rob_asso[4] :
	tsk_dat[.tsk,.rob] = 4
      ANY :
	GOTO err
    END
    IF NOT (tsk_rob[.tsk] BAND bit[tsk_dat[.tsk,.rob]])	GOTO err
    RETURN
err:
    tsk_dat[.tsk,.rob] = 0
.END
.PROGRAM id_robot(.tsk,.buf,.rob)
    IF $tsk_dat[.tsk,.buf] == ""	GOTO err
    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
      SVALUE $rob_id[.tsk,1],$vrp.rob_asso[1] :
	tsk_dat[.tsk,.rob] = 1
      SVALUE $rob_id[.tsk,2],$vrp.rob_asso[2] :
	tsk_dat[.tsk,.rob] = 2
.***  SVALUE $rob_id[.tsk,3],$vrp.rob_asso[3] :
.***	tsk_dat[.tsk,.rob] = 3
.***  SVALUE $rob_id[.tsk,4],$vrp.rob_asso[4] :
.***	tsk_dat[.tsk,.rob] = 4
      ANY :
	GOTO err
    END
    IF NOT (tsk_rob[.tsk] BAND bit[tsk_dat[.tsk,.rob]])	GOTO err
    RETURN
err:
    tsk_dat[.tsk,.rob] = 0
.END
.PROGRAM id_aligner(.tsk,.buf,.rob)
    IF $tsk_dat[.tsk,.buf] == ""	GOTO err
    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
.***  SVALUE $rob_id[.tsk,1],$vrp.rob_asso[1] :
.***    tsk_dat[.tsk,.rob] = 1
.***  SVALUE $rob_id[.tsk,2],$vrp.rob_asso[2] :
.***    tsk_dat[.tsk,.rob] = 2
      SVALUE $rob_id[.tsk,3],$vrp.rob_asso[3] :
	tsk_dat[.tsk,.rob] = 3
      SVALUE $rob_id[.tsk,4],$vrp.rob_asso[4] :
	tsk_dat[.tsk,.rob] = 4
      ANY :
	GOTO err
    END
    IF NOT (tsk_rob[.tsk] BAND bit[tsk_dat[.tsk,.rob]])	GOTO err
    RETURN
err:
    tsk_dat[.tsk,.rob] = 0
.END

.** harish 090327-1 a++
.PROGRAM id_hand(.tsk,.buf,.rob,.hnd)
    .$str = $TOUPPER($tsk_dat[.tsk,.buf])
    CALL id_hand_sub(.tsk,.$str,.rob,.hnd)
    RETURN
.END
    
.PROGRAM id_hand_sub(.tsk,.$str,.rob,.hnd)
    SCASE $TOUPPER(.$str) OF
      SVALUE "B1" :
	tsk_dat[.tsk,.hnd] = 2
      SVALUE "B2" :
	IF NOT (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] BAND armb_dbl)	GOTO err
	tsk_dat[.tsk,.hnd] = 1
      ANY :
	GOTO err
    END
    IF ( .hnd == 2 ) AND ( NOT (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] BAND armb_dbl) ) THEN
	.hnd = 1
    END
    RETURN
err:
    .hnd = 0
    RETURN
.END
.** harish 090327-1 a--

.PROGRAM id_robot_hand(.tsk,.buf,.rob,.hnd,.fng)
    $tsk_dat[.tsk,d_divp] = $TOUPPER($tsk_dat[.tsk,.buf])
    STR_2DIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,d_divp],":"
    IF tsk_div[.tsk] <> 2			GOTO err
;** RobotID ***
    IF $tsk_dat[.tsk,d_divp] == ""	GOTO err
    SCASE $tsk_dat[.tsk,d_divp] OF
      SVALUE $rob_id[.tsk,1],$vrp.rob_asso[1]:
	tsk_dat[.tsk,.rob] = 1
      SVALUE $rob_id[.tsk,2],$vrp.rob_asso[2] :
	tsk_dat[.tsk,.rob] = 2
.**   SVALUE $rob_id[.tsk,3],$vrp.rob_asso[3] :
.**     tsk_dat[.tsk,.rob] = 3
.**   SVALUE $rob_id[.tsk,4],$vrp.rob_asso[4] :
.**     tsk_dat[.tsk,.rob] = 4
      ANY :
	GOTO err
    END
    IF NOT (tsk_rob[.tsk] BAND bit[tsk_dat[.tsk,.rob]])	GOTO err
;** Hand ID ***
    $tsk_dat[.tsk,d_divp+2] = $MID( $tsk_dat[.tsk,d_divp+1],3)
    $tsk_dat[.tsk,d_divp+1] = $LEFT($tsk_dat[.tsk,d_divp+1],2)
    IF $tsk_dat[.tsk,d_divp+1]==""	GOTO err
    SCASE $tsk_dat[.tsk,d_divp+1] OF
      SVALUE "B1" :
	tsk_dat[.tsk,.hnd] = 1
      SVALUE "B2" :
	IF NOT (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] BAND armb_dbl)	GOTO err
	tsk_dat[.tsk,.hnd] = 2
      ANY :
	GOTO err
    END
;** Finger ID ***
    IF $tsk_dat[.tsk,d_divp+2] == "" THEN
	tsk_dat[.tsk,.fng] = finger_rob[tsk_dat[.tsk,.rob],tsk_dat[.tsk,.hnd]]
    ELSE
	tsk_dat[.tsk,.fng] = 0
	IF INSTR(0,$tsk_dat[.tsk,d_divp+2],"F1") THEN
	    tsk_dat[.tsk,.fng] = tsk_dat[.tsk,.fng] + bit[1]
	    $tsk_dat[.tsk,d_divp+2] = $REPLACE($tsk_dat[.tsk,d_divp+2],"F1","")
	END
	IF INSTR(0,$tsk_dat[.tsk,d_divp+2],"F2") THEN
	    tsk_dat[.tsk,.fng] = tsk_dat[.tsk,.fng] + bit[2]
	    $tsk_dat[.tsk,d_divp+2] = $REPLACE($tsk_dat[.tsk,d_divp+2],"F2","")
	END
	IF INSTR(0,$tsk_dat[.tsk,d_divp+2],"F3") THEN
	    tsk_dat[.tsk,.fng] = tsk_dat[.tsk,.fng] + bit[3]
	    $tsk_dat[.tsk,d_divp+2] = $REPLACE($tsk_dat[.tsk,d_divp+2],"F3","")
	END
	IF INSTR(0,$tsk_dat[.tsk,d_divp+2],"F4") THEN
	    tsk_dat[.tsk,.fng] = tsk_dat[.tsk,.fng] + bit[4]
	    $tsk_dat[.tsk,d_divp+2] = $REPLACE($tsk_dat[.tsk,d_divp+2],"F4","")
	END
	IF INSTR(0,$tsk_dat[.tsk,d_divp+2],"F5") THEN
	    tsk_dat[.tsk,.fng] = tsk_dat[.tsk,.fng] + bit[5]
	    $tsk_dat[.tsk,d_divp+2] = $REPLACE($tsk_dat[.tsk,d_divp+2],"F5","")
	END
	IF $tsk_dat[.tsk,d_divp+2] <> ""	GOTO err
	IF tsk_dat[.tsk,.fng] > finger_rob[tsk_dat[.tsk,.rob],tsk_dat[.tsk,.hnd]]	GOTO err
    END
    RETURN
err:
    tsk_dat[.tsk,.rob] = 0
    tsk_dat[.tsk,.hnd] = 0
    tsk_dat[.tsk,.fng] = 0
.END


.PROGRAM id_unit_axis(.tsk,.buf,.rob,.jnt,.cod)
.* .cod == 0 Joint
.* .cod >  0 Joint,Base
.* .cod <  0 Joint,Base & Tool
.*
    $tsk_dat[.tsk,d_divp] = $TOUPPER($tsk_dat[.tsk,.buf])
    STR_2DIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,d_divp],":"
    IF tsk_div[.tsk] <> 2		GOTO err
;** UnitID ***
    CALL id_unit(.tsk,d_divp,.rob)
    IF NOT (tsk_rob[.tsk] BAND bit[tsk_dat[.tsk,.rob]])	GOTO err
;** Axis ID ***
    CALL id_axis(.tsk,d_divp+1,.rob,.jnt,.cod)
    IF NOT (tsk_dat[.tsk,.jnt])				GOTO err
    RETURN
err:
    tsk_dat[.tsk,.rob] = 0
    tsk_dat[.tsk,.jnt] = 0
.END
.PROGRAM id_axis(.tsk,.buf,.rob,.jnt,.cod)
.* .cod == 0 Joint
.* .cod >  0 Joint,Base
.* .cod <  0 Joint,Base & Tool
.*
    IF $tsk_dat[.tsk,.buf] == ""	GOTO err
    tsk_dat[.tsk,.jnt] = GETSTRBLK($id_axis[tsk_dat[.tsk,.rob]],",",$TOUPPER($tsk_dat[.tsk,.buf]))
    IF tsk_dat[.tsk,.jnt]		RETURN
;** Coord ID
    IF (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] BAND armb_nxx) THEN
	IF .cod THEN
	    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
	      SVALUE "TX1":
		tsk_dat[.tsk,.jnt] = axs_base_x1
	      SVALUE "TY1":
		tsk_dat[.tsk,.jnt] = axs_base_y1
	      SVALUE "TR1":
		tsk_dat[.tsk,.jnt] = axs_base_r1
	      SVALUE "TX2":
		tsk_dat[.tsk,.jnt] = axs_base_x2
	      SVALUE "TY2":
		tsk_dat[.tsk,.jnt] = axs_base_y2
	      SVALUE "TR2":
		tsk_dat[.tsk,.jnt] = axs_base_r2
	    END
	END
	IF .cod < 0 THEN
	    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
	      SVALUE "EX1":
		tsk_dat[.tsk,.jnt] = axs_tool_x1
	      SVALUE "EY1":
		tsk_dat[.tsk,.jnt] = axs_tool_y1
	      SVALUE "EX2":
		tsk_dat[.tsk,.jnt] = axs_tool_x2
	      SVALUE "EY2":
		tsk_dat[.tsk,.jnt] = axs_tool_y2
.** harish 090601 a++
	      SVALUE "LR1":
		tsk_dat[.tsk,.jnt] = axs_tool_f1r1
	      SVALUE "RR1":
		tsk_dat[.tsk,.jnt] = axs_tool_f2r1
	      SVALUE "LR2":
		tsk_dat[.tsk,.jnt] = axs_tool_f1r2
	      SVALUE "RR2":
		tsk_dat[.tsk,.jnt] = axs_tool_f2r2
.** harish 090601 a--
	    END
	END
	IF tsk_dat[.tsk,.jnt] >= 21 THEN
	    IF NOT (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] BAND armb_dbl)	GOTO err
	END
.** harish 090601 a++
	IF tsk_dat[.tsk,.jnt] >= 16 THEN
	    IF NOT (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] == arm_nt570) GOTO err
	END
.** harish 090601 a--
    END
    RETURN
err:
    tsk_dat[.tsk,.jnt] = 0
.END
.PROGRAM id_port(.tsk,.buf,.prt)
    CALL id_port_slot(.tsk,.buf,.prt,0)
.END
.PROGRAM id_port_slot(.tsk,.buf,.prt,.slt)
    .$str[1] = $TOUPPER($tsk_dat[.tsk,.buf])
    IF .slt THEN
	STR_2DIV .$str[1],.num = .$str[1],":"
	IF .num <> 2	GOTO err
    END
    STR_MCMP .num = .$str[1],$id_port[.tsk,1],0
    IF (0<.num)AND(.num<=id_port[.tsk,0]) THEN
	tsk_dat[.tsk,.prt] = id_port[.tsk,.num]
    ELSE
	FOR .num = 1 TO id_port[.tsk,0] STEP 1
	    IF .$str[1]==$vpp.portasso[id_port[.tsk,.num]] THEN
		tsk_dat[.tsk,.prt] = id_port[.tsk,.num]
		GOTO slt
	    END
	END
	GOTO err
    END
slt:
    IF .slt THEN
	IF .$str[2] ==""					GOTO err
	IF NOT IS_INTVAL(.$str[2])				GOTO err
	tsk_dat[.tsk,.slt] = VAL(.$str[2],0)
	IF (tsk_dat[.tsk,.slt]<=0)				GOTO err
	IF (tsk_dat[.tsk,.slt]> vpp.slot[tsk_dat[.tsk,.prt]])	GOTO err
    END
    RETURN
err:
    tsk_dat[.tsk,.prt] = 0
    RETURN
.END

.PROGRAM id_int(.tsk,.buf,.int,.rat)
    id_error[.tsk] = ON
    IF $tsk_dat[.tsk,.buf]==""			RETURN
    IF NOT IS_INTVAL($tsk_dat[.tsk,.buf])	RETURN
    tsk_dat[.tsk,.int] = VAL($tsk_dat[.tsk,.buf],0)/.rat
    id_error[.tsk] = OFF
.END

.PROGRAM id_0_1(.tsk,.buf,.dat)
    id_error[.tsk] = OFF
    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
      SVALUE "0","OFF":
	tsk_dat[.tsk,.dat] = 0
      SVALUE "1","ON":
	tsk_dat[.tsk,.dat] = 1
      ANY :
	id_error[.tsk] = ON
    END
.END

.PROGRAM id_pps(.tsk,.buf,.pps)
    tsk_dat[.tsk,.pps] = GETSTRBLK($id_pps,",",$TOUPPER($tsk_dat[.tsk,.buf]))
.END
.PROGRAM id_posid(.tsk,.buf,.rob,.pos)
    tsk_dat[.tsk,.pos] = GETSTRBLK($id_pos,",",$TOUPPER($tsk_dat[.tsk,.buf]))
    IF (tsk_dat[.tsk,d_dat1]==pos_id_coord)THEN
	IF NOT (tsk_arm[rob_tsk[tsk_dat[.tsk,.rob]]] BAND armb_nxx) THEN
	    tsk_dat[.tsk,.pos] = 0
	END
    END
.END

.PROGRAM id_loc(.tsk,.d_rob,.sss,.eee,.pid)
.** Output  #jnt[.tsk]
.**          trn[.tsk,]
    .rob = tsk_dat[.tsk,.d_rob]
    .num = .eee-.sss+1
    CASE tsk_dat[.tsk,.pid] OF
      VALUE pos_id_actl:
	POINT #jnt[.tsk] = #PPOINT()
	IF .num<>id_anum[.rob,0]	GOTO err
	FOR .num=1 TO id_anum[.rob,0] STEP 1
	    CALL str_int(.tsk,$tsk_dat[.tsk,.sss+.num-1],.dat,100)
	    IF NOT str_int[.tsk]	GOTO err
	    POINT #jnt[.tsk] = #PPJSET(#jnt[.tsk],id_anum[.rob,.num],.dat)
	END
      VALUE pos_id_coord:
	ZARRAYSET trn[.tsk,1] = 0,6
	IF (tsk_arm[rob_tsk[.rob]] BAND armb_flp ) THEN
	    IF .num<>5			GOTO err
	ELSE
	    IF .num<>4		GOTO err
	END
.****** X
	CALL str_int(.tsk,$tsk_dat[.tsk,.sss+0],trn[.tsk,1],100)
	IF NOT str_int[.tsk]		GOTO err
.****** Y
	CALL str_int(.tsk,$tsk_dat[.tsk,.sss+1],trn[.tsk,2],100)
	IF NOT str_int[.tsk]		GOTO err
.****** R1
	CALL str_int(.tsk,$tsk_dat[.tsk,.sss+2],trn[.tsk,4],100)
	IF NOT str_int[.tsk]		GOTO err
.****** R2==S1
	.num = 3
	IF (tsk_arm[rob_tsk[.rob]] BAND armb_flp ) THEN
	    CALL str_int(.tsk,$tsk_dat[.tsk,.sss+.num],trn[.tsk,5],100)
	    IF NOT str_int[.tsk]		GOTO err
	    .num = .num + 1
	END
.****** Z
	CALL str_int(.tsk,$tsk_dat[.tsk,.sss+.num],trn[.tsk,3],100)
	IF NOT str_int[.tsk]		GOTO err
      ANY :
	GOTO err
    END
    id_error[.tsk] = OFF
    RETURN
err:
    id_error[.tsk] = ON
.END
.******************************************
.** util.as
.******************************************
.PROGRAM on_off_cmd(.tsk,.dat,.mod)
.** Check Parameter Number
    IF tsk_num[.tsk]<>1		GOTO efm
.** Check 0 or 1
    CALL id_0_1(.tsk,1,d_dat1)
    IF id_error[.tsk]		GOTO epm
.** harish 090527 a++
    IF vc.mode[.tsk] == mode_soft
	CALL send_ack(.tsk)
	$alm_put[.tsk] = $AE_MD_THIS_SOFT
	CALL alarm_put(.tsk,0,0)
	CALL make_res_ng(.tsk)
	RETURN
    END
.** harish 090527 a--
.** Syntax OK
    IF .dat == tsk_dat[.tsk,d_dat1] THEN
	CALL send_ack(.tsk)
    ELSE
	CALL check_bsy_mode(.tsk,tsk_rob[.tsk],.mod)
	IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    END
.** Command Process
    .dat = tsk_dat[.tsk,d_dat1]
    $snd_res[.tsk] = $res_ok
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.REALS
    is_taught[1] = 0
    is_taught[2] = 0
    is_taught[5] = 0
    is_taught[6] = 0
.END
.PROGRAM is_taught(.tsk,.rob,.prt,.slt,.tch)
.* Output #is_taught[.tsk]
.*         is_taught[.tsk]  1 ALL Axes Taught
.*                         -1 No Z Taught
.*                          0 No
.*
    is_taught[.tsk] = 1
.** harish 090312 a++
    IF .slt >= 2 THEN
	NOEXIST_SET_L  #vc.savpos[.rob,.prt,.slt] = #none
	NOEXIST_SET_L  #vc.tchpos[.rob,.prt,.slt] = #none
    END
.** harish 090312 a--
    POINT #is_taught[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	IF DEXT(#is_taught[.tsk],id_anum[.rob,.cnt])<-999 THEN
	    IF (id_anum[.rob,.cnt]==3) THEN
		is_taught[.tsk] = -ABS(is_taught[.tsk])
	    ELSE
		is_taught[.tsk] = 0
	    END
	END
    END
.END
.PROGRAM int_str(.dat,.$dat,.rat)
    .tmp = IFELSE( .dat >= 0,.dat*.rat+0.5,.dat*.rat-0.5)
    .$dat = $TRIM_B($ENCODE(/I,INT(.tmp)))
.END
.PROGRAM str_int(.tsk,.$dat,.dat,.rat)
    str_int[.tsk] = OFF
    IF .$dat==""		RETURN
    IF NOT IS_INTVAL(.$dat)	RETURN
    .dat = VAL(.$dat,0)/.rat
    str_int[.tsk] = ON
.END

.PROGRAM loc_str(.rob,.#loc,.pid,.$res)
    .$res = ""
    IF (.pid==pos_id_coord)AND(tsk_arm[rob_tsk[.rob]] BAND armb_nxx) THEN
	.non = OFF
	FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	    IF DEXT(.#loc,id_anum[.rob,.cnt]) < -999 THEN
		.non = ON
	    END
	END
	IF .non THEN
	    ZARRAYSET .loc[1]=-1000,6
	ELSE
	    ZL3TRN rob_tsk[.rob]: .loc[1] = .#loc, 1
	END
	CALL int_str(.loc[1],.$dat,100)	;** X1
	.$res = .$res + .$dat
	CALL int_str(.loc[2],.$dat,100)	;** Y1
	.$res = .$res + "," + .$dat
	CALL int_str(.loc[4],.$dat,100)	;** R1
	.$res = .$res + "," + .$dat
	IF (tsk_arm[rob_tsk[.rob]] BAND armb_dbl) THEN
	    IF NOT .non THEN
		ZL3TRN rob_tsk[.rob]: .loc[1] = .#loc, 2
		CALL int_str(.loc[1],.$dat,100)	;** X2
		.$res = .$res + "," + .$dat
		CALL int_str(.loc[2],.$dat,100)	;** Y2
		.$res = .$res + "," + .$dat
		CALL int_str(.loc[4],.$dat,100)	;** R2
		.$res = .$res + "," + .$dat
	    END
	ELSEIF (tsk_arm[rob_tsk[.rob]] BAND armb_flp ) THEN
	    CALL int_str(.loc[5],.$dat,100)	;** S1
	    .$res = .$res + "," + .$dat
	END
	CALL int_str(.loc[3],.$dat,100)	;** Z
	.$res = .$res + "," + .$dat
    ELSE
	FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	    IF .cnt<>1 THEN
		.$res = .$res + ","
	    END
	    CALL int_str(DEXT(.#loc,id_anum[.rob,.cnt]),.$dat,100)
	    .$res = .$res + .$dat
	END
    END
.END
.REALS
    is_in_jnt[1] = 20
    is_in_jnt[2] = 10
    is_in_jnt[3] = 10
    is_in_jnt[4] = 10
    is_in_jnt[5] = 10
    is_in_jnt[6] = 10
    is_in_jnt[7] = 10
.** harish 080828 homing m+
.** harish home from U4 collision 081007 m+
    is_in_trn[1] = 50
    is_in_trn[2] = 20
    is_in_trn[3] = 20
.** harish 080924 collision during home from VDA/U4 m+
.*    is_in_trn[4] = 5       .** too small 
.** harish inverse kinematics 080930 a+
.*    is_in_trn[4] = 10
.** harish home from U4 collision 081007 m+
    is_in_trn[4] = 20
    is_in_trn[5] = 45
    is_in_trn[6] = 20
.END
.** harish 080828 homing a++
.** harish 081007 m+
.PROGRAM is_in_loc(.tsk,.rob,.hnd,.prt,.#ccc,.#sss,.#eee)
.** harish 090521 m++
    DECOMPOSE .c[1] = .#ccc
    DECOMPOSE .s[1] = .#sss
    DECOMPOSE .e[1] = .#eee
    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	.jnt = id_anum[.rob,.cnt]
	IF .jnt<> 3 THEN
	    IF (.c[.jnt]<(MINVAL(.s[.jnt],.e[.jnt])-is_in_jnt[.jnt]))	GOTO out
	    IF (.c[.jnt]>(MAXVAL(.s[.jnt],.e[.jnt])+is_in_jnt[.jnt]))	GOTO out
	END
    END
.**    is_in_loc[.tsk] = ON
.**    RETURN
.*trn:
.** harish 090521 m--
    IF (tsk_arm[rob_tsk[.rob]] BAND armb_nxx) THEN
	ZL3TRN rob_tsk[.rob]: .s[1] = .#sss,inv_hand
	ZL3TRN rob_tsk[.rob]: .e[1] = .#eee,inv_hand
	ZL3TRN rob_tsk[.rob]: .c[1] = .#ccc,inv_hand
.** harish home from U4 collision 081007 m++
        CASE vpp.portarea[.prt] OF
	    VALUE p_hori_cs1,p_hori_cs2,p_hori_cs3,p_hori_cs4:
	        .ang = 175
	    ANY :
.** harish 090408-3 a++
		IF tsk_arm[rob_tsk[.rob]]  == arm_nt570     THEN
		    .ang = 179
		ELSE
	            .ang = 181
		END
.** harish 090408-3 a--
        END
	.s[4] = .s[4] + IFELSE(ABS(.s[4])>.ang,IFELSE(.c[4]>0,IFELSE(.s[4]<0,360,0),IFELSE(.s[4]>0,-360,0)),0)
	.s[5] = .s[5] + IFELSE(ABS(.s[5])>.ang,IFELSE(.c[5]>0,IFELSE(.s[5]<0,360,0),IFELSE(.s[5]>0,-360,0)),0)
	.e[4] = .e[4] + IFELSE(ABS(.e[4])>.ang,IFELSE(.c[4]>0,IFELSE(.e[4]<0,360,0),IFELSE(.e[4]>0,-360,0)),0)
	.e[5] = .e[5] + IFELSE(ABS(.e[5])>.ang,IFELSE(.c[5]>0,IFELSE(.e[5]<0,360,0),IFELSE(.e[5]>0,-360,0)),0)
.** harish home from U4 collision 081007 m--
	FOR .cnt = 1 TO 6 STEP 1
	    IF .cnt<>3 THEN
.** harish 090223-3 a++
	      IF .prt == 203 OR .prt == 204
	        IF (.c[.cnt]<(MINVAL(.s[.cnt],.e[.cnt])-0.5))			GOTO out
	        IF (.c[.cnt]>(MAXVAL(.s[.cnt],.e[.cnt])+0.5))			GOTO out
	      ELSE
.** harish 090223-3 a--
		IF (.c[.cnt]<(MINVAL(.s[.cnt],.e[.cnt])-is_in_trn[.cnt]))	GOTO out
		IF (.c[.cnt]>(MAXVAL(.s[.cnt],.e[.cnt])+is_in_trn[.cnt]))	GOTO out
.** harish 090223-3 a	      
	      END
	    END
	END
	is_in_loc[.tsk] = ON
	RETURN
    END
.** harish 090521 m++
.** harish 080828 homing++
trn:
.*   DECOMPOSE .c[1] = .#ccc
.*   DECOMPOSE .s[1] = .#sss
.*   DECOMPOSE .e[1] = .#eee
.*    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
.*	.jnt = id_anum[.rob,.cnt]
.*	IF .jnt<> 3 THEN
.*	    IF (.c[.jnt]<(MINVAL(.s[.jnt],.e[.jnt])-is_in_jnt[.jnt]))	GOTO out
.*	    IF (.c[.jnt]>(MAXVAL(.s[.jnt],.e[.jnt])+is_in_jnt[.jnt]))	GOTO out
.*	END
.*    END
.*    is_in_loc[.tsk] = ON
.*    RETURN
.** harish 080828 homing--
.** harish 090521 m--
out:
    is_in_loc[.tsk] = OFF
    RETURN
.END
.** harish 080828 homing a--
.** harish 090213 a++
.PROGRAM cur_pos.dive(.tsk,.rob,.hnd,.prt,.slt,.pos,.#loc)
     FOR .iii = 1 TO vpp.slot[.prt] STEP 1
         .slt = .iii
	 .hnd = 1
         CALL p_dive(.tsk,.rob,.hnd,.prt,.iii,ON)

	 .pos = gpos_r_tech
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_tech[.tsk],#gp_tech[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_goff[.tsk],#gp_goff[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_putt[.tsk],#gp_putt[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_poff[.tsk],#gp_poff[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 
.** harish 090224 a++
	 .pos = gpos_r_tech
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_goff[.tsk],#gp_tech[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_poff[.tsk],#gp_putt[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
.** harish 090224 a--

	 .pos = gpos_r_home
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_home[.tsk],#gp_home[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 .pos = gpos_r_prep
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_prep[.tsk],#gp_prep[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 .pos = gpos_r_half
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_half[.tsk],#gp_half[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 .pos = gpos_r_near
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_near[.tsk],#gp_near[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt

	 .pos = gpos_r_home
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_home[.tsk],#gp_prep[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
         .pos = gpos_r_prep
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_prep[.tsk],#gp_half[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
	 .pos = gpos_r_half
	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_half[.tsk],#gp_near[.tsk])
	 IF is_in_loc[.tsk]		GOTO slt
.** harish 090602-2 m++
.*	 .pos = gpos_r_near
.*	 CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_near[.tsk],#gp_tech[.tsk])
.*	 IF is_in_loc[.tsk]		GOTO slt
.** harish 090602-2 m-- 
    END
    .pos = 0
    .slt = 0
slt:
.END
.** harish 090213 a--
.******************************************
.** cur_pos.as
.******************************************
.PROGRAM cur_pos(.tsk,.rob,.hnd,.prt,.slt,.pos,.#loc)
.** Output  .prt
.**         .slt
.**         .pos  Nearest の場合は0にする
    .buf = IFELSE(.tsk<=4,tsk_tsk[.tsk],.tsk)
    .n_p = 0
    .n_s = 0
    .min = 999999
.*                                                      /* SCRX12-006-2 am++
    .cntstrt = IFELSE(.prt,0,1)
    .cntend = IFELSE(.prt,0,id_port[.buf,0])
    FOR .cnt = .cntstrt TO .cntend STEP 1
	.prt = IFELSE(.cnt,id_port[.buf,.cnt],.prt)
.*                                                      /* SCRX12-006-2 am--
	CALL is_taught(.tsk,.rob,.prt,1,ON)
	IF is_taught[.tsk] THEN
	    CASE (port_type[0,.prt] BAND port_form ) OF
	      VALUE p_hori:
		CALL p_hori(.tsk,.rob,.hnd,.prt,1,ON)
	      VALUE p_down:
		CALL p_down(.tsk,.rob,.hnd,.prt,1,ON)
	      VALUE p_vert:
		CALL p_vert(.tsk,.rob,.hnd,.prt,1,ON)
	      VALUE p_dive:
.** harish 090213 a++
                CALL cur_pos.dive(.tsk,.rob,.hnd,.prt,.slt,.pos,.#loc)
		IF .pos		
		    GOTO exit
		ELSE
		    GOTO next
		END
.** harish 090213 a--
.**		CALL p_dive(.tsk,.rob,.hnd,.prt,1,ON)
	      ANY :
		TYPE "Cur_Pos port_type Error:",(port_type[0,.prt] BAND port_form )
		TYPE "Port Numbe :",.prt
		HALT
	    END
.**harish 081007 m++
	    .pos = gpos_r_tech
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_tech[.tsk],#gp_tech[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_goff[.tsk],#gp_goff[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_putt[.tsk],#gp_putt[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_poff[.tsk],#gp_poff[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
.**harish 081007 m--
;
.** harish 081007 homing collison from U4 a++
	    .pos = gpos_r_home
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_home[.tsk],#gp_home[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
	    .pos = gpos_r_prep
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_prep[.tsk],#gp_prep[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
	    .pos = gpos_r_half
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_half[.tsk],#gp_half[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
	    .pos = gpos_r_near
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_near[.tsk],#gp_near[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
.** harish 081007 homing collison from U4 a--
;
.**harish 081007 m++
	    .pos = gpos_r_home
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_home[.tsk],#gp_prep[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
;
	    .pos = gpos_r_prep
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_prep[.tsk],#gp_half[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
;
	    .pos = gpos_r_half
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_half[.tsk],#gp_near[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
;
	    .pos = gpos_r_near
	    CALL is_in_loc(.tsk,.rob,.hnd,.prt,.#loc,#gp_near[.tsk],#gp_tech[.tsk])
	    IF is_in_loc[.tsk]		GOTO slt
;
.**harish 081007 m--
;
	    .pos = 0
.** harish 090213 a
next:
;*** Nearest Check
;*** MADA NX系の場合X,Y,Zで近いものを探さないといけない
	    FOR .iii = 1 TO vpp.slot[.prt] STEP 1
		POINT .#dis = #vc.tchpos[.rob,.prt,.iii] - .#loc
		.dis = 0
		FOR .jjj = 1 TO id_anum[.rob,0] STEP 1
		    .dis = .dis + ABS(DEXT(.#dis,id_anum[.rob,.jjj]))
		END
		IF .dis < .min THEN
		    .min = .dis
		    .n_p = .prt
		    .n_s = .iii
		END
	    END
	END
    END
;** Nearest
    .prt = .n_p
    .slt = .n_s
    .pos = 0
    IF pz.cur_pos THEN
	TYPE "Cur Pos Nearest:",.rob,.prt,.slt
    END
    RETURN
;** Find the PATH and decide which slot
slt:
    .hnd = 1
    .slt = 1
    IF vpp.slot[.prt] > 1 THEN
	CASE (port_type[0,.prt] BAND port_form ) OF
	  VALUE p_hori:
	    FOR .slt = 1 TO vpp.slot[.prt] STEP 1
		IF DEXT(.#loc,3)<(DEXT(#vc.tchpos[.rob,.prt,.slt],3)+vpp.pitch[.prt]/2)	GOTO exit
	    END
	    .slt = vpp.slot[.prt]
.** harish 080923 cur_pos bug a++
	  VALUE p_dive:
	     ZL3TRN rob_tsk[.rob]: .loc[1] = .#loc,inv_hand
	     FOR .slt = 1 TO vpp.slot[.prt] STEP 1
	        ZL3TRN rob_tsk[.rob]: .tch[1] = #vc.tchpos[.rob,.prt,.slt],inv_hand
.** harish 090213 m
		IF ABS(.loc[1])<(ABS(.tch[1])+vpp.pitch[.prt]/2)	GOTO exit
.**		IF .loc[1]<(.tch[1]+vpp.pitch[.prt]/2)	GOTO exit
	    END
	    .slt = vpp.slot[.prt]
.** harish 080923 cur_pos bug a--
	  ANY :
	    TYPE "cur_pos port_type Error:",(port_type[0,.prt] BAND port_form )
	    HALT
	END
    END
exit:
    IF pz.cur_pos THEN
	TYPE "Cur Pos in Path:",.rob,.prt,.slt,.pos
    END
.END
.******************************************
.** qrycpos.as
.******************************************
.PROGRAM qrycpos_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>2)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check PosID
    CALL id_posid(.tsk,2,d_rob1,d_dat1)
    IF NOT (tsk_dat[.tsk,d_dat1])	GOTO epm
.** Syntax OK send Ack
    CALL send_ack(.tsk)
.** Make Response
    .rob = tsk_dat[.tsk,d_rob1]
    .hnd = tsk_dat[.tsk,d_hnd1]
    POINT #jnt[.tsk] = #HERE:rob_tsk[.rob]
    CASE tsk_dat[.tsk,d_dat1] OF
      VALUE pos_id_actl,pos_id_coord:
	CALL loc_str(.rob,#jnt[.tsk],tsk_dat[.tsk,d_dat1],$str[.tsk])
      VALUE pos_id_symbl:
	.pos = 0
	.prt = 0
	.slt = 0
	CALL cur_pos(.tsk,.rob,.hnd,.prt,.slt,.pos,#jnt[.tsk])
	CALL app_arm_safe(.tsk,.rob,#jnt[.tsk])
	IF .prt THEN
	    CALL int_str(.slt,$str[.tsk],1)
	    $str[.tsk] = $vpp.portasso[.prt]+":"+$str[.tsk]
	    IF (.pos==gpos_e_home)OR(.pos==gpos_r_home)THEN
		app_arm_safe[.tsk] = ON
	    END
	ELSE
	    $str[.tsk] = "UNKNOWN:0"
	END
	$str[.tsk] = $str[.tsk]+$IFELSE(app_arm_safe[.tsk],",SAFE",",EXTENDED")
      ANY :
	$str[.tsk] = "NONE"
    END
    $snd_res[.tsk] = $res + "," + $str[.tsk]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** qrypos.as
.******************************************
.PROGRAM qrypos_cmd(.tsk,.tch)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>4)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check Port:Slot
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Check Purpose
    CALL id_pps(.tsk,3,d_dat1)
    IF NOT (tsk_dat[.tsk,d_dat1])	GOTO epm
.** Check PosID
    CALL id_posid(.tsk,4,d_rob1,d_dat2)
    IF NOT (tsk_dat[.tsk,d_dat2])		GOTO epm
    IF tsk_dat[.tsk,d_dat2] == pos_id_symbl	GOTO epm
.** Syntax OK send Ack
    CALL send_ack(.tsk)
.** Make Purpose Position
    POINT #jnt[.tsk] = #none
    CALL is_taught(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],.tch)
    IF is_taught[.tsk] THEN
	CASE (port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form ) OF
	  VALUE p_hori:
	    CALL qrypos_hori(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],tsk_dat[.tsk,d_dat1],.tch)
	  VALUE p_down:
	    CALL qrypos_down(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],tsk_dat[.tsk,d_dat1],.tch)
	  VALUE p_vert:
	    CALL qrypos_vert(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],tsk_dat[.tsk,d_dat1],.tch)
	  VALUE p_dive
	    CALL qrypos_dive(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],tsk_dat[.tsk,d_dat1],.tch)
	  ANY :
	    TYPE "qrypos port_type Error:",(port_type[0,.prt] BAND port_form )
	    TYPE "Port Numbe :",.prt
	    HALT
	END
    END
    CALL loc_str(tsk_dat[.tsk,d_rob1],#jnt[.tsk],tsk_dat[.tsk,d_dat2],$str[.tsk])
    $snd_res[.tsk] = $res + "," + $str[.tsk]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** grip_cmd.as
.******************************************
.PROGRAM hold_rels_cmd(.tsk)
     tsk_num[.tsk]=tsk_num[.tsk]+1
    $tsk_dat[.tsk,tsk_num[.tsk]] = $tsk_cmd[.tsk]
    CALL hldwaf_cmd(.tsk)
.END
.PROGRAM hldwaf_cmd(.tsk)
    IF (tsk_num[.tsk]<>2)	GOTO efm
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1]) THEN
	CALL id_aligner(.tsk,1,d_rob1)
	IF NOT (tsk_dat[.tsk,d_rob1] )	GOTO epm
	tsk_dat[.tsk,d_hnd1] = 0
    END
    SCASE $TOUPPER($tsk_dat[.tsk,2]) OF
      SVALUE "HOLD" :
	.req = grip_hold
      SVALUE "RELS" :
	.req = grip_rels
      ANY :
	GOTO epm
    END
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_init)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    IF tsk_dat[.tsk,d_rob1]<=2 THEN
	CALL gripper_out(tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],.req)
	TWAIT 1
	CALL gripper_stat(tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],.out,.sta)
	IF pz.gstat THEN
	    TYPE $hst_now[tsk_hst[.tsk]]
	    TYPE /B4," Output :",.out
	    TYPE /B4," Status :",.sta
	END
    ELSE
	TYPE "MADA hldwaf Aligner"
    END
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** gripper.as
.******************************************
.REALS
    grip_pres = 0	;*** Gripper状態Waferあり
    grip_abst = 1	;*** Gripper状態Waferなし
    grip_rels = 2	;*** Gripper状態開放
    grip_serr = 3	;*** Gripper状態エラー
    grip_hold = 4	;*** Gripper状態Close
    grip_mask = 5

    grps_vacm = -1	;*** VacuumON ,Waferあり
    grps_voff =  0	;*** VacuumOFF,Waferなし

    who_get = 1	;*** Get動作のGripperチェック
    who_gtr = 2	;*** GTR動作のGripperチェック
    who_put = 3	;*** Put動作のGripperチェック
    who_ext = 4	;*** その他の動作のGripperチェック

    tout_abst = 5
    tout_rels = 5
    pres_hori = 0.3
    pres_down = 0.5
    pres_dive = 0.5

    wgone.chk[1,1] = 0
    wgone.chk[2,1] = 0
    wgone.sta[1,1] = 0
    wgone.sta[2,1] = 0
.END
.PROGRAM gripper_stat(.rob,.hnd,.out,.sta)
    .oh1 = so.edg.hold[.rob,.hnd,1]
    .or1 = so.edg.rels[.rob,.hnd,1]
    .ih1 = si.edg.hold[.rob,.hnd,1]
    .ir1 = si.edg.rels[.rob,.hnd,1]
    IF finger_rob[.rob,.hnd] BAND bit[2] THEN
	.oh2 = so.edg.hold[.rob,.hnd,2]
	.or2 = so.edg.rels[.rob,.hnd,2]
	.ih2 = si.edg.hold[.rob,.hnd,2]
	.ir2 = si.edg.rels[.rob,.hnd,2]
    END
    IF SYSDATA(ZSIMENV) THEN
	.ih1 = IFELSE(.ih1>0,.ih1+1000,.ih1-1000)
	.ir1 = IFELSE(.ir1>0,.ir1+1000,.ir1-1000)
	IF finger_rob[.rob,.hnd] BAND bit[2] THEN
	    .ih2 = IFELSE(.ih2>0,.ih2+1000,.ih2-1000)
	    .ir2 = IFELSE(.ir2>0,.ir2+1000,.ir2-1000)
	END
    END
    .out = ABS(SIG(.oh1))*^B1+ABS(SIG(.or1))*^B10
    .sta = ABS(SIG(.ih1))*^B1+ABS(SIG(.ir1))*^B10
    IF finger_rob[.rob,.hnd] BAND bit[2] THEN
	.out = .out + ABS(SIG(.oh2))*^B100+ABS(SIG(.or2))*^B1000
	.sta = .sta + ABS(SIG(.ih2))*^B100+ABS(SIG(.ir2))*^B1000
    END
.END
.PROGRAM gripper_out(.rob,.hnd,.i_req)
    IF .i_req == grip_rels THEN
	wgone.chk[.rob,.hnd] = OFF
	IF pz.wgone THEN
	    TYPE "WGONE ",.rob,.hnd,"-->OFF(RELEASE)",$RPGNAME(0),/x1,$RPGNAME(1)
	END
	SIGNAL -so.edg.hold[.rob,.hnd,1],so.edg.rels[.rob,.hnd,1]
	SIGNAL -so.edg.hold[.rob,.hnd,2],so.edg.rels[.rob,.hnd,2]
	IF pz.gout THEN
	    TYPE "Gripper Rels:",.rob,.hnd
	END
    ELSE
	SIGNAL so.edg.hold[.rob,.hnd,1],-so.edg.rels[.rob,.hnd,1]
	SIGNAL so.edg.hold[.rob,.hnd,2],-so.edg.rels[.rob,.hnd,2]
	IF pz.gout THEN
	    TYPE "Gripper Hold:",.rob,.hnd
	END
    END
.END
.PROGRAM gripper(.rob,.hnd,.fng,.i_req,.who,.tim)
    grip_err[.rob] = OFF
    IF NOT (.rob <= 4)		RETURN
;
    .req = grip_stat[.rob,.i_req,.fng]
;
    IF pz.grip OR tz.grip THEN
	CASE .i_req OF
	  VALUE grip_rels :
	    .$dbg = "Gripper Release"
	  VALUE grip_abst :
	    .$dbg = "Gripper Absent"
	  VALUE grip_pres :
	    .$dbg = "Gripper Present"
	  ANY :
	    .$dbg = $ENCODE("Gripper ",.i_req)
	END
	IF tz.grip THEN
	    PROMPT .$dbg,.$key
	    IF .$key=="C" OR .$key=="c" THEN
		tz.grip = OFF
	    ELSEIF .$key<>"" THEN
		HALT
	    END
	ELSE
	    TYPE .$dbg
	END
    END
;
    CALL gripper_stat(.rob,.hnd,.out,.sta)
    IF (.req==.sta) THEN
	IF (.i_req==grip_pres)AND(.out==grip_stat[.rob,grip_abst,.fng])		GOTO exit
	IF (.i_req==grip_abst)AND(.out==grip_stat[.rob,grip_abst,.fng])		GOTO exit
	IF (.i_req==grip_rels)AND(.out==grip_stat[.rob,grip_rels,.fng])		GOTO exit
    END
    IF (vc.mode[tsk_tsk[.rob]] BAND m_pcyc) THEN
	IF (.i_req==grip_pres)AND(.out==grip_stat[.rob,grip_abst,.fng])AND(.sta==grip_stat[.rob,grip_abst,.fng])	GOTO exit
    END
;
    IF SYSDATA(ZSIMENV) THEN
	CALL gripper_dum(.rob,.hnd,.i_req,.req)
    END
;
    .old = -999

.** harish 090617-2 a++
    .retry_cnt = 0
Chk_Retry:
.** harish 090617-2 a--

    UTIMER .@tim = 0
    CALL gripper_out(.rob,.hnd,.i_req)
.**    IF SYSDATA(ZSIMENV) THEN
.**	TWAIT 0.2
.**    END
    IF (.i_req==grip_abst)			GOTO loop
    IF NOT(vc.mode[tsk_tsk[.rob]] BAND m_phtm)	GOTO loop
    IF pz.gtime THEN
	TYPE "Phamntom"
    END
    RETURN
Loop:
    CALL gripper_stat(.rob,.hnd,.out,.sta)
    .now = UTIMER(.@tim)
    IF .sta == grip_stat[.rob,grip_rels,.fng] THEN
	IF (.req == .sta) THEN
	    gtime_rels[.rob] = .now
	    IF pz.gtime THEN
		TYPE "Relase  Time:",.now
	    END
	    GOTO exit
	END
    ELSEIF .sta == grip_stat[.rob,grip_abst,.fng] THEN
	IF (.req == .sta) THEN
	    gtime_hold[.rob] = .now
	    IF pz.gtime THEN
		TYPE "Extend  Time:",.now
	    END
	    wgone.chk[.rob,.hnd] = OFF
	    IF pz.wgone THEN
		TYPE "WGONE ",.rob,.hnd,"-->OFF(ABSENT)",$RPGNAME(0),/x1,$RPGNAME(1)
	    END
	    GOTO exit
	END
	IF (vc.mode[tsk_tsk[.rob]] BAND m_pcyc) AND (.i_req == grip_pres) THEN
	    .sta = grip_stat[.rob,grip_pres,.fng]
	    GOTO pre
	END
    ELSEIF .sta == grip_stat[.rob,grip_pres,.fng] THEN
pre:
	IF (.req == .sta) THEN
	    IF (.old<>.sta) THEN
		.ptm = .now
	    END
	    IF (.now-.ptm)>.tim THEN
		IF pz.gtime THEN
		    TYPE "Present Time:",(.now-.ptm)
		END
		wgone.sta[.rob,.hnd] = .sta
		wgone.chk[.rob,.hnd] = ON
		IF pz.wgone THEN
		    TYPE "WGONE ",.rob,.hnd,"--> ON",.sta," (PRESENT)",$RPGNAME(0),/x1,$RPGNAME(1)
		END
		GOTO Exit
	    END
	    GOTO Next
	END
    ELSE
    END
.** IF (.now > vtp.tgrip[tsk_tsk[.rob]])	GOTO Err
.** harish 090617-2 a++
    IF (.now > vtp.tgrip[tsk_tsk[.rob]])	THEN
.*	;gripper retry
       $GRIP_LOG_BUF[.rob] = $DATE(3)+" "+$TIME+" -> "+$IFELSE(.i_req==grip_rels,"Rels_err:","Hold_err:")+$ENCODE(/I1,.retry_cnt+1)+", (Rob:"+$ENCODE(/I1,.rob)+",hnd:"+$ENCODE(/I1,.hnd)+",fng:"+$ENCODE(/I1,.fng)+")"
	IF gid_dbg_disp THEN
	    TYPE $GRIP_LOG_BUF[.rob]
        END
	IF .retry_cnt < grip_chk_retry[.rob] THEN
.*	    ;return gripper status
            IF .i_req == grip_rels
	        CALL gripper_out(.rob,.hnd,grip_hold)
	    ELSE
	        CALL gripper_out(.rob,.hnd,grip_rels)
	    END
	    TWAIT grip_retry_wait[.rob]
	    .retry_cnt = .retry_cnt + 1
	    GOTO Chk_Retry
	ELSE
	    .$tmp = ",time"
	    IF .i_req == grip_rels THEN
		FOR .i = grip.cnt_rel[.rob,.hnd] TO 1 STEP -1
		    .$tmp = .$tmp + "," + $ENCODE(/F5.3,grip_tim_rel[.rob,.hnd,.i])
		END
		FOR .i = grip_cnt_max TO grip.cnt_rel[.rob,.hnd] + 1 STEP -1
		    .$tmp = .$tmp + "," + $ENCODE(/F5.3,grip_tim_rel[.rob,.hnd,.i])
		END
	    ELSE
		FOR .i = grip.cnt_hld[.rob,.hnd] TO 1 STEP -1
		    .$tmp = .$tmp + "," + $ENCODE(/F5.3,grip_tim_hld[.rob,.hnd,.i])
		END
		FOR .i = grip_cnt_max TO grip.cnt_hld[.rob,.hnd] + 1 STEP -1
		    .$tmp = .$tmp + "," + $ENCODE(/F5.3,grip_tim_hld[.rob,.hnd,.i])
		END
	    END
	    GRIP_LOG_CNT[.rob] = RING(GRIP_LOG_CNT[.rob],1,grip_log_cnt_max)
	    $GRIP_LOG[.rob,GRIP_LOG_CNT[.rob]] = $GRIP_LOG_BUF[.rob] + .$tmp
	    GOTO Err
	END
    END
.** harish 090617-2 a--
Next:
    .old = .sta
    TWAIT 0.016
    GOTO Loop
Err:
    CALL gripper_edg_err(.rob,.hnd,.fng,.i_req,.req,.who,.sta)
    grip_err[.rob] = ON
Exit:
    IF pz.gstat THEN
	TYPE "Gripper Stats Robot:",.rob," Hand:",.hnd," Finger:",.fng
	TYPE /B4," Output :",.out
	TYPE /B4," Status :",.sta
	TYPE /B4," Request:",.req
	TYPE ""
    END
    RETURN
.END


.PROGRAM gripper_edg_err(.rob,.hnd,.fng,.i_req,.req,.who,.sta)
    IF finger_rob[.rob,.hnd] == 1 THEN
	$str[.rob] = $ENCODE("$ae_edg_err[",.who,",",.i_req,",",.sta,"]")
	IF EXISTDATA($str[.rob],s) THEN
	    $grip_err[.rob] = $ae_edg_err[.who,.i_req,.sta]
	ELSE
	    $grip_err[.rob] = $ae_edg_err[0,0,0]+"("+$ENCODE(.who,.req,.sta)+")"
	END
	CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
    ELSE
	FOR .iii = 1 TO 5 STEP 1
	    IF bit[.iii] < finger_rob[.rob,.hnd] THEN
		.sss = (.sta BAND grip_stat[.rob,grip_mask,.iii])
		.rrr = (.req BAND grip_stat[.rob,grip_mask,.iii])
TYPE /B,.sss,.rrr
		IF .sss <> .rrr THEN
		    CASE .rrr OF
		      VALUE grip_stat[.rob,grip_pres,finger_rob[.rob,.hnd]] BAND grip_stat[.rob,grip_mask,.iii] :
			.rrr = grip_pres
		      VALUE grip_stat[.rob,grip_abst,finger_rob[.rob,.hnd]] BAND grip_stat[.rob,grip_mask,.iii] :
			.rrr = grip_abst
		      VALUE grip_stat[.rob,grip_rels,finger_rob[.rob,.hnd]] BAND grip_stat[.rob,grip_mask,.iii] :
			.rrr = grip_rels
		      ANY :
			HALT
		    END
		    CASE .sss OF
		      VALUE grip_stat[.rob,grip_pres,finger_rob[.rob,.hnd]] BAND grip_stat[.rob,grip_mask,.iii] :
			.sss = grip_pres
		      VALUE grip_stat[.rob,grip_abst,finger_rob[.rob,.hnd]] BAND grip_stat[.rob,grip_mask,.iii] :
			.sss = grip_abst
		      VALUE grip_stat[.rob,grip_rels,finger_rob[.rob,.hnd]] BAND grip_stat[.rob,grip_mask,.iii] :
			.sss = grip_rels
		      ANY :
			HALT
		    END
		    $str[.rob] = $ENCODE("$ae_edg_err[",.who,",",.rrr,",",.sss,"]")
		    IF EXISTDATA($str[.rob],s) THEN
			$grip_err[.rob] = $ae_edg_err[.who,.rrr,.sss]
		    ELSE
			$grip_err[.rob] = $ae_edg_err[0,0,0]+"("+$ENCODE(.who,.rrr,.sss)+")"
		    END
		    $grip_err[.rob] = $REPLACE($grip_err[.rob],"%B",$ENCODE("%BF",/I1,.iii))
		    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
		END
	    END
	END
    END
.END

.PROGRAM gripper_dum(.rob,.hnd,.i_req,.req)
    CALL gripper_stat(.rob,.hnd,.out,.sta)
    IF pz.gstat OR tz.gstat THEN
	TYPE /S,"Gripper Dummy Robot:",.rob," Hand:",.hnd
	IF .i_req == grip_pres THEN
	    TYPE " Req:Present"
	ELSEIF .i_req == grip_abst THEN
	    TYPE " Req:Absent"
	ELSEIF .i_req == grip_rels THEN
	    TYPE " Req:Release"
	ELSE
	    TYPE " Req:ELSE"
	END
	TYPE /B4," Output :",.out
	TYPE /B4," Status :",.sta
	TYPE /B4," Request:",.req
	TYPE ""
    END
;
    .dum = .req
    IF tz.gstat THEN
	PROMPT "Input Status:",.$key
	IF .$key=="C" OR .$key=="c" THEN
	    tz.gstat = OFF
	ELSEIF .$key <> "" THEN
	    .dum = VAL(.$key,1)
	END
    END
;
    IF .i_req == grip_rels THEN
	wgone.chk[.rob,.hnd] = OFF
	IF pz.wgone THEN
	    TYPE "WGONE ",.rob,.hnd,"-->OFF(RELEASE)",$RPGNAME(0),/x1,$RPGNAME(1)
	END
    ELSEIF .i_req == grip_abst THEN
	wgone.chk[.rob,.hnd] = OFF
	IF pz.wgone THEN
	    TYPE "WGONE ",.rob,.hnd,"-->OFF(ABSENT)",$RPGNAME(0),/x1,$RPGNAME(1)
	END
    END

    IF (finger_rob[.rob,.hnd] BAND bit[1]) THEN
	SIGNAL IFELSE((.dum BAND ^B0001),1,-1)*(si.edg.hold[.rob,.hnd,1]+IFELSE(si.edg.hold[.rob,.hnd,1]>0,1000,-1000))
	SIGNAL IFELSE((.dum BAND ^B0010),1,-1)*(si.edg.rels[.rob,.hnd,1]+IFELSE(si.edg.rels[.rob,.hnd,1]>0,1000,-1000))
    END
    IF (finger_rob[.rob,.hnd] BAND bit[2]) THEN
	SIGNAL IFELSE((.dum BAND ^B0100),1,-1)*(si.edg.hold[.rob,.hnd,2]+IFELSE(si.edg.hold[.rob,.hnd,2]>0,1000,-1000))
	SIGNAL IFELSE((.dum BAND ^B1000),1,-1)*(si.edg.rels[.rob,.hnd,2]+IFELSE(si.edg.rels[.rob,.hnd,2]>0,1000,-1000))
    END
    TWAIT 0.2
.END

.PROGRAM gdum11()
    CALL gdum(1,1)
.END
.PROGRAM gdum21()
    CALL gdum(2,1)
.END
.PROGRAM gdum(.rob,.hnd)
    PROMPT "Input Status:",.$key
    .dum = VAL(.$key,1)
    IF (finger_rob[.rob,.hnd] BAND bit[1]) THEN
	SIGNAL IFELSE((.dum BAND ^B0001),1,-1)*(si.edg.hold[.rob,.hnd,1]+IFELSE(si.edg.hold[.rob,.hnd,1]>0,1000,-1000))
	SIGNAL IFELSE((.dum BAND ^B0010),1,-1)*(si.edg.rels[.rob,.hnd,1]+IFELSE(si.edg.rels[.rob,.hnd,1]>0,1000,-1000))
    END
    IF (finger_rob[.rob,.hnd] BAND bit[2]) THEN
	SIGNAL IFELSE((.dum BAND ^B0100),1,-1)*(si.edg.hold[.rob,.hnd,2]+IFELSE(si.edg.hold[.rob,.hnd,2]>0,1000,-1000))
	SIGNAL IFELSE((.dum BAND ^B1000),1,-1)*(si.edg.rels[.rob,.hnd,2]+IFELSE(si.edg.rels[.rob,.hnd,2]>0,1000,-1000))
    END
.END


.PROGRAM vacuum(.rob,.rob,.hnd,.req,.who,.tim)
    TYPE "MADA Vacuum"
    RETURN
;
    IF .req == grip_rels THEN
	algn_pos[.rob] = NONE
	algn_ang[.rob] = NONE
	algn_rob[.rob] = 0
    ELSE
    END
.END

.** harish 090617-2 a++
.PROGRAM qa_chuck()
    .cnt = 0
rob:
.** SCRY10-003-6 m
    IF tsk_arm[0] THEN
	PROMPT "Input Robot Number:",.$key 
	IF .$key=="1" THEN
	    .rob = 1
	ELSEIF .$key=="2" THEN
	    .rob = 2
	ELSEIF (.$key=="e") OR (.$key=="E") THEN
	    RETURN
	ELSE
	    GOTO rob
	END
    ELSE
	.rob = 1
    END
hnd:
.** SCRY10-003-6 m
    IF tsk_arm[.rob] BAND armb_dbl THEN
	PROMPT "Input Blade Number:",.$key 
	IF .$key == "1" THEN
	    .hnd = 1
	ELSEIF .$key=="2" THEN
	    .hnd = 2
	ELSEIF (.$key=="e") OR (.$key=="E") THEN
	    RETURN
	ELSE
	    GOTO hnd
	END
    ELSE
	.hnd = 1
    END
fng:
.** SCRY10-003-6 m
    IF tsk_arm[.rob] == arm_nt570 THEN
	PROMPT "Input Finger Number:",.$key 
	IF .$key == "1" THEN
	    .fng = 1
	ELSEIF .$key=="2" THEN
	    .fng = 2
	ELSEIF (.$key=="e") OR (.$key=="E") THEN
	    RETURN
	ELSE
	    GOTO fng
	END
    ELSE
	.fng = 1
    END
loop:
    .cnt = .cnt+1
    UTIMER .@tim = 0
    SIGNAL -so.edg.hold[.rob,.hnd,.fng],so.edg.rels[.rob,.hnd,.fng]
    TWAIT 0.016
.***Q090223-2-1 m++
.** SCRY10-003-6 m
    SWAIT -si.edg.hold[.rob,.hnd,.fng],si.edg.rels[.rob,.hnd,.fng]
.***Q090223-2-1 m--
    .opn = UTIMER(.@tim)
    TWAIT 2
;
    UTIMER .@tim = 0
    SIGNAL so.edg.hold[.rob,.hnd,.fng],-so.edg.rels[.rob,.hnd,.fng]
    TWAIT 0.016
.***Q090223-2-1 m++
.** SCRY10-003-6 m
    SWAIT si.edg.hold[.rob,.hnd,.fng],-si.edg.rels[.rob,.hnd,.fng]
.***Q090223-2-1 m--
    .cls = UTIMER(.@tim)
    TWAIT 2
    TYPE /I4,.cnt,/F6.3," Count Timer      Grip: ",.cls,"       Open: ",.opn
;
    GOTO loop
exit:
    RETURN
.END
.** harish 090617-2 a--

.** harish 081120 a++
.******************************************
.** gripper.as
.******************************************
.PROGRAM gripper_stat1(.rob,.hnd,.out,.sta)
    .oh1 = so.edg.hold[.rob,.hnd,1]
    .or1 = so.edg.rels[.rob,.hnd,1]
    .ih1 = si.edg.hold[.rob,.hnd,1]
    .ir1 = si.edg.rels[.rob,.hnd,1]
    IF finger_rob[.rob,.hnd] BAND bit[2] THEN
	.oh2 = so.edg.hold[.rob,.hnd,2]
	.or2 = so.edg.rels[.rob,.hnd,2]
	.ih2 = si.edg.hold[.rob,.hnd,2]
	.ir2 = si.edg.rels[.rob,.hnd,2]
    END
    IF SYSDATA(ZSIMENV) THEN
	.ih1 = IFELSE(.ih1>0,.ih1+1000,.ih1-1000)
	.ir1 = IFELSE(.ir1>0,.ir1+1000,.ir1-1000)
	IF finger_rob[.rob,.hnd] BAND bit[2] THEN
	    .ih2 = IFELSE(.ih2>0,.ih2+1000,.ih2-1000)
	    .ir2 = IFELSE(.ir2>0,.ir2+1000,.ir2-1000)
	END
    END
    CASE tsk_dat[.rob,d_fng1] OF
      VALUE  1 :
	.out = ABS(SIG(.oh1))*^B1+ABS(SIG(.or1))*^B10
	.sta = ABS(SIG(.ih1))*^B1+ABS(SIG(.ir1))*^B10
      VALUE  2 :
	.out = ABS(SIG(.oh2))*^B1+ABS(SIG(.or2))*^B10
	.sta = ABS(SIG(.ih2))*^B1+ABS(SIG(.ir2))*^B10
      ANY :
        .out = ABS(SIG(.oh1))*^B1+ABS(SIG(.or1))*^B10
        .sta = ABS(SIG(.ih1))*^B1+ABS(SIG(.ir1))*^B10
        IF finger_rob[.rob,.hnd] BAND bit[2] THEN
	  .out = .out + ABS(SIG(.oh2))*^B100+ABS(SIG(.or2))*^B1000
	  .sta = .sta + ABS(SIG(.ih2))*^B100+ABS(SIG(.ir2))*^B1000
        END
    END
.END
.PROGRAM gripper_out1(.rob,.hnd,.i_req)
    IF .i_req == grip_rels THEN
	wgone.chk[.rob,.hnd] = OFF
	IF pz.wgone THEN
	    TYPE "WGONE ",.rob,.hnd,"-->OFF(RELEASE)",$RPGNAME(0),/x1,$RPGNAME(1)
	END
.** harish 081120 m++
	CASE tsk_dat[.rob,d_fng1] OF
          VALUE  1 :
		SIGNAL -so.edg.hold[.rob,.hnd,1],so.edg.rels[.rob,.hnd,1]
	  VALUE  2 :
		SIGNAL -so.edg.hold[.rob,.hnd,2],so.edg.rels[.rob,.hnd,2]
	  ANY :
		SIGNAL -so.edg.hold[.rob,.hnd,1],so.edg.rels[.rob,.hnd,1]
		SIGNAL -so.edg.hold[.rob,.hnd,2],so.edg.rels[.rob,.hnd,2]
	END
.** harish 081120 m--
	IF pz.gout THEN
	    TYPE "Gripper Rels:",.rob,.hnd
	END
    ELSE
.** harish 081120 m++
        CASE tsk_dat[.rob,d_fng1] OF
          VALUE  1 :
		SIGNAL so.edg.hold[.rob,.hnd,1],-so.edg.rels[.rob,.hnd,1]
	  VALUE  2 :
		SIGNAL so.edg.hold[.rob,.hnd,2],-so.edg.rels[.rob,.hnd,2]
	  ANY :
		SIGNAL so.edg.hold[.rob,.hnd,1],-so.edg.rels[.rob,.hnd,1]
		SIGNAL so.edg.hold[.rob,.hnd,2],-so.edg.rels[.rob,.hnd,2]
	END
.** harish 081120 m--
	IF pz.gout THEN
	    TYPE "Gripper Hold:",.rob,.hnd
	END
    END
.END
.PROGRAM gripper1(.rob,.hnd,.fng,.i_req,.who,.tim)
    grip_err[.rob] = OFF
    IF NOT (.rob <= 4)		RETURN
;
    .req = grip_stat1[.rob,.i_req,.fng]
;
    IF pz.grip OR tz.grip THEN
	CASE .i_req OF
	  VALUE grip_rels :
	    .$dbg = "Gripper Release"
	  VALUE grip_abst :
	    .$dbg = "Gripper Absent"
	  VALUE grip_pres :
	    .$dbg = "Gripper Present"
	  ANY :
	    .$dbg = $ENCODE("Gripper ",.i_req)
	END
	IF tz.grip THEN
	    PROMPT .$dbg,.$key
	    IF .$key=="C" OR .$key=="c" THEN
		tz.grip = OFF
	    ELSEIF .$key<>"" THEN
		HALT
	    END
	ELSE
	    TYPE .$dbg
	END
    END
;
    CALL gripper_stat1(.rob,.hnd,.out,.sta)
    IF (.req==.sta) THEN
	IF (.i_req==grip_pres)AND(.out==grip_stat1[.rob,grip_abst,.fng])		GOTO exit
	IF (.i_req==grip_abst)AND(.out==grip_stat1[.rob,grip_abst,.fng])		GOTO exit
	IF (.i_req==grip_rels)AND(.out==grip_stat1[.rob,grip_rels,.fng])		GOTO exit
    END
    IF (vc.mode[tsk_tsk[.rob]] BAND m_pcyc) THEN
	IF (.i_req==grip_pres)AND(.out==grip_stat1[.rob,grip_abst,.fng])AND(.sta==grip_stat1[.rob,grip_abst,.fng])	GOTO exit
    END
;
    IF SYSDATA(ZSIMENV) THEN
	CALL gripper_dum(.rob,.hnd,.i_req,.req)
    END
;
    .old = -999
    UTIMER .@tim = 0
    CALL gripper_out1(.rob,.hnd,.i_req)
.**    IF SYSDATA(ZSIMENV) THEN
.**	TWAIT 0.2
.**    END
    IF (.i_req==grip_abst)			GOTO loop
    IF NOT(vc.mode[tsk_tsk[.rob]] BAND m_phtm)	GOTO loop
    IF pz.gtime THEN
	TYPE "Phamntom"
    END
    RETURN
Loop:
    CALL gripper_stat1(.rob,.hnd,.out,.sta)
    .now = UTIMER(.@tim)
    IF .sta == grip_stat1[.rob,grip_rels,.fng] THEN
	IF (.req == .sta) THEN
	    gtime_rels[.rob] = .now
	    IF pz.gtime THEN
		TYPE "Relase  Time:",.now
	    END
	    GOTO exit
	END
    ELSEIF .sta == grip_stat1[.rob,grip_abst,.fng] THEN
	IF (.req == .sta) THEN
	    gtime_hold[.rob] = .now
	    IF pz.gtime THEN
		TYPE "Extend  Time:",.now
	    END
	    wgone.chk[.rob,.hnd] = OFF
	    IF pz.wgone THEN
		TYPE "WGONE ",.rob,.hnd,"-->OFF(ABSENT)",$RPGNAME(0),/x1,$RPGNAME(1)
	    END
	    GOTO exit
	END
	IF (vc.mode[tsk_tsk[.rob]] BAND m_pcyc) AND (.i_req == grip_pres) THEN
	    .sta = grip_stat1[.rob,grip_pres,.fng]
	    GOTO pre
	END
    ELSEIF .sta == grip_stat1[.rob,grip_pres,.fng] THEN
pre:
	IF (.req == .sta) THEN
	    IF (.old<>.sta) THEN
		.ptm = .now
	    END
	    IF (.now-.ptm)>.tim THEN
		IF pz.gtime THEN
		    TYPE "Present Time:",(.now-.ptm)
		END
		wgone.sta[.rob,.hnd] = .sta
		wgone.chk[.rob,.hnd] = ON
		IF pz.wgone THEN
		    TYPE "WGONE ",.rob,.hnd,"--> ON",.sta," (PRESENT)",$RPGNAME(0),/x1,$RPGNAME(1)
		END
		GOTO Exit
	    END
	    GOTO Next
	END
    ELSE
    END
    IF (.now > vtp.tgrip[tsk_tsk[.rob]])	GOTO Err
Next:
    .old = .sta
    TWAIT 0.016
    GOTO Loop
Err:
    CALL gripper_edg_err(.rob,.hnd,.fng,.i_req,.req,.who,.sta)
    grip_err[.rob] = ON
Exit:
    IF pz.gstat THEN
	TYPE "Gripper Stats Robot:",.rob," Hand:",.hnd," Finger:",.fng
	TYPE /B4," Output :",.out
	TYPE /B4," Status :",.sta
	TYPE /B4," Request:",.req
	TYPE ""
    END
    RETURN
.END
.** harish 081120 a--
.******************************************
.** sens.as
.******************************************
.PROGRAM sens_cmd(.tsk)
    IF (tsk_num[.tsk]<>1)	GOTO efm
    SCASE $TOUPPER($tsk_dat[.tsk,1]) OF
      SVALUE "WAF"+$rob_id[.tsk,1]+"B1"  ,"WAF"+$vrp.rob_asso[1]+"B1"   :
	.rob = 1
	.hnd = 1
	.fng = finger_rob[.rob,.hnd]
	GOTO waf
      SVALUE "WAF"+$rob_id[.tsk,1]+"B1F1","WAF"+$vrp.rob_asso[1]+"B1F1" :
	.rob = 1
	.hnd = 1
	.fng = bit[1]
	GOTO waf
      SVALUE "WAF"+$rob_id[.tsk,1]+"B1F2","WAF"+$vrp.rob_asso[1]+"B1F2" :
	.rob = 1
	.hnd = 1
	.fng = bit[2]
	GOTO waf
      SVALUE "WAF"+$rob_id[.tsk,2]+"B1"  ,"WAF"+$vrp.rob_asso[2]+"B1"   :
	.rob = 2
	.hnd = 1
	.fng = finger_rob[.rob,.hnd]
	GOTO waf
      SVALUE "WAF"+$rob_id[.tsk,2]+"B1F1","WAF"+$vrp.rob_asso[2]+"B1F1" :
	.rob = 2
	.hnd = 1
	.fng = bit[1]
	GOTO waf
      SVALUE "WAF"+$rob_id[.tsk,2]+"B1F2","WAF"+$vrp.rob_asso[2]+"B1F2" :
	.rob = 2
	.hnd = 1
	.fng = bit[2]
	GOTO waf
      ANY :
	GOTO epm
    END
    IF 0 
waf:
	IF NOT(tsk_rob[.tsk] BAND bit[.rob])		GOTO epm
	IF (finger_rob[.rob,.hnd] < .fng)		GOTO epm
	CALL sens_waf(.tsk,.rob,.hnd,.fng)
	RETURN
    END
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM sens_waf(.tsk,.rob,.hnd,.fng)
    CALL send_ack(.tsk)
    IF NOT((tsk_cmd[.rob])OR(tsk_use[.rob])OR(snd_res[.rob])) THEN
	CALL gripper_out(.rob,.hnd,grip_hold)
	TWAIT 1
    END
    CALL gripper_stat(.rob,.hnd,.out,.sta)
    CASE (.sta BAND grip_stat[.rob,grip_mask,.fng]) OF
      VALUE (grip_stat[.rob,grip_pres,.fng] BAND grip_stat[.rob,grip_mask,.fng]) :
	$snd_res[.tsk] = $res+",1"
      VALUE (grip_stat[.rob,grip_abst,.fng] BAND grip_stat[.rob,grip_mask,.fng]) :
	$snd_res[.tsk] = $res+",2"
      ANY :
	$snd_res[.tsk] = $res+",0"
    END
    .out = OFF
    FOR .iii = 1 TO finger_rob[.rob,.hnd] STEP 1
	IF (.sta BAND grip_stat[.rob,grip_mask,.iii])==(grip_stat[.rob,grip_pres,.iii] BAND grip_stat[.rob,grip_mask,.iii]) THEN
	    .out = ON
	END
    END
    IF .out THEN
	wgone.sta[.rob,.hnd] = .sta
	wgone.chk[.rob,.hnd] = ON
	IF pz.wgone THEN
	    TYPE "WGONE ",.rob,.hnd,"-->ON (SENS)",/B,.sta
	END
    ELSE
	wgone.chk[.rob,.hnd] = OFF
	IF pz.wgone THEN
	    TYPE "WGONE ",.rob,.hnd,"-->OFF (SENS)"
	END
    END
.END
.******************************************
.** teach.as
.******************************************
.PROGRAM teach_init()
    IF tsk_rob[pc3] THEN
	CALL tchext_exe(pc3,tsk_rob[pc3],port_all,teach_respos)
    END
    IF tsk_rob[pc4] THEN
	CALL tchext_exe(pc4,tsk_rob[pc4],port_all,teach_respos)
    END
.END
.************
.** TCHPOS **
.************
.PROGRAM tchpos_exe(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
.** Limit Check
    IF NOT .trn THEN
	CALL limit_err(.tsk,.rob,OFF,ON,ON,#jnt[.tsk])
	IF limit_err[.tsk]		GOTO err
    END
.***TYPE "MADA Track Check"
    CASE (port_type[0,.prt] BAND port_form ) OF
      VALUE p_hori:
	CALL t_hori(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE p_down:
	CALL t_down(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE p_vert:
	CALL t_vert(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE p_dive
	CALL t_dive(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      ANY :
    END
.** harish 080701 a+
    IF tsk_err[.tsk]	GOTO err
    IF $alm_put[.tsk] == "" THEN 
.** harish 090617-1 a
        CALL log_tch(.tsk,.rob,#jnt[.tsk])
	$snd_res[.tsk] = $res_ok
	RETURN
    END
    CALL alarm_put(.tsk,.rob,0)
err:
    $snd_res[.tsk] = $res_ng
.END
.******************************************
.** tchpos.as
.******************************************
.PROGRAM tchpos_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>3)	GOTO efm
.** Check Robot:Blade
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check Port:Slot
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Check Purpose
    CALL id_pps(.tsk,3,d_dat1)
    IF NOT (tsk_dat[.tsk,d_dat1])	GOTO epm
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Set Teaching Location
    POINT #jnt[.tsk] = #HERE : rob_tsk[tsk_dat[.tsk,d_rob1]]
.** TYPE "MADA IMAP Port"
    CALL tchpos_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],tsk_dat[.tsk,d_dat1],OFF)
    CALL gcpos_cls(tsk_dat[.tsk,d_rob1])
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** tchajp.as
.******************************************
.PROGRAM adjustpos_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>4)        GOTO efm
.** Check Robot or Robot:Blade
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1]) THEN
        CALL id_robot(.tsk,1,d_rob1)
        IF NOT (tsk_dat[.tsk,d_rob1])        GOTO epm
        tsk_dat[.tsk,d_hnd1] = 0
        tsk_dat[.tsk,d_fng1] = 0
    END
.** Check Port or Port:Slot
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1]) THEN
        CALL id_port(.tsk,2,d_prt1)
        IF NOT (tsk_dat[.tsk,d_prt1])        GOTO epm
        tsk_dat[.tsk,d_slt1] = 0
    END
.** Check Axis ID
    CALL id_axis(.tsk,3,d_rob1,d_jnt1,-1)        ;** Axis,Base,Tool
    IF NOT (tsk_dat[.tsk,d_jnt1])        GOTO epm
.** Check Step Data
    CALL id_int(.tsk,4,d_dat1,100)
    IF id_error[.tsk]                                                GOTO epm
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")        RETURN
.** Check AJPLIM
    IF (1<ABS(tsk_dat[.tsk,d_dat1]))AND(vtp.ajplim[.tsk]) THEN
        CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_ajp_data)
        GOTO err
    END
;** MADA Training Slots
    CALL adjustpos_sub(.tsk)
    CALL gcpos_cls(tsk_dat[.tsk,d_rob1])
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
err:
    $snd_res[.tsk] = $res_ng
    RETURN
.END
.PROGRAM adjustpos_sub(.tsk)
    CALL is_taught(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],ON)
    IF is_taught[.tsk] <= 0 THEN
        CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_n_taught)
        GOTO Err
    END
    IF tsk_dat[.tsk,d_jnt1] < 10 THEN
        POINT #jnt[.tsk] = #is_taught[.tsk]+#PPJSET(#PPOINT(),tsk_dat[.tsk,d_jnt1],tsk_dat[.tsk,d_dat1])
    ELSE
        ZARRAYSET .tool[1] = 0,6
        ZARRAYSET .base[1] = 0,6
        CASE tsk_dat[.tsk,d_jnt1] OF
          VALUE axs_base_x1 :
            .base[1] = tsk_dat[.tsk,d_dat1]
          VALUE axs_base_y1 :
            .base[2] = tsk_dat[.tsk,d_dat1]
          VALUE axs_base_r1 :
            .tool[4] = tsk_dat[.tsk,d_dat1]
          VALUE axs_tool_x1 :
            .tool[1] = tsk_dat[.tsk,d_dat1]
          VALUE axs_tool_y1 :
            .tool[2] = tsk_dat[.tsk,d_dat1]
          ANY :
            CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_ajp_hand)
            GOTO Err
        END
        ZL3TRN rob_tsk[tsk_dat[.tsk,d_rob1]]: .dest[1] = #is_taught[.tsk],inv_hand
        ZL3TRN rob_tsk[tsk_dat[.tsk,d_rob1]]: .dest[1] = .base[1] ADD .dest[1]
        ZL3TRN rob_tsk[tsk_dat[.tsk,d_rob1]]: .dest[1] = .dest[1] ADD .tool[1]
        ZL3JNT/ERR .err rob_tsk[tsk_dat[.tsk,d_rob1]]: #jnt[.tsk] = .dest[1],#is_taught[.tsk],inv_normal,inv_hand
        IF .err THEN
            CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_ajp_out)
            GOTO Err
        END
    END
    .pps = IFELSE( tsk_dat[.tsk,d_slt1],pps_ajp,pps_tch)
    CALL tchpos_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],.pps,OFF)
    RETURN
err:
    $snd_res[.tsk] = $res_ng
.END
.******************************************
.** tchext.as
.******************************************
.**********************
.** SAVPOS           **
.** CANCELTCH/RESPOS **
.** RMVPOS           **
.**********************
.PROGRAM tchext_cmd(.tsk,.mod)
.** Check Parameter Number
    IF (tsk_num[.tsk]==0) THEN
	tsk_dat[.tsk,d_rob1] = tsk_rob[.tsk]
	tsk_dat[.tsk,d_prt1] = port_all
    ELSEIF (tsk_num[.tsk]==2) THEN
	CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
	IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
	tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
	CALL id_port(.tsk,2,d_prt1)
	IF NOT (tsk_dat[.tsk,d_prt1])							GOTO epm
    ELSE
	GOTO efm
    END
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,tsk_dat[.tsk,d_rob1],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Execute Command
    CALL tchext_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],.mod)
.** harish 090617-1 a++
    FOR .rob = 1 TO 2 STEP 1
	IF (tsk_dat[.tsk,d_rob1] BAND bit[.rob])AND(rob_tsk[.rob]) THEN
	    CALL log_tch(.tsk,.rob,#none)
	END
    END
.** harish 090617-1 a--
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM tchext_exe(.tsk,.i_r,.i_p,.mod)
    FOR .num = 1 TO id_port[.tsk,0] STEP 1
	IF (.i_p==port_all) OR(.i_p==id_port[.tsk,.num]) THEN
	    IF (.i_r BAND bit[1]) THEN
		CALL tchext_exe_sub(.tsk,1,id_port[.tsk,.num],.mod)
	    END
	    IF (.i_r BAND bit[2]) THEN
		CALL tchext_exe_sub(.tsk,2,id_port[.tsk,.num],.mod)
	    END
	END
    END
    IF (.i_r BAND bit[1]) THEN
	CALL gcpos_cls(1)
    END
    IF (.i_r BAND bit[2]) THEN
	CALL gcpos_cls(2)
    END
.END
.PROGRAM tchext_exe_sub(.tsk,.rob,.prt,.mod)
;** MADA AdjustPos Each Slot Data ETC...
    CASE .mod OF
      VALUE teach_respos :
	NOEXIST_SET_L #vx.defpos[.rob,.prt,0] = #none,,OFF
	NOEXIST_SET_L #vc.defpos[.rob,.prt,0] = #vx.defpos[.rob,.prt,0],,OFF
	NOEXIST_SET_L #vc.savpos[.rob,.prt,0] = #none,,OFF
	NOEXIST_SET_R  vc.savpos[.rob,.prt]   = port_type[.rob,.prt]
	FOR .slt = 1 TO vpp.slot[.prt]
	    NOEXIST_SET_L #vc.savpos[.rob,.prt,.slt] = #none,,OFF
	END
	ZARRAYCPY #vc.tchpos[.rob,.prt,0] = #vc.savpos[.rob,.prt,0],vpp.slot[.prt]+1
	           vc.tchpos[.rob,.prt]   =  vc.savpos[.rob,.prt]
      VALUE teach_savpos :
	ZARRAYCPY #vc.savpos[.rob,.prt,0] = #vc.tchpos[.rob,.prt,0],vpp.slot[.prt]+1
	           vc.savpos[.rob,.prt]   =  vc.tchpos[.rob,.prt]
      VALUE teach_rmvpos :
	ZARRAYSET #vc.savpos[.rob,.prt,0] = #none,vpp.slot[.prt]+1
	ZARRAYSET #vc.tchpos[.rob,.prt,0] = #none,vpp.slot[.prt]+1
	           vc.savpos[.rob,.prt]   =  port_type[.rob,.prt]
	           vc.tchpos[.rob,.prt]   =  vc.savpos[.rob,.prt]
      VALUE teach_clrpos :
	ZARRAYSET #vc.tchpos[.rob,.prt,0] = #none,vpp.slot[.prt]+1
	           vc.tchpos[.rob,.prt]   =  port_type[.rob,.prt]
      ANY :
	TYPE "TCHEXE MOD Error :",.mod
	HALT
    END
.END
.PROGRAM cleartch_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]==1) THEN
	tsk_dat[.tsk,d_rob1] = tsk_rob[.tsk]
	tsk_dat[.tsk,d_prt1] = port_all
	.buf = 1
    ELSEIF (tsk_num[.tsk]==3) THEN
	CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
	IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
	tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
	CALL id_port(.tsk,2,d_prt1)
	IF NOT (tsk_dat[.tsk,d_prt1])							GOTO epm
	.buf = 3
    ELSE
	GOTO efm
    END
    CALL id_0_1(.tsk,.buf,d_dat1)
    IF id_error[.tsk]					GOTO epm
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,tsk_dat[.tsk,d_rob1],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Execute Command
    CALL tchext_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],teach_clrpos)
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** pintch.as
.******************************************
.REALS
    pintch_znum = 2
.END
.PROGRAM pintch_cmd(.tsk)
    IF (tsk_num[.tsk]<>3)        GOTO efm
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])        GOTO epm
    CALL id_port(.tsk,2,d_prt1)
    IF tsk_dat[.tsk,d_prt1] THEN
        tsk_dat[.tsk,d_slt1] = 0
    ELSE
        CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
        IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])                                GOTO epm
    END
    CALL id_int(.tsk,3,d_dat1,1)
    IF id_error[.tsk]                                                                        GOTO epm
.** harish 090626 a++
    IF tsk_arm[tsk_dat[.tsk,d_rob1]]  == arm_nt570
        CALL pintch_NT570(.tsk)
        RETURN
    END
.** harish 090626 a--
    IF ((port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form) <> p_hori)                        GOTO epm
    IF NOT (can_map[tsk_dat[.tsk,d_rob1]])                                                GOTO epm
    IF (tsk_dat[.tsk,d_dat1]==1) THEN
        tsk_mod[.tsk] = m_tech
        tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    ELSEIF (tsk_dat[.tsk,d_dat1]==2) THEN
.** Check Busy , Mode and Send Ack
        CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
        IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")        RETURN
;
        .slt = 0
        POINT #jnt[.tsk] = #HERE:rob_tsk[tsk_dat[.tsk,d_rob1]]
        POINT #jnt[.tsk] = #PPJSET(#jnt[.tsk],3,DEXT(#vc.tchpos[tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],.slt],3))
        CALL tchpos_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_prt1],.slt,pps_pin,OFF)
        CALL gcpos_cls(tsk_dat[.tsk,d_rob1])
    ELSE
        GOTO epm
    END
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.** harish 090626 a++
.PROGRAM pintch_NT570(.tsk)
    .rob = tsk_dat[.tsk,d_rob1]
    .hnd = tsk_dat[.tsk,d_hnd1]
    .prt = tsk_dat[.tsk,d_prt1]
    .slt = tsk_dat[.tsk,d_slt1]
    .dat = tsk_dat[.tsk,d_dat1]
    CASE .dat OF
        VALUE 1 :
            CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
            IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")        RETURN
            POINT .#loc = #HERE:rob_tsk[tsk_dat[.tsk,d_rob1]]
.** harish 090703 m
            .zzz = DEXT(.#loc,3) + vpp.pinzofst[.prt]
            .ztc = .zzz - IFELSE(.slt,(.slt-1)*vpp.pitch[.prt],0)
            FOR .slt = 1 TO vpp.slot[.prt] STEP 1
              IF .slt >= 2 THEN
                NOEXIST_SET_L  #vc.tchpos[.rob,.prt,.slt] = #vc.tchpos[.rob,.prt,1]
              END
.** harish 090703 m++
              IF port_type[.rob,.prt] == p_dive_buff
                  POINT #vc.tchpos[.rob,.prt,.slt] = #PPJSET(#vc.tchpos[.rob,.prt,.slt],3,.zzz)
              ELSE
                  POINT #vc.tchpos[.rob,.prt,.slt] = #PPJSET(#vc.tchpos[.rob,.prt,.slt],3,.ztc+(.slt-1)*vpp.pitch[.prt])
              END
.** harish 090703 m--
            END
            POINT #vc.tchpos[.rob,.prt,0] = #vc.tchpos[.rob,.prt,1]
            POINT .#loc = #PPJSET(#none,3,.zzz)
            CALL log_tch(.tsk,.rob,.#loc)
            $snd_res[.tsk] = $res_ok
        VALUE 2 :
            CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
            IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")        RETURN
.** harish 090703 m
            .slt = IFELSE(port_type[.rob,.prt] == p_dive_buff,1,0)
;            .slt = 0
            POINT #jnt[.tsk] = #HERE:rob_tsk[tsk_dat[.tsk,d_rob1]]
            POINT #jnt[.tsk] = #PPJSET(#jnt[.tsk],3,DEXT(#vc.tchpos[tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],.slt],3))
            CALL tchpos_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_prt1],.slt,pps_pin,OFF)
            CALL gcpos_cls(tsk_dat[.tsk,d_rob1])
        ANY :
            GOTO epm
    END
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
err:
    $snd_res[.tsk] = $res_ng
.END
.** harish 090626 a--
.PROGRAM pintch_rob(.rob)
    .hnd = tsk_dat[.rob,d_hnd1]
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
.***Wafer無しチェック
    CALL gripper(.rob,.hnd,finger_rob[.rob,.hnd],grip_abst,who_ext,0)
    IF grip_err[.rob] THEN
;**        CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
        RETURN
    END
.***Sensor OFFチェック
    POINT .#loc = #DEST:rob_tsk[.rob]
    IF SIG(si.map.find[.rob,.hnd]) THEN
        POINT .#loc = .#loc - #PPOINT(0,0,vpp.pintchzdn[.prt])
        CALL speed(.rob,MINVAL(now.speed[.rob],vpp.pintchspd[.prt]),ON)
        CALL jmove(.rob,.hnd,gpos_nchg,.#loc,"PINTCH_DOWN")
        CALL break(.rob)
    END
    .low = DEXT(.#loc,3)
    .upp = MINVAL((.low+vpp.pintchzup[.prt]),DEXT(#lim_user_u[.rob],3))
    .zzz = 0
    FOR .cnt = 1 TO pintch_znum STEP 1
        IF (.cnt MOD 2) THEN
            POINT .#loc = #PPJSET(.#loc,3,.upp)
        ELSE
            POINT .#loc = #PPJSET(.#loc,3,.low)
        END
        CALL speed(.rob,MINVAL(now.speed[.rob],vpp.pintchspd[.prt]),ON)
        CALL map_sens(.rob,.hnd,.#loc)
        IF tsk_err[.rob]                GOTO err
        IF SYSDATA(ZSIMENV) THEN
            .zzz = .zzz + (.upp+.low)/2
        ELSE
            IF NOT map_sens.cnt[.rob] THEN
                CALL alarm_str(.rob,.rob,0,$ae_not_pintchz)
                GOTO err
            END
            IF (.cnt MOD 2) THEN
                .zzz = .zzz + map_sens.pos[.rob,1]
                IF pz.pintch THEN
                    TYPE "Detected Position:",map_sens.pos[.rob,1]
                END
            ELSE
                .zzz = .zzz + map_sens.pos[.rob,map_sens.cnt[.rob]]
                IF pz.pintch THEN
                    TYPE "Detected Position:",map_sens.pos[.rob,map_sens.cnt[.rob]]
                END
            END
        END
    END
    IF pz.pintch THEN
        TYPE "Average  Position:",.zzz/pintch_znum
        TYPE "PitTch Z Position:",.zzz/pintch_znum + maps_pos.z[.rob]
    END
    .zzz = .zzz / pintch_znum + maps_pos.z[.rob]
    .zzz = .zzz + vpp.pinzofst[.prt]
    .ztc = .zzz - IFELSE(.slt,(.slt-1)*vpp.pitch[.prt],0)
    FOR .slt = 1 TO vpp.slot[.prt] STEP 1
.** harish 081110 a++
        IF .slt >= 2 THEN
            NOEXIST_SET_L  #vc.tchpos[.rob,.prt,.slt] = #vc.tchpos[.rob,.prt,1]
        END
.** harish 081110 a--
        POINT #vc.tchpos[.rob,.prt,.slt] = #PPJSET(#vc.tchpos[.rob,.prt,.slt],3,.ztc+(.slt-1)*vpp.pitch[.prt])
    END
    POINT #vc.tchpos[.rob,.prt,0] = #vc.tchpos[.rob,.prt,1]
.** harish 090617-1 a++
    POINT .#loc = #PPJSET(#none,3,.zzz)
    CALL log_tch(tsk_tsk[.rob],.rob,.#loc)
.** harish 090617-1 a--
;*************
;** Retract **
;*************
    POINT .#loc = #DEST:rob_tsk[.rob]
    CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_slow]),ON)
    IF (tsk_arm[rob_tsk[.rob]] BAND armb_nxx) THEN
        .ct = 1
        ZL3TRN rob_tsk[.rob]: .c[1] = .#loc,1
back:
        ZL3TRN rob_tsk[.rob]: .d[1] = 0,vpp.pinyback[.prt]/.ct,0,0,0,0
        ZL3TRN rob_tsk[.rob]: .d[1] = .c[1] SUB .d[1]
        ZL3JNT/ERR .err rob_tsk[.rob]: .#dst = .d[1],.#loc,inv_normal,inv_hand
        IF NOT .err THEN
            IF (ABS(DEXT(.#dst-.#loc,4))>30) THEN
                .err = ON
            END
            FOR .iii = 1 TO id_anum[.rob,0] STEP 1
                IF DEXT(.#dst-#lim_user_l[.rob],id_anum[.rob,.iii])<0 THEN
                    .err = ON
                END
                IF DEXT(.#dst-#lim_user_u[.rob],id_anum[.rob,.iii])>0 THEN
                    .err = ON
                END
            END
        END
        IF .err THEN
            .ct = .ct+1
            IF .ct < 5        GOTO back
        ELSE
            IF (ABS(DEXT(.#loc,4))<1)OR(ABS(DEXT(.#dst,4))<1)THEN
                POINT .#dst = .#loc
            ELSEIF (DEXT(.#loc,4)*DEXT(.#dst,4)<=0) THEN
                CALL jmove(.rob,.hnd,gpos_nchg,.#dst,"PINTCH_RETRACT")
            ELSE
                CALL lmove(.rob,.hnd,gpos_nchg,.#dst,"PINTCH_RETRACT")
            END
            POINT .#loc = .#dst
        END
    ELSE
        POINT .#loc = #PPJSET(.#loc,4,home_x[.rob])
        CALL jmove(.rob,.hnd,gpos_nchg,.#loc,"PINTCH_RETRACT")
    END
;** ZUP ATCH RT
    POINT .#loc = #PPJSET(.#loc,3,.zzz+vpp.zupatchrt[.prt])
    CALL jmove(.rob,.hnd,gpos_nchg,.#loc,"PINTCH_ZUP")
    prg_stop[.rob] = ON
err:
    RETURN
.END
.******************************************
.** setpos.as
.******************************************
.PROGRAM setpos_cmd(.tsk)
.** Check Parameter Number
.** とりあえず3つ以下かどうかチェックしておく
    IF (tsk_num[.tsk]<=3)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check Port:Slot
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Check PosID
    CALL id_posid(.tsk,tsk_num[.tsk],d_rob1,d_dat1)
    IF NOT (tsk_dat[.tsk,d_dat1])		GOTO epm
    IF tsk_dat[.tsk,d_dat1] == pos_id_symbl	GOTO epm
.** Check Location Data
    CALL id_loc(.tsk,d_rob1,3,tsk_num[.tsk]-1,d_dat1)
    IF id_error[.tsk]				GOTO epm
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Execute Set Pos
    CASE tsk_dat[.tsk,d_dat1] OF
      VALUE pos_id_actl:
	CALL tchpos_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],pps_tch,OFF)
      VALUE pos_id_coord:
	CALL tchpos_exe(.tsk,tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1],pps_tch,ON)
    END
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** t_hori.as
.******************************************
.PROGRAM t_hori(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    CASE (port_type[.rob,.prt]) OF
      VALUE p_hori_ns :
	CALL t_hori_ns(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE p_hori_wet :
	CALL t_hori_wet(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      ANY :
	CALL t_hori_app(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    END
;
;;    IF $alm_put[.tsk] <> ""	GOTO err
    IF NOT tchpos[.tsk]		GOTO err
;
    POINT .#tch = #jnt[.tsk]
;
    CASE .pps OF
      VALUE pps_tch,pps_get,pps_put,pps_pin :
	IF .slt <> 0 THEN
	    POINT .#tch = .#tch - #PPOINT(0,0,vpp.pitch[.prt]*(.slt-1))
	END
	POINT #vc.tchpos[.rob,.prt,0] = .#tch
	FOR .cnt = 1 TO vpp.slot[.prt] STEP 1
.** SCRX10-027-5 a++
            IF (.cnt > 1) AND (port_type[0,.prt] BAND port_util) AND (tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
		POINT .#tch = #vc.tchpos[.rob,.prt,0]
		ZL3TRN rob_tsk[.rob]: .tch[1] = .#tch,inv_hand
		ZL3TRN rob_tsk[.rob]: .add[1] = 0,vpp.x1offset[.prt]
		ZL3TRN rob_tsk[.rob]: .add[1] = .tch[1] ADD .add[1]
		ZL3JNT/ERR .err rob_tsk[.rob]: .#tch = .add[1],.#tch, inv_normal, inv_hand
	        IF .err THEN
	            CALL alarm_str(.tsk,tsk_dat[.tsk,d_rob1],0,$ae_outof_area)
		    $snd_res[.tsk] = $res_ng
	            GOTO err
	        END		    
	    END
.** SCRX10-027-5 a--
	    POINT #vc.tchpos[.rob,.prt,.cnt] = .#tch+#PPOINT(0,0,vpp.pitch[.prt]*(.cnt-1))
	END
      VALUE pps_ajp :
	POINT #vc.tchpos[.rob,.prt,.slt] = .#tch
      ANY :
	$alm_put[.tsk] = $ae_tchpos_ppos
	GOTO err
    END
    vc.tchpos[.rob,.prt] = tchpos[.tsk]
    RETURN
err:
    tchpos[.tsk] = OFF
.END


.PROGRAM t_hori_ns(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    tchpos[.tsk] = OFF
    IF .trn THEN
	TYPE "t_hori_ns must not be trn"
	HALT
	RETURN
    END
    IF (tsk_arm[rob_tsk[.rob]] BAND armb_flp) THEN
	IF ABS(DEXT(#jnt[.tsk],5))>5 THEN
	    $alm_put[.tsk] = $ae_spin_hori
	    RETURN
	END
    END
    tchpos[.tsk] = p_hori_ns
.END


.PROGRAM t_hori_wet(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    tchpos[.tsk] = OFF
    IF .trn THEN
	TYPE "t_hori_wet must not be trn"
	HALT
	RETURN
    END
.** IF (tsk_arm[rob_tsk[.rob]] BAND armb_flp) THEN
	IF ABS(DEXT(#jnt[.tsk],5)-180)>5 THEN
	    $alm_put[.tsk] = $ae_spin_face
	    RETURN
	END
.** END
    tchpos[.tsk] = p_hori_wet
.END
.******************************************
.** t_down.as
.******************************************
.PROGRAM t_down(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    CASE (port_type[.rob,.prt]) OF
      VALUE p_down_wet :
	CALL t_down_wet(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      VALUE p_down_tunl :
	CALL t_down_tunl(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
      ANY :
	CALL t_down_app(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    END
;
;;    IF $alm_put[.tsk] <> ""	GOTO err
    IF NOT tchpos[.tsk]		GOTO err
;
    POINT .#tch = #jnt[.tsk]
;
    CASE .pps OF
      VALUE pps_tch,pps_get,pps_put,pps_pin :
	IF .slt <> 0 THEN
	    POINT .#tch = .#tch - #PPOINT(0,0,vpp.pitch[.prt]*(.slt-1))
	END
	POINT #vc.tchpos[.rob,.prt,0] = .#tch
	FOR .cnt = 1 TO vpp.slot[.prt] STEP 1
	    POINT #vc.tchpos[.rob,.prt,.cnt] = .#tch+#PPOINT(0,0,vpp.pitch[.prt]*(.cnt-1))
	END
      VALUE pps_ajp :
	POINT #vc.tchpos[.rob,.prt,.slt] = .#tch
      ANY :
	$alm_put[.tsk] = $ae_tchpos_ppos
	GOTO err
    END
    vc.tchpos[.rob,.prt] = tchpos[.tsk]
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** t_vert.as
.******************************************
.PROGRAM t_vert(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    TYPE "MADA T_VERT :",$rpgname()
.END
.******************************************
.** t_dive.as
.******************************************
.PROGRAM t_dive(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
    CALL t_dive_app(.tsk,.rob,.hnd,.prt,.slt,.pps,.trn)
;
;;    IF $alm_put[.tsk] <> ""	GOTO err
    IF NOT tchpos[.tsk]		GOTO err
;
    POINT .#tch = #jnt[.tsk]
;
    CASE .pps OF
      VALUE pps_tch,pps_get,pps_put,pps_pin :
	IF (.slt==0)OR(.slt==1) THEN
	    POINT #vc.tchpos[.rob,.prt,1] = .#tch
	ELSE
	    POINT #vc.tchpos[.rob,.prt,.slt] = .#tch
	END
.** harish 080701 a++
.** harish 080923 a++
        CASE (port_type[.rob,.prt]) OF 
            VALUE  p_dive_buff :
	        IF NOT GETSTRBLK($vpp.tslots[.prt],";",$TRIM_B($ENCODE(.slt))) GOTO exit
      	        .tsd = OFF
	        IF (.pps==pps_tch) THEN
	            POINT .#loc = .#tch
.** harish 080922 error in Austin tool m+
	            CALL tslots(.tsk,.rob,.hnd,.prt,.slt,.#loc,OFF,.tsd)
;		    IF $tsk_alm[.tsk] <> ""	RETURN
                    IF tsk_err[.rob]  RETURN		    
.*	            POINT #vc.tchpos[.rob,.prt,0] = #vc.tchpos[.rob,.prt,1]
.*		    IF (.slt==0)OR(.slt==1) THEN
.*			POINT #vc.tchpos[.rob,.prt,1] = .#tch
.*		    ELSE
.*			POINT #vc.tchpos[.rob,.prt,.slt] = .#tch
.*		    END
	        END
	        IF NOT (.tsd )
		  POINT #vc.tchpos[.rob,.prt,0] = .#tch
		  IF (.slt==0)OR(.slt==1) THEN
.*	            POINT #vc.tchpos[.rob,.prt,0] = .#tch
	            POINT #vc.tchpos[.rob,.prt,1] = #vc.tchpos[.rob,.prt,0]
		    FOR .i = 2 TO vpp.slot[.prt] STEP 1
		      ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #vc.tchpos[.rob,.prt,.i-1],inv_hand
 		      ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.pitch[.prt]
		      ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
		      ZL3JNT rob_tsk[.rob]: #vc.tchpos[.rob,.prt,.i] = add[.tsk,1],#vc.tchpos[.rob,.prt,.i-1], inv_normal, inv_hand
	            END
		  ELSE
		    POINT #vc.tchpos[.rob,.prt,.slt] = .#tch
  	            FOR .i = .slt TO 1 STEP -1
		      ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #vc.tchpos[.rob,.prt,.i],inv_hand
 		      ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,-vpp.pitch[.prt]
		      ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
		      ZL3JNT rob_tsk[.rob]: #vc.tchpos[.rob,.prt,.i-1] = add[.tsk,1],#vc.tchpos[.rob,.prt,.i], inv_normal, inv_hand
	            END
		  END
	        END
.** harish 090225-2 m
	    ANY :
.** harish 090225-2 a
	        POINT #vc.tchpos[.rob,.prt,0] = .#tch
                IF vpp.slot[.prt] > 1
                    FOR .i = 2 TO vpp.slot[.prt] STEP 1
	                ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #vc.tchpos[.rob,.prt,.i-1],inv_hand
			ZL3TRN rob_tsk[.rob]: add[.tsk,1] = 0,vpp.pitch[.prt]
			ZL3TRN rob_tsk[.rob]: add[.tsk,1] = trn[.tsk,1] ADD add[.tsk,1]
			ZL3JNT rob_tsk[.rob]: #vc.tchpos[.rob,.prt,.i] = add[.tsk,1],#vc.tchpos[.rob,.prt,.i-1], inv_normal, inv_hand
	            END	      
                END
	END
.** harish 080923 a--
.** harish 080701 a--
      VALUE pps_ajp :
	POINT #vc.tchpos[.rob,.prt,.slt] = .#tch
      ANY :
	$alm_put[.tsk] = $ae_tchpos_ppos
	GOTO err
    END
exit:
    vc.tchpos[.rob,.prt] = tchpos[.tsk]
    RETURN
err:
    tchpos[.tsk] = OFF
.END
.******************************************
.** defpos.as
.******************************************
.PROGRAM setdefaultpos(.tsk)
.** Check Parameter Number
.** とりあえず3つ以下かどうかチェックしておく
    IF (tsk_num[.tsk]<=3)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check Port:Slot
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Check Location Data
    CALL id_loc(.tsk,d_rob1,3,tsk_num[.tsk],pos_id_actl)
    IF id_error[.tsk]				GOTO epm
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Execute SetDefaultPos
    POINT #vc.defpos[tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],0] = #jnt[.tsk]
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM qrydefaultpos(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>2)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check Port:Slot
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Syntax OK send Ack
    CALL send_ack(.tsk)
    POINT #jnt[.tsk] = #vc.defpos[tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_prt1],0]
    CALL loc_str(tsk_dat[.tsk,d_rob1],#jnt[.tsk],pos_id_actl,$str[.tsk])
    $snd_res[.tsk] = $res + "," + $str[.tsk]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** prg_ctrl.as
.******************************************
.REALS
    wait_ereset = 1
    wait_pon    = 1
.END
.PROGRAM init_cmd(.tsk)
    IF (tsk_num[.tsk]==0) THEN
	tsk_dat[.tsk,d_rob1] = tsk_rob[.tsk]
    ELSEIF (tsk_num[.tsk]==1) THEN
	CALL id_unit(.tsk,1,d_rob1)
	tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
    ELSE
	GOTO efm
    END
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    CALL check_bsy_mode(.tsk,tsk_dat[.tsk,d_rob1],m_init)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    CALL init_exe(.tsk,tsk_dat[.tsk,d_rob1])
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM init_exe(.tsk,.unt)
;** Init Flag to Robot
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    rob_ini[.rob] = ON
	END
    END
;** Check Servo ON/OFF
    CALL is_servo(.tsk,.unt,OFF)
    IF is_servo[.tsk]	GOTO fan
;** Check Switch
    CALL is_switch(.tsk,.unt)
    IF NOT is_switch[.tsk]	GOTO err
;** Power ON
    CALL power_on(.tsk,.unt)
    IF NOT power_on[.tsk]	GOTO err
;** Limit Check
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    POINT #jnt[.tsk] = #HERE:rob_tsk[.rob]
	    CALL limit_err(.tsk,.rob,ON,ON,ON,#jnt[.tsk])
	    IF limit_err[.tsk]		GOTO err
	END
    END
;** Servo ON
    CALL servo_on(.tsk,.unt)
    IF NOT servo_on[.tsk]		GOTO err
fan:
    .err = 0
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    CALL fan_check(.tsk,.rob)
	    IF fan_check[.tsk] THEN
		CALL fan_error(.tsk,.rob)
		.err = ON
	    END
	END
    END
    IF .err	GOTO err
ok:
    $snd_res[.tsk] = $res_ok
    RETURN
err:
    $snd_res[.tsk] = $res_ng
    RETURN
.END
.PROGRAM is_servo(.tsk,.unt,.snd)
    is_servo[.tsk]=ON
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF NOT SWITCH("CS",rob_tsk[.rob]) THEN
		IF .snd THEN
		    $alm_put[.tsk] = $ae_not_init
		    CALL alarm_put(.tsk,.rob,0)
		    CALL make_res_ng(.tsk)
		END
		is_servo[.tsk] = OFF
	    END
	END
    END
.END
.PROGRAM is_switch(.tsk,.unt)
    is_switch[.tsk]=ON
chk_hold_run:
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF NOT SWITCH("RUN",rob_tsk[.rob]) THEN
		$alm_put[.tsk] = $ae_not_run
		CALL alarm_put(.tsk,0,0)
		is_switch[.tsk] = OFF
		GOTO chk_tch_rep
	    END
	END
    END
chk_tch_rep:
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF NOT SWITCH("REPEAT",rob_tsk[.rob]) THEN
		$alm_put[.tsk] = $ae_not_repeat
		CALL alarm_put(.tsk,0,0)
		is_switch[.tsk] = OFF
		GOTO chk_tlock
	    END
	END
    END
chk_tlock:
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF SWITCH("TEACH_LOCK",rob_tsk[.rob]) THEN
		$alm_put[.tsk] = $ae_tlock_on
		CALL alarm_put(.tsk,0,0)
		is_switch[.tsk] = OFF
		GOTO chk_ext_run
	    END
	END
    END
chk_ext_run:
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF NOT SWITCH("EXTRUN",rob_tsk[.rob]) THEN
		$alm_put[.tsk] = $ae_not_extrun
		CALL alarm_put(.tsk,.rob,0)
		is_switch[.tsk] = OFF
	    END
	END
    END
.END

.PROGRAM power_on(.tsk,.unt)
    IF cont == cont_d60 THEN
	TYPE "MADA?? D60 Hard Patch Need???:",$rpgname()
	IF 0 THEN
	    IF ((.unt==bit[2])AND(rob_tsk[3]))OR((.unt==bit[3])AND(rob_tsk[2])) THEN
		MC ERESET rob_tsk[2]:
		MC ERESET rob_tsk[3]:
		TWAIT wait_ereset
		MC ZPOWER rob_tsk[2]: ON
		MC ZPOWER rob_tsk[3]: ON
		TWAIT wait_pon
	    END
	END
    END
;
    power_on[.tsk ] = ON
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    MC ERESET rob_tsk[.rob]:
	END
    END
    TWAIT wait_ereset
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    MC ZPOWER rob_tsk[.rob]: ON
	END
    END
    TWAIT wait_pon
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF SWITCH("POWER",rob_tsk[.rob]) AND (ERROR:rob_tsk[.rob]==0)THEN
	    ELSE
		power_on[.tsk ] = OFF
		CALL alarm_sys(.tsk,.rob,0,0)
	    END
	END
    END
.END

.PROGRAM power_off(.tsk,.unt)
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    MC ZPOWER rob_tsk[.rob]: OFF
	END
    END
    TWAIT 1
.END

.PROGRAM chk_prime(.tsk,.unt)
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    MC PRIME rob_tsk[.rob]:robot
	END
    END
.END


.PROGRAM servo_on(.tsk,.unt)
    servo_on[.tsk] = ON
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
	    IF NOT SWITCH("CS",rob_tsk[.rob]) THEN
		MC/ERR .err = EXECUTE rob_tsk[.rob]:robot
		IF .err THEN
		    CALL alarm_sys(.tsk,.rob,.err,0)
		    servo_on[.tsk] = OFF
		END
	    END
	END
    END
.** SCRY10-003-1 a++
    UTIMER .@tim = 0
loop:
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob]) THEN
            IF TASK(.rob)<>1 THEN
                IF UTIMER(.@tim) > 2 THEN
                    CALL alarm_sys(.tsk,.rob,0,0)
		    servo_on[.tsk] = OFF
		    GOTO exit
	        END
	        TWAIT 0.016
		GOTO loop
            END
exit:
	END
    END
.** SCRY10-003-1 a--
.END


.PROGRAM stop_cmd(.tsk)
.** d_dat1 : StopUrgency
.** d_rob1 : UnitID
.**
    IF (tsk_num[.tsk]<>1)AND(tsk_num[.tsk]<>2)		GOTO efm
    CALL id_0_1(.tsk,1,d_dat1)
    IF id_error[.tsk]					GOTO epm
    IF (tsk_num[.tsk]==1) THEN
	tsk_dat[.tsk,d_rob1] = tsk_rob[.tsk]
    ELSE
	CALL id_unit(.tsk,2,d_rob1)
	tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
    END
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    CALL send_ack(.tsk)
     .hst =  tsk_hst[.tsk]
    .$mes = $tsk_mes[.tsk]
    .unt = 0
;***TYPE "Hold Process"
    FOR .rob = 1 TO 4 STEP 1
	IF (tsk_dat[.tsk,d_rob1] BAND bit[.rob] ) THEN
	    IF SWITCH("CS",rob_tsk[.rob]) THEN
		MC HOLD rob_tsk[.rob]:
		.unt = .unt BOR bit[.rob]
	    END
	END
    END
;***TYPE "Send Response of held command"
    FOR .rob = 1 TO 4 STEP 1
	IF (.unt BAND bit[.rob] ) THEN
	    IF tsk_cmd[.rob] THEN
		 tsk_hst[.tsk] =  tsk_hst[.rob]
		$tsk_mes[.tsk] = $tsk_mes[.rob]
		$alm_put[.tsk] = $IFELSE(tsk_dat[.tsk,d_dat1],$ae_stop_cmdi,$ae_stop_cmd)
		CALL alarm_put(.tsk,.rob,0)
		CALL send_stop_res(.tsk,.rob)
		snd_res[.rob] = OFF
		tsk_cmd[.rob] = OFF
		CALL dev_free(.rob)
	    END
	END
    END
     tsk_hst[.tsk] = .hst
    $tsk_mes[.tsk] = .$mes
    TWAIT 2
    IF tsk_dat[.tsk,d_dat1] THEN
;*******TYPE "Restart robot program by StopUrgency 1"
	CALL servo_on(.tsk,.unt)
	IF NOT servo_on[.tsk] THEN
	    $snd_res[.tsk] = $res_ng
	    RETURN
	END
    END
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END


.PROGRAM motrpwr_cmd(.tsk)
    IF (tsk_num[.tsk]==1) THEN
	.buf = 1
	tsk_dat[.tsk,d_rob1] = tsk_rob[.tsk]
    ELSEIF (tsk_num[.tsk]==2)
	CALL id_unit(.tsk,1,d_rob1)
	tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
	.buf = 2
    ELSE
	GOTO efm
    END
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    SCASE $TOUPPER($tsk_dat[.tsk,.buf]) OF
      SVALUE "ON" :
	tsk_dat[.tsk,d_dat1] = ON
      SVALUE "OFF" :
	tsk_dat[.tsk,d_dat1] = OFF
      ANY :
	GOTO epm
    END
    CALL check_bsy_mode(.tsk,tsk_dat[.tsk,d_rob1],m_init)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    $snd_res[.tsk] = $res_ok
    IF tsk_dat[.tsk,d_dat1] THEN
	IF SWITCH("REPEAT",rob_tsk[1]) THEN
	    CALL init_exe(.tsk,tsk_dat[.tsk,d_rob1])
	ELSE
	    CALL power_on(.tsk,tsk_dat[.tsk,d_rob1])
	END
    ELSE
	CALL power_off(.tsk,tsk_dat[.tsk,d_rob1])
    END
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** limit.as
.******************************************
.PROGRAM qrylim_cmd(.tsk)
.** Check Parameter Number
    IF tsk_num[.tsk]<>1		GOTO efm
.** Check RobotID:AxisID
    CALL id_unit_axis(.tsk,1,d_rob1,d_jnt1,0)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_jnt1])	GOTO epm
.** Syntax OK
    CALL send_ack(.tsk)
.** Make Response
    $snd_res[.tsk] = $res
    .dat = DEXT(#lim_user_l[tsk_dat[.tsk,d_rob1]],tsk_dat[.tsk,d_jnt1])
    CALL int_str(.dat,$str[.tsk],100)
    $snd_res[.tsk] = $snd_res[.tsk] + "," + $str[.tsk]
    .dat = DEXT(#lim_user_u[tsk_dat[.tsk,d_rob1]],tsk_dat[.tsk,d_jnt1])
    CALL int_str(.dat,$str[.tsk],100)
    $snd_res[.tsk] = $snd_res[.tsk] + "," + $str[.tsk]
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.PROGRAM movlim_cmd(.tsk)
    CALL on_off_cmd(.tsk,vtp.movlim[.tsk],m_init)
.END
.PROGRAM ajplim_cmd(.tsk)
    CALL on_off_cmd(.tsk,vtp.ajplim[.tsk],m_init)
.END


.PROGRAM limit_err(.tsk,.rob,.zlm,.zzz,.snd,.#loc)
    limit_err[.tsk] = OFF
    IF .zlm THEN
	ZGETZLLIMIT rob_tsk[.rob]: .#llm
	ZGETZULIMIT rob_tsk[.rob]: .#ulm
    ELSE
	POINT .#llm = #lim_user_l[.rob]
	POINT .#ulm = #lim_user_u[.rob]
    END
    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	.jnt = id_anum[.rob,.cnt]
	IF (.jnt<>3)OR(.zzz==ON) THEN
	    IF DEXT(.#loc-.#llm,.jnt)<0 THEN
		IF .snd THEN
		    CALL alm_lim_dir(.tsk,.rob,.jnt,OFF)
		END
		limit_err[.tsk] = ON
	    END
	    IF DEXT(.#ulm-.#loc,.jnt)<0 THEN
		IF .snd THEN
		    CALL alm_lim_dir(.tsk,.rob,.jnt,ON)
		END
		limit_err[.tsk] = ON
	    END
	END
    END
.END
.PROGRAM alm_lim_dir(.tsk,.rob,.jnt,.dir)
    IF .dir THEN
	$alm_put[.tsk] = $ae_lim_u
	SCASE $GETSTRBLK($id_axis[.rob],",",.jnt) OF
	  SVALUE "R","L1","B1","B2":
	    $str[.tsk] = "CW"
	  SVALUE "Z":
	    $str[.tsk] = "DOWN"
	  SVALUE "X1","X2":
	    $str[.tsk] = "RETRACT"
	  SVALUE "S1","S2":
	    $str[.tsk] = "Spin to Horizontal"
	  SVALUE "T":
	    $str[.tsk] = "LEFT"
	  ANY :
	    TYPE "No Entry DIR(ON ) for:",$GETSTRBLK($id_axis[.rob],",",.jnt)
	    $str[.tsk] = "NEGATIVE"
;*	    PAUSE
	  END
    ELSE
	$alm_put[.tsk] = $ae_lim_l
	SCASE $GETSTRBLK($id_axis[.rob],",",.jnt) OF
	  SVALUE "R","L1","B1","B2":
	    $str[.tsk] = "CCW"
	  SVALUE "Z":
	    $str[.tsk] = "UP"
	  SVALUE "X1","X2":
	    $str[.tsk] = "EXTEND"
	  SVALUE "S1","S2":
	    $str[.tsk] = "Spin to Vertical"
	  SVALUE "T":
	    $str[.tsk] = "RIGHT"
	  ANY :
	    TYPE "No Entry DIR(OFF) for:",$GETSTRBLK($id_axis[.rob],",",.jnt)
	    .$dir = "POSITIVE"
;*	    PAUSE
	  END
    END
    $alm_put[.tsk] = $REPLACE($alm_put[.tsk],"%DIR",$str[.tsk],-1)
    CALL alarm_put(.tsk,.rob,bit[.jnt])
.END

.PROGRAM limit_robot(.rob)
    POINT #jnt[.rob] = #DEST:rob_tsk[.rob]
    .out = OFF
    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	.jnt = id_anum[.rob,.cnt]
	IF (DEXT(#jnt[.rob]-#lim_user_l[.rob],.jnt))<0 THEN
	    .out = ON
	ELSEIF (DEXT(#lim_user_u[.rob]-#jnt[.rob],.jnt))<0 THEN
	    .out = ON
	END
    END
    IF (tsk_arm[rob_tsk[.rob]] BAND armb_nxx)AND(tsk_arm[rob_tsk[.rob]] BAND armb_dbl) THEN
	IF ABS(DEXT(#jnt[.rob],6)-DEXT(#jnt[.rob],7))>hand_inter[.rob] THEN
	    ZL3HANDI rob_tsk[.rob]: hand_inter[.rob]+10
	    .out = ON
	ELSE
	    ZL3HANDI rob_tsk[.rob]: hand_inter[.rob]
	END
    END
    IF .out THEN
	ZGETZLLIMIT rob_tsk[.rob]: #jnt[.rob]
	LLIMIT #jnt[.rob]
	ZGETZULIMIT rob_tsk[.rob]: #jnt[.rob]
	ULIMIT #jnt[.rob]
	CALL gcpos_nhom(.rob)
	IF pz.limit THEN
	    TYPE "Limit-->ZLIMIT(",.rob,")"
	END
    ELSE
	ULIMIT #lim_user_u[.rob]
	LLIMIT #lim_user_l[.rob]
	CALL gcpos_cls(.rob)
	IF pz.limit THEN
	    TYPE "Limit-->Normal(",.rob,")"
	END
    END
.END
.******************************************
.** gcpos.as
.******************************************
.PROGRAM gcpos1()
    ENDKILL
    CALL gcpos_dbg(1)
.END
.PROGRAM gcpos2()
    ENDKILL
    CALL gcpos_dbg(2)
.END
.PROGRAM gcpos_cls(.rob)
    g.base[.rob] = gbas_none
    g.hand[.rob] = 0
    g.port[.rob] = 0
    g.slot[.rob] = 0
    g.cpos[.rob] = 0
    IF pz.gcpos THEN
	CALL gcpos_dbg(.rob)
    END
.END
.PROGRAM gcpos_home(.rob,.pos)
    g.base[.rob] = gbas_home
    g.hand[.rob] = 0
    g.port[.rob] = 0
    g.slot[.rob] = 0
    g.cpos[.rob] = .pos
    IF pz.gcpos THEN
	CALL gcpos_dbg(.rob)
    END
.END
.PROGRAM gcpos_nhom(.rob)
    g.base[.rob] = gbas_nhom
    g.hand[.rob] = 0
    g.port[.rob] = 0
    g.slot[.rob] = 0
    g.cpos[.rob] = 0
    IF pz.gcpos THEN
	CALL gcpos_dbg(.rob)
    END
.END
.PROGRAM gcpos_set(.rob,.bas,.hnd,.prt,.slt,.pos)
    g.base[.rob] = .bas BAND ^H00FF
    g.hand[.rob] = .hnd
    g.port[.rob] = .prt
    g.slot[.rob] = .slt
    g.cpos[.rob] = .pos
    IF pz.gcpos THEN
	CALL gcpos_dbg(.rob)
    END
.END
.PROGRAM gcpos_pos(.rob,.pos)
    g.cpos[.rob] = .pos
    IF pz.gcpos THEN
	CALL gcpos_dbg(.rob)
    END
.END
.PROGRAM gcpos_base(.rob,.bas)
    g.base[.rob] = .bas
    IF pz.gcpos THEN
	CALL gcpos_dbg(.rob)
    END
.END
.PROGRAM gcpos_dbg(.rob)
    .$dbg = $vrp.rob_asso[.rob]+" "
    .prt = OFF
    CASE g.base[.rob] OF
      VALUE gbas_none :
	.$dbg = .$dbg + "NONE"
      VALUE gbas_nhom :
	.$dbg = .$dbg + "NEED"
      VALUE gbas_home :
	.$dbg = .$dbg + "HOME"
      VALUE gbas_escp:
	.$dbg = .$dbg + "ESC_"
	.prt = ON
      VALUE gbas_getp:
	.$dbg = .$dbg + "GET_"
	.prt = ON
      VALUE gbas_putp:
	.$dbg = .$dbg + "PUT_"
	.prt = ON
      VALUE gbas_tget :
	.$dbg = "TGET_"
	.prt = ON
      VALUE gbas_tger :
	.$dbg = "ERR_TGET_"
	.prt = ON
      VALUE gbas_tput :
	.$dbg = "TPUT_"
	.prt = ON
      VALUE gbas_tper :
	.$dbg = "ERR_TPUT_"
	.prt = ON
      ANY :
    END
    IF .prt THEN
	.$dbg = .$dbg + $MID($gpos_port,g.cpos[.rob]*5+1,5)
    END
    .$dbg = .$dbg + $ENCODE(" PORT:",g.port[.rob]," SLOT:",g.slot[.rob])
    TYPE $RPGNAME(),/X1,.$dbg
.END
.******************************************
.** move.as
.******************************************
.PROGRAM xmove(.rob,.hnd,.pat,.pos,.#dst,.$inf)
    IF .rob <= 4 THEN
	IF path_sped[.pat,.pos] > 0 THEN
	    CALL speed_rate(.rob,path_sped[.pat,.pos])
	END
	ZSACCEL path_acce[.pat,.pos]
	ZSDECEL path_dece[.pat,.pos]
	IF tsk_arm[rob_tsk[.rob]] BAND armb_nxx THEN
	    ZARRAYSET accuracy[1]=path_acur[.pat,.pos],6
	    ZL3ACCURACY accuracy[1]
	ELSE
	    ACCURACY path_acur[.pat,.pos]
	END
	IF path_move[.pat,.pos] == mov_jmv THEN
	    CALL jmove(.rob,.hnd,.pos,.#dst,.$inf)
	ELSEIF path_move[.pat,.pos] == mov_lmv THEN
	    CALL lmove(.rob,.hnd,.pos,.#dst,.$inf)
	ELSE
	END
.****** Event
	IF evt_pos[.rob] THEN
	    IF .pos == gpos_e_extd THEN
		CALL event_rob(.rob,$ev_arm_ext[.rob],$ev2_arm_ext)
	    ELSEIF .pos == gpos_r_home THEN
		CALL event_rob(.rob,$ev_arm_ret[.rob],$ev2_arm_ret)
	    END
	END
    ELSE
	qtm_cnt[.rob] = qtm_cnt[.rob]+1
    END
.END
.PROGRAM jmove(.rob,.hnd,.pos,.#dst,.$inf)
    IF .rob <= 4 THEN
	POINT #move_dst[.rob] = .#dst
	IF tz.move THEN
	    PROMPT $vrp.rob_asso[.rob]+" JMV:"+.$inf+$ENCODE(/x1,/f9.2,.#dst),.$key
	    IF "C"==$TOUPPER(.$key) THEN
		tz.move = OFF
	    ELSEIF .$key<>"" THEN
		HALT
	    END
	END
	JMOVE #move_dst[.rob]
	CALL log_mov(.rob,.$inf)
	IF (.pos<>g.cpos[.rob])AND(.pos<>gpos_nchg) THEN
	    CALL gcpos_pos(.rob,.pos)
	END
	CALL event_out(.rob)
    ELSE
	qtm_cnt[.rob] = qtm_cnt[.rob]+1
    END
.END
.PROGRAM lmove(.rob,.hnd,.pos,.#dst,.$inf)
    IF .rob <= 4 THEN
	POINT #move_dst[.rob] = .#dst
	IF tz.move THEN
	    PROMPT $vrp.rob_asso[.rob]+" LMV:"+.$inf+$ENCODE(/x1,/f9.2,.#dst),.$key
	    IF "C"==$TOUPPER(.$key) THEN
		tz.move = OFF
	    ELSEIF .$key<>"" THEN
		HALT
	    END
	END
	ZL3LMOVE #move_dst[.rob],inv_normal,.hnd
	CALL log_mov(.rob,.$inf)
	IF (.pos<>g.cpos[.rob])AND(.pos<>gpos_nchg) THEN
	    CALL gcpos_pos(.rob,.pos)
	END
	CALL event_out(.rob)
    ELSE
	qtm_cnt[.rob] = qtm_cnt[.rob]+1
    END
.END
.PROGRAM break(.rob)
    IF .rob <=4 THEN
	BREAK
	CALL event_out(.rob)
	IF pz.move OR tz.move THEN
	    TYPE "Robot:",.rob," BREAK"
	END
    END
.END
.PROGRAM delay(.rob,.tim)
    IF .rob <= 4 THEN
	DELAY .tim
	CALL event_out(.rob)
	IF pz.move OR tz.move THEN
	    TYPE "Robot:",.rob," DELAY ",.tim
	END
    END
.END
.PROGRAM mvwait(.rob,.tim)
    IF .rob <=4 THEN
	MVWAIT .tim/1000 s
	CALL event_out(.rob)
	IF pz.move OR tz.move THEN
	    TYPE "Robot:",.rob," MVWAIT",.tim/1000
	END
    END
.END

.PROGRAM dev_lock(.rob,.dev)
    IF .rob==.dev				RETURN
    IF tsk_cmd[.dev]				GOTO use
    IF tsk_use[.dev] AND (tsk_use[.dev]<>.rob)	GOTO use
    tsk_use[.dev] = .rob
    IF pz.dev THEN
	TYPE "DevLock Rob:",.rob," Dev:",.dev
    END
    RETURN
use:
    $alm_tmp[.rob] = $REPLACE($ae_dev_use[.dev],"%R",$vrp.rob_asso[.dev],-1)
    CALL alarm_str(.rob,.rob,0,$alm_tmp[.rob])
.END

.PROGRAM dev_free(.rob)
    FOR .dev = 1 TO 4 STEP 1
	IF tsk_use[.dev] == .rob THEN
	    tsk_use[.dev] = 0
	    IF pz.dev THEN
		TYPE "DevFree Rob:",.rob," Dev:",.dev
	    END
	END
    END
.END

.*
.* robがdevをUSEしているが
.* devを起動した後
.* devがrobをUSEしていることにする
.*
.PROGRAM dev_exec(.rob,.dev)
    $tsk_mes[.dev] = $tsk_mes[.rob]
     tsk_hst[.dev] =  tsk_hst[.rob]
     tsk_tsk[.dev] =  tsk_tsk[.rob]
    IF NOT SWITCH("CS",rob_tsk[.dev])	GOTO ini
     tsk_cmd[.dev] = bit[.rob]
     tsk_use[.dev] = OFF
     tsk_use[.rob] = .dev
    WAIT (tsk_use[.rob]==OFF)
    IF NOT SWITCH("CS",rob_tsk[.dev]) THEN
	tsk_err[.rob] = ON
	CALL make_res_ng(.rob)
    ELSE
	 tsk_err[.rob] =  tsk_err[.dev]
	$snd_res[.rob] = $snd_res[.dev]
    END
    RETURN
ini:
    $alm_tmp[.rob] = $REPLACE($ae_dev_init[.dev],"%R",$vrp.rob_asso[.dev],-1)
    CALL alarm_str(.rob,.rob,0,$alm_tmp[.rob])
.END
.******************************************
.** speed.as
.******************************************
.PROGRAM speed_init()
    FOR tmp = 1 TO 4 STEP 1
	IF rob_tsk[tmp] THEN
	    IF tmp <=2 THEN
		vc.speed[tmp] = spd_table[tmp,spd_medium]
	    ELSE
		vc.speed[tmp] = spd_table[tmp,spd_fast]
	    END
	    now.speed[tmp]  = -999
	    now.coldet[tmp] = -999
	    MC SPEED rob_tsk[tmp]: 100
	END
    END
.END
.PROGRAM setspd_cmd(.tsk)
    IF (tsk_num[.tsk]<>1)	GOTO efm
    STR_2DIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,1],":"
    IF tsk_div[.tsk]<>2			GOTO efm
    CALL id_unit(.tsk,d_divp,d_rob1)
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    tsk_dat[.tsk,d_dat1] = GETSTRBLK($speed_table,",",$TOUPPER($tsk_dat[.tsk,d_divp+1]))
    IF tsk_dat[.tsk,d_dat1] THEN
	tsk_dat[.tsk,d_dat1] = spd_table[tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_dat1]]
    ELSE
	CALL id_int(.tsk,d_divp+1,d_dat1,1)
	IF id_error[.tsk]		GOTO epm
	IF tsk_dat[.tsk,d_dat1]<1	GOTO epm
	IF tsk_dat[.tsk,d_dat1]>100	GOTO epm
    END
.** harish 090527 a++
    IF vc.mode[.tsk] == mode_soft
	CALL send_ack(.tsk)
	$alm_put[.tsk] = $AE_MD_THIS_SOFT
	CALL alarm_put(.tsk,0,0)
	CALL make_res_ng(.tsk)
	RETURN
    END
.** harish 090527 a--
    IF tsk_dat[.tsk,d_dat1]==vc.speed[tsk_dat[.tsk,d_rob1]] THEN
	CALL send_ack(.tsk)
    ELSE
	CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_init)
	IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    END
    vc.speed[tsk_dat[.tsk,d_rob1]] = tsk_dat[.tsk,d_dat1]
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM qryspd_cmd(.tsk)
    IF (tsk_num[.tsk]<>1)	GOTO efm
    CALL id_unit(.tsk,1,d_rob1)
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    CALL send_ack(.tsk)
    CALL int_str(vc.speed[tsk_dat[.tsk,d_rob1]],$str[.tsk],1)
    $snd_res[.tsk] = $res+","+$tsk_dat[.tsk,1]+":"+$str[.tsk]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM speed(.rob,.isp,.alw)
    IF 4 < TASKNO	RETURN
    .spd = MAXVAL(.isp,1)
    IF .alw THEN
	SPEED .spd ALWAYS
	now.speed[.rob] = .spd
    ELSE
	SPEED .spd
    END
    IF pz.speed THEN
	TYPE $RPGNAME(),/X1,$vrp.rob_asso[.rob],.spd,$IFELSE(.alw," ALWAYS","")
    END
.END
.PROGRAM speed_set(.rob)
    IF 4 < TASKNO	RETURN
    IF (tsk_hst[.rob]==host_stp1)OR(tsk_hst[.rob]==host_stp2) THEN
.****** Check動作は最速にしないと遅すぎる
	CALL speed(.rob,spd_table[.rob,spd_fast],ON)
    ELSEIF now.speed[.rob]<>vc.speed[.rob] THEN
	CALL speed(.rob,vc.speed[.rob],ON)
    END
.END
.PROGRAM speed_rate(.rob,.rat)
    IF 4 < TASKNO	RETURN
    CALL speed(.rob,now.speed[.rob]*.rat/100,OFF)
.END
.PROGRAM coldet_cmd(.tsk)
    CALL on_off_cmd(.tsk,vtp.coldet[.tsk],m_init)
.END
.PROGRAM coldet_set(.rob)
    IF .rob > 2		RETURN
    IF now.coldet[.rob]<>vtp.coldet[tsk_tsk[.rob]] THEN
	IF vtp.coldet[tsk_tsk[.rob]] THEN
	    COLRON  rob_tsk[.rob]:
	    COLRJON rob_tsk[.rob]:
.**TYPE "COLDET-->ON"
	ELSE
	    COLROFF  rob_tsk[.rob]:
	    COLRJOFF rob_tsk[.rob]:
.**TYPE "COLDET-->OFF"
	END
    END
    now.coldet[.rob] = vtp.coldet[tsk_tsk[.rob]]
.END
.** harish 080630 a++
.PROGRAM speed_min(.rob,.spd,.alw)
    IF 4 < TASKNO	RETURN
    IF .spd < now.speed[.rob] THEN
	CALL speed(.rob,.spd,.alw)
    END
.END
.** harish 080630 a--
.******************************************
.** robot.as
.******************************************
.PROGRAM robot.init(.rob)
    IF g.base[.rob]>=gbas_escp THEN
	CALL gcpos_base(.rob,gbas_escp)
    END
    now.speed[.rob]  = -999
    now.coldet[.rob] = -999
    IF .rob<=2 THEN
    ELSE
	algn_pos[.rob] = NONE
	algn_ang[.rob] = NONE
	algn_rob[.rob] = 0
    END
    rob_ini[.rob] = OFF
.END
.PROGRAM robot()
    .rob = tsk_rob[TASKNO]
    prg_stop[.rob] = OFF
    ZNUMCYC rob_tsk[.rob]: numcyc[.rob]
    snd_res[.rob] = OFF
    CALL gcpos_cls(.rob)
    rob_ini[.rob] = ON
.*** harish 090327-1 a++
    PrePos[.rob] = OFF
Limit:
    CALL limit_robot(.rob)
Loop:
    tsk_err[.rob] = 0
wait:
    IF rob_ini[.rob] THEN
	CALL robot.init(.rob)
    END
    CALL speed_set(.rob)
    IF tsk_cmd[.rob]	GOTO move
    TWAIT 0.016
    GOTO wait
Move:
    IF (tsk_hst[.rob]==host_stp1)OR(tsk_hst[.rob]==host_stp2) THEN
.****** Check動作は最速にしないと遅すぎる
	CALL speed(.rob,spd_table[.rob,spd_fast],ON)
    END
    $snd_res[.rob] = $res_ok
    CALL coldet_set(.rob)
    CALL robot_sel(.rob)
.*** harish 090327-1 d++
;   CALL fan_check(.rob,.rob)
;   CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
;   IF ($snd_res[.rob]==$res_ok)AND(fan_check[.rob]) THEN
;	CALL fan_error(.rob,.rob)
;	$snd_res[.rob]=$res_ng
;   END
.*** harish 090327-1 d--
.*** harish 090327-1 a++
    IF NOT PrePos[.rob] THEN
	CALL robot_fan(.rob)
	IF tsk_cmd[.rob]==.rob THEN
	    snd_res[.rob] = ON
	    tsk_cmd[.rob] = OFF
	    WAIT (snd_res[.rob] == OFF)
	ELSE
	    tsk_cmd[.rob] = OFF
	END
    ELSE
	CALL PrePos_res(.rob)
    END
.*** harish 090327-1 a--
.*** harish 090327-1 d++
;   IF tsk_cmd[.rob] == bit[.rob] THEN
;	snd_res[.rob] = ON
;	tsk_cmd[.rob] = 0
;	WAIT (snd_res[.rob]==OFF)
;   ELSE
;	tsk_cmd[.rob] = OFF
;   END
.*** harish 090327-1 d--
    CALL dev_free(.rob)
    IF prg_stop[.rob]	RETURN
    IF (g.base[.rob]==gbas_nhom)		GOTO Limit
    GOTO Loop
.END
.PROGRAM robot_sel(.rob)
    IF (g.base[.rob]==gbas_nhom)		GOTO need_home
    IF (g.base[.rob]>=gbas_tget)		GOTO tgetput
.********************************
.** Position Event ON Commands **
.********************************
    evt_pos[.rob] = ON
    SCASE $tsk_cmd[.rob] OF
.********************
.** Wafer Handling **
.********************
.** harish 090327-1 a++
      SVALUE "GETWAFPREPOS":
	CALL getputprpos_rob(.rob,gbas_getp)
      SVALUE "PUTWAFPREPOS":
	CALL getputprpos_rob(.rob,gbas_putp)
      SVALUE "TRWAFPREPOS":
	CALL trwafprpos_rob(.rob)
.** harish 090327-1 a--
      SVALUE "GETWAF" :
	CALL getput_rob(.rob,gbas_getp)
      SVALUE "PUTWAF" :
	CALL getput_rob(.rob,gbas_putp)
      SVALUE "GETX" :
	CALL getputx_rob(.rob,gbas_getp)
      SVALUE "PUTX" :
	CALL getputx_rob(.rob,gbas_putp)
      SVALUE "TRWAF" :
	CALL trwaf_rob(.rob)
      SVALUE "MOVTOLOC" :
	CALL movtoloc_rob(.rob)
.** harish 090515-1 a++
      SVALUE "SCAN_IMAP" :
	CALL scan_imap_rob(.rob,gbas_putp)
.** harish 090515-1 a--
.*************
.** Mapping **
.*************
      SVALUE "MAP","MAPSLOT":
	CALL map_rob(.rob)
      ANY :
	GOTO evt_off
    END
    GOTO Exit
evt_off:
.*********************************
.** Position Event OFF Commands **
.*********************************
    evt_pos[.rob] = OFF
    SCASE $tsk_cmd[.rob] OF
      SVALUE "MOVPREPOS" :
	CALL movprepos_rob(.rob)
      SVALUE "PINTCH" :
	CALL pintch_rob(.rob)
.**********************
.** for only Aligner **
.**********************
      SVALUE "ALGN" :
	CALL algn_rob(.rob)
      SVALUE "ANGLE" :
	CALL angle_rob(.rob)
      ANY :
	GOTO tgetput
    END
    GOTO Exit
tgetput:
    evt_pos[.rob] = OFF
    SCASE $tsk_cmd[.rob] OF
.********************************
.** Wafer Handling Test Motion **
.********************************
      SVALUE "TGETWAF" :
	CALL tgetput_rob(.rob,gbas_tget)
      SVALUE "TPUTWAF" :
	CALL tgetput_rob(.rob,gbas_tput)
      ANY :
	GOTO need_home
    END
    GOTO Exit
need_home:
    evt_pos[.rob] = OFF
    SCASE $tsk_cmd[.rob] OF
.** harish 081120 a++
.***********************
.** Diagnose Plunger  **
.***********************
      SVALUE "PLUNGER" :
        CALL diag_plunger(.rob)
.************************
.** Diagnose Backlash  **
.************************
      SVALUE "BACKLASH" :
        CALL diag_backlash(.rob)
.** harish 081120 a--
.*************
.** Homing  **
.*************
      SVALUE "HOME" :
	CALL home_rob(.rob)
.*******************
.** Manual Motion **
.*******************
      SVALUE "MOVREL":
	CALL m_relabs_rob(.rob,-1)
      SVALUE "MOVABS":
	CALL m_relabs_rob(.rob,+1)
      ANY :
	GOTO cond_err
    END

    GOTO Exit
cond_err:
    IF (g.base[.rob]==gbas_nhom) THEN
	CALL alarm_str(.rob,.rob,0,$ae_need_home)
    END
    CALL tgetput_err(.rob)
    IF NOT tsk_err[.rob] THEN
	CALL alarm_str(.rob,.rob,0,$ae_no_rb_prg)
    END
Exit:
    RETURN
.END

.*** harish 090327-1 a++
.PROGRAM robot_fan(.rob)
    CALL fan_check(.rob,.rob)
.*** harish 090327-1 m++
    IF NOT PrePos[.rob] THEN
        CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
    END
.*** harish 090327-1 m--
    IF ($snd_res[.rob]==$res_ok)AND(fan_check[.rob]) THEN
	CALL fan_error(.rob,.rob)
	$snd_res[.rob] = $res_ng
    END
.END

.PROGRAM PrePos_set(.rob)
    PrePos[.rob] = ON
    PrePos.hst[.rob] = tsk_hst[.rob]
    $PrePos.host[.rob] = $host[tsk_hst[.rob]]
    $PrePos.mes[.rob] = $tsk_mes[.rob]
    snd_res[.rob] = ON
    tsk_cmd[.rob] = OFF
.***Q090123-3 a++
    WAIT (snd_res[.rob] == OFF)
.***Q090123-3 a--
    PrePos.cmd[.rob] = ON
    tsk_err[.rob] = OFF
.END

.PROGRAM PrePos_res(.rob)
    CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
    IF tsk_err[.rob] THEN
.*  PrePos failed (application error)
	CALL prpos.event_rob(.rob, $EV_PRPOS_ERR[.rob], $EV2_PRPOS_ERR)
	CALL prpos.event_out(.rob)
    ELSEIF NOT tsk_cmd[.rob] THEN
.*  PrePos success
	CALL prpos.event_rob(.rob, $EV_PRPOS_SUC[.rob], $EV2_PRPOS_SUC)
	CALL prpos.event_out(.rob)
    END
    Swap.prepos[.rob] = OFF
    PrePos.hst[.rob] = 0
    $PrePos.host[.rob] = ""
    $PrePos.mes[.rob] = ""
    PrePos.cmd[.rob] = OFF
    PrePos[.rob] = OFF
.END
.*** harish 090327-1 a--
.******************************************
.** p_hori.as
.******************************************
.PROGRAM qrypos_hori(.tsk,.rob,.prt,.slt,.pps,.tch)
    .hnd = 1
    POINT #jnt[.tsk] = #none
    CALL p_hori(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE .pps OF
      VALUE pps_get,pps_gts,pps_ptr :
        POINT #jnt[.tsk] = #gp_home[.tsk] - #PPOINT(0,0,vpp.llift[.prt])
      VALUE pps_put,pps_pts,pps_gtr :
        POINT #jnt[.tsk] = #gp_home[.tsk] + #PPOINT(0,0,vpp.ulift[.prt])
      VALUE pps_gtx :
        POINT #jnt[.tsk] = #gp_goff[.tsk] - #PPOINT(0,0,vpp.llift[.prt])
      VALUE pps_ptx :
        POINT #jnt[.tsk] = #gp_putt[.tsk] + #PPOINT(0,0,vpp.ulift[.prt])
      VALUE pps_tch :
        POINT #jnt[.tsk] = #gp_tech[.tsk]
      ANY :
    END
.END
.PROGRAM escape_hori(.rob)
    .hnd = g.hand[.rob]
    .prt = g.port[.rob]
    .slt = g.slot[.rob]
    CALL p_hori(.rob,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
Loop:
    POINT #jnt[.rob] = #DEST:rob_tsk[.rob]
    CASE g.cpos[.rob] OF
.** harish 090202 m+ (U4 collision)
      VALUE gpos_e_home,gpos_r_home,gpos_home:
.*                                                      /* SCRX11-066 d
        ;CALL gripper_out(.rob,.hnd,grip_hold)
        POINT .#dst = #PPJSET(#gp_home[.rob],3,DEXT(#jnt[.rob],3))
        CALL xmove(.rob,.hnd,.pat,gpos_r_home,.#dst,"ESC_HORI_HOME")
        CALL break(.rob)
        RETURN
      VALUE gpos_e_prep,gpos_r_prep:
        IF path_move[.pat,gpos_r_prep] THEN
.*                                                      /* SCRX11-066 d
            ;CALL gripper_out(.rob,.hnd,grip_hold)
            POINT .#dst = #PPJSET(#gp_prep[.rob],3,DEXT(#jnt[.rob],3))
            CALL xmove(.rob,.hnd,.pat,gpos_r_prep,.#dst,"ESC_HORI_PREP")
            CALL break(.rob)
        END
        CALL gcpos_pos(.rob,gpos_r_home)
      VALUE gpos_e_half,gpos_r_half:
        IF path_move[.pat,gpos_r_half] THEN
.*                                                      /* SCRX11-066 d
            ;CALL gripper_out(.rob,.hnd,grip_hold)
            POINT .#dst = #PPJSET(#gp_half[.rob],3,DEXT(#jnt[.rob],3))
            CALL xmove(.rob,.hnd,.pat,gpos_r_half,.#dst,"ESC_HORI_HALF")
            CALL break(.rob)
        END
        CALL gcpos_pos(.rob,gpos_r_prep)
      VALUE gpos_e_near,gpos_r_near:
        IF path_move[.pat,gpos_r_near] THEN
.*                                                      /* SCRX11-066 d
            ;CALL gripper_out(.rob,.hnd,grip_hold)
            POINT .#dst = #PPJSET(#gp_near[.rob],3,DEXT(#jnt[.rob],3))
            CALL xmove(.rob,.hnd,.pat,gpos_r_near,.#dst,"ESC_HORI_NEAR")
            CALL break(.rob)
        END
        CALL gcpos_pos(.rob,gpos_r_half)
      ANY :
.** harish 090216 m++
        CALL gripper_out(.rob,.hnd,grip_rels)
        TWAIT 1
.** harish inverse kinematics 080930 a++
        ZL3TRN rob_tsk[.rob] : .trn[.rob,1] = #jnt[.rob], inv_hand
        IF vpp.portang[.prt] <> -1000
            .trn[.rob,4] = vpp.portang[.prt]
        END
        ZL3JNT rob_tsk[.rob]: #jnt[.rob] = .trn[.rob,1],#jnt[.rob],inv_normal,inv_hand
        .$str = "ESC_HORI_RO"
        CALL jmove(.rob,.hnd,gpos_r_near,#jnt[.rob],.$str)
        CALL break(.rob)
.** harish 090515 a++
        IF (port_type[0,.prt] BAND port_util)AND(tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
          IF (vpp.isimap[.prt]) THEN
            GOTO imap
          END
        END
.** harish 090515 a--
.** harish inverse kinematics 080930 a--
        IF DEXT(#jnt[.rob]-#gp_tech[.rob],3)<0.5 THEN
.** harish 080821 homing a++
          IF DEXT(#jnt[.rob]-#gp_tech[.rob],3)>-vpp.llift[.prt] THEN
            CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])
.*                                                      /* SCRX11-066 md++
            ;POINT .#dst = #gp_goff[.rob]
            POINT .#dst = #PPJSET(#jnt[.rob],3,DEXT(#gp_tech[.rob]-#PPOINT(0,0,vpp.llift[.prt]),3))
.*                                                      /* SCRX11-066 md++
            .$str = "ESC_HORI_LW"
          ELSE
            CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])
            POINT .#dst = #jnt[.rob]
            .$str = "ESC_HORI_ST"
          END
.** harish 080821 homing a--
        ELSE
.** harish 081021 homing a++
          IF DEXT(#jnt[.rob]-#gp_tech[.rob],3)<vpp.ulift[.prt] THEN
            CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])
            POINT .#dst = #PPJSET(#jnt[.rob],3,DEXT(#gp_tech[.rob]+#PPOINT(0,0,vpp.ulift[.prt]),3))
            .$str = "ESC_HORI_UP"
          ELSE
            CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])
            POINT .#dst = #jnt[.rob]
            .$str = "ESC_HORI_ST"
          END
.** harish 081021 homing a--
.** harish 090216 m--
        END
        CALL jmove(.rob,.hnd,gpos_r_near,.#dst,.$str)
.*                                                      /* SCRX11-066-4 a++
        IF DEXT(#jnt[.rob]-#gp_tech[.rob],3)>=0.5 THEN
          IF DEXT(#jnt[.rob]-#gp_tech[.rob],3)<=vpp.ulift[.prt] THEN
            CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])
            POINT .#dst = #PPJSET(#gp_back[.rob],3,DEXT(.#dst,3))
            CALL jmove(.rob,.hnd,gpos_r_back,.#dst,"ESC_HORI_BACK")
            call break(.rob)
            CALL gripper_out(.rob,.hnd,grip_hold)
          END
        END
.*                                                      /* SCRX11-066-4 a--
.** harish 090515-2 a
imap:
        CALL break(.rob)
        CALL gcpos_pos(.rob,gpos_r_near)
    END
    GOTO Loop
.END
.PROGRAM getwaf_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_hori(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
;
    .llf = -IFELSE(tsk_dat[.tsk,d_llf1]>0,tsk_dat[.tsk,d_llf1],vpp.llift[.prt])
    .ulf =  IFELSE(tsk_dat[.tsk,d_ulf1]>0,tsk_dat[.tsk,d_ulf1],vpp.ulift[.prt])
.** harish 080630 a++
    POINT .#upp  = #gp_tech[.tsk] + #PPOINT(0,0, .ulf)
    POINT .#low  = #gp_tech[.tsk] + #PPOINT(0,0, .llf)
.** harish 080630 a--
;
    .aln = 0
    IF (.tsk <= 2)AND((port_type[0,.prt] BAND port_kind) == port_algn) THEN
        .aln = 3
        IF (.dst<>gpos_r_home)                GOTO no_ang
        IF (algn_pos[.aln]<-999)        GOTO no_ang
        IF (algn_ang[.aln]<-999)        GOTO no_ang
        IF (algn_rob[.aln]==.rob)        GOTO no_ang
        CALL dev_lock(.rob,.aln)
        IF tsk_err[.rob]                GOTO err
        $tsk_cmd[.aln] = "ANGLE"
        IF tsk_arm[rob_tsk[.rob]] BAND armb_nxx THEN
            ZL3TRN rob_tsk[.rob]: trn[.tsk,1] = #gp_tech[.tsk],inv_hand
            tsk_dat[.aln,d_dat1] = trn[.tsk,4]
        ELSE
            tsk_dat[.aln,d_dat1] = DEXT(#gp_tech[.tsk],2)
        END
        tsk_dat[.aln,d_dat1] = tsk_dat[.aln,d_dat1] + vpp.alnoff[.rob,.prt]
        CALL dev_exec(.rob,.aln)
        IF tsk_err[.rob]                GOTO err
        algn_rob[.aln] = .rob
no_ang:
    END
;
    .pos = .sta
Loop:
    CASE .pos OF
      VALUE gpos_e_home
        IF NOT ((.bas==gbas_getp)AND(.dst==gpos_e_home)) THEN
            CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_abst,who_get,0)
.** harish 090326 m++
            IF grip_err[.tsk]
                CALL gcpos_home(.rob,gpos_home)
                RETURN
            END
.** harish 090326 m--
.*            IF grip_err[.tsk]        GOTO gerr
        END
        POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,.llf)
.*                                                      /* SCRX12-006 2 am++
        CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_E_HOME",.err)
        if .err then
            return
        end
.*                                                      /* SCRX12-006 2 am--
      VALUE gpos_e_prep
        IF NOT path_move[.pat,gpos_e_prep]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_E_PREP")
.** harish 090616 m++ section deleted **
.** harish 080630 a++
.**        IF (.pat == p_hori_vda) OR (.pat == p_hori_vdb)
.** harish 090328 a
.**          IF TASKNO<=2 THEN
.**            IF (vtp.sniff[tsk_tsk[.rob]]) THEN
.**              IF (vpp.sniff[.prt]) AND (.dst == gpos_r_home) AND (.bas == gbas_getp) THEN
.**                CALL sniff_hori(.tsk,.rob,.hnd,.prt,.slt,.pos,.#low,.sniff_err)
.**                IF .sniff_err GOTO err
.**              END
.**            END
.** harish 090328 a
.**          END
.**        END
.** harish 080630 a--
.** harish 090616 m-- section deleted **
      VALUE gpos_e_half
        IF NOT path_move[.pat,gpos_e_half]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_E_HALF")
.** harish 080630 a++
.** harish 090616 m
.**      IF (.pat <> p_hori_vda) AND (.pat <> p_hori_vdb)
.** harish 090328 a
        IF TASKNO<=2 THEN
        IF (vtp.sniff[tsk_tsk[.rob]]) THEN
            IF (vpp.sniff[.prt]) AND (.dst == gpos_r_home) AND (.bas == gbas_getp) THEN
                CALL sniff_hori(.tsk,.rob,.hnd,.prt,.slt,.pos,.#low,.sniff_err)
                IF .sniff_err GOTO err
            END
        END
.** harish 090328 a
        END
.** harish 090616 m
.**        END
.** harish 080630 a--
      VALUE gpos_e_near
        IF NOT path_move[.pat,gpos_e_near]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_E_NEAR")
      VALUE gpos_e_extd
        CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
        IF grip_err[.tsk]        GOTO gerr

        IF (tsk_hst[.rob]==host_stp1)OR(tsk_hst[.rob]==host_stp2) THEN
;********** チェックモードは信号先出ししないのでここでプランジャーリリースする
            CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
            IF grip_err[.tsk]        GOTO gerr
        END

        POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_E_EXTD")
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
        IF grip_err[.tsk]        GOTO gerr
;***  VALUE gpos_e_slow
;*******TYPE "E_SLOW: N/A"
      VALUE gpos_e_tech
        IF (.dst == gpos_r_tech)OR( .bas==gbas_tget) THEN
            CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
            IF grip_err[.tsk]        GOTO gerr

            CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])

            POINT .#dst = #gp_goff[.tsk]
            CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_E_TECH")
        ELSE
            GOTO next
        END
      VALUE gpos_r_tech
        IF (.dst == gpos_r_tech)AND(.bas==gbas_getp) THEN
            CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
            IF grip_err[.tsk]        GOTO gerr

            CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])

            POINT .#dst = #gp_tech[.tsk]
            CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_TECH")
        ELSE
            GOTO next
        END
;***  VALUE gpos_r_slow
;*******TYPE "R_SLOW: N/A"
      VALUE gpos_r_extd
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
        IF grip_err[.tsk]        GOTO gerr

        CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])

        POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_EXTD")
      VALUE gpos_r_back
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
        IF grip_err[.tsk]        GOTO gerr
        IF TASKNO<=2 THEN
            IF (vtp.getprebck[tsk_tsk[.rob]] OR vpp.getprebck[.prt]) THEN
                POINT .#dst = #gp_back[.tsk] + #PPOINT(0,0,.ulf)
            ELSE
                POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.ulf)
            END
        ELSE
            POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.ulf)
        END
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_BACK")
        CALL mvwait(.tsk,0)
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
        IF grip_err[.tsk] THEN
            IF vpp.errret[.prt] THEN
                CALL gripper_out(.rob,.hnd,grip_rels)
                TWAIT 1
                CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_escape]),ON)
                POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,.ulf)
                CALL xmove(.tsk,.hnd,.pat,gpos_e_extd,.#dst,"GET_HORI_ERR1")

                CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])

.** harish 090216 m
                POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.llf)
.**                POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,.llf)
                CALL xmove(.tsk,.hnd,.pat,gpos_e_extd,.#dst,"GET_HORI_ERR2")
                CALL speed_set(.rob)
            END
            GOTO gerr
        END
      VALUE gpos_r_near
        IF NOT path_move[.pat,gpos_r_near]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_NEAR")
      VALUE gpos_r_half
        IF NOT path_move[.pat,gpos_r_half]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_HALF")
      VALUE gpos_r_prep
        IF NOT path_move[.pat,gpos_r_prep]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_PREP")
      VALUE gpos_r_home
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_HORI_R_HOME")
        IF TASKNO<=2 THEN
            IF (vtp.getretchk[tsk_tsk[.rob]] OR vpp.getretchk[.prt]) THEN
                CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
                CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
                IF grip_err[.tsk]        GOTO gerr
            END
        END
      ANY :
next:
        .pos = .pos + 1
        GOTO Loop
    END
    IF .pos < .dst THEN
        .pos = .pos + 1
        GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
        CALL escape(.rob)
    END
.END

.PROGRAM putwaf_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_hori(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
;
    .llf = -IFELSE(tsk_dat[.tsk,d_llf1]>0,tsk_dat[.tsk,d_llf1],vpp.llift[.prt])
    .ulf =  IFELSE(tsk_dat[.tsk,d_ulf1]>0,tsk_dat[.tsk,d_ulf1],vpp.ulift[.prt])
;
    .pos = .sta
Loop:
    CASE .pos OF
      VALUE gpos_e_home
        IF NOT ((.bas==gbas_putp)AND(.dst==gpos_e_home)) THEN
            CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_pres,who_put,pres_hori)
.** harish 090326 m++
            IF grip_err[.tsk]
                CALL gcpos_home(.rob,gpos_home)
                RETURN
            END
.** harish 090326 m--
.*            IF grip_err[.tsk]        GOTO gerr
        END
        POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,.ulf)
.*                                                      /* SCRX12-006 2 am++
        CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_E_HOME",.err)
        if .err then
            return
        end
.*                                                      /* SCRX12-006 2 am--
      VALUE gpos_e_prep
        IF NOT path_move[.pat,gpos_e_prep]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_E_PREP")
      VALUE gpos_e_half
        IF NOT path_move[.pat,gpos_e_half]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_E_HALF")
      VALUE gpos_e_near
        IF NOT path_move[.pat,gpos_e_near]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_E_NEAR")
      VALUE gpos_e_extd
        CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_putt[.tsk] + #PPOINT(0,0,.ulf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_E_EXTD")
        CALL mvwait(.tsk,0)
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
        IF grip_err[.tsk]        GOTO gerr
;***  VALUE gpos_e_slow
;*******TYPE "E_SLOW: N/A"
      VALUE gpos_e_tech
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
        IF grip_err[.tsk]        GOTO gerr

        CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])

        POINT .#dst = #gp_putt[.tsk]
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_E_TECH")
;***  VALUE gpos_r_tech
;****** TYPE "R_TECH: N/A"
      VALUE gpos_r_slow
        IF NOT path_move[.pat,gpos_r_slow]        GOTO next
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
        IF grip_err[.tsk]        GOTO gerr

        CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])

        POINT .#dst = #gp_pfng[.tsk]
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_SLOW")
      VALUE gpos_r_extd
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
        IF grip_err[.tsk]        GOTO gerr

        CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])

        POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_EXTD")
      VALUE gpos_r_back
        CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
        IF grip_err[.tsk]        GOTO gerr
        POINT .#dst = #gp_back[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_BACK")
        IF TASKNO<=2 THEN
            IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
                CALL mvwait(.tsk,0)
                CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
                IF grip_err[.tsk] THEN
                    IF vpp.errret[.prt] THEN
                        CALL gripper_out(.rob,.hnd,grip_rels)
                        TWAIT 1
                        CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_escape]),ON)
.** harish 081103 a+
.*                        POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,.llf)
.*                                                      /* SCRX12-006 m
                        POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.llf)
                        CALL xmove(.tsk,.hnd,.pat,gpos_e_extd,.#dst,"PUT_HORI_ERR1")

                        CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])

.** harish 081103 a+
.*                        POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,.ulf)
.*                                                      /* SCRX12-006 m
                        POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,.ulf)
                        CALL xmove(.tsk,.hnd,.pat,gpos_e_extd,.#dst,"PUT_HORI_ERR2")
                        CALL speed_set(.rob)
                    END
                    GOTO gerr
                END
            END
        END
      VALUE gpos_r_near
        IF NOT path_move[.pat,gpos_r_near]        GOTO next
        IF TASKNO<=2 THEN
            IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
                CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
                IF grip_err[.tsk]        GOTO gerr
            END
        END
        POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_NEAR")
      VALUE gpos_r_half
        IF NOT path_move[.pat,gpos_r_half]        GOTO next
        IF TASKNO<=2 THEN
            IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
                CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
                IF grip_err[.tsk]        GOTO gerr
            END
        END
        POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_HALF")
      VALUE gpos_r_prep
        IF NOT path_move[.pat,gpos_r_prep]        GOTO next
        IF TASKNO<=2 THEN
            IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
                CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
                IF grip_err[.tsk]        GOTO gerr
            END
        END
        POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_PREP")
      VALUE gpos_r_home
        IF TASKNO<=2 THEN
            IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
                CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
                IF grip_err[.tsk]        GOTO gerr
            END
        END
        POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,.llf)
        CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_HORI_R_HOME")
        CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
        IF grip_err[.tsk]        GOTO gerr
      ANY :
next:
        .pos = .pos + 1
        GOTO Loop
    END
    IF .pos < .dst THEN
        .pos = .pos + 1
        GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
        CALL escape(.rob)
    END
.END


.PROGRAM p_hori(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE (IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt])) OF
      VALUE p_hori_ns :
        CALL p_hori_ns(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE p_hori_wet :
        CALL p_hori_wet(.tsk,.rob,.hnd,.prt,.slt,.tch)
      ANY :
        CALL p_hori_app(.tsk,.rob,.hnd,.prt,.slt,.tch)
    END
.END

.**
.** Normal NS
.**
.REALS
    mapp_path[p_hori_ns] = gpos_e_home + 0.5
    mapp_move[p_hori_ns] = mov_jmv
    mapp_acce[p_hori_ns] = 100
    mapp_dece[p_hori_ns] = 100

    path_move[p_hori_ns,gpos_e_home] = mov_jmv
    path_move[p_hori_ns,gpos_e_prep] = mov_non
    path_move[p_hori_ns,gpos_e_half] = mov_non
    path_move[p_hori_ns,gpos_e_near] = mov_non
    path_move[p_hori_ns,gpos_e_extd] = mov_jmv
    path_move[p_hori_ns,gpos_e_slow] = mov_non        ;**N/A
    path_move[p_hori_ns,gpos_e_tech] = mov_jmv
    path_move[p_hori_ns,gpos_r_tech] = mov_non
    path_move[p_hori_ns,gpos_r_slow] = mov_non        ;**N/A
    path_move[p_hori_ns,gpos_r_extd] = mov_jmv
    path_move[p_hori_ns,gpos_r_back] = mov_jmv
    path_move[p_hori_ns,gpos_r_near] = mov_non
    path_move[p_hori_ns,gpos_r_half] = mov_non
    path_move[p_hori_ns,gpos_r_prep] = mov_non
    path_move[p_hori_ns,gpos_r_home] = mov_jmv

    path_sped[p_hori_ns,gpos_e_home] = 100
    path_sped[p_hori_ns,gpos_e_prep] = 100
    path_sped[p_hori_ns,gpos_e_half] = 100
    path_sped[p_hori_ns,gpos_e_near] = 100
    path_sped[p_hori_ns,gpos_e_extd] = 100
    path_sped[p_hori_ns,gpos_e_slow] = 100
    path_sped[p_hori_ns,gpos_e_tech] = 100
    path_sped[p_hori_ns,gpos_r_tech] = 100
    path_sped[p_hori_ns,gpos_r_slow] = 100
    path_sped[p_hori_ns,gpos_r_extd] = 100
    path_sped[p_hori_ns,gpos_r_back] = 100
    path_sped[p_hori_ns,gpos_r_near] = 100
    path_sped[p_hori_ns,gpos_r_half] = 100
    path_sped[p_hori_ns,gpos_r_prep] = 100
    path_sped[p_hori_ns,gpos_r_home] = 100

    path_acce[p_hori_ns,gpos_e_home] = 100
    path_acce[p_hori_ns,gpos_e_prep] = 100
    path_acce[p_hori_ns,gpos_e_half] = 100
    path_acce[p_hori_ns,gpos_e_near] = 100
    path_acce[p_hori_ns,gpos_e_extd] = 100
    path_acce[p_hori_ns,gpos_e_slow] = 100
    path_acce[p_hori_ns,gpos_e_tech] = 100
    path_acce[p_hori_ns,gpos_r_tech] = 100
    path_acce[p_hori_ns,gpos_r_slow] = 100
    path_acce[p_hori_ns,gpos_r_extd] = 100
    path_acce[p_hori_ns,gpos_r_back] = 100
    path_acce[p_hori_ns,gpos_r_near] = 100
    path_acce[p_hori_ns,gpos_r_half] = 100
    path_acce[p_hori_ns,gpos_r_prep] = 100
    path_acce[p_hori_ns,gpos_r_home] = 100

    path_dece[p_hori_ns,gpos_e_home] = 100
    path_dece[p_hori_ns,gpos_e_prep] = 100
    path_dece[p_hori_ns,gpos_e_half] = 100
    path_dece[p_hori_ns,gpos_e_near] = 100
    path_dece[p_hori_ns,gpos_e_extd] = 100
    path_dece[p_hori_ns,gpos_e_slow] = 100
    path_dece[p_hori_ns,gpos_e_tech] = 100
    path_dece[p_hori_ns,gpos_r_tech] = 100
    path_dece[p_hori_ns,gpos_r_slow] = 100
    path_dece[p_hori_ns,gpos_r_extd] = 100
    path_dece[p_hori_ns,gpos_r_back] = 100
    path_dece[p_hori_ns,gpos_r_near] = 100
    path_dece[p_hori_ns,gpos_r_half] = 100
    path_dece[p_hori_ns,gpos_r_prep] = 100
    path_dece[p_hori_ns,gpos_r_home] = 100

    path_acur[p_hori_ns,gpos_e_home] = 100
    path_acur[p_hori_ns,gpos_e_prep] = 100
    path_acur[p_hori_ns,gpos_e_half] = 100
    path_acur[p_hori_ns,gpos_e_near] = 100
    path_acur[p_hori_ns,gpos_e_extd] =   3
    path_acur[p_hori_ns,gpos_e_slow] =   1
    path_acur[p_hori_ns,gpos_e_tech] =   1
    path_acur[p_hori_ns,gpos_r_tech] =   1
    path_acur[p_hori_ns,gpos_r_slow] =   1
    path_acur[p_hori_ns,gpos_r_extd] =   3
    path_acur[p_hori_ns,gpos_r_back] =   1
    path_acur[p_hori_ns,gpos_r_near] = 100
    path_acur[p_hori_ns,gpos_r_half] = 100
    path_acur[p_hori_ns,gpos_r_prep] = 100
    path_acur[p_hori_ns,gpos_r_home] = 100
.END
.PROGRAM p_hori_ns(.tsk,.rob,.hnd,.prt,.slt,.tch)
.** #gp_tech
.** #gp_goff
.** #gp_back
.** #gp_putt
.** #gp_poff
.** #gp_pfng
.** #gp_home
.** #gp_prep
.** #gp_half
.** #gp_near
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
        POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    POINT #gp_goff[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,0, vpp.gforwd[ .prt])
    POINT #gp_back[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,0,-vpp.backwd[ .prt])
    POINT #gp_putt[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,0,-vpp.putback[.prt])
    POINT #gp_poff[.tsk] = #gp_putt[.tsk] + #PPOINT(0,0,0, vpp.pforwd[ .prt])
    POINT #gp_pfng[.tsk] = #gp_putt[.tsk]
;
    POINT #gp_home[.tsk] = #PPJSET(#gp_tech[.tsk],4,home_x[.rob])
    POINT #gp_prep[.tsk] = #gp_home[.tsk]
    POINT #gp_half[.tsk] = #gp_home[.tsk]
    POINT #gp_near[.tsk] = #gp_home[.tsk]
.END


.**
.** Normal WET FACE UP
.**
.REALS
    mapp_path[p_hori_wet] = 0
    mapp_move[p_hori_wet] = mov_non

    path_move[p_hori_wet,gpos_e_home] = mov_jmv        ;**N/A
    path_move[p_hori_wet,gpos_e_prep] = mov_non
    path_move[p_hori_wet,gpos_e_half] = mov_non
    path_move[p_hori_wet,gpos_e_near] = mov_non
    path_move[p_hori_wet,gpos_e_extd] = mov_jmv
    path_move[p_hori_wet,gpos_e_slow] = mov_non        ;**N/A
    path_move[p_hori_wet,gpos_e_tech] = mov_jmv
    path_move[p_hori_wet,gpos_r_tech] = mov_jmv
    path_move[p_hori_wet,gpos_r_slow] = mov_non        ;**N/A
    path_move[p_hori_wet,gpos_r_extd] = mov_jmv
    path_move[p_hori_wet,gpos_r_back] = mov_jmv
    path_move[p_hori_wet,gpos_r_near] = mov_non
    path_move[p_hori_wet,gpos_r_half] = mov_non
    path_move[p_hori_wet,gpos_r_prep] = mov_non
    path_move[p_hori_wet,gpos_r_home] = mov_jmv
.END
.PROGRAM p_hori_wet(.tsk,.rob,.hnd,.prt,.slt,.tch)
.** #gp_tech
.** #gp_goff
.** #gp_back
.** #gp_putt
.** #gp_poff
.** #gp_pfng
.** #gp_home
.** #gp_prep
.** #gp_half
.** #gp_near
    POINT #gp_tech[.tsk] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
    IF tsk_dat[.tsk,d_zps1] > 0 THEN
        POINT #gp_tech[.tsk] = #PPJSET(#gp_tech[.tsk],3,tsk_dat[.tsk,d_zps1])
    END
    POINT #gp_goff[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,0, vpp.gforwd[ .prt])
    POINT #gp_back[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,0,-vpp.backwd[ .prt])
    POINT #gp_putt[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,0,-vpp.putback[.prt])
    POINT #gp_poff[.tsk] = #gp_putt[.tsk] + #PPOINT(0,0,0, vpp.wpforwdy[ .prt])
    POINT #gp_pfng[.tsk] = #gp_poff[.tsk] + #PPOINT(0,0,  -vpp.wpforwdz[ .prt])
;
    POINT #gp_home[.tsk] = #PPJSET(#gp_tech[.tsk],4,home_x[.rob])
    POINT #gp_prep[.tsk] = #gp_home[.tsk]
    POINT #gp_half[.tsk] = #gp_home[.tsk]
    POINT #gp_near[.tsk] = #gp_home[.tsk]
.END
.******************************************
.** p_down.as
.******************************************
.PROGRAM qrypos_down(.tsk,.rob,.prt,.slt,.pps,.tch)
    .hnd = 1
    POINT #jnt[.tsk] = #none
    CALL p_down(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE .pps OF
      VALUE pps_get,pps_gts,pps_gtr,pps_put,pps_pts,pps_ptr :
	POINT #jnt[.tsk] = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
      VALUE pps_gtx :
	POINT #jnt[.tsk] = #gp_goff[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
      VALUE pps_ptx :
	POINT #jnt[.tsk] = #gp_putt[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
      VALUE pps_tch :
	POINT #jnt[.tsk] = #gp_tech[.tsk]
      ANY :
    END
.END
.PROGRAM escape_down(.rob)
    .hnd = g.hand[.rob]
    .prt = g.port[.rob]
    .slt = g.slot[.rob]
    CALL p_down(.rob,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
Loop:
.** harish 081031 a+
    CALL gripper_out(.rob,.hnd,grip_hold)
    POINT #jnt[.rob] = #DEST:rob_tsk[.rob]
    CASE g.cpos[.rob] OF
.** harish 090202 m+ (U4 collision)
      VALUE gpos_e_home,gpos_r_home,gpos_home:
	POINT .#dst = #PPJSET(#gp_home[.rob],3,DEXT(#jnt[.rob],3))
	CALL xmove(.rob,.hnd,.pat,gpos_r_home,.#dst,"ESC_DOWN_HOME")
	CALL break(.rob)
	RETURN
      VALUE gpos_e_prep,gpos_r_prep:
	IF path_move[.pat,gpos_r_prep] THEN
	    POINT .#dst = #PPJSET(#gp_prep[.rob],3,DEXT(#jnt[.rob],3))
	    CALL xmove(.rob,.hnd,.pat,gpos_r_prep,.#dst,"ESC_DOWN_PREP")
	    CALL break(.rob)
	END
	CALL gcpos_pos(.rob,gpos_r_home)
      VALUE gpos_e_half,gpos_r_half:
	IF path_move[.pat,gpos_r_half] THEN
	    POINT .#dst = #PPJSET(#gp_half[.rob],3,DEXT(#jnt[.rob],3))
	    CALL xmove(.rob,.hnd,.pat,gpos_r_half,.#dst,"ESC_DOWN_HALF")
	    CALL break(.rob)
	END
	CALL gcpos_pos(.rob,gpos_r_prep)
      VALUE gpos_e_near,gpos_r_near:
	IF path_move[.pat,gpos_r_near] THEN
	    POINT .#dst = #PPJSET(#gp_near[.rob],3,DEXT(#jnt[.rob],3))
	    CALL xmove(.rob,.hnd,.pat,gpos_r_near,.#dst,"ESC_DOWN_NEAR")
	    CALL break(.rob)
	END
	CALL gcpos_pos(.rob,gpos_r_half)
      ANY :
.** harish 090226-1 a
	POINT .#dst = #jnt[.rob]
.** harish 090216 a++
	IF DEXT(#jnt[.rob]-#gp_tech[.rob],3)<0.5 THEN
	    CALL gripper_out(.rob,.hnd,grip_rels)
	    TWAIT 1
	    POINT .#dst = #PPJSET(#gp_pfng[.rob],3,DEXT(#jnt[.rob],3))
	    CALL jmove(.rob,.hnd,gpos_r_near,.#dst,"ESC_DOWN_RTCH")
	    POINT .#dst = #gp_poff[.rob] + #PPOINT(0,0,vpp.wzslowh[.prt])
	    CALL jmove(.rob,.hnd,gpos_e_extd,.#dst,"ESC_DOWN_ETCH")
	END
.** harish 090216 a--
.** harish 090420 a++
    CALL break(.rob)
    CALL gripper_out(.rob,.hnd,grip_hold)
.** harish 090420 a--
.** harish 090226-1 m
	POINT .#dst = #PPJSET(.#dst,3,DEXT(#gp_tech[.rob]+#PPOINT(0,0,vpp.wzaccess[.prt]),3))
.*	POINT .#dst = #PPJSET(#jnt[.rob],3,DEXT(#gp_tech[.rob]+#PPOINT(0,0,vpp.wzaccess[.prt]),3))
	CALL jmove(.rob,.hnd,gpos_r_near,.#dst,"ESC_DOWN_UPPE")
	CALL break(.rob)
	CALL gcpos_pos(.rob,gpos_r_near)
    END
    GOTO Loop
.END
.PROGRAM getwaf_down(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_down(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
;
    .pos = .sta
Loop:
    CASE .pos OF
      VALUE gpos_e_home
	IF NOT ((.bas==gbas_getp)AND(.dst==gpos_e_home)) THEN
	    CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_abst,who_get,0)
.** harish 090326 m++
	    IF grip_err[.tsk]
		CALL gcpos_home(.rob,gpos_home)
		RETURN
	    END
.** harish 090326 m--
.*	    IF grip_err[.tsk]	GOTO gerr
	END
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
.*                                                      /* SCRX12-006 2 am++
	CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_HOME",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am--
      VALUE gpos_e_prep
	IF NOT path_move[.pat,gpos_e_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_PREP")
      VALUE gpos_e_half
	IF NOT path_move[.pat,gpos_e_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_HALF")
      VALUE gpos_e_near
	IF NOT path_move[.pat,gpos_e_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_NEAR")
      VALUE gpos_e_extd
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_EXTD")
      VALUE gpos_e_slow
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_SLOW")
      VALUE gpos_e_tech
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wzspdbg[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_goff[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_E_TECH")
      VALUE gpos_r_tech
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wyspeed[.prt])

	POINT .#dst = #gp_tech[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_TECH")
	CALL break(.tsk)
      VALUE gpos_r_slow
.** harish 090226-2 a++
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk] THEN
	    IF vpp.errret[.prt] THEN
		CALL gripper_out(.rob,.hnd,grip_rels)
		TWAIT 1
		CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_escape]),ON)
.** harish 090226-1 m
		POINT .#dst = #gp_poff[.tsk]
.**		POINT .#dst = #gp_pfng[.tsk]
		CALL xmove(.tsk,.hnd,.pat,gpos_r_tech,.#dst,"GET_DOWN_ERR1")
		POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
		CALL xmove(.tsk,.hnd,.pat,gpos_e_extd,.#dst,"GET_DOWN_ERR2")
.** harish 090420 a
		call break(.rob)
		CALL speed_set(.rob)
	    END
	    GOTO gerr
	END
.** harish 090226-2 a--
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wzspdag[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_SLOW")
      VALUE gpos_r_extd
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_EXTD")
;**   VALUE gpos_r_back
;******TYPE "R_BACK: N/A"
      VALUE gpos_r_near
	IF NOT path_move[.pat,gpos_r_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_NEAR")
      VALUE gpos_r_half
	IF NOT path_move[.pat,gpos_r_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_HALF")
      VALUE gpos_r_prep
	IF NOT path_move[.pat,gpos_r_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_PREP")
      VALUE gpos_r_home
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DOWN_R_HOME")
	IF TASKNO<=2 THEN
	    IF (vtp.getretchk[tsk_tsk[.rob]] OR vpp.getretchk[.prt]) THEN
		CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
		CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
		IF grip_err[.tsk]	GOTO gerr
	    END
	END
      ANY :
next:
	.pos = .pos + 1
	GOTO Loop
    END
    IF .pos < .dst THEN
	.pos = .pos + 1
	GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
	CALL escape(.rob)
    END
.END

.PROGRAM putwaf_down(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_down(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
;
    .pos = .sta
Loop:
    CASE .pos OF
      VALUE gpos_e_home
	IF NOT ((.bas==gbas_putp)AND(.dst==gpos_e_home)) THEN
	    CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_pres,who_put,pres_hori)
.** harish 090326 m++
	    IF grip_err[.tsk]
		CALL gcpos_home(.rob,gpos_home)
		RETURN
	    END
.** harish 090326 m--
.*	    IF grip_err[.tsk]	GOTO gerr
	END
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
.*                                                      /* SCRX12-006 2 am++
	CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_HOME",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am+--
      VALUE gpos_e_prep
	IF NOT path_move[.pat,gpos_e_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_PREP")
      VALUE gpos_e_half
	IF NOT path_move[.pat,gpos_e_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_HALF")
      VALUE gpos_e_near
	IF NOT path_move[.pat,gpos_e_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_NEAR")
      VALUE gpos_e_extd
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_putt[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_EXTD")
      VALUE gpos_e_slow
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_down)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_putt[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_SLOW")
      VALUE gpos_e_tech
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_down)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wzspdbp[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_putt[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_E_TECH")
	CALL break(.tsk)
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
      VALUE gpos_r_tech
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wyspeed[.prt])

	POINT .#dst = #gp_pfng[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_TECH")
      VALUE gpos_r_slow
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wzspdap[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_SLOW")
	IF TASKNO<=2 THEN
	    IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
		CALL break(.tsk)
		CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
		IF grip_err[.tsk]	GOTO gerr
	    END
	END
      VALUE gpos_r_extd
	IF TASKNO<=2 THEN
	    IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
		CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
		IF grip_err[.tsk]	GOTO gerr
	    END
	END
	POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_EXTD")
;**   VALUE gpos_r_back
;******TYPE "R_BACK: N/A"
      VALUE gpos_r_near
	IF NOT path_move[.pat,gpos_r_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_NEAR")
      VALUE gpos_r_half
	IF NOT path_move[.pat,gpos_r_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_HALF")
      VALUE gpos_r_prep
	IF NOT path_move[.pat,gpos_r_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_PREP")
      VALUE gpos_r_home
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DOWN_R_HOME")
      ANY :
next:
	.pos = .pos + 1
	GOTO Loop
    END
    IF .pos < .dst THEN
	.pos = .pos + 1
	GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
	CALL escape(.rob)
    END
.END

.PROGRAM p_down(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE (IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt])) OF
      VALUE p_down_wet :
	CALL p_down_wet(.tsk,.rob,.hnd,.prt,.slt,.tch)
      VALUE p_down_tunl :
	CALL p_down_tunl(.tsk,.rob,.hnd,.prt,.slt,.tch)
      ANY :
	CALL p_down_app(.tsk,.rob,.hnd,.prt,.slt,.tch)
    END
.END
.******************************************
.** p_vert.as
.******************************************
.PROGRAM getwaf_vert(.tsk,.rob,.sta,.dst,.bas)
    TYPE "MADA Vert:",$RPGNAME()
    HALT
.END

.PROGRAM putwaf_vert(.tsk,.rob,.sta,.dst,.bas)
    TYPE "MADA Vert:",$RPGNAME()
HALT
.END
.******************************************
.** p_dive.as
.******************************************
.PROGRAM qrypos_dive(.tsk,.rob,.prt,.slt,.pps,.tch)
    .hnd = 1
    POINT #jnt[.tsk] = #none
    CALL p_dive(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CASE .pps OF
      VALUE pps_get,pps_gts,pps_gtr,pps_put,pps_pts,pps_ptr :
	POINT #jnt[.tsk] = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
      VALUE pps_gtx :
	POINT #jnt[.tsk] = #gp_goff[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
      VALUE pps_ptx :
	POINT #jnt[.tsk] = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
      VALUE pps_tch :
	POINT #jnt[.tsk] = #gp_tech[.tsk]
      ANY :
    END
.END
.PROGRAM escape_dive(.rob)
    .hnd = g.hand[.rob]
    .prt = g.port[.rob]
    .slt = g.slot[.rob]
    CALL p_dive(.rob,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
Loop:
.** harish 081031 a+
    CALL gripper_out(.rob,.hnd,grip_hold)
    POINT #jnt[.rob] = #DEST:rob_tsk[.rob]
    CASE g.cpos[.rob] OF
.** harish 090202 m+ (U4 collision)
      VALUE gpos_e_home,gpos_r_home,gpos_home:
	POINT .#dst = #PPJSET(#gp_home[.rob],3,DEXT(#jnt[.rob],3))
	CALL xmove(.rob,.hnd,.pat,gpos_r_home,.#dst,"ESC_DIVE_HOME")
	CALL break(.rob)
	RETURN
      VALUE gpos_e_prep,gpos_r_prep:
	IF path_move[.pat,gpos_r_prep] THEN
	    POINT .#dst = #PPJSET(#gp_prep[.rob],3,DEXT(#jnt[.rob],3))
	    CALL xmove(.rob,.hnd,.pat,gpos_r_prep,.#dst,"ESC_DIVE_PREP")
	    CALL break(.rob)
	END
	CALL gcpos_pos(.rob,gpos_r_home)
      VALUE gpos_e_half,gpos_r_half:
	IF path_move[.pat,gpos_r_half] THEN
	    POINT .#dst = #PPJSET(#gp_half[.rob],3,DEXT(#jnt[.rob],3))
	    CALL xmove(.rob,.hnd,.pat,gpos_r_half,.#dst,"ESC_DIVE_HALF")
	    CALL break(.rob)
	END
	CALL gcpos_pos(.rob,gpos_r_prep)
      VALUE gpos_e_near,gpos_r_near:
	IF path_move[.pat,gpos_r_near] THEN
	    POINT .#dst = #PPJSET(#gp_near[.rob],3,DEXT(#jnt[.rob],3))
	    CALL xmove(.rob,.hnd,.pat,gpos_r_near,.#dst,"ESC_DIVE_NEAR")
	    CALL break(.rob)
	END
	CALL gcpos_pos(.rob,gpos_r_half)
      ANY :
.** harish 090226-1 a
	POINT .#dst = #jnt[.rob]
.** harish 090216 a++
	ZL3TRN rob_tsk[.rob] : .trn[.rob,1] = #jnt[.rob], inv_hand
	ZL3TRN rob_tsk[.rob] : .tch[.rob,1] = #gp_tech[.rob], inv_hand
.** harish 090224 m
	IF ABS(.trn[.rob,1]-.tch[.rob,1])<0.5 THEN
.**	IF (ABS(.trn[.rob,1])-ABS(.tch[.rob,1]))<0.5 THEN
.** harish 090224 a
	  IF ABS(.trn[.rob,3]-.tch[.rob,3])<0.5 THEN
	    CALL gripper_out(.rob,.hnd,grip_rels)
	    TWAIT 1
	    POINT .#dst = #jnt[.rob] + #PPOINT(0,0,-vpp.wforwdz[.prt])
.** harish 090408-1 a
	    CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])
	    CALL jmove(.rob,.hnd,gpos_r_tech,.#dst,"ESC_DIVE_RTCH")

	    ZL3TRN rob_tsk[.rob]: .dst[.rob,1] = 0,vpp.wforwdy[.prt],-vpp.wforwdz[.prt]
            ZL3TRN rob_tsk[.rob]: .dst[.rob,1] = .trn[.rob,1] ADD .dst[.rob,1]
	    ZL3JNT rob_tsk[.rob]: .#dst = .dst[.rob,1],#jnt[.rob], inv_normal, inv_hand
.** harish 090408-1 a
	    CALL speed_rate(.rob,vpp.wyspeed[.prt])
.** harish 090408-1 m
	    CALL jmove(.rob,.hnd,gpos_r_extd,.#dst,"ESC_DIVE_ETCH")
.** harish 090224 a
          END
        END
.** harish 090216 a--
.** harish 090420 a++
    CALL break(.rob)
    CALL gripper_out(.rob,.hnd,grip_hold)
.** harish 090420 a--
.** harish 090226-1 m
	POINT .#dst = #PPJSET(.#dst,3,DEXT(#gp_tech[.rob]+#PPOINT(0,0,vpp.wzaccess[.prt]),3))
.*	POINT .#dst = #PPJSET(#jnt[.rob],3,DEXT(#gp_tech[.rob]+#PPOINT(0,0,vpp.wzaccess[.prt]),3))
	CALL jmove(.rob,.hnd,gpos_r_near,.#dst,"ESC_DIVE_UPPE")
	CALL break(.rob)
	CALL gcpos_pos(.rob,gpos_r_near)
    END
    GOTO Loop
.END
.PROGRAM getwaf_dive(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_dive(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
;
    .pos = .sta
Loop:
    CASE .pos OF
      VALUE gpos_e_home
	IF NOT ((.bas==gbas_getp)AND(.dst==gpos_e_home)) THEN
	    CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_abst,who_get,0)
.** harish 090326 m++
	    IF grip_err[.tsk]
		CALL gcpos_home(.rob,gpos_home)
		RETURN
	    END
.** harish 090326 m--
.*	    IF grip_err[.tsk]	GOTO gerr
	END
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
.*                                                      /* SCRX12-006 2 am++
	CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_HOME",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am--
      VALUE gpos_e_prep
	IF NOT path_move[.pat,gpos_e_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_PREP")
      VALUE gpos_e_half
	IF NOT path_move[.pat,gpos_e_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_HALF")
      VALUE gpos_e_near
	IF NOT path_move[.pat,gpos_e_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_NEAR")
      VALUE gpos_e_extd
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_EXTD")
      VALUE gpos_e_slow
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
	IF grip_err[.tsk]	GOTO gerr
.** harish 090209-2 a
	CALL speed_rate(.rob,vpp.gziospd[.prt])

	POINT .#dst = #gp_goff[.tsk] + #PPOINT(0,0,-vpp.wforwdz[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_SLOW")
      VALUE gpos_e_tech
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wyspeed[.prt])

	POINT .#dst = #gp_tech[.tsk]+ #PPOINT(0,0,-vpp.wforwdz[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_E_TECH")
      VALUE gpos_r_tech
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_get,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_tech[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_TECH")
	CALL break(.tsk)
      VALUE gpos_r_slow
.** harish 090226-1 a++
.** harish 090209-2 a
        TWAIT vpp.wgriptim[.prt]
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk] THEN
	    IF vpp.errret[.prt] THEN
		CALL gripper_out(.rob,.hnd,grip_rels)
		TWAIT 1
		CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_escape]),ON)
		POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,-vpp.wforwdz[.prt])
.** harish 090408-1 a
		CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])
		CALL xmove(.tsk,.hnd,.pat,gpos_r_tech,.#dst,"GET_DIVE_ERR1")

		POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,-vpp.wforwdz[.prt])
.** harish 090408-1 a
		CALL speed_rate(.rob,vpp.wyspeed[.prt])
.** harish 090408-1 m
		CALL xmove(.tsk,.hnd,.pat,gpos_r_extd,.#dst,"GET_DIVE_ERR2")
		CALL speed_set(.rob)
	    END
	    GOTO gerr
	END
.** harish 090226-1 a--
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.gzspd[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_SLOW")
      VALUE gpos_r_extd
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
.** harish 090209-2 a
	CALL speed_rate(.rob,vpp.gziospd[.prt])

	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_EXTD")
;**   VALUE gpos_r_back
;******TYPE "R_BACK: N/A"
      VALUE gpos_r_near
	IF NOT path_move[.pat,gpos_r_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_NEAR")
      VALUE gpos_r_half
	IF NOT path_move[.pat,gpos_r_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_HALF")
      VALUE gpos_r_prep
	IF NOT path_move[.pat,gpos_r_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_PREP")
      VALUE gpos_r_home
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"GET_DIVE_R_HOME")
	IF TASKNO<=2 THEN
	    IF (vtp.getretchk[tsk_tsk[.rob]] OR vpp.getretchk[.prt]) THEN
		CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
		CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
		IF grip_err[.tsk]	GOTO gerr
	    END
	END
      ANY :
next:
	.pos = .pos + 1
	GOTO Loop
    END
    IF .pos < .dst THEN
	.pos = .pos + 1
	GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
	CALL escape(.rob)
    END
.END

.PROGRAM putwaf_dive(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_dive(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
;
    .pos = .sta
Loop:
    CASE .pos OF
      VALUE gpos_e_home
	IF NOT ((.bas==gbas_putp)AND(.dst==gpos_e_home)) THEN
	    CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_pres,who_put,pres_hori)
.** harish 090326 m++
	    IF grip_err[.tsk]
		CALL gcpos_home(.rob,gpos_home)
		RETURN
	    END
.** harish 090326 m--
.*	    IF grip_err[.tsk]	GOTO gerr
	END
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
.*                                                      /* SCRX12-006 2 am++
	CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_HOME",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am--
      VALUE gpos_e_prep
	IF NOT path_move[.pat,gpos_e_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_PREP")
      VALUE gpos_e_half
	IF NOT path_move[.pat,gpos_e_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_HALF")
      VALUE gpos_e_near
	IF NOT path_move[.pat,gpos_e_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_NEAR")
      VALUE gpos_e_extd
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_EXTD")
      VALUE gpos_e_slow
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_dive)
	IF grip_err[.tsk]	GOTO gerr
.** harish 090209-2 a
	CALL speed_rate(.rob,vpp.pziospd[.prt])

	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,vpp.wzslowh[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_SLOW")
      VALUE gpos_e_tech
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_dive)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_tech[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_E_TECH")
	CALL break(.tsk)
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
      VALUE gpos_r_tech

	CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])

	POINT .#dst = #gp_tech[.tsk] + #PPOINT(0,0,-vpp.wforwdz[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_TECH")
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
      VALUE gpos_r_slow
	CALL gripper(.tsk,.hnd,.fng,grip_rels,who_put,0)
	IF grip_err[.tsk]	GOTO gerr

	CALL speed_rate(.rob,vpp.wyspeed[.prt])

	POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,-vpp.wforwdz[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_SLOW")
	IF TASKNO<=2 THEN
	    IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
		CALL break(.tsk)
		CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
		IF grip_err[.tsk]	GOTO gerr
	    END
	END
      VALUE gpos_r_extd
	IF TASKNO<=2 THEN
	    IF (vtp.aftputabs[tsk_tsk[.rob]] OR vpp.aftputabs[.prt]) THEN
		CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
		IF grip_err[.tsk]	GOTO gerr
	    END
	END
.** harish 090209-2 a
	CALL speed_rate(.rob,vpp.pziospd[.prt])

	POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_EXTD")
;**   VALUE gpos_r_back
;******TYPE "R_BACK: N/A"
      VALUE gpos_r_near
	IF NOT path_move[.pat,gpos_r_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_NEAR")
      VALUE gpos_r_half
	IF NOT path_move[.pat,gpos_r_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_HALF")
      VALUE gpos_r_prep
	IF NOT path_move[.pat,gpos_r_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_PREP")
      VALUE gpos_r_home
	CALL gripper(.tsk,.hnd,.fng,grip_abst,who_put,0)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,vpp.wzaccess[.prt])
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"PUT_DIVE_R_HOME")
      ANY :
next:
	.pos = .pos + 1
	GOTO Loop
    END
    IF .pos < .dst THEN
	.pos = .pos + 1
	GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
	CALL escape(.rob)
    END
.END

.PROGRAM p_dive(.tsk,.rob,.hnd,.prt,.slt,.tch)
    CALL p_dive_app(.tsk,.rob,.hnd,.prt,.slt,.tch)
.END
.******************************************
.** home.as
.******************************************
.PROGRAM home_cmd(.tsk)
    IF tsk_num[.tsk]==0 THEN
	tsk_dat[.tsk,d_rob1] = tsk_rob[.tsk]
    ELSEIF tsk_num[.tsk]==1 THEN
	CALL id_unit(.tsk,1,d_rob1)
	tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
    ELSE
	GOTO efm
    END
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = tsk_dat[.tsk,d_rob1]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM home_rob(.rob)
    CALL homing(.rob)
.END
.PROGRAM homing(.rob)
    IF .rob <= 2 THEN
	CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_escape]),ON)
	IF (g.base[.rob]==gbas_nhom) THEN
	    POINT #jnt[.rob] = #DEST:rob_tsk[.rob]
	    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
		.jnt = id_anum[.rob,.cnt]
            IF (.jnt) THEN
                .dat = LIMVAL(DEXT(#jnt[.rob],.jnt),DEXT(#lim_user_l[.rob],.jnt)+0.5,DEXT(#lim_user_u[.rob],.jnt)-0.5)
                POINT #jnt[.rob] = #PPJSET(#jnt[.rob],.jnt,.dat)
            END
	    END
	    CALL jmove(.rob,0,gpos_nchg,#jnt[.rob],"NEED HOME")
	    CALL break(.rob)
	    CALL limit_robot(.rob)
	END
	IF (g.base[.rob]==gbas_none) THEN
        POINT #jnt[.rob] = #DEST:rob_tsk[.rob]
        .hnd = 0
        .prt = 0
        .slt = 0
        .pos = 0
        CALL cur_pos(.rob,.rob,.hnd,.prt,.slt,.pos,#jnt[.rob])
        IF .pos THEN
        CALL gcpos_set(.rob,gbas_escp,.hnd,.prt,.slt,.pos)
        END
.*                                                      /* SCRX12-006-2 a++
        IF .rob == 2 THEN
            IF DEXT(#dest:.rob,3)>=vnp.duckhgh[.rob]-vnp.ducklofst[.rob] AND DEXT(#dest:.rob,3)<=vnp.duckhgh[.rob]+vnp.duckuofst[.rob] THEN
                CALL gcpos_cls(.rob)
            END
            FOR .cnt = 1 TO id_port[tsk_tsk[.rob],0] STEP 1
                .prt = id_port[tsk_tsk[.rob],.cnt]
                IF vc.tchpos[.rob,.prt] == p_hori_ws1 THEN
                    .cnt = 999
                    CALL cur_pos(.rob,.rob,.hnd,.prt,.slt,.pos,#jnt[.rob])
                    IF .pos THEN
                        CALL gcpos_set(.rob,gbas_escp,.hnd,.prt,.slt,.pos)
                    END
                END
            END
        END
.*                                                      /* SCRX12-006-2 a--
	END
	IF (g.base[.rob]==gbas_none)OR(g.base[.rob]==gbas_home)OR(g.port[.rob]==0) THEN
TYPE "MADA 不定な位置からアーム毎のHOMING処理"
.** harish 080822 homing a++
	    IF tsk_arm[rob_tsk[.rob]]  == arm_nt410     THEN
	        CALL nt410_homing(.rob)
		IF tsk_err[.rob]		        RETURN
	    ELSEIF tsk_arm[rob_tsk[.rob]]  == arm_nt570 THEN
		CALL nt570_homing(.rob)
		IF tsk_err[.rob]		        RETURN
	    END
.** harish 080822 homing a--
	ELSE
	    CALL escape(.rob)
	END
	CALL speed_set(.rob)
	CALL break(.rob)
	CALL limit_robot(.rob)
    ELSE
	POINT .#loc = #PPOINT(0)
	CALL jmove(.rob,0,gpos_nchg,.#loc,"HOME")
    END
    CALL gcpos_home(.rob,0)
.END
.PROGRAM escape(.rob)
    $ev1_inf[.rob] = $vrp.rob_asso[.rob]
    $ev2_inf[.rob] = $vrp.rob_asso[.rob]+$ENCODE(/I1,":B",g.hand[.rob])+","+$vpp.portasso[g.port[.rob]]+":"+$TRIM_B($ENCODE(/I,g.slot[.rob]))+","+$tsk_cmd[.rob]
    CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_escape]),ON)
.** harish 081031 m++
.** harish 081021 a++ .** home with plunger closed
.*    .hnd = g.hand[.rob]
.*    CALL gripper_out(.rob,.hnd,grip_hold)
.*    TWAIT 1
.*    CALL gripper_stat(.rob,.hnd,.out,.sta)
.*    IF pz.gstat THEN
.*       TYPE $hst_now[tsk_hst[.tsk]]
.*       TYPE /B4," Output :",.out
.*       TYPE /B4," Status :",.sta
.*    END
.*    CALL gripper(.rob,.hnd,finger_rob[.rob,.hnd],grip_hold,who_ext,0)
.** harish 081021 a--
.** harish 081031 m++
    CASE (port_type[0,g.port[.rob]] BAND port_form) OF
      VALUE p_hori:
	CALL escape_hori(.rob)
      VALUE p_down:
	CALL escape_down(.rob)
      VALUE p_vert:
	CALL escape_vert(.rob)
      VALUE p_dive:
	CALL escape_dive(.rob)
      ANY :
	TYPE "ESCAPE port_type Error:",(port_type[0,g.port[.rob]] BAND port_form )
	HALT
    END
    CALL speed_set(.rob)
.END
.** harish new prepos from Dave 081010 a++
.** harish 080825 homing a++
.PROGRAM nt410_homing(.rob)
.*    .tsk = tsk_tsk[.rob]
    .hnd = 1
.** harish 081021 a++ .** home with plunger closed
    CALL gripper_out(.rob,.hnd,grip_hold)
    TWAIT 1
    CALL gripper_stat(.rob,.hnd,.out,.sta)
    IF pz.gstat THEN
        TYPE $hst_now[tsk_hst[.tsk]]
        TYPE /B4," Output :",.out
        TYPE /B4," Status :",.sta
    END
.*    CALL gripper(.rob,.hnd,finger_rob[.rob,.hnd],grip_hold,who_ext,0)
.** harish 081021 a--
.** harish 081007 m+
    .prt = 0
    POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
    ZL3TRN .a[1] = .#loc[.rob],1
    DECOMPOSE .b[1] = .#loc[.rob]
    .zzz = DEXT(.#loc[.rob],3)
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr1[p_hori_cs1],#ref_mpr3[p_hori_cs1])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_cs1],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS1-1")
	POINT .#dst = #PPJSET(#ref_home[p_hori_cs1],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS1-2")
	RETURN
    END
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr1[p_hori_cs2],#ref_mpr3[p_hori_cs2])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_cs2],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS2-1")
	POINT .#dst = #PPJSET(#ref_home[p_hori_cs2],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS2-2")
	RETURN
    END
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr1[p_hori_cs3],#ref_mpr3[p_hori_cs3])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_cs3],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS3-1")
	POINT .#dst = #PPJSET(#ref_home[p_hori_cs3],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS3-2")
	RETURN
    END
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr2[p_hori_cs4],#ref_mpr3[p_hori_cs4])
    IF is_in_loc[.rob]
        POINT .#dst = #PPJSET(#ref_mpr2[p_hori_cs4],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS4-1")
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_cs4],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS4-2")
	POINT .#dst = #PPJSET(#ref_home[p_hori_cs4],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECS4-3")
	RETURN
    END
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr1[p_hori_pta],#ref_mpr3[p_hori_pta])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_pta],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEPTA-1")
	POINT .#dst = #PPJSET(#ref_home[p_hori_pta],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEPTA-2")
	RETURN
    END
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr1[p_hori_ptb],#ref_mpr3[p_hori_ptb])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_ptb],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEPTB-1")
	POINT .#dst = #PPJSET(#ref_home[p_hori_ptb],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEPTB-2")
	RETURN
    END
.** harish 081110 m++
.*    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr1[p_hori_vdb],#ref_mpr3[p_hori_vdb])
.*    IF is_in_loc[.rob]
.*	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_vdb],3,.zzz)
.*	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDB-1")
.*	POINT .#dst = #PPJSET(#ref_home[p_hori_vdb],3,.zzz)
.*	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDB-2")
.*	RETURN
.*    END
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr2[p_hori_vdb],#ref_mpr3[p_hori_vdb])
    IF is_in_loc[.rob]
        POINT .#dst = #PPJSET(#ref_mpr2[p_hori_vdb],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDB-1")
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_vdb],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDB-2")
	POINT .#dst = #PPJSET(#ref_home[p_hori_vdb],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDB-3")
	RETURN
    END
.** harish 081110 m--
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr2[p_hori_vda],#ref_mpr3[p_hori_vda])
    IF is_in_loc[.rob]
        POINT .#dst = #PPJSET(#ref_mpr2[p_hori_vda],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDA-1")
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_vda],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDA-2")
	POINT .#dst = #PPJSET(#ref_home[p_hori_vda],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEVDA-3")
	RETURN
    END
.*                                                      /* SCRX12-006-2 a++
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr2[p_hori_ws1],#ref_mpr3[p_hori_ws1])
    IF is_in_loc[.rob]
        POINT .#dst = #PPJSET(#ref_mpr2[p_hori_ws1],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEws1-1")
	POINT .#dst = #PPJSET(#ref_mpr1[p_hori_ws1],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEws1-2")
	POINT .#dst = #PPJSET(#ref_home[p_hori_ws1],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEws1-3")
	RETURN
    END
.*                                                      /* SCRX12-006-2 a--
.** SCRY10-003-2 a++
    FOR .type = p_hori_cs1 TO p_hori_vda STEP 1
        CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[.type],#ref_home[.type])
        IF is_in_loc[.rob]
            POINT .#dst = #PPJSET(#ref_home[.type],3,.zzz)
	    CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEX-1")
	    RETURN
        END
	END
.*                                                      /* SCRX12-006-2 a++
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_hori_ws1],#ref_home[p_hori_ws1])
    IF is_in_loc[.rob]
        POINT .#dst = #PPJSET(#ref_home[p_hori_ws1],3,.zzz)
    CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEX-1")
    RETURN
    END
    
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_map1[p_hori_ws1],#ref_map1[p_hori_ws1])
    IF is_in_loc[.rob]
        POINT .#dst = #PPJSET(#ref_half[p_hori_ws1],3,.zzz)
    CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEWS1-1")
    POINT .#dst = #PPJSET(#ref_home[p_hori_ws1],3,.zzz)
    CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEWS1-2")
    RETURN
    END
.*                                                      /* SCRX12-006-2 a--
.** SCRY10-003-2 a--
.** harish 090403-1 a++
    IF .b[4]<-is_in_jnt[4]			GOTO nxt
    IF .b[4]> is_in_jnt[4]			GOTO nxt
.** harish 090602-1 m++ temp
;   IF .b[6]<-is_in_jnt[6]			GOTO nxt
;   IF .b[6]> is_in_jnt[6]			GOTO nxt
.** harish 090602-1 m-- temp
    POINT .#dst = #PPJSET(#homec[.rob],3,.zzz)
    CALL jmove(.rob,0,gpos_nchg,.#dst,"HOME-1")
    RETURN
nxt:
.** SCRY10-003-2 a  .** Homing logic need to be developed **.
    GOTO cannot
.** harish 090403-1 a--
    IF .a[x_ele_x]<can_homec[.rob,x_ele_x,0]	GOTO next
    IF .a[x_ele_x]>can_homec[.rob,x_ele_x,1]	GOTO next
    IF .a[x_ele_y]<can_homec[.rob,x_ele_y,0]	GOTO next
    IF .a[x_ele_y]>can_homec[.rob,x_ele_y,1]	GOTO next
.** harish 090211 a++
    IF .b[6]<can_homej6[.rob,6,0]		GOTO next
    IF .b[6]>can_homej6[.rob,6,1]		GOTO next
.** harish 090211 a--
    POINT .#dst = #PPJSET(#homec[.rob],3,.zzz)
    CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMECC-1")
    RETURN
next:
    IF .a[x_ele_x]<can_homerc[.rob,x_ele_x,0]	GOTO next1
    IF .a[x_ele_x]>can_homerc[.rob,x_ele_x,1]	GOTO next1
    IF .a[x_ele_y]<can_homerc[.rob,x_ele_y,0]	GOTO next1
    IF .a[x_ele_y]>can_homerc[.rob,x_ele_y,1]	GOTO next1
.** harish 090127 a++
.** harish 090211 a++
    IF .b[6]<can_homej6[.rob,6,0]		GOTO next1
    IF .b[6]>can_homej6[.rob,6,1]		GOTO next1
.** harish 090211 a--
    IF .b[2]>can_homej2[.rob,2,0] AND .b[2]<can_homej2[.rob,2,1] THEN
	IF .b[4]>can_homej4[.rob,4,0] AND .b[4]<can_homej4[.rob,4,1] THEN
	    POINT .#dst = #PPJSET(#homec[.rob],3,.zzz)
            CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEC-1")
            RETURN
	END
    END
.** harish 090127 a--
.** harish 090602-1 m++ temp
;   POINT .#dst = #PPJSET(#homer[.rob],3,.zzz)
;   CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEC-1")
;   RETURN
.** harish 090602-1 m-- temp
next1:
    IF .a[x_ele_x]<can_homel[.rob,x_ele_x,0]	GOTO next2
    IF .a[x_ele_x]>can_homel[.rob,x_ele_x,1]	GOTO next2
    IF .a[x_ele_y]<can_homel[.rob,x_ele_y,0]	GOTO next2
    IF .a[x_ele_y]>can_homel[.rob,x_ele_y,1]	GOTO next2
.** harish 090403-1 a++
    IF .b[2]<can_homej2l[.rob,2,0]		GOTO next2
    IF .b[2]>can_homej2l[.rob,2,1]		GOTO next2
    IF .b[4]<can_homej4l[.rob,4,0]		GOTO next2
    IF .b[4]>can_homej4l[.rob,4,1]		GOTO next2
.** harish 090403-1 a--
    IF .b[6]>can_homea1[.rob,6,0] AND .b[6]<can_homea1[.rob,6,1]
	POINT .#dst = #PPJSET(#preplb[.rob],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMELB-1")
	POINT .#dst = #PPJSET(#homel[.rob],3,.zzz)
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEL-2")
        RETURN
    ELSEIF .b[6]>can_homea2[.rob,6,0] AND .b[6]<can_homea2[.rob,6,1]
        POINT .#dst = #PPJSET(#preplt[.rob],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMELT-1")
	POINT .#dst = #PPJSET(#homel[.rob],3,.zzz)
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEL-2")
        RETURN
    END
next2:
    IF .a[x_ele_x]<can_homer[.rob,x_ele_x,0]	GOTO next3
    IF .a[x_ele_x]>can_homer[.rob,x_ele_x,1]	GOTO next3
    IF .a[x_ele_y]<can_homer[.rob,x_ele_y,0]	GOTO next3
    IF .a[x_ele_y]>can_homer[.rob,x_ele_y,1]	GOTO next3
.** harish 090403-1 a++
    IF .b[2]<can_homej2r[.rob,2,0]		GOTO next3
    IF .b[2]>can_homej2r[.rob,2,1]		GOTO next3
    IF .b[4]<can_homej4r[.rob,4,0]		GOTO next3
    IF .b[4]>can_homej4r[.rob,4,1]		GOTO next3
.** harish 090403-1 a--
    IF .b[6]>can_homea1[.rob,6,0] AND .b[6]<can_homea1[.rob,6,1]
	POINT .#dst = #PPJSET(#preprt[.rob],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMERT-1")
	POINT .#dst = #PPJSET(#homer[.rob],3,.zzz)
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMER-2")
        RETURN
    ELSEIF .b[6]>can_homea2[.rob,6,0] AND .b[6]<can_homea2[.rob,6,1]
        POINT .#dst = #PPJSET(#preprb[.rob],3,.zzz)
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMERB-1")
	POINT .#dst = #PPJSET(#homer[.rob],3,.zzz)
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMER-2")
        RETURN
    END
next3:
.**harish homing at pta/ptb 080915 a++
.** harish 081007 m+
.*  CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_hori_pta],#ref_mpr3[p_hori_pta])
.*  IF is_in_loc[.rob]
.*	POINT .#dst = #PPJSET(#homec[.rob],3,.zzz)
.*	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEC-1")
.*	RETURN
.*  END
.** harish 081007 m+
.*  CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_mpr2[p_hori_ptb],#ref_mpr2[p_hori_ptb])
.*  IF is_in_loc[.rob]
.*	POINT .#dst = #PPJSET(#preplt[.rob],3,DEXT(.#loc[.rob],3))
.*	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMELT-1")
.*	POINT .#dst = #PPJSET(#homel[.rob],3,DEXT(.#loc[.rob],3))
.*      CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEL-2")
.*      RETURN
.*  END
.**harish homing at pta/ptb 080915 a--
cannot:
.*                                                      /* SCRX11-032-3 a++ */
    IF SYSDATA(ZSIMENV) THEN
        Type "Simulation mode: Robot can not find a safe homing path."
        PROMPT "Press Enter to continue homing or Q-Enter to quit.",.$key
        .$key = $TOUPPER(.$key)
        IF .$key <> "Q" THEN
            CALL jmove(.rob,0,gpos_nchg,#homer[.rob],"HOME_SIM")
        END
    ELSE
        CALL alarm_str(.rob,.rob,0,$ae_home_hand)
        TYPE "ERROR - CANNOT HOME AUTOMATICALLY."
    END
.*                                                      /* SCRX11-032-3 a-- */
.END
.** harish homing NT570 081016 m++
.PROGRAM nt570_homing(.rob)
.*    .tsk = tsk_tsk[.rob]
    .hnd = 1
.** harish 081021 a++ .** home with plunger closed
    CALL gripper_out(.rob,.hnd,grip_hold)
    TWAIT 1
    CALL gripper_stat(.rob,.hnd,.out,.sta)
    IF pz.gstat THEN
        TYPE $hst_now[tsk_hst[.tsk]]
        TYPE /B4," Output :",.out
        TYPE /B4," Status :",.sta
    END
.*    CALL gripper(.rob,.hnd,finger_rob[.rob,.hnd],grip_hold,who_ext,0)
.** harish 081021 a--
.** harish 081007 m+
    .prt = 0
    POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
    ZL3TRN .a[1] = .#loc[.rob],1
.** harish 090209-1 a
    DECOMPOSE .b[1] = .#loc[.rob]
.** harish 081007 m+
.** harish new prepos from Dave 081010 m+
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_hori_pass],#ref_mpr3[p_hori_pass])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_home[p_hori_pass],3,DEXT(.#loc[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEPT-1")
	RETURN
    END
.** harish 081007 m+
.** harish new prepos from Dave 081010 m+
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_down_hclu],#ref_mpr3[p_down_hclu])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_home[p_down_hclu],3,DEXT(.#loc[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEHC-1")
        RETURN
    END
.** harish 081007 m+
.** harish new prepos from Dave 081010 m+
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_dive_rins],#ref_mpr3[p_dive_rins])
    IF is_in_loc[.rob]
.** harish 090212 a++
        IF vpp.portz[203]>DEXT(#homes[.rob],3)
	    POINT .#dst = #PPJSET(.#loc[.rob],3,vpp.portz[203])
	ELSE
            POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#homes[.rob],3))
        END
.** harish 090212 a--
.**     POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#homes[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMESH-1")
	POINT .#dst = #PPJSET(#ref_home[p_dive_rins],3,DEXT(#homes[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMESH-2")
        RETURN
    END
.** harish 081007 m+
.** harish new prepos from Dave 081010 m+
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_dive_buff],#ref_mpr3[p_dive_buff])
    IF is_in_loc[.rob]
.** harish 090212 a++
        IF vpp.portz[204]>DEXT(#homes[.rob],3)
	    POINT .#dst = #PPJSET(.#loc[.rob],3,vpp.portz[204])
	ELSE
            POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#homes[.rob],3))
        END
.** harish 090212 a--
.**	POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#homes[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEBF-1")
	POINT .#dst = #PPJSET(#ref_home[p_dive_buff],3,DEXT(#homes[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEBF-2")
        RETURN
    END
.*                                                      /* SCRX11-032-3 a++ */
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_home[p_down_rins],#ref_mpr3[p_down_rins])
    IF is_in_loc[.rob]
	POINT .#dst = #PPJSET(#ref_home[p_down_rins],3,DEXT(.#loc[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMERIN-1")
	RETURN
    END
.*                                                      /* SCRX11-032-3 a-- */
.** harish 081210 a++
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_ship3[.rob],#ref_ship3[.rob])
    IF is_in_loc[.rob]
        CALL jmove(.rob,0,gpos_nchg,#ref_ship3[.rob],"HOMESHP1")
.** harish 090522 m
	POINT .#dst = #PPJSET(#ref_ship3[.rob],7,DEXT(#ref_ship2[.rob],7))
.*	POINT .#dst = #PPJSET(.#loc[.rob],7,DEXT(#ref_ship2[.rob],7))
.** harish 090415 a
        POINT .#dst = #PPJSET(.#dst,3,DEXT(#ref_ship2[.rob],3))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMESHP2")
	CALL jmove(.rob,0,gpos_nchg,#ref_ship2[.rob],"HOMESHP3")
        CALL jmove(.rob,0,gpos_nchg,#ref_ship1[.rob],"HOMESHP4")
        RETURN
    END
.** harish 081210 a--
.** harish 090304-1 a++
    CALL is_in_loc(.rob,.rob,.hnd,.prt,.#loc[.rob],#ref_algn3[.rob],#ref_algn3[.rob])
    IF is_in_loc[.rob]
        CALL jmove(.rob,0,gpos_nchg,#ref_algn3[.rob],"HOMEALN1")
.** harish 090522 m
	POINT .#dst = #PPJSET(#ref_algn3[.rob],7,DEXT(#ref_algn2[.rob],7))
.*	POINT .#dst = #PPJSET(.#loc[.rob],7,DEXT(#ref_algn2[.rob],7))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEALN2")
        POINT .#dst = #PPJSET(.#dst,6,DEXT(#ref_algn2[.rob],6))
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEALN3")
	CALL jmove(.rob,0,gpos_nchg,#ref_algn2[.rob],"HOMEALN4")
        CALL jmove(.rob,0,gpos_nchg,#ref_algn1[.rob],"HOMEALN5")
        RETURN
    END
.** harish 090304-1 a--
    IF .a[x_ele_x]<can_homeb[.rob,x_ele_x,0]	GOTO next
    IF .a[x_ele_x]>can_homeb[.rob,x_ele_x,1]	GOTO next
    IF .a[x_ele_y]<can_homeb[.rob,x_ele_y,0]	GOTO next
    IF .a[x_ele_y]>can_homeb[.rob,x_ele_y,1]	GOTO next
    IF ABS(.a[x_ele_r])<can_homer2[.rob,x_ele_r,0]	GOTO next
    IF ABS(.a[x_ele_r])>can_homer2[.rob,x_ele_r,1]	GOTO next
.** harish 090209-1 a++
    IF .b[2]<can_homej2[.rob,2,0]		GOTO next
    IF .b[2]>can_homej2[.rob,2,1]		GOTO next
.** harish 090209-1 a--
    IF .a[x_ele_s]<can_homes1[.rob,x_ele_s,1] AND .a[x_ele_s]>can_homes1[.rob,x_ele_s,0]
        POINT .#dst = #PPJSET(#homeb[.rob],3,DEXT(.#loc[.rob],3))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEB-1")
        RETURN
    ELSE
        POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#homes[.rob],3))
	POINT .#dst = #PPJSET(.#dst,7,DEXT(#homes[.rob],7))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEB-1")
	POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
        ZL3TRN .a[1] = .#loc[.rob],1
	POINT .#dst = #homes[.rob]
	IF .a[x_ele_r]<0
	    POINT .#dst = #PPJSET(.#dst,6,DEXT(#homes[.rob],6)+180)
	END
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEB-2")
        RETURN
    END
next:
    IF .a[x_ele_x]<can_homef[.rob,x_ele_x,0]	GOTO next1
    IF .a[x_ele_x]>can_homef[.rob,x_ele_x,1]	GOTO next1
    IF .a[x_ele_y]<can_homef[.rob,x_ele_y,0]	GOTO next1
    IF .a[x_ele_y]>can_homef[.rob,x_ele_y,1]	GOTO next1
    IF ABS(.a[x_ele_r])<can_homer2[.rob,x_ele_r,0]	GOTO next1
    IF ABS(.a[x_ele_r])>can_homer2[.rob,x_ele_r,1]	GOTO next1
.** harish 090209-1 a++
    IF .b[2]<can_homej2[.rob,2,0]		GOTO next1
    IF .b[2]>can_homej2[.rob,2,1]		GOTO next1
.** harish 090209-1 a--
    IF (.a[x_ele_s]<can_homes2[.rob,x_ele_s,1] AND .a[x_ele_s]>can_homes2[.rob,x_ele_s,0])
        POINT .#dst = #PPJSET(#homef[.rob],3,DEXT(.#loc[.rob],3))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEF-1")
        RETURN
    ELSE
        POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#homes[.rob],3))
	POINT .#dst = #PPJSET(.#dst,7,DEXT(#homes[.rob],7))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEF-1")
	POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
        ZL3TRN .a[1] = .#loc[.rob],1
	POINT .#dst = #homes[.rob]
	IF .a[x_ele_r]<0
	    POINT .#dst = #PPJSET(.#dst,6,DEXT(#homes[.rob],6)+180)
	END
	CALL jmove(.rob,0,gpos_nchg,.#dst,"HOMEF-2")
        RETURN
    END
next1:
.*                                                      /* SCRX11-032-3 a++ */
    IF SYSDATA(ZSIMENV) THEN
        Type "Simulation mode: Robot can not find a safe homing path."
        PROMPT "Press Enter to continue homing or Q-Enter to quit.",.$key
        .$key = $TOUPPER(.$key)
        IF .$key <> "Q" THEN
            CALL jmove(.rob,0,gpos_nchg,#homes[.rob],"HOME_SIM")
        END
    ELSE
        CALL alarm_str(.rob,.rob,0,$ae_home_hand)
        TYPE "ERROR - CANNOT HOME AUTOMATICALLY."
    END
.*                                                      /* SCRX11-032-3 a-- */
.END
.** harish homing NT570 081016 m--
.** harish 080825 homing a--
.** harish new prepos from Dave 081010 a--
.******************************************
.** m_relabs.as
.******************************************
.PROGRAM m_relabs_cmd(.tsk,.abs)
.** d_rob1
.** d_jnt1
.** d_dat1
.**
    IF tsk_num[.tsk]<>2		GOTO efm
    CALL id_unit_axis(.tsk,1,d_rob1,d_jnt1,.abs)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_jnt1])	GOTO epm
    CALL id_int(.tsk,2,d_dat1,100)
    IF id_error[.tsk]						GOTO epm
TYPE /S," ROB:",tsk_dat[.tsk,d_rob1]
TYPE /S," JNT:",tsk_dat[.tsk,d_jnt1]
TYPE /S," DAT:",tsk_dat[.tsk,d_dat1]
.** harish 090527 a++
    IF vc.mode[.tsk] == mode_soft
	IF .abs>0 OR ABS(tsk_dat[.tsk,d_dat1])>5
	    CALL send_ack(.tsk)
	    $alm_put[.tsk] = $AE_MD_THIS_SOFT
	    CALL alarm_put(.tsk,0,0)
	    CALL make_res_ng(.tsk)
	    RETURN
	END
    END
.** harish 090527 a--
    tsk_mod[.tsk] = m_init
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.PROGRAM m_relabs_rob(.rob,.abs)
    IF tsk_dat[.rob,d_jnt1] < 10 THEN
	CALL m_relabs_jnt(.rob,.abs)
    ELSEIF tsk_dat[.rob,d_jnt1] < 30 THEN
	CALL m_relabs_lin(.rob,.abs)
    END
    IF (g.base[.rob]<>gbas_nhom) THEN
	CALL gcpos_cls(.rob)
    END
    CALL break(.rob)
.END


.PROGRAM m_relabs_jnt(.rob,.abs)
    POINT .#dst = #DEST:rob_tsk[.rob]
    IF .abs > 0 THEN
	.dst = tsk_dat[.rob,d_dat1]
	.rel = .dst - DEXT(.#dst,tsk_dat[.rob,d_jnt1])
    ELSE
	.rel = tsk_dat[.rob,d_dat1]
	.dst = .rel + DEXT(.#dst,tsk_dat[.rob,d_jnt1])
    END
    POINT .#dst = #PPJSET(.#dst,tsk_dat[.rob,d_jnt1],.dst)
;** Limit Check
    .ulm = DEXT(#lim_user_u[.rob],tsk_dat[.rob,d_jnt1])
    .llm = DEXT(#lim_user_l[.rob],tsk_dat[.rob,d_jnt1])
    IF ((.dst<.llm)AND(.rel<0))OR((.ulm<.dst)AND(0<.rel)) THEN
	CALL alarm_str(.rob,.rob,0,$ae_errlim_dst)
	RETURN
    END
;** MovLim Check
    IF NOT vtp.movlim[tsk_tsk[.rob]]				GOTO movlim_ok
    IF (g.base[.rob]==gbas_home) THEN
	IF (tsk_dat[.rob,d_jnt1]==2)				GOTO movlim_ok
	IF (tsk_dat[.rob,d_jnt1]==3)				GOTO movlim_ok
    END
    IF DEXT(#movlim[.rob],tsk_dat[.rob,d_jnt1])<ABS(.rel) THEN
	CALL alarm_str(.rob,.rob,0,$IFELSE(.abs,$ae_movlim_abs,$ae_movlim_rel))
	RETURN
    END
movlim_ok:
;** Track Check
mov:
    CALL jmove(.rob,0,gpos_nchg,.#dst,"")
.END

.** harish 090601 a++
.REALS
flip.dir[1] =  1
flip.dir[2] = -1
flip.dir[3] = -1
flip.dir[4] =  1
flip.dir[5] = -1
flip.dir[6] =  1

flip.x[1] = -1
flip.x[2] =  1
flip.x[3] = -1
flip.x[4] =  1
flip.x[5] =  1
flip.x[6] = -1
.END
.PROGRAM calc_dst(.rob,.hnd,.#dst,.#loc,.dst[],.loc[],.fng)
    POINT .#dst = #DEST:rob_tsk[.rob]
    POINT .#loc = #DEST:rob_tsk[.rob]
    ZL3TRN rob_tsk[.rob]: .loc[1] = .#loc,.hnd
    IF .loc[5]<=45
        IF .fng == 1
          .shift =  310*flip.dir[5]
	  .dat   =  tsk_dat[.rob,d_dat1]*flip.x[5]
	ELSEIF .fng == 2
          .shift =  310*flip.dir[6]
	  .dat   =  tsk_dat[.rob,d_dat1]*flip.x[6]
	END
    ELSEIF .loc[5]>45 AND .loc[5]<135
        IF .fng == 1
          .shift =  310*flip.dir[3]
	  .dat   =  tsk_dat[.rob,d_dat1]*flip.x[3]
	ELSEIF .fng == 2
          .shift =  310*flip.dir[4]
	  .dat   =  tsk_dat[.rob,d_dat1]*flip.x[4]
	END
    ELSEIF .loc[5]>=135
        IF .fng == 1
          .shift =  310*flip.dir[1]
	  .dat   =  tsk_dat[.rob,d_dat1]*flip.x[1]
	ELSEIF .fng == 2
          .shift =  310*flip.dir[2]
	  .dat   =  tsk_dat[.rob,d_dat1]*flip.x[2]
	END
    END
    ZL3TRN rob_tsk[.rob]: .off[1] = .shift,0,0,0,0,0
    ZL3TRN rob_tsk[.rob]: .pos[1] = .loc[1] ADD .off[1]
    ZL3TRN rob_tsk[.rob]: .rot[1] = 0,0,0,.dat,0,0
    ZL3TRN rob_tsk[.rob]: .loc1[1] = .pos[1] ADD .rot[1]
    ZL3TRN rob_tsk[.rob]: .dst[1] = .loc1[1] SUB .off[1]
.END
.** harish 090601 a--

.PROGRAM m_relabs_lin(.rob,.abs)
    ZARRAYSET .tol[1] = 0,6
    ZARRAYSET .bas[1] = 0,6
    CASE tsk_dat[.rob,d_jnt1] OF
      VALUE axs_base_x1
	.hnd = 1
	.bas[1] = tsk_dat[.rob,d_dat1]
	.buf = 1
      VALUE axs_base_x2
	.hnd = 2
	.bas[1] = tsk_dat[.rob,d_dat1]
	.buf = 1
      VALUE axs_base_y1
	.hnd = 1
	.bas[2] = tsk_dat[.rob,d_dat1]
	.buf = 2
      VALUE axs_base_y2
	.hnd = 2
	.bas[2] = tsk_dat[.rob,d_dat1]
	.buf = 2
      VALUE axs_base_r1
	.hnd = 1
	.tol[4] = tsk_dat[.rob,d_dat1]	;** TOOLで回さないとXYが変わる
	.buf = 4
      VALUE axs_base_r2
	.hnd = 2
	.tol[4] = tsk_dat[.rob,d_dat1]	;** TOOLで回さないとXYが変わる
	.buf = 4
      VALUE axs_tool_x1
	.hnd = 1
	.tol[1] = tsk_dat[.rob,d_dat1]
	.buf = 1
      VALUE axs_tool_x2
	.hnd = 2
	.tol[1] = tsk_dat[.rob,d_dat1]
	.buf = 1
      VALUE axs_tool_y1
	.hnd = 1
	.tol[2] = tsk_dat[.rob,d_dat1]
	.buf = 2
      VALUE axs_tool_y2
	.hnd = 2
	.tol[2] = tsk_dat[.rob,d_dat1]
	.buf = 2
.** harish 090601 a++
      VALUE axs_tool_f1r1
        .hnd = 1
        CALL calc_dst(.rob,.hnd,.#dst,.#loc,.dst[],.loc[],1)
	GOTO mov
      VALUE axs_tool_f2r1
	.hnd = 1
        CALL calc_dst(.rob,.hnd,.#dst,.#loc,.dst[],.loc[],2)
	GOTO mov
      VALUE axs_tool_f1r2
	.hnd = 2
        CALL calc_dst(.rob,.hnd,.#dst,.#loc,.dst[],.loc[],1)
	GOTO mov
      VALUE axs_tool_f2r2
	.hnd = 2
        CALL calc_dst(.rob,.hnd,.#dst,.#loc,.dst[],.loc[],2)
	GOTO mov
.** harish 090601 a--
    END
    POINT .#dst = #DEST:rob_tsk[.rob]
    ZL3TRN rob_tsk[.rob]: .loc[1] = .#dst,.hnd
    ZL3TRN rob_tsk[.rob]: .dst[1] = .#dst,.hnd
    IF .abs > 0 THEN
	.dst[.buf] = tsk_dat[.rob,d_dat1]
    ELSE
	ZL3TRN rob_tsk[.rob]: .dst[1] = .bas[1] ADD .dst[1]
	ZL3TRN rob_tsk[.rob]: .dst[1] = .dst[1] ADD .tol[1]
    END
.** harish 090601 a
mov:
;** MovLim Check
    IF vtp.movlim[tsk_tsk[.rob]] THEN
	IF ABS(.dst[.buf]-.loc[.buf])<10 THEN
	    $alm_tmp[.rob] = $IFELSE(.abs,$ae_movlim_abs,$ae_movlim_rel)
	    GOTO alm
	END
    END
;** Get Destination Location Data
    ZL3JNT/ERR tsk_err[.rob] rob_tsk[.rob]: .#dst = .dst[1],.#dst,inv_normal,inv_hand
    IF tsk_err[.rob] THEN
	$alm_tmp[.rob] = $ae_relabs_lin
	GOTO alm
    END
;** Limit Check
    FOR .buf = 1 TO id_anum[.rob,0] STEP 1
	.jnt = id_anum[.rob,.buf]
	IF (DEXT(.#dst-#lim_user_l[.rob],.jnt)<0)OR(0<DEXT(.#dst-#lim_user_u[.rob],.jnt)) THEN
	    $alm_tmp[.rob] = $ae_relabs_lin
	    GOTO alm
	END
    END
.** harish 090701-2 a++
.** Distance Check **
    ZL3TRN rob_tsk[.rob]: .dst2[1] = .#dst,inv_hand
    .dist_trn = sqrt((.dst2[1]-.loc[1])^2 + (.dst2[2]-.loc[2])^2)
    .dist_org = sqrt((.dst[1] -.loc[1])^2 + (.dst[2] -.loc[2])^2)

    TYPE "Hand:",inv_hand,", Org:",.dist_org,", Trn:",.dist_trn
    TYPE /F9.3," Cur :x=",.loc[1],", y=",.loc[2]
    TYPE /F9.3," Dst :x=",.dst[1],", y=",.dst[2]
    TYPE /F9.3," Dst2:x=",.dst2[1],", y=",.dst2[2]

    IF .dist_trn > (.dist_org + chk_lmv_dst) THEN
	$alm_tmp[.rob] = $ae_relabs_lin
	GOTO alm
    END

.** singular point check **
    POINT .#loc = #DEST:rob_tsk[.rob]
    ZL3TRN .sgl[1] = .#loc,3
    TYPE /F9.3," Sgl :x=",.sgl[1],", y=",.sgl[2]
    IF chk_lmv_sgl  > sqrt(.sgl[1]^2 + .sgl[2]^2) THEN
	$alm_tmp[.rob] = $ae_relabs_lin
	GOTO alm
    END

.** JT2 check **
    .r_mov = ABS(DEXT(.#loc,2)-DEXT(.#dst,2))
    TYPE /F9.3," dst=",.dist_org,", ang=",.r_mov
    IF (.dist_org < chk_lmv_rng) AND (.r_mov > chk_lmv_ang) THEN
	$alm_tmp[.rob] = $ae_relabs_lin
	GOTO alm
    END
.** harish 090701-2 a--
    CALL lmove(.rob,.hnd,gpos_nchg,.#dst,"")
    RETURN
alm:
    CALL alarm_str(.rob,.rob,0,$alm_tmp[.rob])
.END
.** harish 090701-2 a++
.REALS
    chk_lmv_dst = 1
    chk_lmv_sgl = 10
    chk_lmv_rng = 10
    chk_lmv_ang = 30
.END
.** harish 090701-2 a--
.******************************************
.** getput.as
.******************************************
.PROGRAM getputx_cmd(.tsk)
    CALL getput_cmd(.tsk)
.END
.PROGRAM getputx_rob(.rob,.bas)
    CALL getput_rob(.rob,.bas)
.END
.PROGRAM getput_cmd(.tsk)
    IF (tsk_num[.tsk]<>2)AND(tsk_num[.tsk]<>5)                                          GOTO efm
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])     GOTO epm
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt1] == BL_RINSE                                                 GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])                              GOTO epm
    IF tsk_num[.tsk]==2 THEN
        tsk_dat[.tsk,d_zps1] = 0
        tsk_dat[.tsk,d_llf1] = 0
        tsk_dat[.tsk,d_ulf1] = 0
    ELSE
        CALL id_int(.tsk,3,d_zps1,100)
        IF id_error[.tsk]                GOTO epm
        CALL id_int(.tsk,4,d_llf1,100)
        IF id_error[.tsk]                GOTO epm
        CALL id_int(.tsk,5,d_ulf1,100)
        IF id_error[.tsk]                GOTO epm
    END
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
.***TYPE /S," ROB:",tsk_dat[.tsk,d_rob1]
.***TYPE /S," HND:",tsk_dat[.tsk,d_hnd1]
.***TYPE /S," FNG:",tsk_dat[.tsk,d_fng1]
.***TYPE /S," PRT:",tsk_dat[.tsk,d_prt1]
.***TYPE /S," SLT:",tsk_dat[.tsk,d_slt1]
.***TYPE /S," ZPS:",tsk_dat[.tsk,d_zps1]
.***TYPE /S," LLF:",tsk_dat[.tsk,d_llf1]
.***TYPE /S," ULF:",tsk_dat[.tsk,d_ulf1]
.***TYPE    ""
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END

.PROGRAM getput_rob(.rob,.bas)
.**TYPE /S," CMD:",$tsk_cmd[.rob]
.**TYPE /S," ROB:",tsk_dat[.rob,d_rob1]
.**TYPE /S," HND:",tsk_dat[.rob,d_hnd1]
.**TYPE /S," FNG:",tsk_dat[.rob,d_fng1]
.**TYPE /S," PRT:",tsk_dat[.rob,d_prt1]
.**TYPE /S," SLT:",tsk_dat[.rob,d_slt1]
.**TYPE /S," ZPS:",tsk_dat[.rob,d_zps1]
.**TYPE /S," LLF:",tsk_dat[.rob,d_llf1]
.**TYPE /S," ULF:",tsk_dat[.rob,d_ulf1]
.**TYPE    ""
    .hnd = tsk_dat[.rob,d_hnd1]
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
    IF (g.base[.rob]<>.bas)OR(g.hand[.rob]<>.hnd)OR(g.port[.rob]<>.prt)OR(g.slot[.rob]<>.slt)        GOTO escp
    IF (gpos_e_extd < g.cpos[.rob])        GOTO escp
    .sta = g.cpos[.rob]
    GOTO move
escp:
    IF g.base[.rob]<>gbas_home THEN
        CALL homing(.rob)
        IF tsk_err[.rob]        RETURN
    END
    .sta = gpos_e_home
Move:
    CALL getput_chk(.rob,.hnd,.prt,.slt)
    IF tsk_err[.rob]                RETURN
    CALL gcpos_set(.rob,.bas,.hnd,.prt,.slt,.sta)
    CALL getput_sel(.rob,.rob,.hnd,.prt,.slt,.sta,gpos_r_home,.bas)
.END
.PROGRAM getput_sel(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    $ev1_inf[.tsk] = $vrp.rob_asso[.rob]
    $ev2_inf[.tsk] = $vrp.rob_asso[.rob]+$ENCODE(/I1,":B",.hnd)+","+$vpp.portasso[.prt]+":"+$TRIM_B($ENCODE(/I,.slt))+","+$tsk_cmd[.tsk]
    CASE .bas OF
      VALUE gbas_getp, gbas_tget :
        CASE (port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form ) OF
          VALUE p_hori:
.** SCRX10-027-1 a++
            IF (port_type[0,.prt] BAND port_util)AND(tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
                IF (vpp.isimap[.prt]) THEN
                    CALL imap_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
                    GOTO exit
                END
            END
.** SCRX10-027-1 a--
            CALL getwaf_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          VALUE p_down:
            CALL getwaf_down(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          VALUE p_vert:
            CALL getwaf_vert(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          VALUE p_dive
            CALL getwaf_dive(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          ANY :
            TYPE "GETPUT_SEL port_type Error:",(port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form)
            HALT
        END
      VALUE gbas_putp, gbas_tput :
        CASE (port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form) OF
          VALUE p_hori:
.** SCRX10-027-1 a++
            IF (port_type[0,.prt] BAND port_util)AND(tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
                IF (vpp.isimap[.prt]) THEN
                    CALL imap_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
                    GOTO exit
                END
            END
.** SCRX10-027-1 a--
            CALL putwaf_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          VALUE p_down:
            CALL putwaf_down(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          VALUE p_vert:
            CALL putwaf_vert(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          VALUE p_dive
            CALL putwaf_dive(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
          ANY :
            TYPE "PUTPUT_SEL port_type Error:",(port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form)
            HALT
        END
      ANY :
        TYPE "GETPUT_SEL .bas Error:",.bas
        HALT
    END
.** SCRX10-027-1 a
exit:
    IF .tsk <= 4 THEN
        IF g.cpos[.rob] == gpos_r_home THEN
            CALL gcpos_home(.rob,0)
        END
    END
.END
.PROGRAM getput_chk(.rob,.hnd,.prt,.slt)
    CALL is_taught(.rob,.rob,.prt,.slt,ON)
    IF is_taught[.rob] <= 0 THEN
        CALL alarm_str(.rob,.rob,0,$ae_n_taught)
        RETURN
    END
    IF (.slt<>1)AND(vpp.pitch[.prt]==0) THEN
        CALL alarm_str(.rob,.rob,0,$ae_pitch_zero)
        RETURN
    END
.** harish 090515 a++
    IF (port_type[0,.prt] BAND port_util)AND(tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
        IF (vpp.isimap[.prt]) THEN
            IF $tsk_cmd[.rob] == "MOVTOLOC" THEN
.** SCRX10-027-1 d
.**                IF tsk_dat[.rob,d_dat1]==pps_get OR tsk_dat[.rob,d_dat1]==pps_put OR tsk_dat[.rob,d_dat1]==pps_hom THEN
.** SCRX10-027-1 m
                IF tsk_dat[.rob,d_dat1]==pps_get OR tsk_dat[.rob,d_dat1]==pps_put OR tsk_dat[.rob,d_dat1]==pps_hom OR tsk_dat[.rob,d_dat1]==pps_tch THEN
                    RETURN
                ELSE
                    CALL alarm_str(.rob,.rob,0,$ae_imap_getput)
                    RETURN
                END
            ELSE
                CALL alarm_str(.rob,.rob,0,$ae_imap_getput)
                RETURN
            END
        END
    END
.** harish 090515 a--
;** Check Mapping Result
;;;TYPE "Check Mapping Result"
.END

.** harish 090327-1 a++
.***
.*** input
.***        .rob        -> robot
.***        .hnd        -> select hand
.***        .prt        -> destination port for prepos command
.***        .spec_hnd   -> select hand for prepos command(non-selected as 0)
.***
.*** output
.***        .prepos_hnd -> prepos hand
.***        .prepos_pps -> prepos port
.PROGRAM prepos_pps(.rob, .hnd, .prt, .spec_hnd, .prepos_hnd, .prepos_pps)
    IF .spec_hnd THEN
.*** selected hand
        .prepos_hnd = .spec_hnd
        CALL gripper_out(.rob,.prepos_hnd,grip_hold)
        TWAIT 1
        IF tsk_err[.rob] RETURN
        CALL gripper_stat(.rob,.prepos_hnd,.out,.sta[.prepos_hnd])
        IF .sta[.prepos_hnd] == grip_pres THEN
            .prepos_pps = pps_pts
        ELSEIF .sta[.prepos_hnd] == grip_abst THEN
            .prepos_pps = pps_gts
        ELSE
            GOTO err
        END

    ELSE
.*** non selected hand
.***        [1]hand setting
        .prepos_hnd = .hnd
.*        [2]destination setting
        IF ( ($tsk_cmd[.rob]=="GETWAFPREPOS") OR ($tsk_cmd[.rob]=="SWAPPREPOS") ) THEN
.*            moving to port P by GetPrePos or moving to port P by SwapPrePos
            .prepos_pps = pps_pts
        ELSE
            .prepos_pps = pps_gts
        END

    END                ; /* IF .spec_hnd THEN */
.*                                                      /* SCRX11-032-6 a */
    PrePos[.rob] = ON
    RETURN

err:
    tsk_err[.rob] = ON
    RETURN
.END
.** harish 090327-1 a--

.** harish 090327-1 a++
.******************************************
.** getputprpos.as
.******************************************
.PROGRAM GetPutPrPos_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<3)OR(4<tsk_num[.tsk])                        GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])        GOTO epm
.** Check PortID:Slot#
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt1] == BL_RINSE                                                    GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])        GOTO epm
.** Check PortID:Slot#
    CALL id_port_slot(.tsk,3,d_prt2,d_slt2)
    IF NOT (tsk_dat[.tsk,d_prt2] AND tsk_dat[.tsk,d_slt2])        GOTO epm
.*
    IF tsk_num[.tsk] == 4 THEN
        CALL id_hand(.tsk,4,d_rob1,d_hnd2)
        IF (NOT tsk_dat[.tsk,d_hnd2])                                GOTO epm
    ELSE
        tsk_dat[.tsk,d_hnd2] = 0
    END
.*
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.*
.* GetWafPrePos-Robot
.*
.PROGRAM GetPutPrPos_rob(.rob,.bas)
    CALL getput_rob(.rob,.bas)
    IF tsk_err[.rob]        RETURN
;   CALL mvwait(.rob,vhp.thalf[tsk_tsk[.rob]])
    tsk_dat[.rob,d_prt1] = tsk_dat[.rob,d_prt2]
    tsk_dat[.rob,d_slt1] = tsk_dat[.rob,d_slt2]
    CALL prepos_pps(.rob,tsk_dat[.rob,d_hnd1],tsk_dat[.rob,d_prt1],tsk_dat[.rob,d_hnd2],.prepos_hnd,.prepos_pps)
    IF tsk_err[.rob]        RETURN
    tsk_dat[.rob,d_hnd1] = .prepos_hnd
    tsk_dat[.rob,d_dat1] = .prepos_pps
    CALL movtoloc_rob(.rob)
    RETURN
.END
.** harish 090327-1 a--
.******************************************
.** getput_t.as
.******************************************
.PROGRAM tgetput_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>3)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check PortID:Slot#
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt1] == BL_RINSE                                 GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Check Motion Type
    CALL id_int(.tsk,3,d_dat1,1)
    IF id_error[.tsk]						GOTO epm
    IF (tsk_dat[.tsk,d_dat1]<1)OR(3<tsk_dat[.tsk,d_dat1])	GOTO epm
.** harish 090527 a++
    IF vc.mode[.tsk] == mode_soft
	CALL send_ack(.tsk)
	$alm_put[.tsk] = $AE_MD_THIS_SOFT
	CALL alarm_put(.tsk,0,0)
	CALL make_res_ng(.tsk)
	RETURN
    END
.** harish 090527 a--
.** Command Data Set
    tsk_mod[.tsk] = m_tech
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM tgetput_err(.rob)
    CASE g.base[.rob] OF
      VALUE gbas_tget :
	.$alm = $ae_tget_ing
      VALUE gbas_tger :
	.$alm = $ae_tget_err
      VALUE gbas_tput :
	.$alm = $ae_tput_ing
      VALUE gbas_tper :
	.$alm = $ae_tput_err
      ANY :
	RETURN
    END
    IF .$alm <> "" THEN
	CALL alarm_str(.rob,.rob,0,.$alm)
    END
.END
.PROGRAM tgetput_rob(.rob,.bas)
.**TYPE /S," CMD:",$tsk_cmd[.rob]
.**TYPE /S," ROB:",tsk_dat[.rob,d_rob1]
.**TYPE /S," HND:",tsk_dat[.rob,d_hnd1]
.**TYPE /S," FNG:",tsk_dat[.rob,d_fng1]
.**TYPE /S," PRT:",tsk_dat[.rob,d_prt1]
.**TYPE /S," SLT:",tsk_dat[.rob,d_slt1]
.**TYPE /S," TYP:",tsk_dat[.rob,d_dat1]
    .hnd = tsk_dat[.rob,d_hnd1]
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
    .typ = tsk_dat[.rob,d_dat1]
    IF (g.base[.rob]==gbas_tper)OR( g.base[.rob]==gbas_tger)	GOTO err
    IF (g.base[.rob]==gbas_tput)AND(.bas==gbas_tget)		GOTO err
    IF (g.base[.rob]==gbas_tget)AND(.bas==gbas_tput)		GOTO err
    IF (g.base[.rob]==.bas) THEN
	IF .typ==3						GOTO err
	IF g.hand[.rob]<>.hnd					GOTO err
	IF g.port[.rob]<>.prt					GOTO err
	IF g.slot[.rob]<>.slt					GOTO err
	.dst = g.cpos[.rob]+1
	GOTO step
    ELSEIF .typ==3 THEN
	CALL getput_rob(.rob,.bas)
	RETURN
    END
    IF g.base[.rob]<>gbas_home THEN
	CALL homing(.rob)
	IF tsk_err[.rob]	RETURN
    END
    CALL gcpos_set(.rob,.bas,.hnd,.prt,.slt,gpos_home)
    .dst = 1
step:
    IF .typ==1 THEN
	CALL speed(.rob,MINVAL(now.speed[.rob],spd_table[.rob,spd_slow]),ON)
    ELSEIF .typ==1 THEN
;****** Do Nothing --> Curret Speed
    END
    CALL getput_chk(.rob,.hnd,.prt,.slt)
    IF tsk_err[.rob] THEN
	IF .dst == 1 THEN
	    CALL gcpos_cls(.rob)
	ELSE
	    CALL gcpos_base(.rob,.bas+2)	;** eRR
	END
	RETURN
    END
    CALL getput_sel(.rob,.rob,.hnd,.prt,.slt,.dst,.dst,.bas)
    IF tsk_err[.rob] THEN
	CALL gcpos_base(.rob,.bas+2)	;** eRR
	RETURN
    END
    IF g.cpos[.rob] == gpos_home THEN
	CALL break(.rob)
	IF (.bas==gbas_tget) THEN
	    CALL event_rob(.rob,$ev_tgp_end[.rob],$ev2_tget_end)
	ELSE
	    CALL event_rob(.rob,$ev_tgp_end[.rob],$ev2_tput_end)
	END
    ELSEIF (tsk_hst[.rob]==host_stp1)OR(tsk_hst[.rob]==host_stp2) THEN
	CALL delay(.rob,0)
	PAUSE
	.dst = g.cpos[.rob]+1
	GOTO step
    END
    RETURN
err:
    CALL tgetput_err(.rob)
.END

.PROGRAM qrytestmotion(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>3)	GOTO efm
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check PortID:Slot#
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt1] == BL_RINSE                                 GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** Check Purpose GET or PUT
    SCASE $TOUPPER($tsk_dat[.tsk,3]) OF
      SVALUE "GET":
	.bas = gbas_tget
      SVALUE "PUT":
	.bas = gbas_tput
      ANY :
	GOTO epm
    END
.** Syntax OK Send Ack
    CALL send_ack(.tsk)
.** Make Response
    .rob = tsk_dat[.tsk,d_rob1]
    .hnd = tsk_dat[.tsk,d_hnd1]
    .prt = tsk_dat[.tsk,d_prt1]
    .slt = tsk_dat[.tsk,d_slt1]
    IF tsk_cmd[.rob] OR tsk_use[.rob] THEN
	.sta = 2	;* Busy
	.pos = 0
    ELSEIF (g.base[.rob]==gbas_tger)OR(g.base[.rob]==gbas_tper) THEN
	.sta = 0	;* TGET or TPUT Err -> IDEL : Cal Step
	.pos = 1
    ELSEIF (g.base[.rob]<>gbas_tget)AND(g.base[.rob]<>gbas_tput) THEN
	.sta = 0	;* IDEL : Cal Step
	.pos = 1
    ELSEIF (g.port[.rob]<>.prt)OR(g.slot[.rob]<>.slt)OR(g.hand[.rob]<>.hnd) THEN
	.sta = 3	;* Different Parameter
	.pos = 0
    ELSEIF (g.base[.rob]==.bas) THEN
	.sta = 1	;* In the middle --> Cal Step
	.pos = g.cpos[.rob]+1
    ELSE
	.sta = 3	;* Different Parameter
	.pos = 0
    END
.** Cal Steps
    .stp = 0
    IF (.sta==0)OR(.sta==1) THEN
	qtm_cnt[.tsk] = 0
	CALL is_taught(.tsk,.rob,.prt,.slt,ON)
	IF is_taught[.tsk] <= 0		GOTO res
	CALL getput_sel(.tsk,.rob,.hnd,.prt,.slt,.pos,gpos_r_home,.bas)
	.stp = qtm_cnt[.tsk]
    END
res:
    $snd_res[.tsk] = $res+","+$TRIM_B($ENCODE(/I1,.sta))+","+$TRIM_B($ENCODE(/I2,.stp))
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM zqtm1()
    CALL zqtm0(pc3)
.END
.PROGRAM zqtm2()
    CALL zqtm0(pc4)
.END
.PROGRAM zqtm0(.tsk)
    CALL dwait0(.tsk,"init")
    CALL dwait0(.tsk,"mode,pterm")
    CALL dwait0(.tsk,"coldet,0")
    FOR .rob = 1 TO 2 STEP 1
	IF (tsk_rob[.tsk] BAND bit[.rob]) THEN
	    CALL dwait0(.tsk,"SetSpd,"+$rob_id[.tsk,.rob]+":100")
	    FOR .iii = 1 TO id_port[.tsk,0] STEP 1
		.prt = id_port[.tsk,.iii]
		CALL is_taught(0,.rob,.prt,1,ON)
		IF is_taught[0] THEN
		    .$rbps = $rob_id[.tsk,.rob]+":B1,"+$id_port[.tsk,.iii]+":1"
		    CALL zqtm_sub(.tsk,.rob,.$rbps,"GET")
		    CALL zqtm_sub(.tsk,.rob,.$rbps,"PUT")
		END
	    END
	END
    END
.END
.PROGRAM zqtm_sub(.tsk,.rob,.$rbps,.$get_put)
    .$res = ""
    CALL dwait0_res(.tsk,"QryTestMotion,"+.$rbps+","+.$get_put,.$res)
    .$key = "Res,0,"
    IF .$key<>$LEFT(.$res,LEN(.$key))	GOTO err
    .all = VAL($MID(.$res,LEN(.$key)+1))
    IF .all == 0 THEN
	PROMPT "Is step 0 OK ?",.$key
	IF .$key <> ""		GOTO err
    END
    FOR .cnt = 1 TO .all STEP 1
	CALL dwait0(.tsk, "T"+.$get_put+"Waf,"+.$rbps+",2")
	CALL dwait0_res(.tsk,"QryTestMotion,"+.$rbps+","+.$get_put,.$res)
	IF .cnt <> .all THEN
	    .$key = "Res,1,"
	    IF .$key<>$LEFT(.$res,LEN(.$key))	GOTO err
	    .rem = VAL($MID(.$res,LEN(.$key)+1))
	    IF .rem <> (.all-.cnt)			GOTO err
	ELSE
	    .$key = "Res,0,"
	    IF .$key<>$LEFT(.$res,LEN(.$key))	GOTO err
	    .rem = VAL($MID(.$res,LEN(.$key)+1))
	    IF .rem <> .all			GOTO err
	    IF g.base[.rob]<>gbas_home		GOTO err
	END
    END
    RETURN
err:
    TYPE "Err"
    HALT
.END
.******************************************
.** swap.as
.******************************************
.************
.** TRWAF ***
.************
.PROGRAM trwaf_cmd(.tsk)
.* Param 3 : TrWaf,R1:B1,P1:1,A1:1
.* Param 4 : TrWaf,R1:B1,P1:1,A1:1,Angle
.* Param 9 : TrWaf,R1:B1,P1:1,ZPS,LLF,ULF,A1:1,ZPS,LLF,ULF
.* Param10 : TrWaf,R1:B1,P1:1,ZPS,LLF,ULF,A1:1,ZPS,LLF,ULF,Angle
    CASE tsk_num[.tsk] OF
      VALUE 3,4,9,10:
      ANY :
        GOTO efm
    END
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])        GOTO epm
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt1] == BL_RINSE                                                   GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])                                GOTO epm
    IF tsk_num[.tsk] <=4 THEN
        tsk_dat[.tsk,d_zps1] = 0
        tsk_dat[.tsk,d_llf1] = 0
        tsk_dat[.tsk,d_ulf1] = 0
        .buf = 3
    ELSE
        CALL id_int(.tsk,3,d_zps1,100)
        IF id_error[.tsk]                GOTO epm
        CALL id_int(.tsk,4,d_llf1,100)
        IF id_error[.tsk]                GOTO epm
        CALL id_int(.tsk,5,d_ulf1,100)
        IF id_error[.tsk]                GOTO epm
        .buf = 6
    END
    tsk_dat[.tsk,d_rob2] = tsk_dat[.tsk,d_rob1]
    tsk_dat[.tsk,d_hnd2] = tsk_dat[.tsk,d_hnd1]
    tsk_dat[.tsk,d_fng2] = tsk_dat[.tsk,d_fng1]
    CALL id_port_slot(.tsk,.buf,d_prt2,d_slt2)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt2] == BL_RINSE                                                   GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt2] AND tsk_dat[.tsk,d_slt2])                                GOTO epm
    IF tsk_num[.tsk] <=4 THEN
        tsk_dat[.tsk,d_zps2] = 0
        tsk_dat[.tsk,d_llf2] = 0
        tsk_dat[.tsk,d_ulf2] = 0
        .buf = 4
    ELSE
        CALL id_int(.tsk,7,d_zps2,100)
        IF id_error[.tsk]                GOTO epm
        CALL id_int(.tsk,8,d_llf2,100)
        IF id_error[.tsk]                GOTO epm
        CALL id_int(.tsk,9,d_ulf2,100)
        IF id_error[.tsk]                GOTO epm
        .buf = 10
    END
    IF tsk_num[.tsk] == .buf THEN
        IF (port_type[0,tsk_dat[.tsk,d_prt2]] BAND port_kind) <> port_algn                GOTO efm
        tsk_dat[.tsk,d_dat1] = ON
        CALL id_int(.tsk,.buf,d_dat2,100)
        IF id_error[.tsk]                GOTO epm
    ELSE
        tsk_dat[.tsk,d_dat1] = OFF
        tsk_dat[.tsk,d_dat2] = 0
    END
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM trwaf_rob(.rob)
.**TYPE /S," CMD:",$tsk_cmd[.rob]
.**TYPE /S," ROB:",tsk_dat[.rob,d_rob1]
.**TYPE /S," HND:",tsk_dat[.rob,d_hnd1]
.**TYPE /S," FNG:",tsk_dat[.rob,d_fng1]
.**TYPE /S," PRT:",tsk_dat[.rob,d_prt1]
.**TYPE /S," SLT:",tsk_dat[.rob,d_slt1]
.**TYPE /S," ZPS:",tsk_dat[.rob,d_zps1]
.**TYPE /S," LLF:",tsk_dat[.rob,d_llf1]
.**TYPE /S," ULF:",tsk_dat[.rob,d_ulf1]
.**TYPE    ""
.**TYPE /S," CMD:",$tsk_cmd[.rob]
.**TYPE /S," ROB:",tsk_dat[.rob,d_rob2]
.**TYPE /S," HND:",tsk_dat[.rob,d_hnd2]
.**TYPE /S," FNG:",tsk_dat[.rob,d_fng2]
.**TYPE /S," PRT:",tsk_dat[.rob,d_prt2]
.**TYPE /S," SLT:",tsk_dat[.rob,d_slt2]
.**TYPE /S," ZPS:",tsk_dat[.rob,d_zps2]
.**TYPE /S," LLF:",tsk_dat[.rob,d_llf2]
.**TYPE /S," ULF:",tsk_dat[.rob,d_ulf2]
.**TYPE    ""
.**TYPE /S," ALN:",tsk_dat[.rob,d_dat1]
.**TYPE /S," ANG:",tsk_dat[.rob,d_dat2]
.**TYPE    ""
    CALL getput_rob(.rob,gbas_getp)
    IF tsk_err[.rob]        RETURN
;
    CALL event_rob(.rob,$ev_half_move[.rob],"")
    CALL mvwait(.rob,vtp.thalf[tsk_tsk[.rob]])
;
    ZARRAYCPY tsk_dat[.rob,d_rob1] = tsk_dat[.rob,d_rob2],(d_dat1-d_rob1)
    CALL getput_rob(.rob,gbas_putp)
    IF tsk_err[.rob]        RETURN
;
    IF tsk_dat[.rob,d_dat1] THEN
        CALL mvwait(.rob,vtp.thalf[tsk_tsk[.rob]])
        .dev = 3
        CALL dev_lock(.rob,.dev)
        IF tsk_err[.rob]        RETURN
        $tsk_cmd[.dev] = "ALGN"
        tsk_dat[.dev,d_dat1] = tsk_dat[.rob,d_dat2]
        CALL dev_exec(.rob,.dev)
    END
.END

.*** harish 090327-1 a++
.*
.* TrWafPrePos-Command
.*
.PROGRAM TrwafPrPos_cmd(.tsk)
    IF (tsk_num[.tsk]<4)OR(5<tsk_num[.tsk])                GOTO efm
.**
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])        GOTO epm
.**
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt1] == BL_RINSE                                                   GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])                                GOTO epm
.**
    CALL id_port_slot(.tsk,3,d_prt2,d_slt2)
.*                                                      /* SCRX11-032-2 a */
    IF tsk_dat[.tsk,d_prt2] == BL_RINSE                                                   GOTO epm
    IF NOT (tsk_dat[.tsk,d_prt2] AND tsk_dat[.tsk,d_slt2])                                GOTO epm
.**
    CALL id_port_slot(.tsk,4,d_dat1,d_dat2)
    IF NOT (tsk_dat[.tsk,d_dat1] AND tsk_dat[.tsk,d_dat2])                                GOTO epm
.**
    IF tsk_num[.tsk] == 5 THEN
        CALL id_hand(.tsk,5,d_rob1,d_hnd2)
        IF (NOT tsk_dat[.tsk,d_hnd2])                                GOTO epm
    ELSE
        tsk_dat[.tsk,d_hnd2] = 0
    END
.**
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END

.*
.* TrWafPrPos-Robot
.*
.PROGRAM TrwafPrPos_rob(.rob)
.*** harish 090327-1 m++
.*** TRWAFの中でp_dt3の内容でアライナ操作しているのでここでは独自にデータ解釈する
.*;    CALL trwaf_rob(.rob)
    CALL getput_rob(.rob,gbas_getp)
    IF tsk_err[.rob]        RETURN
    CALL event_rob(.rob,$ev_half_move[.rob],"")
    CALL mvwait(.rob,vtp.thalf[tsk_tsk[.rob]])
    tsk_dat[.rob,d_prt1]=tsk_dat[.rob,d_prt2]
    tsk_dat[.rob,d_slt1]=tsk_dat[.rob,d_slt2]
    CALL getput_rob(.rob,gbas_putp)
.*** harish 090327-1 m--
    IF tsk_err[.rob]        RETURN
.*
.*  最後のPREPOSのための準備
    tsk_dat[.rob,d_prt1]=tsk_dat[.rob,d_dat1]
    tsk_dat[.rob,d_slt1]=tsk_dat[.rob,d_dat2]
    CALL prepos_pps(.rob,tsk_dat[.rob,d_hnd1],tsk_dat[.rob,d_prt1],tsk_dat[.rob,d_hnd2],.prepos_hnd,.prepos_pps)
    IF tsk_err[.rob]        RETURN
    tsk_dat[.rob,d_hnd1] = .prepos_hnd
    tsk_dat[.rob,d_dat1] = .prepos_pps
    CALL movtoloc_rob(.rob)
    RETURN
.END
.*** harish 090327-1 a--
.******************************************
.** movtoloc.as
.******************************************
.PROGRAM movtoloc_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>3)        GOTO efm
;** Check Normal Syntax
    IF 1 THEN
.** Check Robot:Blade
        CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
        IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])        GOTO special
.** Check Port:Slot
        CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
        IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])                                GOTO epm
.** Check Purpose
        CALL id_pps(.tsk,3,d_dat1)
        IF NOT (tsk_dat[.tsk,d_dat1])        GOTO epm
.*                                                      /* SCRX11-032-2 a++ */
.*      GTS, Taught, and Home are the 2nd, 9th, and 10th positions in $id_pps
        IF (tsk_dat[.tsk,d_prt1] == BL_RINSE) THEN
            IF (tsk_dat[.tsk,d_dat1] <> 2) AND (tsk_dat[.tsk,d_dat1] <> 6) AND (tsk_dat[.tsk,d_dat1] <> 9) AND (tsk_dat[.tsk,d_dat1] <> 10)   GOTO epm
        END
.*                                                      /* SCRX11-032-2 a-- */
    ELSE
Special:
.** harish 081210 a++
        CALL id_unit(.tsk,1,d_rob1)
        tsk_dat[.tsk,d_rob1] = bit[tsk_dat[.tsk,d_rob1]]
        IF NOT (tsk_dat[.tsk,d_rob1])                        GOTO epm
        CALL id_pps(.tsk,2,d_dat1)
        IF NOT (tsk_dat[.tsk,d_dat1])                        GOTO epm
        CALL id_int(.tsk,3,d_dat2,1)
        IF id_error[.tsk]                                GOTO epm
        TYPE "MADA Special Purpose:",$RPGNAME()
;        GOTO epm
.** harish 081210 a--
    END
    IF tsk_dat[.tsk,d_dat1] == pps_map THEN
TYPE "PPS_MAP check"
        IF NOT can_map[tsk_dat[.tsk,d_rob1]]                                        GOTO epm
        IF NOT ((port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form )==p_hori)        GOTO epm
    END
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM movtoloc_rob(.rob)
    .hnd = tsk_dat[.rob,d_hnd1]
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
    .pps = tsk_dat[.rob,d_dat1]
    .esc = ON
    IF (g.hand[.rob]==.hnd)AND(g.port[.rob]==.prt)AND(g.slot[.rob]==.slt) THEN
        .esc = OFF
    END
    CASE .pps OF
      VALUE pps_get :
        .bas = gbas_getp
        .dst = gpos_e_home
      VALUE pps_gts :
        .bas = gbas_getp
        .dst = gpos_e_home
      VALUE pps_gtx :
        .bas = gbas_getp
        .dst = gpos_e_extd
      VALUE pps_gtr :
        .bas = gbas_getp
        .dst = gpos_r_home
        IF (.esc)                        GOTO gtr_err
        IF g.base[.rob]<>gbas_getp        GOTO gtr_err
        IF g.cpos[.rob]==gpos_e_extd        GOTO mov
        IF g.cpos[.rob]==gpos_e_tech        GOTO mov
        IF g.cpos[.rob]==gpos_r_tech        GOTO mov
gtr_err:
        CALL alarm_str(.rob,.rob,0,$ae_mloc_pp_gtr)
        RETURN
      VALUE pps_put :
        .bas = gbas_putp
        .dst = gpos_e_home
      VALUE pps_pts :
        .bas = gbas_putp
        .dst = gpos_e_home
      VALUE pps_ptx :
        .bas = gbas_putp
        .dst = gpos_e_extd
      VALUE pps_ptr :
        .bas = gbas_putp
        .dst = gpos_r_home
        IF (.esc)                        GOTO ptr_err
        IF g.base[.rob]<>gbas_putp        GOTO ptr_err
        IF g.cpos[.rob]==gpos_e_extd        GOTO mov
ptr_err:
        CALL alarm_str(.rob,.rob,0,$ae_mloc_pp_ptr)
        RETURN
      VALUE pps_tch :
        .bas = gbas_getp
        .dst = gpos_r_tech
      VALUE pps_hom :
        .bas = gbas_getp
        .dst = gpos_e_home
      VALUE pps_map :
        GOTO esc
.** harish 081210 a++
      VALUE pps_shp :
        IF tsk_arm[rob_tsk[.rob]]      == arm_nt410     THEN
            CALL nt410_shipping(.rob)
            IF tsk_err[.rob]                        RETURN
        ELSEIF tsk_arm[rob_tsk[.rob]]  == arm_nt570     THEN
            CALL nt570_shipping(.rob)
            IF tsk_err[.rob]                        RETURN
        END
        CALL gcpos_cls(.rob)
        RETURN
.** harish 081210 a--
.** harish 090304-1 a++
      VALUE pps_aln :
        IF tsk_arm[rob_tsk[.rob]]      == arm_nt570     THEN
            CALL nt570_align(.rob)
            IF tsk_err[.rob]                        RETURN
        ELSEIF tsk_arm[rob_tsk[.rob]]  == arm_nt410     THEN
            CALL nt410_shipping(.rob)
            IF tsk_err[.rob]                        RETURN
        END
        CALL gcpos_cls(.rob)
        RETURN
.** harish 090304-1 a--
;**   VALUE pps_imp :
;**   VALUE pps_imr :
;**   VALUE pps_gwv :
;**   VALUE pps_gwr :
      ANY :
        TYPE "MADA Purpose:",$RPGNAME()
        CALL alarm_str(.rob,.rob,0,$ae_mloc_ppos)
        RETURN
    END
mov:
    IF (.bas<>g.base[.rob]) THEN
        .esc = ON
    ELSEIF (.dst<g.cpos[.rob]) THEN
        .esc = ON
    END
    IF (.esc)AND(g.base[.rob]<>gbas_home) THEN
esc:
        CALL homing(.rob)
        IF tsk_err[.rob]        RETURN
    END
    .sta = g.cpos[.rob]
    CALL getput_chk(.rob,.hnd,.prt,.slt)
    IF tsk_err[.rob]                RETURN
    IF .pps == pps_map THEN
        CALL movtoloc_map(.rob,.hnd,.prt,.slt)
    ELSE
        CALL gcpos_set(.rob,.bas,.hnd,.prt,.slt,.sta)
.*                                                      /* SCRX11-032-6 a++ */
        IF PrePos[.rob] THEN
            CALL robot_fan(.rob)         ;debug
            CALL PrePos_set(.rob)
        END
.*                                                      /* SCRX11-032-6 a-- */
        CALL getput_sel(.rob,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
.** harish 090327-1 a
        IF tsk_err[.rob]                RETURN
    END
.*** harish 090327-1 a++
.*                                                      /* SCRX11-032-6 m */
    IF NOT PrePos[.rob] AND (.pps == pps_gts OR .pps == pps_pts) THEN
.*** PrePos
.*** harish 090327-1 m++
        PrePos[.rob] = ON
.*** harish 090327-1 m--
        CALL robot_fan(.rob)                ;debug
        CALL PrePos_set(.rob)
    END
.*** harish 090327-1 a--
.END
.** harish 081210 a++
.PROGRAM nt410_shipping(.rob)
    .index = tsk_dat[.rob,d_dat2]
    CASE .index OF
      VALUE 1 :
        CALL homing(.rob)
        IF tsk_err[.rob]        RETURN
        POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
        POINT .#dst = #PPJSET(#ref_ship1[.rob],3,DEXT(.#loc[.rob],3))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"SHIP1")
        POINT .#dst = #PPJSET(.#dst,3,DEXT(#ref_ship1[.rob],3))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"SHIP2")
        TYPE " special purpose - SHIPPING  ",$RPGNAME()
      ANY :
        CALL alarm_str(.rob,.rob,0,$ae_mloc_ppos)
    END
.END
.PROGRAM nt570_shipping(.rob)
    .index = tsk_dat[.rob,d_dat2]
    CASE .index OF
      VALUE 1 :
        CALL homing(.rob)
        IF tsk_err[.rob]        RETURN
        POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
        POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#ref_ship1[.rob],3))
        POINT .#dst = #PPJSET(.#dst,7,DEXT(#ref_ship1[.rob],7))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"SHIP1")
        POINT .#dst = #PPJSET(.#dst,6,DEXT(#ref_ship1[.rob],6))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"SHIP2")
        CALL jmove(.rob,0,gpos_nchg,#ref_ship2[.rob],"SHIP3")
        POINT .#dst = #PPJSET(#ref_ship3[.rob],7,DEXT(#ref_ship2[.rob],7))
.** harish 090415 a
        POINT .#dst = #PPJSET(.#dst,3,DEXT(#ref_ship2[.rob],3))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"SHIP4")
        CALL jmove(.rob,0,gpos_nchg,#ref_ship3[.rob],"SHIP5")
        TYPE " special purpose - SHIPPING  ",$RPGNAME()
      ANY :
        CALL alarm_str(.rob,.rob,0,$ae_mloc_ppos)
    END
.END
.** harish 081210 a--
.** harish 090304 a++
.PROGRAM nt570_align(.rob)
    .index = tsk_dat[.rob,d_dat2]
    CASE .index OF
      VALUE 1 :
        CALL homing(.rob)
        IF tsk_err[.rob]        RETURN
        POINT .#loc[.rob] = #DEST:rob_tsk[.rob]
        POINT .#dst = #PPJSET(.#loc[.rob],3,DEXT(#ref_algn1[.rob],3))
        POINT .#dst = #PPJSET(.#dst,7,DEXT(#ref_algn1[.rob],7))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"ALGN1")
        POINT .#dst = #PPJSET(.#dst,6,DEXT(#ref_algn1[.rob],6))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"ALGN2")
        CALL jmove(.rob,0,gpos_nchg,#ref_algn2[.rob],"ALGN3")
        POINT .#dst = #PPJSET(#ref_algn3[.rob],7,DEXT(#ref_algn2[.rob],7))
        POINT .#dst = #PPJSET(.#dst,6,DEXT(#ref_algn2[.rob],6))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"ALGN4")
        POINT .#dst = #PPJSET(#ref_algn3[.rob],7,DEXT(#ref_algn2[.rob],7))
        CALL jmove(.rob,0,gpos_nchg,.#dst,"ALGN5")
        CALL jmove(.rob,0,gpos_nchg,#ref_algn3[.rob],"ALGN6")
        TYPE " special purpose - ALIGN  ",$RPGNAME()
      ANY :
        CALL alarm_str(.rob,.rob,0,$ae_mloc_ppos)
    END
.END
.** harish 090304 a--
.PROGRAM movtoloc_map(.rob,.hnd,.prt,.slt)
.** harish 081202 m
    CALL map_pos(.rob,.rob,.hnd,.prt,.slt,OFF,OFF)
.**    CALL map_pos(.rob,.rob,.hnd,.prt,.slt,OFF,ON)
    IF tsk_err[.rob]        RETURN
;
    .pat = vc.tchpos[.rob,.prt]
    CALL p_hori(.rob,.rob,.hnd,.prt,.slt,ON)
;
    .zzz = map_pos[.rob,0] - vnp.mtlmapz
    POINT .#dst = #PPJSET(#gp_home[.rob],3,.zzz)
.*                                                      /* SCRX12-006 2 am++
    CALL pmove(.rob,.hnd,.pat,gpos_e_home,.#dst,"MAP_HOME",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am--
    IF (gpos_e_prep < mapp_path[.pat])AND(path_move[.pat,gpos_e_prep]) THEN
        POINT .#dst = #PPJSET(#gp_prep[.rob],3,.zzz)
        CALL xmove(.rob,.hnd,.pat,gpos_e_prep,.#dst,"MAP_PREP")
    END
    IF (gpos_e_half < mapp_path[.pat])AND(path_move[.pat,gpos_e_half]) THEN
        POINT .#dst = #PPJSET(#gp_half[.rob],3,.zzz)
        CALL xmove(.rob,.hnd,.pat,gpos_e_half,.#dst,"MAP_HALF")
    END
    IF (gpos_e_near < mapp_path[.pat])AND(path_move[.pat,gpos_e_near]) THEN
        POINT .#dst = #PPJSET(#gp_near[.rob],3,.zzz)
        CALL xmove(.rob,.hnd,.pat,gpos_e_near,.#dst,"MAP_NEAR")
    END
    ACCEL mapp_acce[.pat]
    DECEL mapp_acce[.pat]
    POINT .#dst =  #PPJSET(#map_pos[.rob,1],3,.zzz)
    IF mapp_move[.pat] == mov_jmv THEN
        CALL jmove(.rob,.hnd,gpos_nchg,.#dst,"MAP_SSS")
    ELSEIF mapp_move[.pat] == mov_lmv THEN
        CALL lmove(.rob,.hnd,gpos_nchg,.#dst,"MAP_SSS")
    END
    CALL gcpos_cls(.rob)
.END
.******************************************
.** map_cmd.as
.******************************************
.PROGRAM map_cmd(.tsk,.one)
    IF (tsk_num[.tsk]<>1)AND(tsk_num[.tsk]<>3)        GOTO efm
    IF .one THEN
        CALL id_port_slot(.tsk,1,d_prt1,d_slt1)
        IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])        GOTO epm
    ELSE
        CALL id_port(.tsk,1,d_prt1)
        IF NOT (tsk_dat[.tsk,d_prt1])                                GOTO epm
        tsk_dat[.tsk,d_slt1] = 0
    END
    IF NOT ((port_type[0,tsk_dat[.tsk,d_prt1]] BAND port_form )==p_hori)        GOTO epm
    IF tsk_num[.tsk]==1 THEN
        tsk_dat[.tsk,d_dat1] = 0        ;** Normal Speed
        tsk_dat[.tsk,d_dat2] = IFELSE(.one,vnp.sfmapslot,vnp.safemap)
    ELSE
        SCASE $TOUPPER($tsk_dat[.tsk,2]) OF
          SVALUE "NORMAL" :
            tsk_dat[.tsk,d_dat1] = 0        ;** Normal Speed
          SVALUE "SLOW" :
            tsk_dat[.tsk,d_dat1] = ON        ;** Slow Speed
          ANY :
            GOTO epm
        END
        CALL id_0_1(.tsk,3,d_dat2)
        IF id_error[.tsk]        GOTO epm
    END
    .can = OFF
    FOR .rob = 1 TO 2 STEP 1
        IF tsk_rob[.tsk] BAND bit[.rob] THEN
            IF can_map[.rob] THEN
                .can = IFELSE(.can,.can,.rob)
                CALL is_taught(.tsk,.rob,tsk_dat[.tsk,d_prt1],1,ON)
                IF is_taught[.tsk] > 0                GOTO ok
            END
        END
    END
    IF NOT .can                GOTO efm
;** Some robot can map but not be taught yet.
    CALL check_bsy_mode(.tsk,bit[.can],m_auto)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")        RETURN
    $alm_put[.tsk] = $ae_n_taught0
    CALL alarm_put(.tsk,0,0)
    CALL map_err(.tsk,tsk_dat[.tsk,d_prt1],tsk_dat[.tsk,d_slt1])
    RETURN
ok:
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[.rob]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM map_rob(.rob)
.**TYPE /S," CMD:",$tsk_cmd[.rob]
.**TYPE /S," ROB:",tsk_dat[.rob,d_rob1]
.**TYPE /S," PRT:",tsk_dat[.rob,d_prt1]
.**TYPE /S," SLT:",tsk_dat[.rob,d_slt1]
.**TYPE /S," SLW:",tsk_dat[.rob,d_dat1]
.**TYPE /S," SAF:",tsk_dat[.rob,d_dat2]
.**TYPE    ""
    .hnd = 1
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
    .slw = tsk_dat[.rob,d_dat1]
    .saf = tsk_dat[.rob,d_dat2]
;**
    CALL map_pre(.rob,.hnd)
    IF tsk_err[.rob]        GOTO end
;**
.** harish 081202 m
    CALL map_pos(.rob,.rob,.hnd,.prt,.slt,.saf,OFF)
.**    CALL map_pos(.rob,.rob,.hnd,.prt,.slt,.saf,ON)
    IF tsk_err[.rob]        GOTO end
;**
    $ev1_inf[.rob] = $vrp.rob_asso[.rob]
    $ev2_inf[.rob] = $vrp.rob_asso[.rob]+$ENCODE(/I1,":B",.hnd)+","+$vpp.portasso[.prt]+$IFELSE(.slt,":"+$TRIM_B($ENCODE(/I,.slt)),"")+","+$tsk_cmd[.rob]
;
    CALL map_move(.rob,.hnd,.prt,.slt,.slw,.saf)
    IF tsk_err[.rob]        GOTO end
;**
    CALL map_comb(.rob)
    IF tsk_err[.rob]        GOTO end
;**
    CALL map_ana(.rob,.hnd,.prt,.slt,.slw,.saf)
    IF tsk_err[.rob]        GOTO end
end:
    IF tsk_err[.rob] THEN
        CALL map_err(.rob,tsk_dat[.rob,d_prt1],tsk_dat[.rob,d_slt1])
    ELSE
        CALL map_res(.rob,tsk_dat[.rob,d_prt1],tsk_dat[.rob,d_slt1])
        IF tsk_dat[.rob,d_slt1] THEN
;**MADA map_wafz
        END
    END
;**MADA Event
    CALL break(.rob)
.END
.*******************
.* Mapping Prepare *
.*******************
.PROGRAM map_pre(.rob,.hnd)
.***退避動作
    IF g.base[.rob]<>gbas_home THEN
        CALL homing(.rob)
        IF tsk_err[.rob]        RETURN
    END
.***Wafer無しチェック
    CALL gripper(.rob,.hnd,finger_rob[.rob,.hnd],grip_abst,who_ext,0)
    IF grip_err[.rob] THEN
;**        CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
    END
.END
.** harish 080630 a++
.********************
.* Sniffing Motion  *
.********************
.PROGRAM sniff_hori(.tsk,.rob,.hnd,.prt,.slt,.pos,.#low,.sniff_err)
    .sniff_err = OFF
    .pat = vc.tchpos[.rob,.prt]
    ACCEL mapp_acce[.pat]
    DECEL mapp_acce[.pat]
    CALL map_pos(.rob,.rob,.hnd,.prt,.slt,OFF,OFF)
    POINT .#loc = #PPJSET(#map_pos[.rob,1],3,DEXT(.#low,3))
    CALL jmove(.rob,hand_none,.pos,.#loc,"SNIFF_LOW")
    CALL speed_min(.rob,vnp.snfspd,ON)
.*                                                      /* SCRX12-006 m
    POINT .#loc = .#loc + #PPOINT(0,0,vpp.snfzup[.prt])
    CALL map_sens(.rob,.hnd,.#loc)
    CALL map_comb(.rob)
    CALL speed_set(.rob)
    POINT .#loc = #PPJSET(#map_pos[.rob,1],3,DEXT(.#low,3))
    CALL jmove(.rob,hand_none,.pos,.#loc,"SNIFF_END")
    IF (NOT SYSDATA(ZSIMENV)) AND (NOT(vc.mode[tsk_tsk[.rob]] BAND m_phtm)) THEN
        IF map_waf.cnt[.rob] <> 1 THEN
            CALL alarm_str(.rob,.rob,0,$ae_snf_nwaf)
            .sniff_err = ON
            RETURN
        END
        IF ABS((map_waf.low[.rob,1]+maps_pos.z[.rob])-DEXT(#gp_tech[.tsk],3))>vnp.snfzlim THEN
            CALL alarm_str(.rob,.rob,0,$ae_usesnfz)
            .sniff_err = ON
            RETURN
        END
    END
.END
.** harish 080630 a--
.********************
.* Mapping Position *
.********************
.PROGRAM map_pos(.tsk,.rob,.hnd,.prt,.slt,.saf,.tch)
.** #map_pos[#,0]        Mapping Sensor Line = Wafer Edge
.** #map_pos[#,1]        Mapping Position
.**  map_pos[#,0]        Based Height
.**  map_pos[#,1]        Below Height
.**  map_pos[#,2]        Above Height
.**  map_pos[#,9]        Analyze Height
    IF .slt THEN
.** harish 080630 m++
;        POINT .#waf[ 0] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,.slt],#vc.savpos[.rob,.prt,.slt])
        POINT .#waf[ 0] = #IFELSE(.tch,#vc.savpos[.rob,.prt,.slt],#vc.tchpos[.rob,.prt,.slt])
        POINT .#waf[10] = .#waf[0]
    ELSE
;        POINT .#waf[ 0] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,1             ],#vc.savpos[.rob,.prt,1             ])
;        POINT .#waf[10] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,vpp.slot[.prt]],#vc.savpos[.rob,.prt,vpp.slot[.prt]])
        POINT .#waf[ 0] = #IFELSE(.tch,#vc.savpos[.rob,.prt,1             ],#vc.tchpos[.rob,.prt,1             ])
        POINT .#waf[10] = #IFELSE(.tch,#vc.savpos[.rob,.prt,vpp.slot[.prt]],#vc.tchpos[.rob,.prt,vpp.slot[.prt]])
.** harish 080630 m--
    END
    map_num[.tsk] = 1
    IF .saf THEN
        map_num[.tsk] = INT(vnp.sfmpdist/vnp.sfmppitch)+2
        IF (vnp.sfmpdist MOD vnp.sfmppitch)<>0 THEN
            map_num[.tsk] = map_num[.tsk] + 1
        END
    END
    FOR .off = 0 TO 10 STEP 10
        IF (tsk_arm[rob_tsk[.rob]] BAND armb_nxx) THEN
            ZL3TRN rob_tsk[.rob]: .pos[1] = .#waf[.off],inv_hand
.**            IF r_arm[.rob] BAND armb_dbl THEN
.**                IF (x_area_map[x_path[.rob,.prt],x_ele_r]>-999) THEN
.**                    .pos[x_ele_r] = x_area_map[x_path[.rob,.prt],x_ele_r]
.**                    .pos[x_ele_s] = x_area_map[x_path[.rob,.prt],x_ele_s]
.**                ELSEIF (x_area_map[x_path[.rob,.prt],x_ele_s]>-999) THEN
.**                    .pos[x_ele_s] = .pos[x_ele_r] + x_area_map[x_path[.rob,.prt],x_ele_s]
.**                ELSE
.**                    ZL3TRN r_tsk[.rob]: .pos[1] = .pos[1] ALGN
.**                    .pos[x_ele_s] = .pos[x_ele_r] + x_area_map[x_path[.rob,.prt],x_ele_s]
.**                END
.**            ELSEIF (r_arm[.rob] == arm_x31) THEN
.**                IF x_sft_yrot[x_path[.rob,.prt]] > -999 THEN
.**                    .pos[x_ele_r] = x_sft_yrot[x_path[.rob,.prt]]
.**                END
.**            END
.** harish 080630 a++
.*                                                      /* SCRX11-032-4 m */
;            CASE (IFELSE(.tch,vc.tchpos[.rob,.prt],vc.savpos[.rob,.prt])) OF
            CASE (IFELSE(.tch,vc.savpos[.rob,.prt],vc.tchpos[.rob,.prt])) OF
                VALUE  p_hori_vdb :
;                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],maps_pos.z[.rob],maps_pos.r[.rob],0,0
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,0,maps_pos.z[.rob],-maps_pos.rvdb[.rob],0,0
                    ZL3TRN rob_tsk[.rob]: .pos[1] = .pos[1] SUB .add[1]
                    ZL3JNT rob_tsk[.rob]: #map_pos[.tsk,0+.off] = .pos[1],.#waf[0],inv_normal,inv_hand
                    ZL3TRN rob_tsk[.rob]: .pos[1] = #map_pos[.tsk,0+.off],inv_hand
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],0,0,0,0
                VALUE  p_hori_vda :
;                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],maps_pos.z[.rob],maps_pos.r[.rob],0,0
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,0,maps_pos.z[.rob],maps_pos.rvda[.rob],0,0
                    ZL3TRN rob_tsk[.rob]: .pos[1] = .pos[1] SUB .add[1]
                    ZL3JNT rob_tsk[.rob]: #map_pos[.tsk,0+.off] = .pos[1],.#waf[0],inv_normal,inv_hand
                    ZL3TRN rob_tsk[.rob]: .pos[1] = #map_pos[.tsk,0+.off],inv_hand
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],0,0,0,0
.*                                                      /* SCRX12-006-2 a++
                VALUE  p_hori_ws1 :
;                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],maps_pos.z[.rob],maps_pos.r[.rob],0,0
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,0,maps_pos.z[.rob],-maps_pos.rws1[.rob],0,0
                    ZL3TRN rob_tsk[.rob]: .pos[1] = .pos[1] SUB .add[1]
                    ZL3JNT rob_tsk[.rob]: #map_pos[.tsk,0+.off] = .pos[1],.#waf[0],inv_normal,inv_hand
                    ZL3TRN rob_tsk[.rob]: .pos[1] = #map_pos[.tsk,0+.off],inv_hand
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],0,0,0,0
.*                                                      /* SCRX12-006-2 a--
                ANY :
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],maps_pos.z[.rob],0,0,0
            END
.** harish 080630 a--
;            ZL3TRN rob_tsk[.rob]: .add[1] = 0,maps_pos.x[.rob],maps_pos.z[.rob],0,0,0
            ZL3TRN rob_tsk[.rob]: .pos[1] = .pos[1] SUB .add[1]
            ZL3JNT rob_tsk[.rob]: #map_pos[.tsk,0+.off] = .pos[1],.#waf[0],inv_normal,inv_hand
            ZL3TRN rob_tsk[.rob]: .add[1] = 0,vnp.mapdepth[.rob]
            ZL3TRN rob_tsk[.rob]: .pos[1] = .pos[1] ADD .add[1]
            ZL3JNT rob_tsk[.rob]: #map_pos[.tsk,1+.off] = .pos[1],.#waf[0],inv_normal,inv_hand
;*
            FOR .cnt = 2 TO map_num[.tsk] STEP 1
                IF .cnt == 2 THEN
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,vnp.sfmplast
                ELSE
                    ZL3TRN rob_tsk[.rob]: .add[1] = 0,vnp.sfmppitch
                END
                ZL3TRN rob_tsk[.rob]: .pos[1] = .pos[1] SUB .add[1]
                ZL3JNT rob_tsk[.rob]: #map_pos[.tsk,.cnt+.off] = .pos[1],.#waf[0],inv_normal,inv_hand
            END
        ELSE
            POINT #map_pos[.tsk,0+.off] = .#waf[.off]
            POINT #map_pos[.tsk,0+.off] = #map_pos[.tsk,0+.off]-#PPOINT(0,0,maps_pos.z[.rob],maps_pos.x[.rob])
            POINT #map_pos[.tsk,1+.off] = #map_pos[.tsk,0+.off]+#PPOINT(0,0,0,vnp.mapdepth[.rob])
            FOR .cnt = 2 TO map_num[.tsk] STEP 1
                IF .cnt == 2 THEN
                    POINT #map_pos[.tsk,.cnt+.off] = #PPOINT(0,0,0,vnp.sfmplast)
                ELSE
                    POINT #map_pos[.tsk,.cnt+.off] = #PPOINT(0,0,0,vnp.sfmppitch)
                END
                POINT #map_pos[.tsk,.cnt+.off] = #map_pos[.tsk,.cnt-1+.off] - #map_pos[.tsk,.cnt+.off]
            END
        END
    END
    IF .slt THEN
        map_pos[.tsk,0] = DEXT(#map_pos[.tsk,0],3)
        map_pos[.tsk,1] = map_pos[.tsk,0] - vnp.mapbelow
        map_pos[.tsk,2] = map_pos[.tsk,0] + vnp.mapabove
    ELSE
        map_pos[.tsk,0] = DEXT(#map_pos[.tsk,0],3)
        map_pos[.tsk,1] = map_pos[.tsk,0] - vnp.mapbelow
        map_pos[.tsk,2] = map_pos[.tsk,0] + vnp.mapabove + (vpp.slot[.prt]-1)*vpp.pitch[.prt]
    END
    FOR .cnt = 0 TO map_num[.tsk] STEP 1
        POINT #map_pos[.tsk,.cnt] = #PPJSET(#map_pos[.tsk,.cnt],3,map_pos[.tsk,0])
    END
.*                                                      /* SCRX11-032-4 m */
;    POINT .#waf[0] = #IFELSE(.tch,#vc.tchpos[.rob,.prt,0],#vc.savpos[.rob,.prt,0])
    POINT .#waf[0] = #IFELSE(.tch,#vc.savpos[.rob,.prt,0],#vc.tchpos[.rob,.prt,0])
    POINT .#waf[0] = .#waf[0]-#PPOINT(0,0,maps_pos.z[.rob],maps_pos.x[.rob])
    map_pos[.tsk,9] = DEXT(.#waf[0],3)
.END
.*******************
.* Mapping Motion **
.*******************
.PROGRAM map_move(.rob,.hnd,.prt,.slt,.slw,.saf)
    .pat = vc.tchpos[.rob,.prt]
    CALL gcpos_cls(.rob)
;** Mapping is for only Horizontal Port
    CALL p_hori(.rob,.rob,.hnd,.prt,.slt,ON)
;
.*                                                      /* SCRX11-032-9 a++ */
    IF SIG(si.map.find[.rob,.hnd]) THEN
        CALL alarm_str(.rob,.rob,0,$ae_map_sta_on)
        RETURN
    END
.*                                                      /* SCRX11-032-9 a-- */
    FOR .cnt = map_num[.rob] TO 1 STEP -1
        CALL speed_set(.rob)
        .off = 10
        IF (.cnt MOD 2 ) THEN
            POINT .#sss = #PPJSET(#map_pos[.rob,.cnt     ],3,map_pos[.rob,1])
            POINT .#eee = #PPJSET(#map_pos[.rob,.cnt+.off],3,map_pos[.rob,2])
        ELSE
            POINT .#sss = #PPJSET(#map_pos[.rob,.cnt+.off],3,map_pos[.rob,2])
            POINT .#eee = #PPJSET(#map_pos[.rob,.cnt     ],3,map_pos[.rob,1])
        END
        IF .cnt == map_num[.rob] THEN
            POINT .#dst = #PPJSET(#gp_home[.rob],3,DEXT(.#sss,3))
.*                                                      /* SCRX12-006 2 am++
            CALL pmove(.rob,.hnd,.pat,gpos_e_home,.#dst,"MAP_HOME_E",.err)
            if .err then
                return
            end
.*                                                      /* SCRX12-006 2 am--
            IF (gpos_e_prep < mapp_path[.pat])AND(path_move[.pat,gpos_e_prep]) THEN
                POINT .#dst = #PPJSET(#gp_prep[.rob],3,DEXT(.#sss,3))
                CALL xmove(.rob,.hnd,.pat,gpos_e_prep,.#dst,"MAP_PREP_E")
            END
            IF (gpos_e_half < mapp_path[.pat])AND(path_move[.pat,gpos_e_half]) THEN
                POINT .#dst = #PPJSET(#gp_half[.rob],3,DEXT(.#sss,3))
                CALL xmove(.rob,.hnd,.pat,gpos_e_half,.#dst,"MAP_HALF_E")
            END
            IF (gpos_e_near < mapp_path[.pat])AND(path_move[.pat,gpos_e_near]) THEN
                POINT .#dst = #PPJSET(#gp_near[.rob],3,DEXT(.#sss,3))
                CALL xmove(.rob,.hnd,.pat,gpos_e_near,.#dst,"MAP_NEAR_E")
            END
        END
        ACCEL mapp_acce[.pat]
        DECEL mapp_acce[.pat]
        POINT .#dst = .#sss
        IF mapp_move[.pat] == mov_jmv THEN
            CALL jmove(.rob,.hnd,gpos_nchg,.#dst,"MAP_SSS")
        ELSEIF mapp_move[.pat] == mov_lmv THEN
            CALL lmove(.rob,.hnd,gpos_nchg,.#dst,"MAP_SSS")
        ELSE
        END
        IF .cnt == 1 THEN
            IF (.slw)OR(vnp.usemapz) THEN
                CALL speed(.rob,MINVAL(now.speed[.rob],spd_map_slow[.rob]),ON)
            ELSE
                CALL speed(.rob,MINVAL(now.speed[.rob],spd_map_nrml[.rob]),ON)
            END
        ELSE
            CALL speed(.rob,MINVAL(now.speed[.rob],spd_map_safe[.rob]),ON)
        END
        CALL map_sens(.rob,.hnd,.#eee)
        IF (.cnt<>1)AND(map_sens.cnt[.rob]) THEN
            CALL alarm_str(.rob,.rob,0,$ae_n_safe_map)
        END
.*                                                      /* SCRX11-032-9 am++ */
        IF tsk_err[.rob] THEN
.*                                                      /* SCRX12-006 m
            .loc_z = DEXT(#DEST:.rob,JT3)
            CALL speed_min(.rob,spd_table[.rob,spd_escape],ON)
            GOTO ext
        END
    END
    .loc_z = DEXT(.#eee,JT3)
    CALL speed_set(.rob)
ext:
    CALL speed_set(.rob)
    IF (gpos_e_near < mapp_path[.pat])AND(path_move[.pat,gpos_r_near]) THEN
        POINT .#dst = #PPJSET(#gp_near[.rob],JT3,.loc_z)
        CALL xmove(.rob,.hnd,.pat,gpos_r_near,.#dst,"MAP_NEAR_R")
    END
    IF (gpos_e_half < mapp_path[.pat])AND(path_move[.pat,gpos_r_half]) THEN
        POINT .#dst = #PPJSET(#gp_half[.rob],JT3,.loc_z)
        CALL xmove(.rob,.hnd,.pat,gpos_r_half,.#dst,"MAP_HALF_R")
    END
    IF (gpos_e_prep < mapp_path[.pat])AND(path_move[.pat,gpos_r_prep]) THEN
        POINT .#dst = #PPJSET(#gp_prep[.rob],JT3,.loc_z)
        CALL xmove(.rob,.hnd,.pat,gpos_r_prep,.#dst,"MAP_PREP_R")
    END
    POINT .#dst = #PPJSET(#gp_home[.rob],JT3,.loc_z)
    CALL xmove(.rob,.hnd,.pat,gpos_r_home,.#dst,"MAP_HOME_R")
;
    CALL gcpos_home(.rob,gpos_home)
    CALL speed_set(.rob)
.*                                                      /* SCRX11-032-9 am-- */
.END

.********************
.* Mapping Sensing **
.********************
.PROGRAM map_sens(.rob,.hnd,.#eee)
.* OUTPUT
.*    map_sens.cnt[R#]
.*    map_sens.pos[R#,#]
.*
.*                                                      /* SCRX11-032-10 a++ */
    CALL Break(.rob)
    TWAIT wait_map_start 
.*                                                      /* SCRX11-032-10 a-- */
.*                                                      /* SCRX11-032-9 a */
    map_sens.cnt[.rob] = 0
    .sig = si.map.find[.rob,.hnd]
    WAIT (ABS(DEXT((#HERE:rob_tsk[.rob]-#DEST:rob_tsk[.rob]),3))<0.1)
    IF SIG(.sig) THEN
        CALL alarm_str(.rob,.rob,0,$ae_map_sta_on)
        RETURN
    END
    HSENSESET .rob = .sig, rob_tsk[.rob]:
    CALL jmove(.rob,.hnd,gpos_nchg,.#eee,"MAP_EEE")
    .cin = 0
    .lst = OFF
Loop:
    IF tsk_err[.rob]        GOTO cin
    HSENSE .rob .sta,.onf,.#loc,.err,.rst
    IF .sta THEN
        IF .onf==.lst THEN
            CALL alarm_str(.rob,.rob,0,$ae_map_on_off)
        ELSE
            map_sens.cnt[.rob] = map_sens.cnt[.rob] + 1
            map_sens.pos[.rob,map_sens.cnt[.rob]] = DEXT(.#loc,3)
            .lst = .onf
        END
.**        IF pz.map_sens THEN
.**            IF .onf THEN
.**                TYPE map_sens.cnt[.rob],"  ON ",map_sens.pos[.rob,map_sens.cnt[.rob]]
.**            ELSE
.**                TYPE map_sens.cnt[.rob],"  OFF",map_sens.pos[.rob,map_sens.cnt[.rob]]
.**            END
.**        END
    END
    IF .rst        GOTO Loop
cin:
    TWAIT 0.1
    IF (ABS(DEXT((#HERE:rob_tsk[.rob]-#DEST:rob_tsk[.rob]),3))<0.1) THEN
        .cin = .cin + 1
    END
    IF .cin < 3                GOTO Loop
    HSENSESET .rob = 0
;*
    IF SIG(.sig) THEN
        CALL alarm_str(.rob,.rob,0,$ae_map_end_on)
    END
.END
.**************************
.** Mapping Data Combine **
.**************************
.PROGRAM map_comb(.rob)
    .waf = 0
    .upp = 0
    FOR .cnt = 1 TO map_sens.cnt[.rob] STEP 1
        IF (.cnt MOD 2 ) THEN
            IF (.cnt == 1)OR(.upp>0) THEN
                .waf = .waf + 1
                .low = map_sens.pos[.rob,.cnt]
                .upp = 0
.**                IF pz.map_comb THEN
.**                    TYPE /I2,.cnt," ",.waf," ON  :",/G10.3,map_sens.pos[.rob,.cnt]
.**                END
            ELSE
.**                IF pz.map_comb THEN
.**                    TYPE /I2,.cnt," ",/x2, " xxx :",/G10.3,map_sens.pos[.rob,.cnt]
.**                END
            END
        END
        .cnt = .cnt + 1
        .upp = map_sens.pos[.rob,.cnt]
        .off = 0
        IF .cnt <> map_sens.cnt[.rob] THEN
            .off = ABS(.upp-map_sens.pos[.rob,.cnt+1])
            IF (.off<vnp.mapcomb) THEN
                .upp = 0
            END
        END
.**        IF pz.map_comb THEN
.**            IF .upp > 0 THEN
.**                TYPE /I2,.cnt," ",.waf," OFF :",/G10.3,map_sens.pos[.rob,.cnt],.off
.**            ELSE
.**                TYPE /I2,.cnt," ",/x2 ," xxx :",/G10.3,map_sens.pos[.rob,.cnt],.off
.**            END
.**        END
        map_waf.low[.rob,.waf] = .low
        map_waf.upp[.rob,.waf] = .upp
    END
    map_waf.cnt[.rob] = .waf
.END
.**************************
.** Mapping Data Analyze **
.**************************
.PROGRAM map_ana(.rob,.hnd,.prt,.i_s,.slw,.saf)
.**    IF pz.map_data THEN
.**        FOR .waf = 1 TO map_waf.cnt[.rob] STEP 1
.**            .lo = map_waf.low[.rob,.waf]
.**            .hi = map_waf.upp[.rob,.waf]
.**            .tt = .hi-.lo
.**            .pp = .lo+(.tt/2)-map_pos[.rob,9]
.**            TYPE "Wafer:",/I3,.waf," Pos:",/F8.3,.pp," Thick:",.tt," LO:",.lo," HI:",.hi
.**        END
.**    END
    .waf = 1
    IF (.i_s) THEN
        CALL map_ana.res(.rob,.prt,.i_s,0,map_absent)
    ELSE
        FOR .slt = 1 TO vpp.slot[.prt] STEP 1
            CALL map_ana.res(.rob,.prt,.slt,0,map_absent)
        END
    END
    FOR .slt = 1 TO vpp.slot[.prt] STEP 1
waf:
        IF map_waf.cnt[.rob] < .waf        GOTO nxt
.*                                                      /* SCRX11-032-5 m */
        .slot_h = (.slt-1)*vpp.pitch[.prt]+vnp.wafthick/2        ;***.sltにおける位置
        .next_h = .slot_h + vpp.pitch[.prt]        ;***.sltの次のスロットにおけん位置
        .ttt = map_waf.upp[.rob,.waf]-map_waf.low[.rob,.waf]
        .mid = map_waf.low[.rob,.waf]+(.ttt)/2-map_pos[.rob,9]
        IF (.mid>=(.next_h-vnp.mapzlim))AND(vpp.slot[.prt]<>1)                GOTO nxt        ;**次SLOTの手前にWaferなし
        IF (ABS(.mid-.slot_h)<vnp.mapzlim) THEN
.*                                                      /* SCRX11-066-2 m
            IF (.ttt<=vnp.wafthick*vnp.dblfactor) THEN
                CALL map_ana.res(.rob,.prt,.slt,.waf,map_present)
            ELSEIF (.ttt>vnp.mapwaf3x) THEN
                CALL map_ana.res(.rob,.prt,.slt,.waf,map_cross3w)
            ELSE
                CALL map_ana.res(.rob,.prt,.slt,.waf,map_double1)
            END
.*                                                      /* SCRX11-066-2 m
        ELSEIF (.ttt<=vnp.wafthick*vnp.crsfactor) THEN
            CALL map_ana.res(.rob,.prt,.slt,.waf,map_unknown)
        ELSE
            CALL map_ana.res(.rob,.prt,.slt,.waf,map_crossed)
        END
        .waf = .waf + 1
        GOTO waf
nxt:
    END
    IF .i_s == 0 THEN
        CALL map_recal(.rob,.prt,.i_s)
    END
.END
.*
.PROGRAM map_recal(.rob,.prt,.i_s)
    .min = 9999
    FOR .slt = 1 TO vpp.slot[.prt] STEP 1
        IF map[.prt,.slt,map_res] == map_present THEN
            .min = MINVAL(.min,map[.prt,.slt,map_ttt])
        END
    END
    IF .min > 9000 THEN
        .min = 0
    END
    TYPE "MADA EV_MAP_THICK"
;**    IF (vnp.wafthick*1.75)<=.min THEN
;**        CALL event_rob(.rob,$EV_MAP_THICK[.rob])
;**        CALL event_out(.rob)
;**    END
    IF (vnp.maprecal)AND(.min) THEN
        FOR .slt = 1 TO vpp.slot[.prt] STEP 1
            IF (map[.prt,.slt,map_res]==map_present)OR(map[.prt,.slt,map_res]==map_double1) THEN
.**                IF pz.map_ana OR pz.maprecal THEN
.**                    IF (map[.prt,.slt,map_res]==map_present) THEN
.**                        TYPE /S,"Wafer Present-->"
.**                    ELSEIF (map[.prt,.slt,map_res]==map_double1) THEN
.**                        TYPE /S,"Wafer Double1-->"
.**                    END
.**                END
                IF (.min+vnp.maprecal)<=map[.prt,.slt,map_ttt] THEN
                    map[.prt,.slt,map_res] = map_double1
.**                    TYPE "Double1 ",.slt
                ELSE
                    map[.prt,.slt,map_res] = map_present
.**                    TYPE "Present ",.slt
                END
            END
        END
    END
.END
.******************************************
.** map_dat.as
.******************************************
.REALS
    map_absent  =  0
    map_present =  1
    map_crossed =  2
    map_double1 =  3
    map_unknown =  4
    map_double2 = -3
    map_cross3w = -2
    map_notdone = -4

    map_res = 1		;** Mapping Result
    map_low = 2		;** Wafer Low Position
    map_upp = 3		;** Wafer Upper Position
    map_mid = 4		;** Wafer Center Position
    map_acc = 5		;** Wafer Access Position
    map_ttt = 6		;** Wafer Thickness
    map_gap = 7		;** Wafer GAP(Pitch between under wafer
    qrymap_len = 220
.END

.PROGRAM  map_dat.init(.prt)
    FOR .slt = 1 TO vpp.slot[.prt] STEP 1
	map[.prt,.slt,map_res] = map_notdone
	FOR .qry = 2 TO 7 STEP 1
	    map[.prt,.slt,.qry] = 0
	END
    END
.END

.PROGRAM map_err(.tsk,.prt,.slt)
    tsk_err[.tsk] = ON
    IF .slt THEN
	CALL map_ana.res(.dum,.prt,.slt,.dum,map_notdone)
    ELSE
	CALL map_dat.init(.prt)
    END
    CALL map_res(.tsk,.prt,.slt)
.END

.PROGRAM map_res(.tsk,.prt,.slt)
    $snd_res[.tsk] = $IFELSE(tsk_err[.tsk],$res_ng,$res_ok)+":"
    IF .slt THEN
	$snd_res[.tsk] = $snd_res[.tsk]+$TRIM_B($ENCODE(/I,ABS(map[.prt,.slt,map_res])))
    ELSE
	FOR .cnt = 1 TO vpp.slot[.prt] STEP 1
	    IF .cnt <> 1 THEN
		$snd_res[.tsk] = $snd_res[.tsk]+","
	    END
	    $snd_res[.tsk] = $snd_res[.tsk]+$TRIM_B($ENCODE(/I,ABS(map[.prt,.cnt,map_res])))
	END
    END
.END

.PROGRAM map_ana.res(.rob,.prt,.slt,.waf,.ana)
    CASE .ana OF
      VALUE map_absent  :
	map[.prt,.slt,map_res] = .ana
	map[.prt,.slt,map_low] = 0
	map[.prt,.slt,map_upp] = 0
      VALUE map_present :
	IF map[.prt,.slt,map_res] == map_absent THEN
	    map[.prt,.slt,map_res] = .ana
	    map[.prt,.slt,map_low] = map_waf.low[.rob,.waf]
	    map[.prt,.slt,map_upp] = map_waf.upp[.rob,.waf]
	    IF pz.map_ana THEN
		TYPE "Wafer Present:",.slt,map[.prt,.slt,map_low],map[.prt,.slt,map_upp]
	    END
	ELSEIF map[.prt,.slt,map_res] <> map_crossed THEN
	    map[.prt,.slt,map_res] = map_double2
	    map[.prt,.slt,map_upp] = map_waf.upp[.rob,.waf]
	    IF pz.map_ana THEN
		TYPE "Wafer Double2:",.slt,map[.prt,.slt,map_low],map[.prt,.slt,map_upp]
	    END
	END
      VALUE map_crossed :
	IF 1 THEN
	    map[.prt,.slt,map_res] = .ana
	    map[.prt,.slt,map_low] = 0
	    map[.prt,.slt,map_upp] = 0
	    IF pz.map_ana THEN
		TYPE "Wafer Crossed:",.slt
	    END
	END
	IF .slt<vpp.slot[.prt] THEN
	    map[.prt,.slt+1,map_res] = .ana
	    map[.prt,.slt+1,map_low] = 0
	    map[.prt,.slt+1,map_upp] = 0
	    IF pz.map_ana THEN
		TYPE "Wafer Crossed:",.slt+1
	    END
	END
      VALUE map_double1 :
	IF map[.prt,.slt,map_res] == map_absent THEN
	    map[.prt,.slt,map_res] = .ana
	    map[.prt,.slt,map_low] = map_waf.low[.rob,.waf]
	    map[.prt,.slt,map_upp] = map_waf.upp[.rob,.waf]
	    IF pz.map_ana THEN
		TYPE "Wafer Double1:",.slt,map[.prt,.slt,map_low],map[.prt,.slt,map_upp]
	    END
	END
      VALUE map_unknown :
	map[.prt,.slt,map_res] = .ana
	map[.prt,.slt,map_low] = map_waf.low[.rob,.waf]
	map[.prt,.slt,map_upp] = map_waf.upp[.rob,.waf]
	IF pz.map_ana THEN
	    TYPE "Wafer Unknown:",.slt,map[.prt,.slt,map_low],map[.prt,.slt,map_upp]
	END
      VALUE map_cross3w :	;** "2" 3段クロス
	IF 1<.slt THEN
	    map[.prt,.slt-1,map_res] = .ana
	    map[.prt,.slt-1,map_low] = 0
	    map[.prt,.slt-1,map_upp] = 0
	    IF pz.map_ana THEN
		TYPE "Wafer Cross3w:",.slt-1
	    END
	END
	IF 1 THEN
	    map[.prt,.slt,map_res] = .ana
	    map[.prt,.slt,map_low] = 0
	    map[.prt,.slt,map_upp] = 0
	    IF pz.map_ana THEN
		TYPE "Wafer Cross3w:",.slt
	    END
	END
	IF .slt<vpp.slot[.prt] THEN
	    map[.prt,.slt+1,map_res] = .ana
	    map[.prt,.slt+1,map_low] = 0
	    map[.prt,.slt+1,map_upp] = 0
	    IF pz.map_ana THEN
		TYPE "Wafer Cross3w:",.slt+1
	    END
	END
      ANY :
;**   VALUE map_notdone :
;**   VALUE map_double2 :
	map[.prt,.slt,map_res] = .ana
	map[.prt,.slt,map_low] = 0
	map[.prt,.slt,map_upp] = 0
    END
    IF map[.prt,.slt,map_low] AND map[.prt,.slt,map_upp] THEN
	map[.prt,.slt,map_acc] =  map[.prt,.slt,map_low] + maps_pos.z[.rob]
	map[.prt,.slt,map_ttt] =  map[.prt,.slt,map_upp] - map[.prt,.slt,map_low]
	map[.prt,.slt,map_mid] = (map[.prt,.slt,map_upp] + map[.prt,.slt,map_low])/2
    ELSE
	map[.prt,.slt,map_acc] = 0
	map[.prt,.slt,map_ttt] = 0
	map[.prt,.slt,map_mid] = 0
    END
    map[.prt,.slt,map_gap] = 0
    IF (map[.prt,.slt,map_res]==map_present)AND(.slt<>1) THEN
	IF map[.prt,.slt-1,map_mid] THEN
	    map[.prt,.slt,map_gap] = map[.prt,.slt,map_mid] - map[.prt,.slt-1,map_mid]
	END
    END
.END
.******************************************
.** map_qry.as
.******************************************
.PROGRAM qrymap(.tsk,.typ,.one)
    .hst = tsk_hst[.tsk]
    IF $hst_old[.hst]==$hst_now[.hst] THEN
        .rem = ON
        IF qrymap.cnt[.hst]        GOTO ack
    END
    .rem = OFF
    .cnt = 1
    $qrymap[.hst,.cnt] = ""
;
    IF (tsk_num[.tsk]<>1)        GOTO efm
    IF .one THEN
        CALL id_port_slot(.tsk,1,d_prt1,d_slt1)
        IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])        GOTO epm
    ELSE
        CALL id_port(.tsk,1,d_prt1)
        IF NOT (tsk_dat[.tsk,d_prt1])                                GOTO epm
        tsk_dat[.tsk,d_slt1] = 0
    END
ack:
    CALL send_ack(.tsk)
    .prt = tsk_dat[.tsk,d_prt1]
    IF NOT .rem THEN
        .sss = IFELSE(.one,tsk_dat[.tsk,d_slt1],1)
        .eee = IFELSE(.one,tsk_dat[.tsk,d_slt1],vpp.slot[.prt])
        .rat = IFELSE(.typ==map_res,1,100)
        FOR .slt = .sss TO .eee STEP 1
            $str[.tsk] = $ENCODE("map[",.prt,",",.slt,",",.typ,"]")
            IF EXISTDATA($str[.tsk],r) THEN
                .dat = map[.prt,.slt,.typ]
            ELSE
                .dat = IFELSE(.typ==map_res,4,0)
            END
            CALL int_str(.dat,$str[.tsk],.rat)
            IF (LEN($qrymap[.hst,.cnt])+LEN($str[.tsk])+1) > qrymap_len THEN
                .cnt = .cnt + 1
                $qrymap[.hst,.cnt] = ""
            END
            IF $qrymap[.hst,.cnt] <> "" THEN
                $qrymap[.hst,.cnt] = $qrymap[.hst,.cnt] + ","
            END
            $qrymap[.hst,.cnt] = $qrymap[.hst,.cnt] + $str[.tsk]
        END
        qrymap.cnt[.hst] = .cnt
        qrymap.max[.hst] = .cnt
    END
    qrymap.cnt[.hst] = qrymap.cnt[.hst] - 1
    $snd_res[.tsk] = $qrymap[.hst,qrymap.max[.hst]-qrymap.cnt[.hst]]
    IF NOT((.typ == map_res)OR(.one)) THEN
        CALL int_str(qrymap.cnt[.hst],$str[.tsk],1)
        $snd_res[.tsk] = $str[.tsk] + ":" + $snd_res[.tsk]
    END
    $snd_res[.tsk] = $res + "," + $snd_res[.tsk]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
.*                                                      /* SCRX11-032-11 a++ */
;*  tsk_dat[] will return 0 for station A1.
    IF .one  AND  NOT (tsk_dat[.tsk,d_prt1]) THEN
;*      QryMapSlotZPos return -10000 for A1 only.
;*      To return -10000 for all QryMapXXX commands (i.e. QryMapSlotZPos, QryMapThick, QryMapPitch, and QryMapZPos for A1,
;*        remove .one in above IF statement.
        $snd_ack[.tsk] = $ack_ok
        twait wait_map_ack
        $qrymap[.hst,0] = ",-10000"
        $snd_res[.tsk] = $res+$qrymap[.hst,0]
    ELSE
.*                                                      /* SCRX11-032-11 a-- */
        $snd_ack[.tsk] = $ack_err_pm
    END
    RETURN
.END
.******************************************
.** movpre.as
.******************************************
.PROGRAM movprepos_cmd(.tsk)
    IF (tsk_num[.tsk]<>2)	GOTO efm
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
    CALL id_port_slot(.tsk,2,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
.** harish 090527 a++
    IF vc.mode[.tsk] == mode_soft
	CALL send_ack(.tsk)
	$alm_put[.tsk] = $AE_MD_THIS_SOFT
	CALL alarm_put(.tsk,0,0)
	CALL make_res_ng(.tsk)
	RETURN
    END
.** harish 090527 a--
    tsk_mod[.tsk] = m_tech
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM movprepos_rob(.rob)
    CALL homing(.rob)
    IF tsk_err[.rob]		RETURN
    .hnd = tsk_dat[.rob,d_hnd1]
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
    .pat = vpp.portarea[.prt]
    IF NOT .pat THEN
	CALL alarm_str(.rob,.rob,0,$ae_no_area)
	RETURN
    END
    .zzz = vpp.portz[.prt]
    IF .zzz < -999 THEN
	.zzz = DEXT(#DEST:rob_tsk[.rob],3)
    END
    CALL gcpos_cls(.rob)
    POINT .#dst = #PPJSET(#ref_home[.pat],3,.zzz)
    ACCEL 30
    DECEL 30
.*                                                      /* SCRX12-006 2 am++
    CALL pmove(.rob,.hnd,.pat,0,.#dst,"MOVPREPOS H",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am--
    POINT .#dst = #PPJSET(#ref_mpr1[.pat],3,.zzz)
    ACCEL 30
    DECEL 30
    CALL jmove(.rob,.hnd,gpos_nchg,.#dst,"MOVPREPOS 1")
    POINT .#dst = #PPJSET(#ref_mpr2[.pat],3,.zzz)
    ACCEL 30
    DECEL 30
    CALL jmove(.rob,.hnd,gpos_nchg,.#dst,"MOVPREPOS 2")
.** harish 081010 a++
    POINT .#dst = #PPJSET(#ref_mpr3[.pat],3,.zzz)
    ACCEL 30
    DECEL 30
    CALL jmove(.rob,.hnd,gpos_nchg,.#dst,"MOVPREPOS 3")
.** harish 081010 a--
.END
.** harish 090515 a++
.********************************************************
.*
.* File Name : scan.as
.*
.* Function  :
.*
.* Owner     :
.*
.* History   :
.*
.********************************************************
.PROGRAM scan_cmd(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>3)	GOTO efm
.** Scan type check
    SCASE $TOUPPER($tsk_dat[.tsk,1]) OF
      SVALUE "IMAP"  :
	GOTO nxt
      ANY :
	GOTO epm
    END
nxt:
.** Check RobotID:HandID
    CALL id_robot_hand(.tsk,2,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1])	GOTO epm
.** Check PortID:Slot#
    CALL id_port_slot(.tsk,3,d_prt1,d_slt1)
    IF NOT (tsk_dat[.tsk,d_prt1] AND tsk_dat[.tsk,d_slt1])				GOTO epm
    IF tsk_dat[.tsk,d_slt1] <> 1							GOTO epm
    $tsk_cmd[.tsk] = $tsk_cmd[.tsk]+"_"+$TOUPPER($tsk_dat[.tsk,1])
    tsk_cmd[.tsk]  = bit[tsk_dat[.tsk,d_rob1]]
    tsk_mod[.tsk]  = m_auto
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM scan_imap_rob(.rob,.bas)
    .hnd = tsk_dat[.rob,d_hnd1]
    .prt = tsk_dat[.rob,d_prt1]
    .slt = tsk_dat[.rob,d_slt1]
    .pat = vc.tchpos[.rob,.prt]

    IF (port_type[0,.prt] BAND port_util)AND(tsk_arm[rob_tsk[.rob]] == arm_nt410) THEN
	IF (vpp.isimap[.prt])	GOTO imap
    END
    CALL alarm_str(.rob,.rob,0,$ae_cannot_imap)
    GOTO err
imap:
    IF (g.base[.rob]<>.bas)OR(g.hand[.rob]<>.hnd)OR(g.port[.rob]<>.prt)OR(g.slot[.rob]<>.slt)	GOTO escp
    IF (gpos_e_extd < g.cpos[.rob])	GOTO escp
    .sta = g.cpos[.rob]
    GOTO move
escp:
    IF g.base[.rob]<>gbas_home THEN
	CALL homing(.rob)
	IF tsk_err[.rob]	RETURN
    END
    .sta = gpos_e_home
Move:
    CALL is_taught(.rob,.rob,.prt,.slt,ON)
    IF is_taught[.rob] <= 0 THEN
	CALL alarm_str(.rob,.rob,0,$ae_n_taught)
	RETURN
    END
    IF (.slt<>1)AND(vpp.pitch[.prt]==0) THEN
	CALL alarm_str(.rob,.rob,0,$ae_pitch_zero)
	RETURN
    END
    CALL gcpos_set(.rob,.bas,.hnd,.prt,.slt,.sta)
    $ev1_inf[.rob] = $vrp.rob_asso[.rob]
    $ev2_inf[.rob] = $vrp.rob_asso[.rob]+$ENCODE(/I1,":B",.hnd)+","+$vpp.portasso[.prt]+":"+$TRIM_B($ENCODE(/I,.slt))+","+$tsk_cmd[.rob]
    CALL imap_hori(.rob,.rob,.hnd,.prt,.slt,.sta,gpos_r_home,.bas)
err:
    RETURN
.END
.PROGRAM imap_hori(.tsk,.rob,.hnd,.prt,.slt,.sta,.dst,.bas)
    .fng = tsk_dat[.tsk,d_fng1]
    CALL p_hori(.tsk,.rob,.hnd,.prt,.slt,ON)
    .pat = vc.tchpos[.rob,.prt]
    .pos = .sta
    .llf = 0
    .ulf = 0
Loop:
    CASE .pos OF
      VALUE gpos_e_home
	IF NOT ((.bas==gbas_putp)AND(.dst==gpos_e_home)) THEN
	    CALL gripper(.tsk,.hnd,tsk_dat[.tsk,d_fng1],grip_pres,who_put,pres_hori)
	    IF grip_err[.tsk]
		CALL gcpos_home(.rob,gpos_home)
		RETURN
	    END
	END
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,.ulf)
.*                                                      /* SCRX12-006 2 am++
	CALL pmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_E_HOME",.err)
    if .err then
        return
    end
.*                                                      /* SCRX12-006 2 am--
      VALUE gpos_e_prep
	IF NOT path_move[.pat,gpos_e_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,.ulf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_E_PREP")
      VALUE gpos_e_half
	IF NOT path_move[.pat,gpos_e_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,.ulf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_E_HALF")
      VALUE gpos_e_near
	IF NOT path_move[.pat,gpos_e_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,.ulf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_E_NEAR")
      VALUE gpos_e_extd
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	CALL speed_min(.rob,vnp.mxinvel,OFF)
	POINT .#dst = #gp_putt[.tsk] + #PPOINT(0,0,.ulf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_E_EXTD")
      VALUE gpos_e_tech
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	CALL speed_min(.rob,vnp.mxinvel,OFF)
	POINT .#dst = #gp_putt[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_E_TECH")
.** SCRX10-027-1 a++
      VALUE gpos_r_tech
        IF (.dst == gpos_r_tech)
	    CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	    IF grip_err[.tsk]	GOTO gerr
	    CALL speed_min(.rob,vnp.mxinvel,OFF)
	    POINT .#dst = #gp_tech[.tsk]
	    CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_TECH")
	ELSE
	    GOTO next
	END
.** SCRX10-027-1 a++
      VALUE gpos_r_slow
	IF NOT path_move[.pat,gpos_r_slow]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])
	POINT .#dst = #gp_pfng[.tsk]
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_SLOW")
      VALUE gpos_r_extd
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	CALL speed_rate(.rob,vpp.pzspd[.prt]*zspd_rate[.rob])
	POINT .#dst = #gp_poff[.tsk] + #PPOINT(0,0,.llf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_EXTD")
      VALUE gpos_r_back
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
	POINT .#dst = #gp_back[.tsk] + #PPOINT(0,0,.llf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_BACK")
      VALUE gpos_r_near
	IF NOT path_move[.pat,gpos_r_near]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	POINT .#dst = #gp_near[.tsk] + #PPOINT(0,0,.llf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_NEAR")
      VALUE gpos_r_half
	IF NOT path_move[.pat,gpos_r_half]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	CALL speed_min(.rob,vnp.mxoutvel,OFF)
	POINT .#dst = #gp_half[.tsk] + #PPOINT(0,0,.llf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_HALF")
      VALUE gpos_r_prep
	IF NOT path_move[.pat,gpos_r_prep]	GOTO next
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	POINT .#dst = #gp_prep[.tsk] + #PPOINT(0,0,.llf)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_PREP")
      VALUE gpos_r_home
	POINT .#dst = #gp_home[.tsk] + #PPOINT(0,0,.llf)
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_put,pres_hori)
	CALL xmove(.tsk,.hnd,.pat,.pos,.#dst,"IMAP_R_HOME")
.** harish 090627 m
	CALL gripper(.tsk,.hnd,.fng,grip_pres,who_get,pres_hori)
	IF grip_err[.tsk]	GOTO gerr
      ANY :
next:
	.pos = .pos + 1
	GOTO Loop
    END
    IF .pos < .dst THEN
	.pos = .pos + 1
	GOTO Loop
    END
    RETURN
gerr:
;**    CALL alarm_str(.rob,.rob,.hnd,$grip_err[.rob])
err:
    IF vpp.errret[.prt] THEN
	CALL escape(.rob)
    END
.END
.** harish 090515 a--
.******************************************
.** algn.as
.******************************************
.REALS
    algn_pos[3] = NONE
    algn_ang[3] = NONE
    algn_rob[3] = 0

    notch_home = 180
.END
.PROGRAM algn_cmd(.tsk)
    IF (tsk_num[.tsk]<>1)		GOTO efm
    STR_2DIV $tsk_dat[.tsk,d_divp],tsk_div[.tsk] = $tsk_dat[.tsk,1],":"
    IF tsk_div[.tsk]<>2			GOTO efm
    CALL id_aligner(.tsk,d_divp,d_rob1)
    IF NOT (tsk_dat[.tsk,d_rob1])	GOTO epm
    CALL id_int(.tsk,d_divp+1,d_dat1,100)
    IF id_error[.tsk]			GOTO epm
    tsk_mod[.tsk] = m_auto
    tsk_cmd[.tsk] = bit[tsk_dat[.tsk,d_rob1]]
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM algn_rob(.rob)
    ONI/LVL -si.aln.vacm[.rob]	GOTO no_vac
    wafoff.valid[.rob] = 0
    CALL vacuum(.rob,.rob,0,grip_pres,who_ext,vnp.toutavac[.rob])
    IF tsk_err[.rob]	GOTO err
    .pos = DEXT(#DEST:rob_tsk[.rob],1)
;
    CALL algn_lin(.rob,.pos)
;
    IF tsk_err[.rob]	GOTO err
    .now = DEXT(#DEST:rob_tsk[.rob],1)
;
    FOR .i = 1 TO 10 STEP 1
	IF (.now-.pos)>180 THEN
	    .pos = .pos + 360
	ELSEIF (.now-.pos)<-180 THEN
	    .pos = .pos - 360
	ELSE
	    .i = 999
	END
    END
    FOR .i = 1 TO 3 STEP 1
	IF .pos>720 THEN
	    .pos = .pos - 360
	ELSEIF .pos < -720 THEN
	    .pos = .pos + 360
	ELSE
	    .i = 999
	END
    END
    POINT .#loc = #PPOINT(.pos)
    SPEED vnp.spdalnwaf
    CALL jmove(.rob,0,gpos_nchg,.#loc,"ALGN-->NOTCH")
    algn_rob[.rob] = 0
    algn_pos[.rob] = 0
    algn_ang[.rob] = tsk_dat[.rob,d_dat1]
    BREAK
    RETURN
no_vac:
    IGNORE si.aln.vacm[.rob]
    $alm_tmp[.rob] = $REPLACE($ae_algn_vac[.rob],"%R",$vrp.rob_asso[.rob],-1)
    CALL alarm_str(.rob,.rob,0,$alm_tmp[.rob])
err:
    algn_rob[.rob] = 0
    algn_pos[.rob] = none
    algn_rob[.rob] = none
    wafoff.valid[.rob] = 0
.END
.PROGRAM algn_lin(.rob,.pos)
    ONI/LVL -si.aln.vacm[.rob]	GOTO no_vac
.* ラインセンサアライナプログラム
.* アライナ実行時、モニタ速度は、１００％でないとダメ
.* テストした速度は、20,25,30,35の３つのみ
    .move_limit = 990
    .enc_adjust = 32
    .ignore_data = 1000
    .trigger3 = -1
    .level = 0.10
    .rough_p = 0
    .ave = 1
.*計測個数とトリガ値の設定
    IF vnp.spdlalgn <= 20+0.1 THEN
	.drive_deg =  530
	.mesure_num = 18000
	.trigger = 0.04
	.sp = 0.035
	IF vnp.wafer == wafer_qz THEN
	    .trigger  = 0.012
	    .trigger3 = 0.024
	    .level = 0.03
	    .rough_p = 100
	    .ave = 3
	END
    ELSEIF vnp.spdlalgn <= 25+0.1 THEN
	.drive_deg = 565
	.mesure_num = 15000
	.trigger = 0.05
	.sp = 0.042
	IF vnp.wafer == wafer_qz THEN
	    .trigger  = 0.015
	    .trigger3 = 0.03
	    .level = 0.03
	    .rough_p = 100
	    .ave = 3
	END
    ELSEIF vnp.spdlalgn <= 30+0.1 THEN
	.drive_deg = 610
	.mesure_num = 14000
	.trigger = 0.06
	.sp = 0.052
	IF vnp.wafer == wafer_qz THEN
	    .trigger  = 0.018
	    .trigger3 = 0.036
	    .level = 0.03
	    .rough_p = 100
	    .ave = 3
	END
    ELSE
	.drive_deg = 640
	.mesure_num = 13000
	.trigger = 0.07
	.sp = 0.059
	IF vnp.wafer == wafer_qz THEN
	    .trigger  = 0.020
	    .trigger3 = 0.040
	    .level = 0.03
	    .rough_p = 100
	    .ave = 3
	END
    END
    .now = DEXT(#DEST:rob_tsk[.rob],1)
    IF .now < 0.0 THEN
	.direct = 1
    ELSE
	.direct = -1
    END
    LSENSESET rob_tsk[.rob],1,.mesure_num,dz.algn
    POINT .#loc = #PPOINT(.now+.drive_deg*.direct)
    SPEED vnp.spdlalgn
    CALL jmove(.rob,0,gpos_nchg,.#loc,"ALGN-->LSENSE")
    BREAK
    IF SYSDATA(ZSIMENV) OR tz.algn THEN
	CALL lsens_dum(.pos,.err,"First")
    ELSE
	LSENSE .pos,.err,.ave,.sp,-1,.trigger,-1,.trigger3,0,10,.enc_adjust,.ignore_data,-1,.level,.rough_p,0,-1
    END
    IF (.err==-3)AND(vnp.wafer==wafer_qz) THEN
	IF SYSDATA(ZSIMENV)OR tz.algn THEN
	    CALL lsens_dum(.pos,.err,"Recal for Quartz")
	ELSE
	    IF pz.algn THEN
		TYPE "RECAL for Quartz"
	    END
	    LSENSE .pos,.err,.ave,.sp,-1,.trigger,-1,.trigger3,0,10,.enc_adjust,.ignore_data,-1,.level,0,0,-1
	END
    END
    CASE .err OF
      VALUE  0:
	$alm_tmp[.rob] = ""
      VALUE -1:
	$alm_tmp[.rob] = $AE_LALIGN01[.rob]
      VALUE -2:
	$alm_tmp[.rob] = $AE_LALIGN02[.rob]
      VALUE -3:
	$alm_tmp[.rob] = $AE_LALIGN03[.rob]
      VALUE -4:
	$alm_tmp[.rob] = $AE_LALIGN04[.rob]
      VALUE -5:
	$alm_tmp[.rob] = $AE_LALIGN05[.rob]
      ANY :
	$alm_tmp[.rob] = $AE_LALIGNXX[.rob]+"(CODE:"+$TRIM_B($ENCODE(.err))+")"
    END
    IF $alm_tmp[.rob]<>"" THEN
	$alm_tmp[.rob] = $REPLACE($alm_tmp[.rob],"%R",$vrp.rob_asso[.rob],-1)
	GOTO err
    END
    GETDCEN .offang,.offdst,.valid
    IF .valid == 0 THEN
	.angle = .now - .offang + vnp.alnpos[.rob]
	wafoff.off_x[.rob] = -(.offdst * sin(.angle))
	wafoff.off_y[.rob] =  (.offdst * cos(.angle))
	wafoff.valid[.rob] = 1
    ELSE
	wafoff.valid[.rob] = 0
    END
    RETURN
err:
    CALL alarm_str(.rob,.rob,0,$alm_tmp[.rob])
    RETURN
no_vac:
    IGNORE si.aln.vacm[.rob]
    $alm_tmp[.rob] = $REPLACE($ae_algn_vac[.rob],"%R",$vrp.rob_asso[.rob],-1)
    CALL alarm_str(.rob,.rob,0,$alm_tmp[.rob])
.END
.PROGRAM lsens_dum(.pos,.err,.$str)
    IF tz.algn THEN
	PROMPT "Input Notch Position:",.$key
	.pos = VAL(.$key)
	PROMPT "Input Error Code:",.$key
	.err = VAL(.$key)
    ELSE
	.pos = 0
	.err = 0
    END
.END


.PROGRAM angle_rob(.rob)
    .now = DEXT(#DEST:rob_tsk[.rob],1)
    .dst = tsk_dat[.rob,d_dat1]+vnp.alnpos[.rob]+algn_pos[.rob]+algn_ang[.rob]+notch_home
    FOR .i = 1 TO 10 STEP 1
	IF (.now-.dst)>180 THEN
	    .dst = .dst + 360
	ELSEIF (.now-.dst)<-180 THEN
	    .dst = .dst - 360
	ELSE
	    .i = 999
	END
    END
    FOR .i = 1 TO 3 STEP 1
	IF .dst > 720 THEN
	    .dst = .dst - 360
	ELSEIF .dst < -720 THEN
	    .dst = .dst + 360
	ELSE
	    .i = 999
	END
    END
    POINT .#loc = #PPOINT(.dst)
    speed vnp.spdalnwaf
    CALL jmove(.rob,0,gpos_nchg,.#loc,"GET-->NOTCH")
    CALL jmove(.rob,0,gpos_nchg,.#loc,"ALGN-->LSENSE")
    BREAK
.END
.******************************************
.** ver_cmd.as
.******************************************
.REALS
    docver = 200
.END
.PROGRAM ver_init
    $ver_file = "AM"+$ver_app+$ver_mdl+$ver_rot+"."+$IFELSE($ver_tst<>"",$ver_tst,"apl")
    SETAPPLIVER $ver_file
    IF SYSDATA(ZSIMENV) THEN
	Z_TYAMA ON
    END
;** Make a response to VER command
;** Software Version ***
    $ver_assys = $REPLACE($TRIM_B($TOUPPER($STR_ID(2        ))),"UAS","",-1)
    $ver_srvif = $REPLACE($TRIM_B($TOUPPER($SYSDATA(SRVIF.ID))),"SRVIF-","",-1)
    $ver_armif = $REPLACE($TRIM_B($TOUPPER($SYSDATA(ARMIF.ID))),"ARMID-","",-1)
    $ver_npcom = $REPLACE($TRIM_B($TOUPPER($SYSDATA(COM.ID  ))),"NPCOM-","",-1)
    $ver_soft = "S"+$ver_assys+"-A"+$ver_app+$ver_mdl+$ver_rot+$ver_tst+"-V"+$ver_srvif+"-M"+$ver_armif+"-C"+$ver_npcom
;** Hardware Version ***
    $ver_contn = $TRIM_B($ENCODE(SYSDATA(ZCONT.MGFNO,1)))
    $ver_ymamp = ""
    FOR .rob = 1 TO rob_tsk[0] STEP 1
	$ver_ymamp=$ver_ymamp+"/"
	FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
	    .$tmp = "*"
	    .jnt = id_anum[.rob,.cnt]
	    IF rob_tsk[.rob] THEN
		IF SYSDATA(JTEXIST,rob_tsk[.rob],.jnt) THEN
		    .$tmp=$RIGHT($SYSDATA(YM.ID,rob_tsk[.rob],.jnt),1)
		END
		.$tmp = $IFELSE(.$tmp=="","*",.$tmp)
	    END
	    $ver_ymamp=$ver_ymamp+.$tmp
	END
    END
    $ver_hard = "SN"+$ver_contn
    $ver  = $ver_fic+","+$ver_soft+","+$ver_hard
    $verz = $ver+"-AMP"+$ver_ymamp
    TYPE "Ver :",$ver
    TYPE "VerZ:",$verz
;** Make a response to MODL command
    FOR .rob = 1 TO 2 STEP 1
	IF rob_tsk[.rob] THEN
	    $modl_rob[.rob] = $LEFT($SYSDATA(ZROB.NAME,rob_tsk[.rob]),6)+$modl_hnd[.rob]
	    $modl_rob[.rob] = $modl_rob[.rob]+$TRIM_B($ENCODE(SYSDATA(ZROB.MGFNO,rob_tsk[.rob])))
	    .$tmp = $TRIM_B($TOUPPER($SYSDATA(ARMBD.ID,rob_tsk[.rob])))
	    SCASE $LEFT(.$tmp,6) OF
	      SVALUE "1GV-00" :
		.$tmp = "G"+$RIGHT(.$tmp,2)
	      SVALUE "1GV-PV" :
		.$tmp = "P"+$RIGHT(.$tmp,2)
	      ANY :
		.$tmp = "???"
	    END
	    $modl_rob[.rob] = $modl_rob[.rob]+.$tmp
	ELSE
	    $modl_rob[.rob] = "*"
	END
    END
    $modl = $modl_rob[1]+"/"+$modl_rob[2]+"/"+$modl_rob[3]+":Kawasaki"
    Z_TYAMA OFF
.END
.PROGRAM ver_cmd(.tsk)
    IF (tsk_num[.tsk]<>0)		GOTO efm
    CALL send_ack(.tsk)
    $snd_res[.tsk] = $res + "," + $ver
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM modl_cmd(.tsk)
    IF (tsk_num[.tsk]<>0)		GOTO efm
    CALL send_ack(.tsk)
    $snd_res[.tsk] = $res + "," + $modl
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM setdocver(.tsk)
    IF (tsk_num[.tsk]<>1)		GOTO efm
    tsk_dat[.tsk,d_dat1] = INT(VAL($tsk_dat[.tsk,1])*100+0.001)
    IF tsk_dat[.tsk,d_dat1] <  106	GOTO epm
    IF tsk_dat[.tsk,d_dat1] >  docver	GOTO epm
    CALL check_bsy_mode(.tsk,tsk_rob[.tsk],m_init)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    vtc.docver[.tsk] = tsk_dat[.tsk,d_dat1]
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.******************************************
.** stat.as
.******************************************
.PROGRAM stat_cmd(.tsk)
    IF tsk_num[.tsk]<>1		GOTO efm
    CALL id_unit(.tsk,1,d_rob1)
    IF NOT tsk_dat[.tsk,d_rob1]		GOTO epm
    CALL send_ack(.tsk)
    IF SWITCH("CS",rob_tsk[tsk_dat[.tsk,d_rob1]]) THEN
	IF tsk_cmd[tsk_dat[.tsk,d_rob1]] OR tsk_use[tsk_dat[.tsk,d_rob1]] THEN
	    $snd_res[.tsk] = "2"	;** BSY
	ELSEIF g.base[tsk_dat[.tsk,d_rob1]] == gbas_nhom THEN
	    $snd_res[.tsk] = "4"	;** Need Home
	ELSE
	    $snd_res[.tsk] = "1"	;** Ready
	END
    ELSE
	$snd_res[.tsk] = "3"	;** Servo OFF
    END
    $snd_res[.tsk] = $res + "," + $snd_res[.tsk]
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.******************************************
.** fan.as
.******************************************
.REALS
    fan_rob1 = 1		;** Robot FAN1
    fan_rob2 = 2		;** Robot FAN2
    fan_rob3 = 3		;** Robot FAN3
    fan_rob4 = 4		;** Robot FAN4
    fan_trkr = 5		;** Track FAN Right
    fan_trkl = 6		;** Track FAN Left
    fan_enum = 6		;** Robot1台当たりの最大FAN監視数
    fan_ctl  = 7	;* コントローラFAN複数一括チェック
.**
.**    Exsample
.**    si.fan.rob[rob,fan_rob1] = ****
.**    si.fan.rob[rob,fan_rob2] = ****
.**    si.fan.rob[rob,fan_rob3] = ****
.**    si.fan.rob[rob,fan_rob4] = ****
.**    si.fan.rob[rob,fan_trkr] = ****
.**    si.fan.rob[rob,fan_trkl] = ****
.END
.PROGRAM fan_init()
.** FAN Check Cfg should be set ON when controller start.
    ZARRAYSET vtp.fanrob[ pc3]=1,2
    ZARRAYSET vtp.fanctl[ pc3]=1,2
    ZARRAYSET vtp.fantrkr[pc3]=1,2
    ZARRAYSET vtp.fantrkl[pc3]=1,2
    FOR .rob = 1 TO 2 STEP 1
	FOR .fan = 1 TO fan_enum STEP 1
	    NOEXIST_SET_R si.fan.rob[.rob,.fan] = 0
	    IF si.fan.rob[.rob,.fan] THEN
		OBSSIGNAL (.rob-1)*fan_enum+.fan = si.fan.rob[.rob,.fan],1,200
	    END
	END
    END
    ZSETFANSIG 0,0,0,0,0,0,0,0
    IF SYSDATA(ZSIMENV) THEN
	PCEXECUTE 5: fan_sim
    END
.END
.PROGRAM fan_check(.tsk,.rob)
.** Output fan_check[.tsk]
    fan_check[.tsk] = 0
    IF .rob > 2		RETURN
    .cfg = tsk_tsk[.tsk]
    IF vtp.fanrob[.cfg] THEN
	FOR .fan = 1 TO 4 STEP 1
	    IF si.fan.rob[.rob,.fan] THEN
		IF NOT OBSSIGNAL((.rob-1)*fan_enum+.fan) THEN
		    fan_check[.tsk] = fan_check[.tsk] BOR bit[.fan]
		END
	    END
	END
    END
    .fan = fan_trkr
    IF vtp.fantrkr[.cfg] AND si.fan.rob[.rob,.fan] THEN
	IF si.fan.rob[.rob,.fan] THEN
	    IF NOT OBSSIGNAL((.rob-1)*fan_enum+.fan) THEN
		fan_check[.tsk] = fan_check[.tsk] BOR bit[.fan]
	    END
	END
    END
    .fan = fan_trkl
    IF vtp.fantrkl[.cfg] AND si.fan.rob[.rob,.fan] THEN
	IF si.fan.rob[.rob,.fan] THEN
	    IF NOT OBSSIGNAL((.rob-1)*fan_enum+.fan) THEN
		fan_check[.tsk] = fan_check[.tsk] BOR bit[.fan]
	    END
	END
    END
    .ctl_err = OFF
    IF (cont == cont_d60)AND(NOT SIG(1001,1002,1003,1006)) THEN
	.ctl_err = on
    ELSEIF (cont == cont_d61)AND(NOT SIG(1001,1002,1003,1004,1006,1007)) THEN
	.ctl_err = on
    END
    IF .ctl_err THEN
	IF vtp.fanctl[.cfg] THEN
	    fan_check[.tsk] = fan_check[.tsk] BOR bit[fan_ctl]
	ELSEIF TASKNO<= 2 THEN
	    CALL mvwait(.rob,vtp.tres[tsk_tsk[.rob]])
	    CALL event_rob(.rob,$ev_fan_ctl,$ev2_fan_ctl)
	ELSEIF TASKNO>4 THEN
	    CALL event_entry(.tsk,$ev_fan_ctl,$ev2_fan_ctl)
	END
    END
.END
.PROGRAM fan_error(.tsk,.rob)
    FOR .fan = 1 TO fan_ctl STEP 1
	IF fan_check[.tsk] BAND bit[.fan] THEN
	    CALL alarm_str(.tsk,.rob,0,$ae_fan[.fan])
	END
    END
.END



.PROGRAM fan_sim()
loop:
    FOR .rob = 1 TO 2 STEP 1
	FOR .fan = 1 TO 6 STEP 1
	    IF si.fan.rob[.rob,.fan] THEN
		PULSE si.fan.rob[.rob,.fan]+1000,1
	    END
	END
    END
    SIGNAL 2001,2002,2003,2004,2006,2007
    TWAIT 2
    GOTO Loop
.END
.******************************************
.** servo.as
.******************************************
.PROGRAM servo_init()
    FOR .rob = 1 TO 4 STEP 1
	IF rob_tsk[.rob] THEN
	    IF servo[.rob] THEN
		FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
		    CALL servo(.rob,id_anum[.rob,.cnt])
		END
	    ELSE
		TYPE "Servo Parameter does not be changed:",.rob
	    END
	END
    END
.END
.PROGRAM servo(.rob,.jnt)
    ZYMSRVPRM   rob_tsk[.rob]:.jnt KVP		= srvprm[.rob,.jnt,01]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt JRAT		= srvprm[.rob,.jnt,02]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt TVI		= srvprm[.rob,.jnt,03]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt VR_TVI1	= srvprm[.rob,.jnt,04]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt VR_TVI2	= srvprm[.rob,.jnt,05]
.** VR_V1<=VR_V2という入力条件があるので
.** 例えば0,0→100,100に変更する場合
.** エラー停止するので、一旦VR_V1を0にして
.** VR_V2設定後にVR_V1を設定することにする
    ZYMSRVPRM   rob_tsk[.rob]:.jnt VR_V1	= 0
    ZYMSRVPRM   rob_tsk[.rob]:.jnt VR_V2	= srvprm[.rob,.jnt,07]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt VR_V1	= srvprm[.rob,.jnt,06]
;
    ZYMSRVPRM   rob_tsk[.rob]:.jnt SQTCLM	= srvprm[.rob,.jnt,08]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt SYSILM	= srvprm[.rob,.jnt,09]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt CCWTLM	= srvprm[.rob,.jnt,10]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt CWTLM	= srvprm[.rob,.jnt,11]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt TCFIL	= srvprm[.rob,.jnt,12]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt TCNFILA	= srvprm[.rob,.jnt,13]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt TCNFILB	= srvprm[.rob,.jnt,14]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt KP		= srvprm[.rob,.jnt,15]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt TPI		= srvprm[.rob,.jnt,16]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt FFGN		= srvprm[.rob,.jnt,17]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt FFFIL	= srvprm[.rob,.jnt,18]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt VCFIL	= srvprm[.rob,.jnt,19]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt NOTCH_f1	= srvprm[.rob,.jnt,20]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt NOTCH_f2	= srvprm[.rob,.jnt,21]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt NOTCH_z1	= srvprm[.rob,.jnt,22]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt NOTCH_z2	= srvprm[.rob,.jnt,23]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt AS_PERLM	= srvprm[.rob,.jnt,24]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt AS_ENVERRKP	= srvprm[.rob,.jnt,25]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OFLV		= srvprm[.rob,.jnt,26]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OFWLV	= srvprm[.rob,.jnt,27]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt PCOMER	= srvprm[.rob,.jnt,28]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt GRAV		= srvprm[.rob,.jnt,29]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt BONDLY	= srvprm[.rob,.jnt,30]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt BOFFDLY	= srvprm[.rob,.jnt,31]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt BONBGN	= srvprm[.rob,.jnt,32]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt AFBK		= srvprm[.rob,.jnt,33]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt AFBFIL	= srvprm[.rob,.jnt,34]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OBG		= srvprm[.rob,.jnt,35]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt ANRES	= srvprm[.rob,.jnt,36]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OBLPF1	= srvprm[.rob,.jnt,37]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OBLPF2	= srvprm[.rob,.jnt,38]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OLWLV	= srvprm[.rob,.jnt,39]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt OSDATA	= srvprm[.rob,.jnt,40]
    ZYMSRVPRM   rob_tsk[.rob]:.jnt INP		= srvprm[.rob,.jnt,41]
;***ZYMSRVPRM/Z rob_tsk[.rob]:.jnt NEAR		= srvprm[.rob,.jnt,42]
;***ZYMSRVPRM/Z rob_tsk[.rob]:.jnt LOWV		= srvprm[.rob,.jnt,43]
;***ZYMSRVPRM/Z rob_tsk[.rob]:.jnt VA		= srvprm[.rob,.jnt,44]
;***ZYMSRVPRM/Z rob_tsk[.rob]:.jnt VCMP		= srvprm[.rob,.jnt,45]
;***ZYMSRVPRM/Z rob_tsk[.rob]:.jnt ZV		= srvprm[.rob,.jnt,46]
.END
.******************************************
.** setzero.as
.******************************************
.PROGRAM setzero_cmd(.tsk)
.** Check Parameter Number
    IF tsk_num[.tsk]<>2		GOTO efm
.** Get Unit & Axis ID
    CALL id_unit_axis(.tsk,1,d_rob1,d_jnt1,0)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_jnt1])	GOTO epm
.** Get Zero Value
    CALL id_int(.tsk,2,d_dat1,100)
    IF id_error[.tsk]						GOTO epm
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Power OFF
    CALL power_off(.tsk,bit[tsk_dat[.tsk,d_rob1]])
.**
    POINT #jnt[.tsk] = #PPJSET(#PPOINT(),tsk_dat[.tsk,d_jnt1],tsk_dat[.tsk,d_dat1])
    ZJZERO rob_tsk[tsk_dat[.tsk,d_rob1]]: bit[tsk_dat[.tsk,d_jnt1]],#jnt[.tsk]
;
    $snd_res[.tsk] = $res_ok
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.** harish 090309 a++
.PROGRAM setzero_cmd1(.tsk)
.** Check Parameter Number
    IF (tsk_num[.tsk]<>1)AND(tsk_num[.tsk]<>2)	GOTO efm
    tsk_dat[.tsk,d_jnt1] = 0
.** Get Unit & Axis ID
    CALL id_unit(.tsk,1,d_rob1)
    IF (NOT tsk_dat[.tsk,d_rob1]) THEN
	CALL id_unit_axis(.tsk,1,d_rob1,d_jnt1,0)
	IF (NOT tsk_dat[.tsk,d_rob1])OR(NOT tsk_dat[.tsk,d_jnt1])		GOTO epm
    END
.** Get Zero Value
.** harish 090327-1 m
    POINT #jnt[.tsk]=#j_zero[bit[tsk_dat[.tsk,d_rob1]]]
;   POINT #jnt[.tsk]=#PPOINT()
    IF tsk_num[.tsk]==2 THEN
	CALL id_int(.tsk,2,d_dat1,100)
	IF id_error[.tsk]					GOTO epm
	IF tsk_dat[.tsk,d_jnt1] THEN
	    POINT #jnt[.tsk]=#PPJSET(#PPOINT(),tsk_dat[.tsk,d_jnt1],tsk_dat[.tsk,d_dat1])
.** harish 090327-1 a++
	ELSE
	    POINT #jnt[.tsk]=#PPOINT(0,tsk_dat[.tsk,d_dat1],tsk_dat[.tsk,d_dat1],tsk_dat[.tsk,d_dat1],0,tsk_dat[.tsk,d_dat1],tsk_dat[.tsk,d_dat1])
.** harish 090327-1 a--
	END
    END
.** Check Busy , Mode and Send Ack
    CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN

    IF tsk_dat[.tsk,d_jnt1] THEN
        tsk_dat[.tsk,d_jnt1] = bit[tsk_dat[.tsk,d_jnt1]]
    ELSE
	tsk_dat[.tsk,d_jnt1] = ^HFF
    END

.** Power OFF
    CALL power_off(.tsk,bit[tsk_dat[.tsk,d_rob1]])
    ZJZERO rob_tsk[tsk_dat[.tsk,d_rob1]]: tsk_dat[.tsk,d_jnt1],#jnt[.tsk]

    $snd_res[.tsk] = $res_ok
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.** harish 090309 a--
.********************************************************
.*
.* File Name : upgrade.as
.*
.* Function  :
.*
.* Owner     :
.*
.* History   :
.*
.********************************************************
.PROGRAM upgrade()
    IF 0
.*******Q6410-1 a
;	vno.robottype = robottype
	CALL tchext_exe_sub
	CALL cfg_robot
	CALL cfg_none
	CALL cfg_port
    END
.END
.***Q6419-1 a++
.*..MONCMD NOEXIST_SET_R robottype = -1,,OFF
.*..MONCMD NOEXIST_SET_R vno.robottype = robottype,,OFF
.***Q6419-1 a--
.** harish 081120 a++
.********************************************************
.*
.* File Name : diagnose.as
.*
.* Function  : DIAGNOSE-Command
.*
.* Owner     :
.*
.* History   :
.*
.********************************************************
.REALS
    diag_pln_time = 2
    diag_pln_wait = 2
.END
.PROGRAM diagnose_cmd(.tsk)
    IF tsk_num[.tsk]<>2 GOTO efm
    CALL id_robot_hand(.tsk,1,d_rob1,d_hnd1,d_fng1)
    IF NOT (tsk_dat[.tsk,d_rob1] AND tsk_dat[.tsk,d_hnd1] AND tsk_dat[.tsk,d_fng1]) THEN
        CALL id_aligner(.tsk,1,d_rob1)
        IF NOT (tsk_dat[.tsk,d_rob1] )	GOTO epm
        tsk_dat[.tsk,d_hnd1] = 0
    END
    SCASE $TOUPPER($tsk_dat[.tsk,2]) OF
      SVALUE "PLUNGER"  :
	IF tsk_dat[.tsk,d_rob1] > 2 		GOTO epm
      SVALUE "BACKLASH" :
	IF tsk_dat[.tsk,d_rob1] > 2 		GOTO epm
.** harish 090629-1 a++
	CALL diag_backlash(.tsk,tsk_dat[.tsk,d_rob1])
	RETURN
.** harish 090629-1 a--
      ANY :
	GOTO epm
    END
    TYPE $TOUPPER($tsk_dat[.tsk,2]),tsk_dat[.tsk,d_rob1],tsk_dat[.tsk,d_hnd1],tsk_dat[.tsk,d_fng1]
    $tsk_cmd[.tsk] = $TOUPPER($tsk_dat[.tsk,2])
    tsk_cmd[.tsk]  = bit[tsk_dat[.tsk,d_rob1]]
    tsk_mod[.tsk]  = m_tech
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM diag_plunger(.rob)
    .hnd = tsk_dat[.rob,d_hnd1]
    .fng = tsk_dat[.rob,d_fng1]
;** 先ず持っていないことを確認する **
    CALL gripper(.rob,.hnd,.fng,grip_abst,who_ext,0)
    IF grip_err[.rob]	GOTO ext
    TWAIT 0.5
;** 開放する **
    CALL gripper(.rob,.hnd,.fng,grip_rels,who_ext,0)
    IF grip_err[.rob]	GOTO ext
    TWAIT 0.5
;** もう一度持っていないことを確認する **
    CALL gripper(.rob,.hnd,.fng,grip_abst,who_ext,0)
    IF grip_err[.rob]	GOTO ext
    TWAIT 0.5
;** ここから計測 **
    .rel = 0
    .hld = 0
    FOR .cnt = 1 TO diag_pln_time STEP 1
;** 開放
	TWAIT diag_pln_wait
	CALL gripper1(.rob,.hnd,.fng,grip_rels,who_ext,0)
    	IF grip_err[.rob]	GOTO ext
	.rel = .rel + gtime_rels[.rob]
;** 把持
	TWAIT diag_pln_wait
	CALL gripper1(.rob,.hnd,.fng,grip_abst,who_ext,0)
    	IF grip_err[.rob]	GOTO ext
	.hld = .hld + gtime_hold[.rob]
    END
    .rel = .rel / diag_pln_time
    .hld = .hld / diag_pln_time
ext:
    IF tsk_err[.rob] THEN
	$snd_res[.rob] = $res_ng
	.hld = -1000
	.rel = -1000
    ELSE
	$snd_res[.rob] = $res_ok
    END
    $snd_res[.rob] = $snd_res[.rob] + ",GRIP "+$TRIM_B($ENCODE(/F12.3,.hld))+" RELEASE "+$TRIM_B($ENCODE(/F12.3,.rel))
.END
.** harish 090629-1 a++
.PROGRAM diag_backlash(.tsk,.rob)
.** Check Busy , Mode and Send Ack
        CALL check_bsy_mode(.tsk,bit[tsk_dat[.tsk,d_rob1]],m_tech)
        IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
.** Check servo ON
	CALL is_servo(.tsk,.rob,ON)
	IF NOT is_servo[.tsk]	RETURN
	CALL event_entry(.tsk,$ev_refuse,$ev2_refuse)
	TWAIT 3
.** Change servo parameters
	CALL power_off(.tsk,.rob)
        IF .rob == 1
	    CALL srvprmnew1
	ELSE
	    CALL srvprmnew2
	END
	CALL power_on(.tsk,.rob)
;	IF is_servo[.tsk]
;           CALL servo_on(.tsk,.unt)
;	END
.** Backlash check
    $bl_err[.rob] = ""
    $bl_res[.rob] = ""
    CASE rob_tsk[.rob]
        VALUE 1 :
	    IF tsk_arm[rob_tsk[.rob]]  <> arm_nt570 RETURN
;	    CALL diag_hys1
            bklsh_chk[1] = ON
	    MC/ERR .err = EXECUTE rob_tsk[.rob]:diag_hys1
	    IF .err THEN
	        CALL alarm_sys(.tsk,.rob,.err,0)
	        servo_on[.tsk] = OFF
		$snd_res[.tsk] = $res_ng
		GOTO err
	    END
	VALUE 2 :
            IF tsk_arm[rob_tsk[.rob]]  <> arm_nt410 RETURN
;	    CALL diag_hys2
            bklsh_chk[2] = ON
	    MC/ERR .err = EXECUTE rob_tsk[.rob]:diag_hys2
	    IF .err THEN
	        CALL alarm_sys(.tsk,.rob,.err,0)
	        servo_on[.tsk] = OFF
		$snd_res[.tsk] = $res_ng
		GOTO err
	    END
	ANY :
    END
    TWAIT 1
    WAIT (bklsh_chk[.rob]==OFF OR TASK(.rob)<>1)
    bklsh_chk[.rob] = OFF
    TWAIT 0.1
    IF SWITCH("POWER",.rob) AND (ERROR:.rob==0) AND SWITCH("REPEAT") THEN
    ELSE
        CALL alarm_sys(.tsk,.rob,0,0)
        $snd_res[.tsk] = $res_ng
    END
err:
.** Change servo parameters
	CALL power_off(.tsk,.rob)
        IF .rob == 1
	    CALL srvprmold1
	ELSE
	    CALL srvprmold2
	END
.** harish 090701-3 a++
        IF vc.mode[.tsk] == mode_soft OR NOT SWITCH("REPEAT")
            CALL soft_param(.tsk,.rob,ON)
	END
.** harish 090701-3 a--
	CALL power_on(.tsk,.rob)
ext:
    IF $bl_err[.rob] <> ""
        $alm_put[.tsk] = $bl_err[.rob]
        CALL alarm_put(.tsk,.rob,0)
;       CALL make_res_ng(.tsk)
	$snd_res[.tsk] = $res_ng
    ELSEIF $bl_res[.rob] <> ""
        $snd_res[.tsk]= $bl_res[.rob]
    END
    CALL event_entry(.tsk,$ev_accept,$ev2_accept)
.END

.***************************************************************************
.* Diag_backlash.AS
.***************************************************************************

.PROGRAM hyschkfwd(.rob,.jnt,.disp,.perrlim,.currlim,.log[,],.iterno,.limit)
.** same as backchkfwd **
    .currsub = .rob*1000 + .jnt
    .perrsub = .rob*1000 + 400 +.jnt
    FOR .i=1 TO .iterno
        .current=0
        .poserr=0
        .cummove=0
	WHILE .current<=.currlim AND .poserr=<.perrlim
	    drive .jnt,.disp
	    delay 0.1
	    .cummove = .cummove + .disp
	    if .cummove > .limit then
		.log[1,0]=-1000
		goto end
    	    end
	    .current = ZDA_DATA(.currsub)
	    .poserr = ZDA_DATA(.perrsub)
	    ;TYPE .current,.poserr
        END
        ;TYPE .current,.poserr
        HERE .#loc
        DECOMPOSE .x[1] = .#loc
        ;type "Dir1 JT-",.jnt,"=",.x[.jnt]
        .log[.i,1] = .x[.jnt]
        .current=0
        .poserr=0
        .cummove=0
	WHILE .current>=-.currlim AND .poserr>=-.perrlim
	    drive .jnt,-.disp
	    delay 0.1
	    .cummove = .cummove + .disp
	    if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	    end
	    .current = ZDA_DATA(.currsub)
	    .poserr = ZDA_DATA(.perrsub)
        END
	;TYPE .current,.poserr
	HERE .#loc
	DECOMPOSE .x[1] = .#loc
	;type "Dir2 JT-",.jnt,"=",.x[.jnt]
	.log[.i,2] = .x[.jnt]
    END
    ;FOR .i=1 to .iterno
        ;TYPE .i,"\t",.log[.i,1],"\t",.log[.i,2]
    ;END
    RETURN
end:
.END

.PROGRAM hyschkrev(.rob,.jnt,.disp,.perrlim,.currlim,.log[,],.iterno,.limit)
.** same as backchkrev **
    .currsub = .rob*1000 + .jnt
    .perrsub = .rob*1000 + 400 +.jnt
    FOR .i=1 TO .iterno
	.current=0
	.poserr=0
	.cummove=0
	WHILE .current>=.currlim AND .poserr>=.perrlim
	    drive .jnt,.disp
	    delay 0.1
	    .cummove = .cummove + .disp
	    if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	    end
	    .current = ZDA_DATA(.currsub)
	    .poserr = ZDA_DATA(.perrsub)
        END
        ;TYPE .current,.poserr
	HERE .#loc
	DECOMPOSE .x[1] = .#loc
	.log[.i,1] = .x[.jnt]
	.current=0
	.poserr=0
	.cummove=0
	WHILE .current<=-.currlim AND .poserr<=-.perrlim
	    drive .jnt,-.disp
	    delay 0.1
	    .cummove = .cummove + .disp
	    if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	    end
	    .current = ZDA_DATA(.currsub)
     	    .poserr = ZDA_DATA(.perrsub)
    	END
	;TYPE .current,.poserr
	HERE .#loc
	DECOMPOSE .x[1] = .#loc
	.log[.i,2] = .x[.jnt]
    END
    ;FOR .i=1 to .iterno
        ;TYPE .i,"\t",.log[.i,1],"\t",.log[.i,2]
    ;END
end:
.END

.PROGRAM diag_hys2()
.** same as inthys2 **
    .rob = 2
    speed 5 always
    .log6[1,0]=0
    call hyschkfwd(2,6,vnp.blstep6[2],vnp.blperrlim6[2],vnp.blcurlim6[2],.log6[,],vnp.blcyclim6[2],vnp.blanglim6[2])
    if .log6[1,0] == -1000
	goto end
    end
    .log4[1,0]=0
    call hyschkfwd(2,4,vnp.blstep4[2],vnp.blperrlim4[2],vnp.blcurlim4[2],.log4[,],vnp.blcyclim4[2],vnp.blanglim4[2])
    if .log4[1,0] == -1000
	goto end
    end
    .log2[1,0]=0
    call hyschkfwd(2,2,vnp.blstep2[2],vnp.blperrlim2[2],vnp.blcurlim2[2],.log2[,],vnp.blcyclim2[2],vnp.blanglim2[2])
    if .log2[1,0] == -1000
	goto end
    end
    call intcalcave(.log6[,],5,.jt6)
    call intcalcave(.log4[,],5,.jt4)
    call intcalcave(.log2[,],5,.jt2)
.** SCRX10-027-10 d
.** .result = .jt6*350+.jt4*700+.jt2*1200 - 1.1
.** SCRX10-027-10 m
    .result = .jt6*300+.jt4*500+.jt2*500
    type "Joint-6",.jt6
    type "Joint-4",.jt4
    type "Joint-2",.jt2
    type .result
.** logging **
    .jnts[1]=2
    .jnts[2]=4
    .jnts[3]=6
    .ind[1]=.jt2
    .ind[2]=.jt4
    .ind[3]=.jt6
    call log_bklsh(2,.result,.ind[],.jnts[],3)
.** check criteria
    if(.result<vnp.blpass[2])
	.$result = "PASS"
    else
	.$result = "FAIL"
    end
    $bl_res[2] = $res_ok+","+.$result
    GOTO exit
end:
;   $snd_res[2] = $res_ng
    $bl_err[2] = $AE_BAKLSH_JIG
exit:
    bklsh_chk[2] = OFF
.END

.PROGRAM diag_hys1()
.** same as inthys1 **
    speed 5 always
    .log6[1,0]=0
    call hyschkrev(1,6,vnp.blstep6[1],vnp.blperrlim6[1],vnp.blcurlim6[1],.log6[,],vnp.blcyclim6[1],vnp.blanglim6[1])
    if .log6[1,0] == -1000
	goto end
    end
    .log4[1,0]=0
    call hyschkrev(1,4,vnp.blstep4[1],vnp.blperrlim4[1],vnp.blcurlim4[1],.log4[,],vnp.blcyclim4[1],vnp.blanglim4[1])
    if .log4[1,0] == -1000
	goto end
    end
    .log2[1,0]=0
    call hyschkrev(1,2,vnp.blstep2[1],vnp.blperrlim2[1],vnp.blcurlim2[1],.log2[,],vnp.blcyclim2[1],vnp.blanglim2[1])
    if .log2[1,0] == -1000
	goto end
    end
    call intcalcave(.log6[,],5,.jt6)
    call intcalcave(.log4[,],5,.jt4)
    call intcalcave(.log2[,],5,.jt2)
    type "Joint-6",.jt6
    type "Joint-4",.jt4
    type "Joint-2",.jt2
.** SCRX10-027-10 d
.** .result = .jt6*350+.jt4*700+.jt2*1200 - 1.1
.** SCRX10-027-10 m
    .result = .jt6*300+.jt4*600+.jt2*1100
    type .result
.** logging **
    .jnts[1]=2
    .jnts[2]=4
    .jnts[3]=6
    .ind[1]=.jt2
    .ind[2]=.jt4
    .ind[3]=.jt6
    call log_bklsh(1,.result,.ind[],.jnts[],3)
.** check criteria
    if(.result<vnp.blpass[2])
	.$result = "PASS"
    else
	.$result = "FAIL"
    end
    $bl_res[1] = $res_ok+","+.$result
    GOTO exit
end:
;   $snd_res[1] = $res_ng
    $bl_err[1] = $AE_BAKLSH_JIG
exit:
    bklsh_chk[1] = OFF
.END
.** harish 090629-1 a--
.** harish 081120 a--
.******************************************
.** alarm.as
.******************************************
.PROGRAM alarm_init()
    ZARRAYSET  alm_ext[1] =  0,4
    ZARRAYSET $alm_put[1] = "",6
    $alm_put[pc4] = ""

    $alm_log[pc3] = ""
    $alm_log[pc4] = ""

     alm_buf[pc3] = 100
     alm_buf[pc4] = 100

    FOR tmp = pc3 TO pc4 STEP 1
	NOEXIST_SET_R alm.cnt[tmp] = 0
	NOEXIST_SET_R alm.max[tmp] = 0
	IF SYSDATA(ZSIMENV) THEN
	    FOR tmp2 = 1 TO alm_buf[tmp] STEP 1
		NOEXIST_SET_S $alm.str[tmp,tmp2] = ""
		NOEXIST_SET_S $alm.inf[tmp,tmp2] = ""
		NOEXIST_SET_R  alm.mes[tmp,tmp2] = 0
	    END
	END
    END

.END
.PROGRAM alarm_sys(.tsk,.rob,.i_e,.i_x)
    .err = .i_e
    .ext = .i_x
    IF (NOT .err) THEN
	.err = SYSDATA(ERROR.CODE,rob_tsk[.rob])
	.ext = SYSDATA(ERROR.AXIS,rob_tsk[.rob])
    END
IF 0 THEN
PROMPT "Input Err:",.err
PROMPT "Input Ext:",.ext
END
    .err = ABS(.err)
    IF .err THEN
	.$tbl = $ENCODE($sys_err_idx[INT(.err/10000)],"[",(.err MOD 10000),"]")
	.$alm = $ENCODE("$alm_put[",.tsk,"]")
	ZPRGEXE (.$alm +"="+ .$tbl),.dum
	IF .dum THEN
	    .$err = "("+$TRIM_B($ENCODE(-.err))+"){"+$ERROR(-.err,ENG)+"}"
	    .$err = $LEFT(.$err,200-LEN($ae_undef+$ae_kawasaki))
	    $alm_put[.tsk] = $ae_undef+.$err+$ae_kawasaki
	END
    ELSE
	$alm_put[.tsk] = $ae_pow_off
    END
    CALL alarm_put(.tsk,.rob,.ext)
.END
.PROGRAM alarm_str(.tsk,.rob,.ext,.$alm)
    IF .tsk <= 4 THEN
	WAIT ($alm_put[.rob]=="")
	alm_ext[.rob] = .ext
	$alm_put[.rob] = .$alm
	snd_alm[.rob] = ON
	tsk_err[.rob] = ON
.******* harish 090327-1 m++
	IF NOT PrePos[.rob] THEN
	    CALL make_res_ng(.tsk)
	END
.******* harish 090327-1 m--
.*	CALL make_res_ng(.tsk)
	WAIT (snd_alm[.rob]==0)
    ELSE
	$alm_put[.tsk] = .$alm
	CALL alarm_put(.tsk,.rob,.ext)
    END
.END
.PROGRAM alarm_put(.tsk,.rob,.ext)
.** Input  $alm_put[.tsk]
.**        $tsk_mes[.tsk]-->
.**         tsk_hst[.tsk]-->
.** Output $alm_log[.tsk]
    IF TASKNO <= 4 THEN
	TYPE "ALARM_PUT TASKNO ERROR"
	HALT
    END
    tsk_err[.tsk] = ON
.** harish 081117 a++   .** qryalrm com spec 2.00 **.
    IF vtc.docver[.tsk] >= 200
        $alm_put[.tsk] = $LEFT($alm_put[.tsk],6)+"0"+$MID($alm_put[.tsk],6)
;;	IF INSTR(0,$alm_put[.tsk],",%R,") OR INSTR(0,$alm_put[.tsk],",%R:%B,") OR INSTR(0,$alm_put[.tsk],",%R:%BF1,")OR INSTR(0,$alm_put[.tsk],",%R:%BF2,") OR INSTR(0,$alm_put[.tsk],",%R:%J,")
;;        $alm_put[.tsk] = $REPLACE($alm_put[.tsk],",0,",",1,",-1)
;;      END
    END
.** harish 081117 a--
    IF INSTR(0,$alm_put[.tsk],"%R") AND .rob THEN
	$alm_put[.tsk] = $REPLACE($alm_put[.tsk],"%R",$vrp.rob_asso[.rob],-1)
	IF INSTR(0,$alm_put[.tsk],"%J")AND .ext THEN
	    .off = 0
	    IF .rob == 2 THEN
		.off = 7		;** ロボットの最大軸数7
	    ELSEIF .rob == 3 THEN
		.off = 14		;** ロボットの最大軸数7x2
	    ELSEIF .rob == 4 THEN
		.off = 15		;** アライナ軸1
	    END
	    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
		.jnt = id_anum[.rob,.cnt]
		IF (.ext BAND bit[.jnt] ) THEN
		    .$str = $TRIM_B($ENCODE(/h5,VAL($LEFT($alm_put[.tsk],5),2)+.off+.jnt-1))
		    $alm_log[.tsk] = .$str+$MID($alm_put[.tsk],6,)
		    $alm_log[.tsk] = $REPLACE($alm_log[.tsk],"%J",$GETSTRBLK($id_axis[.rob],",",.jnt)+"(JT"+$ENCODE(/I1,.jnt)+")")
		    CALL alarm_log(.tsk)
		END
	    END
	ELSEIF INSTR(0,$alm_put[.tsk],"%MJ") AND .ext THEN
	    .$str = $TRIM_B($ENCODE(/h5,VAL($LEFT($alm_put[.tsk],5),2)+.rob-1))
	    $alm_log[.tsk] = .$str+$MID($alm_put[.tsk],6,)
	    .$str = ""
	    FOR .cnt = 1 TO id_anum[.rob,0] STEP 1
		.jnt = id_anum[.rob,.cnt]
		IF (.ext BAND bit[.jnt] ) THEN
		    IF .$str <> "" THEN
			.$str = .$str + ","
		    END
		    .$str = .$str + $GETSTRBLK($id_axis[.rob],",",.jnt)+"(JT"+$ENCODE(/I1,.jnt)+")"
		END
	    END
	    $alm_log[.tsk] = $REPLACE($alm_log[.tsk],"%MJ",.$str)
	    CALL alarm_log(.tsk)
	ELSEIF INSTR(0,$alm_put[.tsk],"%B") AND .ext THEN
;*** WAFER GONE でFINGERを考慮すると．．．
;*** WAFER GONE であらかじめ%B→%BF1/%BF2とかに変換して本関数をCALLする
;*** つまりFINGERによってエラー番号は変えないこととしてしまう
	    .off = IFELSE(.rob==2,2,0)	;** ロボットの最大ハンド数は２
	    FOR .cnt = 1 TO 2 STEP 1
		IF (.ext BAND bit[.cnt]) THEN
		    .$str = $TRIM_B($ENCODE(/h5,VAL($LEFT($alm_put[.tsk],5),2)+.off+.cnt-1))
		    $alm_log[.tsk] = .$str+$MID($alm_put[.tsk],6,)
		    $alm_log[.tsk] = $REPLACE($alm_log[.tsk],"%B",$ENCODE("B",/I1,.cnt))
		    CALL alarm_log(.tsk)
		END
	    END
	ELSE
	    .$str = $TRIM_B($ENCODE(/h5,VAL($LEFT($alm_put[.tsk],5),2)+.rob-1))
	    $alm_log[.tsk] = .$str+$MID($alm_put[.tsk],6,)
	    CALL alarm_log(.tsk)
	END
    ELSEIF INSTR(0,$alm_put[.tsk],"%N") AND .ext THEN
	FOR .cnt = 1 TO 8 STEP 1
	    IF (.ext BAND bit[.cnt]) THEN
		.$str = $TRIM_B($ENCODE(/h5,VAL($LEFT($alm_put[.tsk],5),2)+.cnt-1))
		$alm_log[.tsk] = .$str+$MID($alm_put[.tsk],6,)
		$alm_log[.tsk] = $REPLACE($alm_log[.tsk],"%N",$ENCODE(/I1,.cnt))
		CALL alarm_log(.tsk)
	    END
	END
    ELSE
	$alm_log[.tsk] = $alm_put[.tsk]
	CALL alarm_log(.tsk)
    END
    $alm_put[.tsk] = ""
.END
.PROGRAM alarm_log(.tsk)
.** Input  $alm_log[.tsk]
.**        $tsk_mes[.tsk]
.**         tsk_hst[.tsk]
.** Output $alm.inf[.tsk,#]
.**        $alm.str[.tsk,#]
.**
    alm.cnt[.tsk] = RING(alm.cnt[.tsk],1,alm_buf[.tsk])
    $alm.inf[.tsk,alm.cnt[.tsk]] = $DATE(3)+" "+$TIME+"("+$ENCODE(/I1,.tsk)+":"+$host[tsk_hst[.tsk]]+")"
    $alm.str[.tsk,alm.cnt[.tsk]] = $alm_log[.tsk]
     alm.mes[.tsk,alm.cnt[.tsk]] = VAL($tsk_mes[.tsk])
     alm.max[.tsk] = MAXVAL(alm.max[.tsk],alm.cnt[.tsk])
    IF (tsk_hst[.tsk]==host_stp1)OR(tsk_hst[.tsk]==host_stp2)THEN
	alm.mes[.tsk,alm.cnt[.tsk]] = 0
	$snd_dat[.tsk] = $alm_log[.tsk]
	CALL send_data_make(.tsk,$tsk_mes[.tsk])
    END
    IF pz.alarm THEN
	TYPE /S,$alm.inf[.tsk,alm.cnt[.tsk]]
	TYPE    $alm.str[.tsk,alm.cnt[.tsk]]
    END
    $alm_log[.tsk] = ""
.END

.PROGRAM qryalrm_cmd(.tsk,.all)
    tsk_dat[.tsk,d_dat1] = 0
    IF tsk_num[.tsk]==0			GOTO ack
    IF tsk_num[.tsk]<>1			GOTO efm
    CALL id_int(.tsk,1,d_dat1,1)
    IF id_error[.tsk]		GOTO epm
    IF (tsk_dat[.tsk,d_dat1]<=0)OR(999<tsk_dat[.tsk,d_dat1])	GOTO epm
ack:
    CALL send_ack(.tsk)
out:
    .rem = 0
    $snd_res[.tsk] = $ae_none
    FOR .cnt = 1 TO alm.max[.tsk] STEP 1
	.alm = alm.cnt[.tsk]-(.cnt-1)
	.alm = IFELSE(.alm<=0,alm.max[.tsk]+.alm,.alm)
	IF (alm.mes[.tsk,.alm]) THEN
	    IF (tsk_dat[.tsk,d_dat1]==0)OR(tsk_dat[.tsk,d_dat1]==alm.mes[.tsk,.alm]) THEN
		IF $snd_res[.tsk]==$ae_none THEN
		    $snd_res[.tsk] = $alm.str[.tsk,.alm]
		    alm.mes[.tsk,.alm] = 0
		ELSE
		    .rem = .rem + 1
		END
	    END
	END
    END
    IF vtc.docver[.tsk] < 200 THEN
	$snd_res[.tsk] = $LEFT($snd_res[.tsk],1)+$MID($snd_res[.tsk],3)
    END
    $snd_res[.tsk] = $res+","+$TRIM_B($ENCODE(/I,.rem))+":"+$snd_res[.tsk]
    CALL send_res_data(.tsk)
    IF .all AND .rem	GOTO out
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
.END
.******************************************
.** event.as
.******************************************
.REALS
    event_buf = 5
.** harish 081119 a++
..MONCMD NOEXIST_SET_R vtc.event[pc3] = OFF
..MONCMD NOEXIST_SET_R vtc.event[pc4] = OFF
.** harish 081119 a--
.END
.STRINGS
    $ev1_inf[1] = "R1"
    $ev1_inf[2] = "R1"
    $ev2_inf[1] = "R1:B1"
    $ev2_inf[2] = "R2:B1"
.END
.PROGRAM event_init()
.***EvtEnable Switch
    NOEXIST_SET_R vtc.event[pc3] = OFF
    NOEXIST_SET_R vtc.event[pc4] = OFF
    NOEXIST_SET_R vtc.eventlevel[pc3] = 8
    NOEXIST_SET_R vtc.eventlevel[pc4] = 8
    $event1[1,1] = ""
    $event1[2,1] = ""
    $event2[1,1] = ""
    $event2[2,1] = ""
    event.buf[ pc3] = 0
    event.buf[ pc4] = 0
    event.mess[pc3] = 0
    event.mess[pc4] = 0
.** harish 081210 a++
    pc_stop_evt[pc3] = 0
    pc_stop_evt[pc4] = 0
.** harish 081210 a--
    CALL event_clear(pc3)
    CALL event_clear(pc4)
.END
.PROGRAM event_clear(.tsk)
    FOR .evt = 1 TO event_buf STEP 1
	event.prio[.tsk,.evt] = 0	;** 優先順位
	event.send[.tsk,.evt] = 0	;** 送信回数
	event.trid[.tsk,.evt] = 0	;** TrID
	event.host[.tsk,.evt] = 0	;** 対象HOST
	$event1[.tsk,.evt]    = ""	;** イベント文字列
	$event2[.tsk,.evt]    = ""	;** イベント文字列
    END
.END
.PROGRAM event_rob(.rob,.$ev1,.$ev2)
.** Input  $ev1_inf[.rob]
.**        $ev2_inf[.rob]
    IF TASKNO>2		RETURN
    IF (tsk_hst[.rob]==host_stp1)OR(tsk_hst[.rob]==host_stp2)	RETURN
    $event1[.rob,1] = $REPLACE(.$ev1,"%DATA",$ev1_inf[.rob],-1)
    $event2[.rob,1] = $REPLACE(.$ev2,"%DATA",$ev2_inf[.rob],-1)
.END
.PROGRAM event_out(.rob)
    IF TASKNO>2		RETURN
    IF $event1[.rob,1]<>"" OR $event2[.rob,1]<>"" THEN
	WAIT (tsk_evt[.rob]==0)
	$event1[.rob,2] = $event1[.rob,1]
	$event2[.rob,2] = $event2[.rob,1]
	$event1[.rob,1] = ""
	$event2[.rob,1] = ""
	tsk_evt[.rob] = tsk_hst[.rob]
    END
.END

.*** harish 090327-1 a++
.*** PrePos用イベントセットプログラム。
.*** PCタスクで、イベントセットすることができる。
.*** そのため、ロボットタスクがシステムエラー停止した場合に、イベントを送ることができる。
.PROGRAM prpos.event_rob(.rob,.$ev1,.$ev2)
    IF TASKNO > 2 THEN
.*  他タスクからイベントセットできるのは、指定ロボットのタスクが回って無いときだけ。
	IF SWITCH("CS",rob_tsk[.rob])		RETURN
    END
    $event1[.rob,1] = $REPLACE(.$ev1,"%DATA",$ev1_inf[.rob],-1)
    $event2[.rob,1] = $REPLACE(.$ev2,"%DATA",$ev2_inf[.rob],-1)
    tsk_hst[.rob]   = prepos.hst[.rob]
.END

.*** PrePos用イベント登録プログラム。
.*** PCタスクで、イベント送信登録することができる。
.*** そのため、ロボットタスクがシステムエラー停止した場合に、イベントを送ることができる。
.PROGRAM prpos.event_out(.rob)
    IF TASKNO > 2 THEN
.*  他タスクからイベント登録できるのは、指定ロボットのタスクが回って無いときだけ。
	IF SWITCH("CS",rob_tsk[.rob])		RETURN
    END
    IF $event1[.rob,1]<>"" OR $event2[.rob,1]<>"" THEN
	WAIT (tsk_evt[.rob]==0)
	$event1[.rob,2] = $event1[.rob,1]
	$event2[.rob,2] = $event2[.rob,1]
	$event1[.rob,1] = ""
	$event2[.rob,1] = ""
	tsk_evt[.rob] = tsk_hst[.rob]
    END
.END
.*** harish 090327-1 a--

.PROGRAM pc_event(.tsk)
    event.buf[.tsk] = RING(event.buf[.tsk],1,event_buf)
    IF event.prio[.tsk,event.buf[.tsk]] THEN
	IF UTIMER(@event[.tsk,event.buf[.tsk]])>vtp.t2[.tsk] THEN
	    CALL event_send(.tsk,event.buf[.tsk])
	END
	IF event.send[.tsk,event.buf[.tsk]]>=vtp.evtnum[.tsk] THEN
	    CALL event_delete(.tsk,event.buf[.tsk])
	    IF pz.event THEN
		TYPE "Event Delete Resend:",.tsk,event.buf[.tsk]
	    END
	END
    END
.END



.PROGRAM event_delete(.tsk,.evt)
    FOR .buf = 1 TO event_buf STEP 1
	IF event.prio[.tsk,.buf] > event.prio[.tsk,.evt] THEN
	    event.prio[.tsk,.buf] = event.prio[.tsk,.buf] - 1
	END
    END
    event.prio[.tsk,.evt] = 0
.END
.PROGRAM event_entry(.tsk,.$ev1,.$ev2)
    IF NOT vtc.event[.tsk]	RETURN
    .ent = OFF
    FOR .evt = 1 TO event_buf STEP 1
	IF event.prio[.tsk,.evt] THEN
	    event.prio[.tsk,.evt] = event.prio[.tsk,.evt]+1
	END
	IF NOT .ent THEN
	    IF (event.prio[.tsk,.evt]>event_buf) THEN
		event.prio[.tsk,.evt] = 0
		IF pz.event THEN
		    TYPE "Event Delete FULL:",.tsk,.evt
		END
	    END
	    IF NOT (event.prio[.tsk,.evt]) THEN
		$event1[.tsk,.evt] = .$ev1
		$event2[.tsk,.evt] = .$ev2
		event.mess[.tsk] = RING(event.mess[.tsk],1,999)
		event.trid[.tsk,.evt] = -ABS(event.mess[.tsk])
		event.send[.tsk,.evt] = 0
		event.prio[.tsk,.evt] = 1
		event.host[.tsk,.evt] = tsk_hst[.tsk]
		.ent = ON
		CALL event_send(.tsk,.evt)
	    END
	END
    END
.END
.PROGRAM event_send(.tsk,.evt)
    event.send[.tsk,.evt] = event.send[.tsk,.evt] + 1
    .$evt = $IFELSE(vtc.docver[.tsk]>=200,$event2[.tsk,.evt],$event1[.tsk,.evt])
    IF .$evt == "" THEN
	IF pz.event THEN
	    TYPE "No Event at current DocVer"
	END
	GOTO tim
    END
    IF vtc.docver[.tsk]>=200 THEN
	IF VAL($LEFT(.$evt,1),2)>vtc.eventlevel[.tsk] THEN
	    IF pz.event THEN
		TYPE "Event Level is low"
	    END
	    GOTO tim
	END
    END
    .$mes = $TRIM_B($ENCODE(event.trid[.tsk,.evt]))
    $snd_dat[.tsk] = "Evt,"+.$evt
     tsk_hst[.tsk] = event.host[.tsk,.evt]
    CALL send_data_make(.tsk,.$mes)
tim:
    UTIMER @event[.tsk,.evt] = 0
.END

.PROGRAM event_ack(.tsk)
    IF (tsk_num[.tsk]<>1)	GOTO efm
    .mes = VAL($tsk_mes[.tsk])
    .pri = 99999
    .lst = 0
    FOR .evt = 1 TO event_buf STEP 1
	IF event.prio[.tsk,.evt] THEN
	    IF (event.trid[.tsk,.evt]==.mes) THEN
		IF ($tsk_dat[.tsk,1]=="0") THEN
		    CALL event_delete(.tsk,.evt)
.** harish 081125 m
		    tcp_client_chk[.tsk] = 0
		    IF pz.event THEN
			TYPE "Event Delete Got Ack:",.tsk,.evt
		    END
		ELSE
		    CALL event_send(.tsk,.evt)
		END
		GOTO Exit
	    END
.********** もしmesに一致するEvtが無かった場合は
.********** 最後に送信したEvtを再送信するので、最後のEvtを探す
	    IF event.prio[.tsk,.evt]<.pri THEN
		.pri = event.prio[.tsk,.evt]
		.lst = .evt
	    END
	END
    END
    IF .lst THEN
	CALL event_send(.tsk,.lst)
    END
exit:
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END

.PROGRAM evtenable_cmd(.tsk)
    IF (tsk_num[.tsk]<>1)AND(tsk_num[.tsk]<>2)		GOTO efm
    CALL id_0_1(.tsk,1,d_dat1)
    IF id_error[.tsk]					GOTO epm
    IF (tsk_num[.tsk]==1) THEN
	tsk_dat[.tsk,d_dat2] = -1
    ELSE
	CALL id_int(.tsk,2,d_dat2,1)
	IF id_error[.tsk] THEN
	    tsk_dat[.tsk,d_dat2] = VAL($tsk_dat[.tsk,2],2)
	    IF tsk_dat[.tsk,d_dat2] < 10		GOTO epm
	END
	IF tsk_dat[.tsk,d_dat2] < 0			GOTO epm
	IF tsk_dat[.tsk,d_dat2] > 15			GOTO epm
    END
    CALL check_bsy_mode(.tsk,tsk_rob[.tsk],m_init)
    IF ($snd_ack[.tsk]<>"")OR($snd_res[.tsk]<>"")	RETURN
    vtc.event[.tsk] = tsk_dat[.tsk,d_dat1]
    IF tsk_dat[.tsk,d_dat2] < 0 THEN
	vtc.docver[.tsk] = 106
    ELSE
	vtc.eventlevel[.tsk] = tsk_dat[.tsk,d_dat2]
	IF vtc.docver[.tsk] < 200 THEN
	    vtc.docver[.tsk] = 200
	END
    END
    $snd_res[.tsk] = $res_ok
    RETURN
efm:
    $snd_ack[.tsk] = $ack_err_fm
    RETURN
epm:
    $snd_ack[.tsk] = $ack_err_pm
    RETURN
.END
.PROGRAM qrydocevt_cmd(.tsk)
    CALL send_ack(.tsk)
    $snd_res[.tsk] = $res
    CALL int_str(vtc.docver[.tsk],$str[.tsk],1)
    $snd_res[.tsk] = $snd_res[.tsk]+",DocVer:"+$str[.tsk]
    CALL int_str(vtc.event[.tsk],$str[.tsk],1)
    $snd_res[.tsk] = $snd_res[.tsk]+",EvtEnable:"+$str[.tsk]
    CALL int_str(vtc.eventlevel[.tsk],$str[.tsk],1)
    $snd_res[.tsk] = $snd_res[.tsk]+",EvtLevel:"+$str[.tsk]
.END
.******************************************
.** evt_str_106.as
.******************************************
.STRINGS
    $EV_FAN_CTL      = "7700,Controller FAN stop and the FAN check is invalidated. Report to KAWASAKI."
.** SCRX10-027-4 a
    $EV_RETEACH     = "7701,%DATA for interpolated taught positions has changed. %R must be re-taught to %P."
    $EV_START        = "8f00,Robot Controller Started."
    $EV_HALF_MOVE[1] = "8f01,Half-Move Completed for %DATA"
    $EV_HALF_MOVE[2] = "8f02,Half-Move Completed for %DATA"
.** harish 081125 a
    $EV_CLIENT_CHK        = "8f0f,Robot is checking client software."
.*********************   f03
.*********************   ---
.*********************   f0f
    $EV_REFUSE       = "8f10,Robot Controller will refuse FIC Commands."
    $EV_ACCEPT       = "8f11,Robot Controller will accept FIC Commands."
    $EV_INTERLOCK    = "8f12,Interlock has triggered, motors are off. Issue Init to Continue."
.*********************   f13
.*********************  ----
.*********************   f18
    $EV_MAP_THICK[1] = "7f19,%DATA Mapper needs adjustment."
    $EV_MAP_THICK[2] = "7f1a,%DATA Mapper needs adjustment."
.*********************   f1b
.*********************   f1c
    $EV_TGP_END[1]   = "8f1d,%DATA has completed a Test Motion Sequence."
    $EV_TGP_END[2]   = "8f1e,%DATA has completed a Test Motion Sequence."
.*********************   f1f
.*********************   f20
    $EV_ARM_ENT[1]   = "8f21,%DATA is now pointing at station for command"
    $EV_ARM_ENT[2]   = "8f22,%DATA is now pointing at station for command"
.*********************   f23
.*********************   f24
    $EV_ARM_EXT[1]   = "8f25,%DATA is now extended into station during command"
    $EV_ARM_EXT[2]   = "8f26,%DATA is now extended into station during command"
.*********************   f27
.*********************   f28
    $EV_ARM_RET[1]   = "8f29,%DATA is now retracted from station during command"
    $EV_ARM_RET[2]   = "8f2a,%DATA is now retracted from station during command"
.*********************   f2b
.*********************   ---
.*** harish 090327-1 a++
    $EV_PRPOS_SUC[1]    = "8f30,Robot now idle"
    $EV_PRPOS_ERR[1]    = "8f31,Prepos move failed"

    $EV_PRPOS_SUC[2]    = "8f32,Robot now idle"
    $EV_PRPOS_ERR[2]    = "8f33,Prepos move failed"
.*** harish 090327-1 a--
.END
.******************************************
.** evt_str_200.as
.******************************************
.STRINGS
    $EV2_FAN_CTL     = "70700,Controller FAN stop and the FAN check is invalidated. Report to KAWASAKI."
.** SCRX10-027-4 a
    $EV2_RETEACH     = "70701,3,%DATA,%R,%P,^1 for interpolated taught positions has changed. ^2 must be re-taught to ^3."
    $EV2_START       = "8f000,0,Robot Controller Started."
    $EV2_REFUSE      = "8f001,0,Robot Controller will refuse FIC Commands."
    $EV2_ACCEPT      = "8f002,0,Robot Controller will accept FIC Commands."
    $EV2_INTERLOCK   = "8f003,0,Interlock has triggered, motors are off. Issue Init to Continue."
.** harish 081125 a
    $EV2_CLIENT_CHK       = "8f00f,0,Robot is checking client software."
.**
    $EV2_TGET_END    = "8f004,2,%DATA,^1 has completed a Test Get Motion Sequence at ^2."
    $EV2_TPUT_END    = "8f005,2,%DATA,^1 has completed a Test Put Motion Sequence at ^2."
.**
    $EV2_MAP_THICK2  = "7f100,5,%DATA,^1 Mapper needs adjustment, detected wafer at ^2 witch thickness ^3(threshold ^4, double threshold ^5)."
    $EV2_MAP_THICK   = "7f101,1,%DATA,^1 Mapper needs adjustment."
.**
    $EV2_ARM_ENT     = "8f201,3,%DATA,^1 is now pointing at ^2 during ^3 command"
    $EV2_ARM_EXT     = "8f202,3,%DATA,^1 is now extended into ^2 during ^3 command"
    $EV2_ARM_RET     = "8f203,3,%DATA,^1 is now retracted from ^2 during ^3 command"
.*** harish 090327-1 a++
    $EV2_PRPOS_SUC    = "8f300,0,Robot now idle"
    $EV2_PRPOS_ERR    = "8f301,0,Prepos move failed"
.*** harish 090327-1 a--
.END
.******************************************
.** err_0_sy.as
.**   D-Error で各軸毎に必要なAMP関連エラーを除いたもの
.******************************************
.STRINGS
    $sys_err_tbl_d[0001] = "60001,Fatal Error(D0001). CPU error. Report to KAWASAKI."
    $sys_err_tbl_d[0002] = "60002,Fatal Error(D0002). Main CPU BUS error. Report to KAWASAKI."
    $sys_err_tbl_d[0003] = "60003,Fatal Error(D0003). VME Bus error. Report to KAWASAKI."
    $sys_err_tbl_d[0900] = "60004,Fatal Error(D0900). DataBase memory is broken. Report to KAWASAKI."
    $sys_err_tbl_d[0904] = "60005,Fatal Error(D0904). Memory is locked due to AC_FAILE. Report to KAWASAKI."
    $sys_err_tbl_d[1508] = "60006,Fatal Error(D1508). 24V DC power source is too low. Report to KAWASAKI."
    $sys_err_tbl_d[1511] = "60007,Fatal Error(D1511). +12V DC or -12V DC is abnormal. Report to KAWASAKI."
    $sys_err_tbl_d[1514] = "60008,Fatal Error(D1514). I/O 24V fuse is open. Report to KAWASAKI."
    $sys_err_tbl_d[3800] = "60010,H/W Error(D3800). Serial Communication board memory error. Report to KAWASAKI."
    $sys_err_tbl_d[3813] = "60011,H/W Error(D3813). Amp communication I/F board initialed check error. Report to KAWASAKI."
    $sys_err_tbl_d[3814] = "60012,H/W Error(D3814). Amp communication I/F board undefinition error. Report to KAWASAKI."
    $sys_err_tbl_d[9929] = "60013,H/W Error(D9929). Teach/Repeat relay switch error. Report to KAWASAKI."
    $sys_err_tbl_d[3823] = "60020,H/W Error(D3823). FAN NO.%N in Controller is out of order. Report to KAWASAKI."
.**************************"60021,                  FAN NO.2"
.**************************"60022,                  FAN NO.3"
.**************************"60023,                  FAN NO.4"
.**************************"60024,                  FAN NO.5"
.**************************"60025,                  FAN NO.6"
.**************************"60026,                  FAN NO.7"
.**************************"60027,                  FAN NO.8"
    $sys_err_tbl_d[3824] = "60028,H/W Error(D3824). Fuse No.%N on IO board NO.1 is open. Report to KAWASAKI."
.**************************"60029,                  Fuse No.2"
.**************************"6002a,                  Fuse No.3"
.**************************"6002b,                  Fuse No.4"
    $sys_err_tbl_d[3825] = "6002c,H/W Error(D3825). Fuse No.%N on IO board NO.2 is open. Report to KAWASAKI."
.**************************"6002d,                  Fuse No.2"
.**************************"6002e,                  Fuse No.3"
.**************************"6002f,                  Fuse No.4"
    $sys_err_tbl_d[1517] = "60050,H/W Error(D1517). %R Blown fuse on safety circuit emergency line. Report to KAWASAKI."
.**************************"60051,                  R2
.**************************"60052,                  A1
.**************************"60053,                  A2
    $sys_err_tbl_d[1518] = "60054,H/W Error(D1518). %R Mismatch in the Emer. Stop condition on safety circuit."
.**************************"60055,                  R2
.**************************"60056,                  A1
.**************************"60057,                  A2
    $sys_err_tbl_d[1519] = "60058,H/W Error(D1519). %R Mismatch in safety circuit LS conditions. Report to KAWASAKI."
.**************************"60059,                  R2
.**************************"6005a,                  A1
.**************************"6005b,                  A2
    $sys_err_tbl_d[1520] = "6005c,H/W Error(D1520). %R Mismatch in safety circuit TEACH/REPEAT condition. Report to KAWASAKI."
.**************************"6005d,                  R2
.**************************"60005e,                  A1
.**************************"6005f,                  A2
    $sys_err_tbl_d[1521] = "60060,H/W Error(D1521). %R Mismatch in safety circuit safety-fence condition."
.**************************"60061,                  R2
.**************************"60062,                  A1
.**************************"60063,                  A2
    $sys_err_tbl_d[1522] = "60064,H/W Error(D1522). %R Mismatch in cond. of safety circuit enabling device."
.**************************"60065,                  R2
.**************************"60066,                  A1
.**************************"60067,                  A2
    $sys_err_tbl_d[1523] = "60068,H/W Error(D1523). %R Mismatch in cond. of safety circuit ext.enabling device. Report to KAWASAKI."
.**************************"60069,                  R2
.**************************"6006a,                  A1
.**************************"6006b,                  A2
    $sys_err_tbl_d[1524] = "6006c,H/W Error(D1524). %R Incorrect operation of the safety relay. Report to KAWASAKI."
.**************************"6006d,                  R2
.**************************"6006e,                  A1
.**************************"6006f,                  A2
    $sys_err_tbl_d[1525] = "60070,H/W Error(D1525). %R Incorrect operation of MC(K1). Report to KAWASAKI."
.**************************"60071,                  R2
.**************************"60072,                  A1
.**************************"60073,                  A2
    $sys_err_tbl_d[1526] = "60074,H/W Error(D1526). %R Incorrect operation of MC(K2). Report to KAWASAKI."
.**************************"60075,                  R2
.**************************"60076,                  A1
.**************************"60077,                  A2
    $sys_err_tbl_d[3821] = "60078,%R Motor and/or signal harness connection point is error(D3821)."
.**************************"60079,R2
.**************************"6007a,A1
.**************************"6007b,A2
    $sys_err_tbl_d[3826] = "6007c,H/W Error(D3826). %R Robot DC voltage error. Report to KAWASAKI."
.**************************"6007d,                  R2
.**************************"6007e,                  A1
.**************************"6007f,                  A2
    $sys_err_tbl_d[3828] = "60080,H/W Error(D3828). %R Controller type error. Report to KAWASAKI."
.**************************"60081,                  R2
.**************************"60082,                  A1
.**************************"60083,                  A2
    $sys_err_tbl_d[3829] = "60084,H/W Error(D3829). %R K1 and/or K2 works wrong. Report to KAWASAKI."
.**************************"60085,                  R2
.**************************"60086,                  A1
.**************************"60087,                  A2
    $sys_err_tbl_d[3830] = "60088,H/W Error(D3830). %R PN high voltage error. Report to KAWASAKI."
.**************************"60089,                  R2
.**************************"6008a,                  A1
.**************************"6008b,                  A2
    $sys_err_tbl_d[3831] = "6008c,H/W Error(D3831). %R PN low voltage error. Report to KAWASAKI."
.**************************"6008d,                  R2
.**************************"6008e,                  A1
.**************************"6008f,                  A2
    $sys_err_tbl_d[3832] = "60090,H/W Error(D3832). %R Register over time error. Report to KAWASAKI."
.**************************"60091,                  R2
.**************************"60092,                  A1
.**************************"60093,                  A2
    $sys_err_tbl_d[3833] = "60094,H/W Error(D3833). %R Discharge resistor overheated. Report to KAWASAKI."
.**************************"60095,                  R2
.**************************"60096,                  A1
.**************************"60097,                  A2
    $sys_err_tbl_d[3834] = "60098,H/W Error(D3834). %R Power board switching circuit is abnormal. Report to KAWASAKI."
.**************************"60099,                  R2
.**************************"6009a,                  A1
.**************************"6009b,                  A2
    $sys_err_tbl_d[3835] = "6009c,H/W Error(D3835). %R Power board inrush current limiting circuit is abnormal. Report to KAWASAKI."
.**************************"6009d,                  R2
.**************************"6009e,                  A1
.**************************"6009f,                  A2
    $sys_err_tbl_d[3836] = "600a0,H/W Error(D3836). %R DC Power voltage is abnormal. Report to KAWASAKI."
.**************************"600a1,                  R2
.**************************"600a2,                  A1
.**************************"600a3,                  A2
    $sys_err_tbl_d[3838] = "600a4,H/W Error(D3838). %R Power board is abnormal. Report to KAWASAKI."
.**************************"600a5,                  R2
.**************************"600a6,                  A1
.**************************"600a7,                  A2
    $sys_err_tbl_d[9919] = "600a8,H/W Error(D9919). %R Servo control line error. Report to KAWASAKI."
.**************************"600a9,                  R2
.**************************"600aa,                  A1
.**************************"600ab,                  A2
    $sys_err_tbl_d[9920] = "600ac,H/W Error(D9920). %R Two MC lines are not consistent. Report to KAWASAKI."
.**************************"600ad,                  R2
.**************************"600ae,                  A1
.**************************"600af,                  A2
    $sys_err_tbl_d[9922] = "600b0,H/W Error(D9922). %R Regenerative resistor overheat or disconnect. Report to KAWASAKI."
.**************************"600b1,                  R2
.**************************"600b2,                  A1
.**************************"600b3,                  A2
.END
.******************************************
.** err_1_sy.as
.**   D-Errorで各軸毎に必要なAMP関連エラー
.**   200に続く
.********************************************************
.STRINGS
    $sys_err_tbl_d[3801] = "60100,AMP Error(D3801). %R:%J axis amp interface error 1. Report to KAWASAKI."
    $sys_err_tbl_d[3802] = "60110,AMP Error(D3802). %R:%J axis amp interface error 2. Report to KAWASAKI."
    $sys_err_tbl_d[3803] = "60120,AMP Error(D3803). %R:%J axis amp interface error 3. Report to KAWASAKI."
    $sys_err_tbl_d[3804] = "60130,AMP Error(D3804). %R:%J axis amp power element error. Report to KAWASAKI"
    $sys_err_tbl_d[3805] = "60140,AMP Error(D3805). %R:%J axis amp current detector error. Report to KAWASAKI."
    $sys_err_tbl_d[3806] = "60150,AMP Error(D3806). %R:%J axis amp main circuit voltage unmatch. Report to KAWASAKI."
    $sys_err_tbl_d[3807] = "60160,AMP Error(D3807). %R:%J axis amp memory error(EEPROM error). Report to KAWASAKI."
    $sys_err_tbl_d[3808] = "60170,AMP Error(D3808). %R:%J axis amp inside RAM error. Report to KAWASAKI."
    $sys_err_tbl_d[3809] = "60180,AMP Error(D3809). %R:%J axis amp servo processor error. Report to KAWASAKI."
    $sys_err_tbl_d[3810] = "60190,AMP Error(D3810). %R:%J axis amp parameter error. Report to KAWASAKI."
    $sys_err_tbl_d[3811] = "601a0,AMP Error(D3811). %R:%J axis amp initial processing error. Report to KAWASAKI."
    $sys_err_tbl_d[3812] = "601b0,AMP Error(D3812). %R:%J axis amp undefinition error1. Report to KAWASAKI."
    $sys_err_tbl_d[3815] = "601c0,AMP Error(D3815). It is not possible to communicate with %R:%J axis amp."
    $sys_err_tbl_d[3816] = "601d0,AMP Error(D3816). %R:%J axis amp communication frame reception error. Report to KAWASAKI."
    $sys_err_tbl_d[3817] = "601e0,AMP Error(D3817). %R:%J axis amp communication frame reception timeout. Report to KAWASAKI."
    $sys_err_tbl_d[3818] = "601f0,AMP Error(D3818). %R:%J axis amp communication bank data error. Report to KAWASAKI."
.END
.******************************************
.** err_2_sy.as
.**   D-Errorで各軸毎に必要なAMP関連エラー
.**   100からの続く
.******************************************
.STRINGS
    $sys_err_tbl_d[3819] = "60200,AMP Error(D3819). %R:%J axis amp init timeout. Report to KAWASAKI."
    $sys_err_tbl_d[3820] = "60210,AMP Error(D3820). %R:%J axis amp communication undefinition error. Report to KAWASAKI."
    $sys_err_tbl_d[3822] = "60220,AMP Error(D3822). %R:%J Motor parameter is not consistent with controller. Report to KAWASAKI."
    $sys_err_tbl_d[3837] = "60230,AMP Error(D3837). %R:%J axis amp control power supply error. Report to KAWASAKI."
.***************************60240
.***************************60250
    $sys_err_tbl_e[3800] = "60260,AMP Error(E3800). %R:%J axis amp servo amp heating. Report to KAWASAKI."
    $sys_err_tbl_e[3801] = "60270,AMP Error(E3801). %R:%J axis amp main circuit power supply decrease. Report to KAWASAKI."
    $sys_err_tbl_e[3802] = "60280,AMP Error(E3802). %R:%J Encoder harness disconnected. Report to KAWASAKI."
    $sys_err_tbl_e[3803] = "60290,AMP Error(E3803). %R:%J axis amp speed control error. Report to KAWASAKI."
    $sys_err_tbl_e[3804] = "602a0,AMP Error(E3804). %R:%J axis amp velocity feedback error. Report to KAWASAKI."
    $sys_err_tbl_e[3805] = "602b0,AMP Error(E3805). %R:%J axis amp position envelope error. Report to KAWASAKI."
    $sys_err_tbl_e[3806] = "602c0,AMP Error(E3806). %R:%J axis amp servo ready does not turn on. Report to KAWASAKI."
    $sys_err_tbl_e[3807] = "602d0,AMP Error(E3807). %R:%J axis amp IPM overheated. Report to KAWASAKI."
    $sys_err_tbl_e[3811] = "602e0,AMP Error(E3811). %R:%J axis amp position command error. Report to KAWASAKI."
.***************************602f0
.END
.******************************************
.** err_3_sy.as
.******************************************
.STRINGS
    $sys_err_tbl_e[0903] = "60300,F/W Error(E0903). Check sum error of system data. Report to KAWASAKI."
    $sys_err_tbl_e[1006] = "60301,H/W Error(E1006). Touch panel switch is short-circuited. Report to KAWASAKI."
    $sys_err_tbl_e[1007] = "60302,H/W Error(E1007). Power sequence board is not installed. Report to KAWASAKI."
    $sys_err_tbl_e[1008] = "60303,H/W Error(E1008). Second Power sequence board is not installed. Report to KAWASAKI."
    $sys_err_tbl_e[1009] = "60304,H/W Error(E1009). I/O board is not installed. Report to KAWASAKI."
    $sys_err_tbl_e[1010] = "60305,H/W Error(E1010). Power sequence detects error. Report to KAWASAKI."
    $sys_err_tbl_e[1015] = "60306,H/W Error(E1015). Amp Interface board is not installed. Report to KAWASAKI."
    $sys_err_tbl_e[1117] = "60307,F/W Error(E1117). Process time over. Report to KAWASAKI."
    $sys_err_tbl_e[1153] = "60308,H/W Error(E1153). Power sequence board detected error. Report to KAWASAKI."
    $sys_err_tbl_e[1154] = "60309,H/W Error(E1154). Serial communication port not installed. Report to KAWASAKI."
    $sys_err_tbl_e[1155] = "6030a,H/W Error(E1155). A/D converter is not installed. Report to KAWASAKI."
    $sys_err_tbl_e[1157] = "6030b,H/W Error(E1157). Arm ID I/F board error. Report to KAWASAKI."
.***************************X030c
.***************************X030d
.***************************X030e
.***************************X030f
.***************************X0310
.***************************XXXXX
.***************************X032f
    $sys_err_tbl_e[0100] = "60330,F/W Error(E0100.) %R Abnormal comment statement exists. Report to KAWASAKI."
.***************************60331,R2
.***************************60332,A1
.***************************60333,A2
    $sys_err_tbl_e[0101] = "60334,F/W Error(E0101). %R Nonexistent label. Report to KAWASAKI."
.***************************60335,R2
.***************************60336,A1
.***************************60337,A2
    $sys_err_tbl_e[0102] = "60338,F/W Error(E0102). %R Variable is not defined. Report to KAWASAKI."
.***************************60339,R2
.***************************6033a,A1
.***************************6033b,A2
    $sys_err_tbl_e[0103] = "6033c,F/W Error(E0103). %R Location data is not defined. Report to KAWASAKI."
.***************************6033d,R2
.***************************6033e,A1
.***************************6033f,A2
    $sys_err_tbl_e[0104] = "60340,F/W Error(E0104). %R String variable is not defined. Report to KAWASAKI."
.***************************60341,R2
.***************************60342,A1
.***************************60343,A2
    $sys_err_tbl_e[0105] = "60344,F/W Error(E0105). %R Program or label is not defined. Report to KAWASAKI."
.***************************60345,R2
.***************************60346,A1
.***************************60347,A2
    $sys_err_tbl_e[0106] = "60348,F/W Error(E0106). %R Value is out of range. Report to KAWASAKI."
.***************************60349,R2
.***************************6034a,A1
.***************************6034b,A2
    $sys_err_tbl_e[0107] = "6034c,F/W Error(E0107). %R No array suffix. Report to KAWASAKI."
.***************************6034d,R2
.***************************6034e,A1
.***************************6034f,A2
    $sys_err_tbl_e[0108] = "60350,F/W Error(E0108). %R Divided by zero. Report to KAWASAKI."
.***************************60351,R2
.***************************60352,A1
.***************************60353,A2
    $sys_err_tbl_e[0109] = "60354,F/W Error(E0109). %R Floating point overflow. Report to KAWASAKI."
.***************************60355,R2
.***************************60356,A1
.***************************60357,A2
    $sys_err_tbl_e[0110] = "60358,F/W Error(E0110). %R String too long. Report to KAWASAKI."
.***************************60359,R2
.***************************6035a,A1
.***************************6035b,A2
    $sys_err_tbl_e[0111] = "6035c,F/W Error(E0111). %R Attempted operation with neg. exponent. Report to KAWASAKI."
.***************************6035d,R2
.***************************6035e,A1
.***************************6035f,A2
    $sys_err_tbl_e[0112] = "60360,F/W Error(E0112). %R Too complicated expression. Report to KAWASAKI."
.***************************60361,R2
.***************************60362,A1
.***************************60363,A2
    $sys_err_tbl_e[0113] = "60364,F/W Error(E0113). %R No expressions to evaluate. Report to KAWASAKI."
.***************************60365,R2
.***************************60366,A1
.***************************60367,A2
    $sys_err_tbl_e[0114] = "60368,F/W Error(E0114). %R SQRT parameter is negative. Report to KAWASAKI."
.***************************60369,R2
.***************************6036a,A1
.***************************6036b,A2
    $sys_err_tbl_e[0115] = "6036c,F/W Error(E0115). %R Array suffix value outside range. Report to KAWASAKI."
.***************************6036d,R2
.***************************6036e,A1
.***************************6036f,A2
    $sys_err_tbl_e[0116] = "60370,F/W Error(E0116). %R Faulty or missing argument value. Report to KAWASAKI."
.***************************60371,R2
.***************************60372,A1
.***************************60373,A2
    $sys_err_tbl_e[0117] = "60374,F/W Error(E0117). %R Incorrect joint number. Report to KAWASAKI."
.***************************60375,R2
.***************************60376,A1
.***************************60377,A2
    $sys_err_tbl_e[0118] = "60378,F/W Error(E0118). %R Too many subroutine calls. Report to KAWASAKI."
.***************************60379,R2
.***************************6037a,A1
.***************************6037b,A2
    $sys_err_tbl_e[0119] = "6037c,F/W Error(E0119). %R Nonexistent subroutine. Report to KAWASAKI."
.***************************6037d,R2
.***************************6037e,A1
.***************************6037f,A2
    $sys_err_tbl_e[0901] = "60380,H/W Error(E0901). %R Step data is broken. Report to KAWASAKI."
.***************************60381,R2
.***************************60382,A1
.***************************60383,A2
    $sys_err_tbl_e[0902] = "60384,H/W Error(E0902). %R Expression data is broken. Report to KAWASAKI."
.***************************60385,R2
.***************************60386,A1
.***************************60387,A2
    $sys_err_tbl_e[1021] = "60388,%R Signal harness connection is error or Arm ID board error(E1021)."
.***************************60389,R2
.***************************6038a,A1
.***************************6038b,A2
    $sys_err_tbl_e[1026] = "6038c,H/W Error(E1026). %R Main CPU ID mismatch. Report to KAWASAKI."
.***************************6038d,R2
.***************************6038e,A1
.***************************6038f,A2
    $sys_err_tbl_e[1027] = "50390,(E1027.) %R Safety circuit was cut OFF."
.***************************60391,R2
.***************************60392,A1
.***************************60393,A2
    $sys_err_tbl_e[1078] = "60394,F/W Error(E1078). %R Illegal signal number. Report to KAWASAKI."
.***************************60395,R2
.***************************60396,A1
.***************************60397,A2
    $sys_err_tbl_e[1081] = "60398,F/W Error(E1081). %R Cannot use negative value. Report to KAWASAKI."
.***************************60399,R2
.***************************6039a,A1
.***************************6039b,A2
    $sys_err_tbl_e[1088] = "6039c,F/W Error(E1088). %R Destination is out of motion range. Report to KAWASAKI."
.***************************6039d,R2
.***************************6039e,A1
.***************************6039f,A2
    $sys_err_tbl_e[1094] = "603a0,F/W Error(E1094). %R Illegal joint number. Report to KAWASAKI."
.***************************603a1,R2
.***************************603a2,A1
.***************************603a3,A2
    $sys_err_tbl_e[1122] = "503a4,(E1122). %R Unexpected motor power OFF."
.***************************603a5,R2
.***************************603a6,A1
.***************************603a7,A2
    $sys_err_tbl_e[1135] = "503a8,(E1135). %R Motor power OFF."
.***************************603a9,R2
.***************************603aa,A1
.***************************603ab,A2
    $sys_err_tbl_e[3808] = "503ac,(E3808). %R Motor power OFF (EXT_EMG)."
.***************************603ad,R2
.***************************603ae,A1
.***************************603af,A2
    $sys_err_tbl_e[7503] = "603b0,F/W Error(E7503). %R POWER SEQUENCE setting data incorrect. Report to KAWASAKI."
.***************************603b1,R2
.***************************603b2,A1
.***************************603b3,A2
    $sys_err_tbl_e[9907] = "603b4,H/W Error(E9907). %R Brake release signal error. Report to KAWASAKI."
.***************************603b5,R2
.***************************603b6,A1
.***************************603b7,A2
    $sys_err_tbl_e[9908] = "603b8,H/W Error(E9908). %R The wiring for the robot system is wrong. Report to KAWASAKI."
.***************************603b9,R2
.***************************603ba,A1
.***************************603bb,A2
    $sys_err_tbl_e[9909] = "603bc,H/W Error(E9909). %R Power sequence ready off. Report to KAWASAKI."
.***************************603bd,R2
.***************************603be,A1
.***************************603bf,A2
    $sys_err_tbl_e[7504] = "603c0,(E7504). %R Angle between %MJ is out of range at start location."
.***************************603c1,R2
.***************************603c2,A1
.***************************603c3,A2
    $sys_err_tbl_e[7505] = "603c4,(E7505). %R Angle between %MJ is out of range at end location."
.***************************603c5,R2
.***************************603c6,A1
.***************************603c7,A2
    $sys_err_tbl_e[7506] = "603c8,(E7506). %R Angle between %MJ is out of range."
.***************************603c9,R2
.***************************603ca,A1
.***************************603cb,A2
    $sys_err_tbl_e[1042] = "603cc,%R Signal harness connection is error(E1042)."
.***************************603cd,R2 Signal harness connection is error(E1042).
.***************************603ce,A1 Signal harness connection is error(E1042).
.***************************603cf,A2 Signal harness connection is error(E1042).
.** SCRY10-003-1 a++
    $sys_err_tbl_w[1065] = "603d0,(W1065). %R Cannot servo on due to external force."
.***************************603d1,R2
.***************************603d2,A1
.***************************603d3,A2
.** SCRY10-003-1 a--
.END
.******************************************
.** err_4_sy.as
.******************************************
.STRINGS
    $sys_err_tbl_e[1028] = "60400,%R:%J Motor overloaded(E1028). Check for motion interference and recover it if nessessary."
    $sys_err_tbl_e[1029] = "60410,ENC Error(E1029). %R:%J Encoder rotation data is abnormal. Report to KAWASAKI."
    $sys_err_tbl_e[1030] = "60420,ENC Error(E1030). %R:%J Encoder data is abnormal. Report to KAWASAKI."
    $sys_err_tbl_e[1031] = "60430,ENC Error(E1031). %R:%J Miscount of encoder data. Report to KAWASAKI."
    $sys_err_tbl_e[1032] = "60440,ENC Error(E1032). %R:%J Mismatch ABS and INC encoder data. Report to KAWASAKI."
    $sys_err_tbl_e[1033] = "60450,ENC Error(E1033). %R:%J Encoder line error. Report to KAWASAKI."
    $sys_err_tbl_e[1034] = "60460,Signal harness connection error or %R:%J Encoder initialize error(E1034)."
    $sys_err_tbl_e[1035] = "60470,ENC Error(E1035). %R:%J Encoder response error. Report to KAWASAKI."
    $sys_err_tbl_e[1036] = "60480,ENC Error(E1036). %R:%J Encoder communication error. Report to KAWASAKI."
    $sys_err_tbl_e[1037] = "60490,ENC Error(E1037). %R:%J Encoder data conversion error. Report to KAWASAKI."
    $sys_err_tbl_e[1038] = "604a0,ENC Error(E1038). %R:%J Encoder ABS-track error. Report to KAWASAKI."
    $sys_err_tbl_e[1039] = "604b0,ENC Error(E1039). %R:%J Encoder INC-pulse error. Report to KAWASAKI."
    $sys_err_tbl_e[1040] = "604c0,ENC Error(E1040). %R:%J Encoder MR-sensor error. Report to KAWASAKI."
.END
.******************************************
.** err_5_sy.as
.******************************************
.STRINGS
    $sys_err_tbl_e[1086] = "60500,(E1086). Start point for %R:%J beyond motion range."
    $sys_err_tbl_e[1087] = "60510,(E1087). End point for %R:%J beyond motion range."
    $sys_err_tbl_e[1118] = "60520,F/W Error(E1118). %R:%J Command value suddenly changed. Report to KAWASAKI."
    $sys_err_tbl_e[1119] = "60530,F/W Error(E1119). %R:%J Command value beyond motion range. Report to KAWASAKI."
    $sys_err_tbl_e[1123] = "60540,F/W Error(E1123). %R:%J Speed error. Report to KAWASAKI."
    $sys_err_tbl_e[1124] = "60550,(E1124). %R:%J Position envelope error. Check for motion interference and recover it if nessessary."
    $sys_err_tbl_e[1126] = "60560,F/W Error(E1126). %R:%J Command speed error. Report to KAWASAKI."
    $sys_err_tbl_e[1128] = "60570,(E1128). %R:%J Uncoincidence error. Check for motion interference and recover it if nessessary."
    $sys_err_tbl_e[7500] = "60580,(E7500). %R:%J Collision is detected. Check for motion interference and recover it if nessessary."
    $sys_err_tbl_e[7501] = "60590,(E7501). %R:%J Unexpected shock is detected. Check for motion interference and recover it if nessessary."
.END
.******************************************
.** err_9_ap.as
.******************************************
.STRINGS
    $ae_lim_l          = "40900,%R:%J is out of lower limit. Move to positive (%DIR) direction."
.************************"40901,R1:J2"
.************************"40902,R1:J3"
.************************"40903,R1:J4"
.************************"40904,R1:J5"
.************************"40905,R1:J6"
.************************"40906,R1:J7"
.************************"40907,R2:J1"
.************************"40908,R2:J2"
.************************"40909,R2:J3"
.************************"4090a,R2:J4"
.************************"4090b,R2:J5"
.************************"4090c,R2:J6"
.************************"4090d,R2:J7"
.************************"4090e,A1:J1"
.************************"4090f,A2:J1"
    $ae_lim_u          = "40910,%R:%J is out of upper limit. Move to negative (%DIR) direction."
.************************"40911,R1:J2"
.************************"40912,R1:J3"
.************************"40913,R1:J4"
.************************"40914,R1:J5"
.************************"40915,R1:J6"
.************************"40916,R1:J7"
.************************"40917,R2:J1"
.************************"40918,R2:J2"
.************************"40919,R2:J3"
.************************"4091a,R2:J4"
.************************"4091b,R2:J5"
.************************"4091c,R2:J6"
.************************"4091d,R2:J7"
.************************"4091e,A1:J1"
.************************"4091f,A2:J1"
.END
.******************************************
.** err_a_ap.as
.******************************************
.STRINGS
    $AE_MD_AUTO_TECH   = "20a00,Automatic-Run command is not allowed when the system is in Teaching Mode."
    $AE_MD_TECH_AUTO   = "20a01,Teaching command is not allowed when the system is in Auto Mode."
    $AE_MD_TECH_PHTM   = "20a02,Teaching command is not allowed when the system is in Phantom Mode."
    $AE_MD_CHAR_AUTO   = "20a03,Characterize command is not allowed when the system is in Auto Mode."
    $AE_MD_CHAR_PHTM   = "20a04,Characterize command is not allowed when the system is in Phantom Mode."
    $AE_MD_ERROR_ANY   = "20a0f,Command received in invalid system mode."
.** harish 090527 a++
    $AE_MD_THIS_SOFT   = "20a05,This command is not allowed when the system is in Soft Mode."
    $AE_MD_AUTO_SOFT   = "20a06,Automatic-Run command is not allowed when the system is in Soft Mode."
.** harish 090527 a--

    $AE_NOT_RUN        = "50a10,Turn RUN/HOLD switch on the operation panel to RUN."
    $AE_NOT_REPEAT     = "50a11,Turn TEACH/REPEAT switch on the operation panel to REPEAT."
    $AE_TLOCK_ON       = "50a12,Turn off TEACH LOCK switch on the teaching pendant."

    $AE_MOVLIM_ABS     = "20a20,Motion distance by MovAbs-Command is limited because it is too big."
    $AE_MOVLIM_REL     = "20a21,Motion distance by MovRel-Command is limited because it is too big."
    $AE_N_TAUGHT0      = "20a22,Specified port position is not taught yet."
    $AE_CFGLOCK        = "20a23,SetCfg is disable. Because specified CfgID (and port) is Locked."
    $AE_MLOC_PPOS      = "20a24,MovToLoc-Command Purpose Error. Robot cannot move to specified purpose position."
    $AE_PITCH_ZERO     = "20a25,Spefified port pitch is ZERO even if slot is not ONE. Confirm the slot and pitch data."
.***
    $AE_TCHPOS_PPOS    = "20a30,TchPos-Command Purpose Error."	;** Purpose for specified port should be one of TAUGHT/GET/GTS/GTX/GTR/PUT/PTS/PTX/PTR/MONGS."
.***    $AE_WVERT_SLOT     = "2a31,Teaching slot should be 1 or Last-slot because of INTERPOLATION setting."
    $AE_OUTOF_AREA     = "20a32,Specified position is out of AREA."
.***    $AE_MOVAREA        = "20a33,Specified Robot can not move to specified AREA."
    $AE_NO_AREA        = "20a34,PORTAREA is not defined. Defind the PORTAREA by [SetCfg,PORTAREA] command."
.***    $AE_MPP_PORTZ      = "2a35,PORTZ is not defined. Defind the PORTZ by [SetCfg,PORTZ] command."
.***    $AE_OUT_MAP[1]     = "2a36,Robot1 mapping or sniffing position is out of range at specified port."
.***    $AE_OUT_MAP[2]     = "2a37,Robot2 mapping or sniffing position is out of range at specified port."
.***    $AE_PIN_OUT        = "2a38,PinTch position is out of range."
.***    $AE_PIN_PANGL      = "2a39,PinTch Error because PORTANGLE is undefined. Set the PORTANGLE by [SetCfg,PORTANGLE] command."
.***    $AE_SPIN_VERT      = "2a40,S1 position error. S1 should be vertical for specified port."
    $AE_SPIN_HORI      = "20a41,S1 position error. S1 should be horizontal for specified port."
    $AE_SPIN_FACE      = "20a42,S1 position error. S1 should be face-up(180deg) for specified port."
.***
    $AE_AJP_DATA       = "20a50,AdjustPos maximum allowed value is +/-1.0 mm and +/-1.0 degree."
    $AE_AJP_HAND       = "20a51,AdjustPos CoordID must be TX1,TY1,TR1,EX1 or EY1."
    $AE_AJP_OUT        = "20a52,Adjusted position is out of range."
.***    $AE_AJP_MSLOTS     = "2a53,AdjustPos command should only be accepted if the slot matches a TRAININGSLOTS."
.***
.***    $AE_TRK_SEN_ERR    = "6a60,Track Sensor Error. Sensor breakdown or Zeroing position of TRACK abnormal."
.***
.** harish 090629-1 a+
    $AE_BAKLSH_JIG    = "20a60, Backlash JIG not mounted correctly. Please check and try again."
.** harish 090515-1 a++
    $AE_IMAP_CORRECT   = "20a70,Robot can not correct taught position for IMAP."
    $AE_CANNOT_IMAP    = "20a71,Robot can not execute specified command. Because specified station is not Scan station."
    $AE_IMAP_GETPUT    = "20a72,Robot can not execute specified command. Because specified station is Scan station."
.** harish 090515-1 a--
.***    $AE_IPRO_CANNOT    = "2a73,Robot can not execute specified command. Because specified station is not IPROBE station."
.***    $AE_IPRO_GETPUT    = "2a74,Robot can not execute specified command. Because specified station is IPROBE station."
.***    $AE_PLOCK_PORT     = "2a80,Specified Port could not be used. PLOCK Error."
.***    $AE_PLOCK_HOME     = "2a81,Other robot is not home. PLOCK Error."
.***    $AE_A_HOLDING[3]   = "2aa0,Aligner1 holds unexpected wafer or Vacuum signal breakdown."
.***    $AE_A_NO_HOLD[3]   = "2aa1,Aligner1 does not hold expected Wafer or Air vacuum error or vacuum signal breakdown."
.***    $AE_A_ERR_RELS[3]  = "2aa2,Aligner1 could not release wafer. Vacuum signal breakdown or Vacuum hose kink."
    $AE_ALGN_VAC[3]    = "20aa3,During ALGN,%R vacuumed sensor off. Vacuum error or vacuum signal breakdown."
    $AE_ANGLE_VAC[3]   = "20aa4,During %R rotation for GetWaf,%R vacuumed sensor off. Vacuum error or vacuum signal breakdown."

    $AE_LALIGN01[3]    = "20ab0,Line Sensor %R Error (1), Check motion speed or parameters."
    $AE_LALIGN02[3]    = "20ab1,Line Sensor %R Error (2), Check motion speed or parameters."
    $AE_LALIGN03[3]    = "20ab2,Line Sensor %R can not find notch, Check Sensor-power or the wafer."
    $AE_LALIGN04[3]    = "20ab3,Line Sensor %R found too small notch, Check the wafer."
    $AE_LALIGN05[3]    = "20ab4,Line Sensor %R found too big notch,Check the wafer."
    $AE_LALIGNXX[3]    = "20ab5,Line Sensor %R Error Number "
.**    $AE_NETCONF        = "2af0,Specified network parameter is wrong."
.END
.******************************************
.** err_b_ap.as
.******************************************
.STRINGS
    $ae_edg_err[1,0,1] = "20b00,%R:%B,Checking wafer presence after GetWaf, blade does not hold a expected wafer."
.************************"20b01,R1:B2
.************************"20b02,R2:B1
.************************"20b03,R2:B2
    $ae_edg_err[2,0,1] = "20b04,%R:%B,Checking wafer presence after GetWaf retraction, blade does not hold a expected wafer."
.************************"20b05,R1:B2
.************************"20b06,R2:B1
.************************"20b07,R2:B2
    $ae_edg_err[3,0,1] = "20b08,%R:%B,Checking wafer presence before PutWaf, blade does not hold a expected wafer."
.************************"20b09,R1:B2"
.************************"20b0a,R2:B1"
.************************"20b0b,R2:B2"
    $ae_edg_err[4,0,1] = "20b0c,%R:%B,Checking wafer presence, blade does not hold a expected wafer."
.************************"20b0d,R1:B2"
.************************"20b0e,R2:B1"
.************************"20b0f,R2:B2"


    $ae_edg_err[1,2,1] = "20b10,%R:%B,Releasing plunger to GetWaf, plunger status is gripped. Plunger is defective or air pressure has decreased."
.************************"20b11,R1:B2"
.************************"20b12,R2:B1"
.************************"20b13,R2:B2"
.**************[2,2,1]****X0b14
.*************************X0b15
.*************************X0b16
.*************************X0b17
    $ae_edg_err[3,2,1] = "20b18,%R:%B,Releasing plunger to PutWaf, plunger status is gripped. Plunger is defective or air pressure has decreased."
.************************"20b19,R1:B2"
.************************"20b1a,R2:B1"
.************************"20b1b,R2:B2"
    $ae_edg_err[4,2,1] = "20b1c,%R:%B,Releasing plunger, plunger status is gripped. Plunger is defective or air pressure has decreased."
.************************"20b1d,R1:B2"
.************************"20b1e,R2:B1"
.************************"20b1f,R2:B2"


    $ae_edg_err[1,1,2] = "20b20,%R:%B,Checking wafer absence before GetWaf, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b21,R1:B2"
.************************"20b22,R2:B1"
.************************"20b23,R2:B2"
.**************[2,1,2]***"x0b24
.************************"x0b25
.************************"x0b26
.************************"x0b27
    $ae_edg_err[3,1,2] = "20b28,%R:%B,Checking wafer absence after PutWaf, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b29,R1:B2"
.************************"20b2a,R2:B1"
.************************"20b2b,R2:B2"
    $ae_edg_err[4,1,2] = "20b2c,%R:%B,Checking wafer absence, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b2d,R1:B2"
.************************"20b2e,R2:B1"
.************************"20b2f,R2:B2"



    $ae_edg_err[1,0,2] = "20b30,%R:%B,Checking wafer presence after GetWaf, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b31,R1:B2"
.************************"20b32,R2:B1"
.************************"20b33,R2:B2"
    $ae_edg_err[2,0,2] = "20b34,%R:%B,Checking wafer presence after GetWaf retraction, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b35,R1:B2"
.************************"20b36,R2:B1"
.************************"20b37,R2:B2"
    $ae_edg_err[3,0,2] = "20b38,%R:%B,Checking wafer presence before PutWaf, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b39,R1:B2"
.************************"20b3a,R2:B1"
.************************"20b3b,R2:B2"
    $ae_edg_err[4,0,2] = "20b3c,%R:%B,Checking wafer presence, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b3d,R1:B2"
.************************"20b3e,R2:B1"
.************************"20b3f,R2:B2"


    $ae_edg_err[1,1,0] = "20b40,%R:%B,Checking wafer absence before GetWaf, blade holds unexpected wafer. Plunger is defective or air pressure has decreased."
.************************"20b41,R1:B2"
.************************"20b42,R2:B1"
.************************"20b43,R2:B2"
.**************[2,1,0]****x0b44
.*************************x0b45
.*************************x0b46
.*************************x0b47
    $ae_edg_err[3,1,0] = "20b48,%R:%B,Checking wafer absence after PutWaf, blade holds unexpected wafer. Plunger is defective or air pressure has decreased."
.************************"20b49,R1:B2"
.************************"20b4a,R2:B1"
.************************"20b4b,R2:B2"
    $ae_edg_err[4,1,0] = "20b4c,%R:%B,Checking wafer absence, blade holds unexpected wafer. Plunger is defective or air pressure has decreased."
.************************"20b4d,R1:B2"
.************************"20b4e,R2:B1"
.************************"20b4f,R2:B2"


    $ae_edg_err[1,2,0] = "20b50,%R:%B,Releasing plunger to GetWaf, plunger status is holding wafer. Plunger is defective or air pressure has decreased."
.************************"20b51,R1:B2"
.************************"20b52,R2:B1"
.************************"20b53,R2:B2"
.**************[2,2,0]****x0b54
.*************************x0b55
.*************************x0b56
.*************************x0b57
    $ae_edg_err[3,2,0] = "20b58,%R:%B,Releasing plunger to PutWaf, plunger status is holding wafer. Plunger is defective or air pressure has decreased."
.************************"20b59,R1:B2"
.************************"20b5a,R2:B1"
.************************"20b5b,R2:B2"
    $ae_edg_err[4,2,0] = "20b5c,%R:%B,Releasing plunger, plunger status is holding wafer. Plunger is defective or air pressure has decreased."
.************************"20b5d,R1:B2"
.************************"20b5e,R2:B1"
.************************"20b5f,R2:B2"



    $ae_edg_err[1,1,3] = "60b60,%R:%B,Checking wafer absence before GetWaf, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b61,R1:B2"
.************************"60b62,R2:B1"
.************************"60b63,R2:B2"
.**************[2,1,3]****x0b64
.*************************x0b65
.*************************x0b66
.*************************x0b67
    $ae_edg_err[3,1,3] = "60b68,%R:%B,Checking wafer absence after PutWaf, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b69,R1:B2"
.************************"60b6a,R2:B1"
.************************"60b6b,R2:B2"
    $ae_edg_err[4,1,3] = "60b6c,%R:%B,Checking wafer absence, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b6d,R1:B2"
.************************"60b6e,R2:B1"
.************************"60b6f,R2:B2"


    $ae_edg_err[1,0,3] = "60b70,%R:%B,Checking wafer presence after GetWaf, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b71,R1:B2"
.************************"60b72,R2:B1"
.************************"60b73,R2:B2"
    $ae_edg_err[2,0,3] = "60b74,%R:%B,Checking wafer presence after GetWaf retraction, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b75,R1:B2"
.************************"60b76,R2:B1"
.************************"60b77,R2:B2"
    $ae_edg_err[3,0,3] = "60b78,%R:%B,Checking wafer presence before PutWaf, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b79,R1:B2"
.************************"60b7a,R2:B1"
.************************"60b7b,R2:B2"
    $ae_edg_err[4,0,3] = "60b7c,%R:%B,Checking wafer presence, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b7d,R1:B2"
.************************"60b7e,R2:B1"
.************************"60b7f,R2:B2"


    $ae_edg_err[1,2,3] = "60b80,%R:%B,Releasing plunger to GetWaf, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b81,R1:B2"
.************************"60b82,R2:B1"
.************************"60b83,R2:B2"
.**************[2,2,3]****x0b84
.**************[2,2,3]****x0b85
.**************[2,2,3]****x0b86
.**************[2,2,3]****x0b87
    $ae_edg_err[3,2,3] = "60b88,%R:%B,Releasing plunger to PutWaf, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b89,R1:B2"
.************************"60b8a,R2:B1"
.************************"60b8b,R2:B2"
    $ae_edg_err[4,2,3] = "60b8c,%R:%B,Releasing plunger, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b8d,R1:B2"
.************************"60b8e,R2:B1"
.************************"60b8f,R2:B2"

    $ae_edg_err[4,4,2] = "20b90,%R:%B,Holding plunger, plunger status is released. Plunger defective or air pressure decreased."
.************************"20b91,R1:B2"
.************************"20b92,R2:B1"
.************************"20b93,R2:B2"
    $ae_edg_err[4,4,3] = "60b94,%R:%B,Holding plunger, plunger sensor breakdown. Robot HW error. Report to KAWASAKI."
.************************"60b95,R1:B2"
.************************"60b96,R2:B1"
.************************"60b97,R2:B2"

    $ae_edg_err[0,0,0] = "60ba0,%R:%B,Plunger Error. There is no error message. Report to KAWASAKI."
.************************"60ba1,R1:B2"
.************************"60ba2,R2:B1"
.************************"60ba3,R2:B2"
.END
.******************************************
.** err_c_ap.as
.******************************************
.STRINGS
    $AE_NEED_HOME      = "30c00,%R need homing. Issue [HOME]-command before motion command."
.************************"30c01,R2
    $AE_N_TAUGHT       = "20c02,%R,Specified port position is not taught yet."
.************************"20c03,R2"
    $AE_TGET_ING       = "20c04,%R,Not completed TGetWaf command. Complete TGetWaf command or issue INIT or HOME command to interrupt TGetWaf command."
.************************"20c05,R2
    $AE_TPUT_ING       = "20c06,%R,Not completed TPutWaf command. Complete TPutWaf command or issue INIT or HOME command to interrupt TPutWaf command."
.************************"20c07,R2
    $AE_TGET_ERR       = "20c08,%R,Previous TGetWaf command fails. Issue INIT or HOME command to allow motion command."
.************************"20c09,R2
    $AE_TPUT_ERR       = "20c0a,%R,Previous TPutWaf command fails. Issue INIT or HOME command to allow motion command."
.************************"20c0b,R2
    $AE_MLOC_PP_GTR    = "20c0c,%R,MovToLoc command GTR Error. The previous Command Purpose must be GTX."
.************************"20c0d,R2"
    $AE_MLOC_PP_PTR    = "20c0e,%R,MovToLoc command PTR Error. The previous Command Purpose must be PTX."
.************************"20c0f,R2"
.**    $AE_TCH_R1P_NEG    = "2c10,R1 rotation angle incorrect. Rotate 360 degrees clockwise(negative) and reteach."
.**    $AE_TCH_R2P_POS    = "2c11,R2 rotation angle incorrect. Rotate 360 degrees counter-clockwise(positive) and reteach."
.**    $AE_ZERO_RX1       = "2c12,[SetZero,%R:RX1] cannot be executed. Because Arm position is out of range."
.**.************************"2c13,R2"
    $AE_MAP_STA_ON     = "20c14,%R mapping sensor ON at mapping start position. Mapping start position error or mapping sensor error."
.************************"20c15,R2"
    $AE_MAP_END_ON     = "20c16,%R mapping sensor ON at mapping end position. Mapping end position error or mapping sensor error."
.************************"20c17,R2"
    $AE_MAP_ON_OFF     = "60c18,%R mapping sensor error. Threshold of mapping sensor AMP is wrong or it is defective."
.************************"60c19,R2"
    $AE_N_SAFE_MAP     = "20c1a,%R detected Protruding Wafer. Mapping is not safe."
.************************"20c1b,R2"
    $AE_NOT_PINTCHZ    = "20c1c,%R Wafer not detected by PinTch Z detection."
.************************"20c1d,R2"
.**    $AE_PT_ADJ_TR_PP   = "2c1e,%R Rotation angle incorrect. Rotation angle of Cassette Port should be between (+/-)170 and (+/-)190 degree."
.**.************************"2c1f,R2"
.**
.**    $AE_MP_CROSSED     = "2c20,%R cannot Get/Put wafer of specified port:slot because mapping result is crossed wafer."
.**.************************"2c21,R2"
.**    $AE_MP_DOUBLE      = "2c22,%R cannot Get/Put wafer of specified port:slot because mapping result is double wafers."
.**.************************"2c23,R2"
.**    $AE_MP_UNKNOWN     = "2c24,%R cannot Get/Put wafer of specified port:slot because mapping result is unknown error."
.**.************************"2c25,R2"
.**
       $AE_SNF_NWAF       = "20c2a,%R Wafer not detected by sniffing."
.**.************************"20c2b,R2"
       $AE_USESNFZ        = "20c2c,%R The Detected wafer position by sniffing is Error."
.**.************************"20c2d,R2"
.**
.**    $AE_ATZ_ZDOWN      = "2c30,%R Only Z Down is available. Because current position is in interference area."
.**.************************"2c31,R2"
.**    $AE_ATZ_DEST       = "2c32,%R The motion is not available. Because destination position is in interference area."
.**.************************"2c33,R2"
.**    $AE_ATZ_PASS       = "2c34,%R To move from current position to destination position, interference area is passed."
.**.************************"2c35,R2"
.**    $AE_ATZ_TCHPOS     = "2c36,%R TchPos is not available. Because current position is in interference area."
.**.************************"2c37,R2"
.**    $AE_DODGE_S        = "2c38,%R start position may interfere center frame."
.**.************************"2c39,R2 start position may interfere center frame."
.**    $AE_DODGE_E        = "2c3a,%R end position may interfere center frame."
.**.************************"2c3b,R2 end position may interfere center frame."
.**    $AE_DODGE_T        = "2c3c,%R can not accept center frame interference position."
.**.************************"2c3d,R2 can not accept center frame interference position."
.*                                                      /* SCRX12-006 a++
       $AE_DUCKRNGZ       = "20c3e,Motion is not possible. Entire %R Z Range is within interference area. Check CfgIds [DUCKHGH], [DUCKHGHUOFST], [DUCKHGHLOFST]."
.***************************"20c3f,Motion is not possible. Entire R2 Z Range is within interference area. Check CfgIds [DUCKHGH], [DUCKHGHUOFST], [DUCKHGHLOFST]."
    $AE_WAFER_GONE     = "40c4c,%R:%B holding wafer was gone. %R was automatically stopped."
.**.************************"4c4d,R1:B2"
.**.************************"4c4e,R2:B1"
.**.************************"4c4f,R2:B2"
.**
    $AE_FAN[1]         = "20c50,%R FAN1 Error. To invalidate the Fan check, issue the [SetCfg,FANROB:0] command. Report to KAWASAKI."
.*************************20c51,R2
    $AE_FAN[2]         = "20c52,%R FAN2 Error. To invalidate the Fan check, issue the [SetCfg,FANROB:0] command. Report to KAWASAKI."
.*************************20c53,R2
    $AE_FAN[5]         = "20c54,%R Right side Track FAN Error. To invalidate the Fan check, issue the [SetCfg,FANTRKR:0] command. Report to KAWASAKI."
.*************************20c55,R2
    $AE_FAN[6]         = "20c56,%R Left side Track FAN Error. To invalidate the Fan check, issue the [SetCfg,FANTRKL:0] command. Report to KAWASAKI."
.*************************20c57,R2
    $AE_FAN[3]         = "20c58,%R FAN3 Error. To invalidate the Fan check, issue the [SetCfg,FANROB:0] command. Report to KAWASAKI."
.*************************20c59,R2
    $AE_FAN[4]         = "20c5a,%R FAN4 Error. To invalidate the Fan check, issue the [SetCfg,FANROB:0] command. Report to KAWASAKI."
.*************************20c5b,R2
    $AE_FAN[7]         = "20c5c,Controller FAN Error. To invalidate the Fan check, issue the [SetCfg,FANCTL:0] command. Report to KAWASAKI."
.**    $AE_MLOC_PP_GWR    = "2c60,%R,MovToLoc command GWR Error. The previous Command Purpose must be GWV."
.**.************************"2c61,R2"
.** harish undefined error on FIC 081007 m++
    $AE_HOME_HAND      = "60c70,%R cannot home automatically. Please move to near the home position by hand."
.************************"60c71,R2"
    $AE_RELABS_LIN     = "20c72,%R cannot move lineally or destination position is out of range."
.************************"20c73,R2"
.**    $ae_fnc_pitch      = "2c80,%R cannot access the specified port. Because slot pitch does not match the end-effectors finger pitch."
.**.************************"2c81,R2"
.**    $ae_fnc_slot       = "2c82,%R cannot access the specified port slot. Because multi-fingered end effector cannot access the valid slot."
.**.************************"2c83,R2"
    $ae_tslots_zerr    = "20c90,%R training-slots Z interpolation data is out of [TSZCALERROR]."
.************************"20c91,R2"
    $ae_tslots_rerr    = "20c92,%R training-slots R interpolation data is out of [TSRCALERROR]."
.**.************************"20c93,R2"
.*                                                      /* SCRX12-006-1 a++
    $ae_X1off_SavErr    = "20c94,%R Attempted change to [X1OFFSET] resulted in a Saved Position out of the robot's range. Decrease [X1OFFSET] or reteach and save position."
.************************"20c95,R2"
    $ae_X1off_TchErr    = "20c96,%R Attempted change to [X1OFFSET] resulted in a Working Position out of the robot's range. Decrease [X1OFFSET] or reteach."
.************************"20c97,R2"
    $ae_X1off_Tching    = "20c98,%R Attempted Teach position is out of the robots range due to [X1OFFSET]. Decrease [X1OFFSET] or reteach."
.************************"20c99,R2"
.*                                                      /* SCRX12-006-1 a--
.** harish undefined error on FIC 081007 m--
    $AE_UNDEF          = "60cfc,%R:Error Number:"
.************************"60cfd,R2:Error Number:"
.************************"60cfe,A1:Error Number:"
.************************"60cff,A2:Error Number:"
.END
.******************************************
.** err_d_ap.as
.******************************************
.STRINGS
    $AE_NOT_INIT       = "40d00,%R has not been initialized. Issue INIT comamnd if necessary."
.************************"40d01,R2"
.************************"40d02,R3"
.************************"40d03,R4"
    $AE_STOP_CMD       = "40d04,%R Motion was interruptted by [STOP,0] command."
.************************"40d05,R2"
.************************"40d06,R3"
.************************"40d07,R4"
    $AE_STOP_CMDI      = "20d08,%R Motion was interruptted by [STOP,1] command."
.************************"20d09,R2"
.************************"20d0a,R3"
.************************"20d0b,R4"
    $AE_NOT_EXTRUN     = "50d0c,%R Release external hold signal."
.************************"50d0d,R2
.************************"50d0e,R3
.************************"50d0f,R4
.** harish 090327-2 m
    $AE_POW_OFF        = "50d10,%R Motor Power OFF because external interlock signal detected or emergency stopped."
.************************"50d11,R2
.************************"50d12,R3
.************************"50d13,R4
    $AE_ERRLIM_DST     = "20d14,%R Destination position is out of range."
.************************"20d15,R2"
.************************"20d16,A1"
.************************"20d17,A2"
    $AE_NO_RB_PRG      = "60d18,%R,ROBOT FW Error. Not Exist Program. Software debug is needed. Report to KAWASAKI."
.************************"60d19,R2
.************************"60d1a,A1
.************************"60d1b,A2
    $AE_DEV_INIT[1]    = "20d20,%R cannot be used. Because %R does not be initialized."
    $AE_DEV_INIT[2]    = "20d21,%R cannot be used. Because %R does not be initialized."
    $AE_DEV_INIT[3]    = "20d22,%R cannot be used. Because %R does not be initialized."
    $AE_DEV_INIT[4]    = "20d23,%R cannot be used. Because %R does not be initialized."
    $AE_DEV_USE[1]     = "20d24,%R cannot be used. Because %R is busy."
    $AE_DEV_USE[2]     = "20d25,%R cannot be used. Because %R is busy."
    $AE_DEV_USE[3]     = "20d26,%R cannot be used. Because %R is busy."
    $AE_DEV_USE[4]     = "20d27,%R cannot be used. Because %R is busy."
.***
.END
.** harish 090223-1 a++ Backlash detection firmware with soft teach
.*************************************
.* Soft Teach
.*************************************
.PROGRAM autostart6.pc
    tlg_m_Repeat0 = 9999
Loop:
    IF TASK(1004)==1 THEN
        CALL tlg_TeachRepeat
    END
    TWAIT 0.032
    goto Loop
.END
.REALS
    tlg_tch_stop = -1
.END
.PROGRAM tlg_TeachParam()
    ZYMSRVPRM 1:2 AS_PERLM = 40*1.5
    ZYMSRVPRM 1:4 AS_PERLM = 40*1.5
    ZYMSRVPRM 1:6 AS_PERLM = 20*1.5
    ZYMSRVPRM 1:7 AS_PERLM = 60*1.5

    ZYMSRVPRM 2:2 AS_PERLM = 30*1.5
    ZYMSRVPRM 2:4 AS_PERLM = 30*1.5
    ZYMSRVPRM 2:6 AS_PERLM = 60*1.5
;
    ZILIMCHG  1:2 CCWILIM = 50*1.5
    ZILIMCHG  1:2 CWILIM  = 50*1.5
    ZILIMCHG  1:4 CCWILIM = 50*1.5
    ZILIMCHG  1:4 CWILIM  = 50*1.5
    ZILIMCHG  1:6 CCWILIM = 25*1.5
    ZILIMCHG  1:6 CWILIM  = 25*1.5
    ZILIMCHG  1:7 CCWILIM = 50*1.5
    ZILIMCHG  1:7 CWILIM  = 50*1.5
    ZAXIS 1:7 = ,,,,,,,,1500
;
    ZILIMCHG  2:2 CCWILIM = 40*1.5
    ZILIMCHG  2:2 CWILIM  = 40*1.5
    ZILIMCHG  2:4 CCWILIM = 30*1.5
    ZILIMCHG  2:4 CWILIM  = 30*1.5
    ZILIMCHG  2:6 CCWILIM = 30*1.5
    ZILIMCHG  2:6 CWILIM  = 30*1.5
.END

.PROGRAM tlg_RepeatParam()
    ZYMSRVPRM 1:2 AS_PERLM = srvprm[srv_nt570,2,24]
    ZYMSRVPRM 1:4 AS_PERLM = srvprm[srv_nt570,4,24]
    ZYMSRVPRM 1:6 AS_PERLM = srvprm[srv_nt570,6,24]
    ZYMSRVPRM 1:7 AS_PERLM = srvprm[srv_nt570,7,24]

    ZYMSRVPRM 2:2 AS_PERLM = srvprm[srv_nt410,2,24]
    ZYMSRVPRM 2:4 AS_PERLM = srvprm[srv_nt410,4,24]
    ZYMSRVPRM 2:6 AS_PERLM = srvprm[srv_nt410,6,24]
;
    ZILIMCHG  1:2 CCWILIM = 0
    ZILIMCHG  1:2 CWILIM  = 0
    ZILIMCHG  1:4 CCWILIM = 0
    ZILIMCHG  1:4 CWILIM  = 0
    ZILIMCHG  1:6 CCWILIM = 0
    ZILIMCHG  1:6 CWILIM  = 0
    ZILIMCHG  1:7 CCWILIM = 0
    ZILIMCHG  1:7 CWILIM  = 0
;
    ZILIMCHG  2:2 CCWILIM = 0
    ZILIMCHG  2:2 CWILIM  = 0
    ZILIMCHG  2:4 CCWILIM = 0
    ZILIMCHG  2:4 CWILIM  = 0
    ZILIMCHG  2:6 CCWILIM = 0
    ZILIMCHG  2:6 CWILIM  = 0
    ZAXIS 1:7 = ,,,,,,,,1000
.END
.PROGRAM tlg_TeachRepeat()
    .m_Current = SWITCH(REPEAT)
    IF  tlg_tch_stop == 0 THEN
	RETURN
    END
    IF .m_Current != tlg_m_Repeat0 THEN
        MC ZPOWER 1: OFF
        TWAIT 0.1
	IF .m_Current == 0 THEN
	    type "REPEAT->TEACH"
	    CALL tlg_TeachParam
	ELSE
	    type "TEACH->REPEAT"
	    CALL tlg_RepeatParam
.** harish 090701-3 a++
            IF vc.mode[pc3] == mode_soft
                CALL softparam1
	    ELSEIF vc.mode[pc4] == mode_soft
	        CALL softparam2
	    END
.** harish 090701-3 a--
	END
    END
    tlg_m_Repeat0 = .m_Current
.END
.***************************************************************************
.* Backlask.AS
.***************************************************************************
.** harish 090320 a++
.*NEW LOGGING STUFF ADDED FOR BACKLASH LOGGING

.REALS
	log_bklsh_buf[1] = 10   		;** buffer length R1
	log_bklsh_buf[2] = 10 			;** buffer length R2
.END
..MONCMD NOEXIST_SET_R log_bklsh_cnt[1]  = 0	;** buffer count R1
..MONCMD NOEXIST_SET_R log_bklsh_cnt[2]  = 0	;** buffer count R2
..MONCMD NOEXIST_SET_R log_bklsh_max[1]  = 0	;** buffer max R1
..MONCMD NOEXIST_SET_R log_bklsh_max[2]  = 0	;** buffer max R2
..MONCMD NOEXIST_SET_S $log_bklsh_max[1]  = ""	;** buffer max String R1
..MONCMD NOEXIST_SET_S $log_bklsh_max[2]  = ""	;** buffer max String R2
..MONCMD NOEXIST_SET_R 	pz.bklsh = false	;** Debug flag

.PROGRAM log_bklsh(.robot,.bklsh,.ind[],.jnts[],.count)
.** .robot is robot number
.** .bklsh is backlash value
    log_bklsh_cnt[.robot] = RING(log_bklsh_cnt[.robot],1,log_bklsh_buf[.robot])
    .$log_bklsh_inf[.robot] = $DATE(3)+" "+$TIME+" ROBOT"+$ENCODE(.robot)+" "+$ENCODE(.bklsh)+" "
    for .i=1 to .count
	.$log_bklsh_inf[.robot] = .$log_bklsh_inf[.robot]+"JT"+$ENCODE(.jnts[.i])+"-"+$ENCODE(.ind[.i])
    end
;**robot serial number
    .$log_rbtsn[.robot] = $TRIM_B($SYSDATA(ZROB.NAME,.robot))+" "+$TRIM_B($ENCODE(SYSDATA(ZROB.MGFNO,.robot)))
;**controller serial number
    .$log_cntsn[.robot] = $TRIM_B($ENCODE(SYSDATA(ZCONT.MGFNO,1))) +" "+$STR_ID(2)+" "+$STR_ID(5)
.** logging of backlash data
    $log_bklsh[.robot,log_bklsh_cnt[.robot]] = .$log_bklsh_inf[.robot]+" "+.$log_rbtsn[.robot]+" "+.$log_cntsn[.robot]
    IF pz.bklsh THEN
	TYPE "BACKLASH LOG ",$log_bklsh[.robot,log_bklsh_cnt[.robot]]
    END
    if log_bklsh_max[.robot]<.bklsh
	$log_bklsh_max[.robot]=$log_bklsh[.robot,log_bklsh_cnt[.robot]]
    end
    log_bklsh_max[.robot] = MAXVAL(log_bklsh_max[.robot],.bklsh)
.END
.** harish 090320 a--
.PROGRAM introbready1()
call dwait1("init")
TWAIT 2
call dwait1("stop,0")
TWAIT 1
call srvprmnew1
TWAIT 4
TYPE "ROBOT READY FOR BACKLASH TEST"
TYPE "COMPLETED INITIALIZATION"
.END

.PROGRAM introbready2()
call dwait2("init")
TWAIT 2
call dwait2("stop,0")
TWAIT 1
call srvprmnew2
TWAIT 4
TYPE "ROBOT READY FOR BACKLASH TEST"
TYPE "COMPLETED INITIALIZATION"
.END

.PROGRAM qryconttype()
TYPE "D-TYPE"
.END

.PROGRAM backchkfwd(.rob,.jnt,.disp,.perrlim,.currlim,.log[,],.iterno,.limit)
.currsub = .rob*1000 + .jnt
.perrsub = .rob*1000 + 400 +.jnt
FOR .i=1 TO .iterno
.current=0
.poserr=0
.cummove=0
WHILE .current<=.currlim AND .poserr=<.perrlim
	drive .jnt,.disp
	delay 0.1
	.cummove = .cummove + .disp
	if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	end
	.current = ZDA_DATA(.currsub)
	.poserr = ZDA_DATA(.perrsub)
	;TYPE .current,.poserr
END
;TYPE .current,.poserr
HERE .#loc
DECOMPOSE .x[1] = .#loc
;type "Dir1 JT-",.jnt,"=",.x[.jnt]
.log[.i,1] = .x[.jnt]
.current=0
.poserr=0
.cummove=0
WHILE .current>=-.currlim AND .poserr>=-.perrlim
	drive .jnt,-.disp
	delay 0.1
	.cummove = .cummove + .disp
	if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	end
	.current = ZDA_DATA(.currsub)
	.poserr = ZDA_DATA(.perrsub)
END
;TYPE .current,.poserr
HERE .#loc
DECOMPOSE .x[1] = .#loc
;type "Dir2 JT-",.jnt,"=",.x[.jnt]
.log[.i,2] = .x[.jnt]
END
;FOR .i=1 to .iterno
;	TYPE .i,"\t",.log[.i,1],"\t",.log[.i,2]
;END
end:
.END

.PROGRAM backchkrev(.rob,.jnt,.disp,.perrlim,.currlim,.log[,],.iterno,.limit)
.currsub = .rob*1000 + .jnt
.perrsub = .rob*1000 + 400 +.jnt
FOR .i=1 TO .iterno
.current=0
.poserr=0
.cummove=0
WHILE .current>=.currlim AND .poserr>=.perrlim
	drive .jnt,.disp
	delay 0.1
	.cummove = .cummove + .disp
	if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	end
	.current = ZDA_DATA(.currsub)
	.poserr = ZDA_DATA(.perrsub)
END
;TYPE .current,.poserr
HERE .#loc
DECOMPOSE .x[1] = .#loc
.log[.i,1] = .x[.jnt]
.current=0
.poserr=0
.cummove=0
WHILE .current<=-.currlim AND .poserr<=-.perrlim
	drive .jnt,-.disp
	delay 0.1
	.cummove = .cummove + .disp
	if .cummove > .limit then
		.log[1,0]=-1000
		goto end
	end
	.current = ZDA_DATA(.currsub)
	.poserr = ZDA_DATA(.perrsub)
END
;TYPE .current,.poserr
HERE .#loc
DECOMPOSE .x[1] = .#loc
.log[.i,2] = .x[.jnt]
END
;FOR .i=1 to .iterno
;	TYPE .i,"\t",.log[.i,1],"\t",.log[.i,2]
;END
end:
.END

.PROGRAM intcalcave(.log[,],.iter,.result)
	.result=0
	for .i=1 to .iter
		;type .log[.i,1],"    ",.log[.i,2]
		.result = .result + abs(.log[.i,1] - .log[.i,2])
	end
	;type "SUM=",.result
	.result = .result/.iter
	;type "Ave=",.result
	.result = (.result * PI)/180
	;type "Calc in Radians=",.result
.END

.program srvprmnew1()
    ZYMSRVPRM   1:2 KVP		= srvprmnew1[1,2,01]
    ZYMSRVPRM   1:2 SYSILM	= srvprmnew1[1,2,09]
    ZYMSRVPRM   1:2 AS_PERLM	= srvprmnew1[1,2,24]

    ZYMSRVPRM   1:4 KVP		= srvprmnew1[1,4,01]
    ZYMSRVPRM   1:4 SYSILM	= srvprmnew1[1,4,09]
    ZYMSRVPRM   1:4 AS_PERLM	= srvprmnew1[1,4,24]

    ZYMSRVPRM   1:6 KVP		= srvprmnew1[1,6,01]
    ZYMSRVPRM   1:6 SYSILM	= srvprmnew1[1,6,09]
    ZYMSRVPRM   1:6 AS_PERLM	= srvprmnew1[1,6,24]
.end
.program srvprmold1()
    ZYMSRVPRM   1:2 KVP		= srvprmold1[1,2,01]
    ZYMSRVPRM   1:2 SYSILM	= srvprmold1[1,2,09]
    ZYMSRVPRM   1:2 AS_PERLM	= srvprmold1[1,2,24]

    ZYMSRVPRM   1:4 KVP		= srvprmold1[1,4,01]
    ZYMSRVPRM   1:4 SYSILM	= srvprmold1[1,4,09]
    ZYMSRVPRM   1:4 AS_PERLM	= srvprmold1[1,4,24]

    ZYMSRVPRM   1:6 KVP		= srvprmold1[1,6,01]
    ZYMSRVPRM   1:6 SYSILM	= srvprmold1[1,6,09]
    ZYMSRVPRM   1:6 AS_PERLM	= srvprmold1[1,6,24]
.end
.** harish 090223-1 m++
.program srvprmnew2()
    ZYMSRVPRM   2:2 KVP		= srvprmnew2[2,2,01]
    ZYMSRVPRM   2:2 SYSILM	= srvprmnew2[2,2,09]
    ZYMSRVPRM   2:2 AS_PERLM	= srvprmnew2[2,2,24]

    ZYMSRVPRM   2:4 KVP		= srvprmnew2[2,4,01]
    ZYMSRVPRM   2:4 SYSILM	= srvprmnew2[2,4,09]
    ZYMSRVPRM   2:4 AS_PERLM	= srvprmnew2[2,4,24]

    ZYMSRVPRM   2:6 KVP		= srvprmnew2[2,6,01]
    ZYMSRVPRM   2:6 SYSILM	= srvprmnew2[2,6,09]
    ZYMSRVPRM   2:6 AS_PERLM	= srvprmnew2[2,6,24]
.end
.program srvprmold2()
    ZYMSRVPRM   2:2 KVP		= srvprmold2[2,2,01]
    ZYMSRVPRM   2:2 SYSILM	= srvprmold2[2,2,09]
    ZYMSRVPRM   2:2 AS_PERLM	= srvprmold2[2,2,24]

    ZYMSRVPRM   2:4 KVP		= srvprmold2[2,4,01]
    ZYMSRVPRM   2:4 SYSILM	= srvprmold2[2,4,09]
    ZYMSRVPRM   2:4 AS_PERLM	= srvprmold2[2,4,24]

    ZYMSRVPRM   2:6 KVP		= srvprmold2[2,6,01]
    ZYMSRVPRM   2:6 SYSILM	= srvprmold2[2,6,09]
    ZYMSRVPRM   2:6 AS_PERLM	= srvprmold2[2,6,24]
.end
.** harish 090223-1 m--
.PROGRAM inthys2()
	speed 5 always
	.log6[1,0]=0
	call backchkfwd(2,6,0.01,40,20,.log6[,],5,2)
	if .log6[1,0] == 1000
		TYPE "ERROR-1"
		goto end
	end
	.log4[1,0]=0
	call backchkfwd(2,4,0.01,40,40,.log4[,],5,2)
	if .log4[1,0] == 1000
		TYPE "ERROR-2"
		goto end
	end
	.log2[1,0]=0
	call backchkfwd(2,2,0.01,40,60,.log2[,],5,2)
	if .log2[1,0] == 1000
		TYPE "ERROR-3"
		goto end
	end
	call intcalcave(.log6[,],5,.jt6)
	call intcalcave(.log4[,],5,.jt4)
	call intcalcave(.log2[,],5,.jt2)
.** SCRX10-027-10 d
.**	.result = .jt6*350+.jt4*700+.jt2*1200 - 1.1
.** SCRX10-027-10 m
	.result = .jt6*300+.jt4*500+.jt2*500
	type "Joint-6",.jt6
	type "Joint-4",.jt4
	type "Joint-2",.jt2
	type .result
.** harish 090320 a++
	.jnts[1]=2
	.jnts[2]=4
	.jnts[3]=6
	.ind[1]=.jt2
	.ind[2]=.jt4
	.ind[3]=.jt6
	call log_bklsh(2,.result,.ind[],.jnts[],3)
.** harish 090320 a--
	if(.result<bcrit2)
		TYPE "PASS"
	else
		TYPE "FAIL"
	end
	end:
	TYPE "BACKLASH TEST COMPLETED"
.END

.PROGRAM inthys1()
	speed 5 always
	.log6[1,0]=0
	call backchkrev(1,6,0.01,-40,-20,.log6[,],5,2)
	if .log6[1,0] == 1000
		TYPE "ERROR-1"
		goto end
	end
	.log4[1,0]=0
	call backchkrev(1,4,0.01,-40,-40,.log4[,],5,2)
	if .log4[1,0] == 1000
		TYPE "ERROR-2"
		goto end
	end
	.log2[1,0]=0
	call backchkrev(1,2,0.01,-40,-60,.log2[,],5,2)
	if .log2[1,0] == 1000
		TYPE "ERROR-3"
		goto end
	end
	call intcalcave(.log6[,],5,.jt6)
	call intcalcave(.log4[,],5,.jt4)
	call intcalcave(.log2[,],5,.jt2)
	type "Joint-6",.jt6
	type "Joint-4",.jt4
	type "Joint-2",.jt2
.** SCRX10-027-10 d
.**	.result = .jt6*350+.jt4*700+.jt2*1200 - 1.1
.** SCRX10-027-10 m
        .result = .jt6*300+.jt4*600+.jt2*1100
	type .result
.** harish 090320 a++
	.jnts[1]=2
	.jnts[2]=4
	.jnts[3]=6
	.ind[1]=.jt2
	.ind[2]=.jt4
	.ind[3]=.jt6
	call log_bklsh(1,.result,.ind[],.jnts[],3)
.** harish 090320 a--
	if(.result<bcrit1)
		TYPE "PASS"
	else
		TYPE "FAIL"
	end
	end:
	TYPE "BACKLASH TEST COMPLETED"
.END

.reals
    srvprmnew1[1,2,01] = 163
    srvprmnew1[1,2,09] = 20
    srvprmnew1[1,2,24] = 296
    srvprmnew1[1,4,01] = 137
    srvprmnew1[1,4,09] = 20
    srvprmnew1[1,4,24] = 329
    srvprmnew1[1,6,01] = 192
    srvprmnew1[1,6,09] = 20
    srvprmnew1[1,6,24] = 161

.** harish 090223-1 m++
    srvprmold1[1,2,01] = 360
    srvprmold1[1,2,09] = 250
    srvprmold1[1,2,24] = 117
    srvprmold1[1,4,01] = 315
    srvprmold1[1,4,09] = 255
    srvprmold1[1,4,24] = 143
    srvprmold1[1,6,01] = 325
    srvprmold1[1,6,09] = 120
    srvprmold1[1,6,24] = 186
.** harish 090223-1 m--

.** harish 090223-1 m++
    srvprmnew2[2,2,01] = 100
    srvprmnew2[2,2,09] = 20
    srvprmnew2[2,2,24] = 117
    srvprmnew2[2,4,01] = 100
    srvprmnew2[2,4,09] = 20
    srvprmnew2[2,4,24] = 143
    srvprmnew2[2,6,01] = 100
    srvprmnew2[2,6,09] = 20
    srvprmnew2[2,6,24] = 186
.** harish 090223-1 m--

.** harish 090223-1 m++
    srvprmold2[2,2,01] = 163
    srvprmold2[2,2,09] = 275
    srvprmold2[2,2,24] = 296
    srvprmold2[2,4,01] = 137
    srvprmold2[2,4,09] = 260
    srvprmold2[2,4,24] = 329
    srvprmold2[2,6,01] = 192
    srvprmold2[2,6,09] = 140
    srvprmold2[2,6,24] = 161
.** harish 090223-1 m--

    bcrit1 = 1.2
    bcrit2 = 1.2
.end
.** harish 090223-1 a--
.******************************************
.** sycx_test.as
.******************************************
.*                                                      /* SCRX11-032-1 d */
.** SCRX10-027-7 m
.PROGRAM z_stxy()
    CALL setposjnt1
    CALL setposjnt2
.END

.PROGRAM setposjnt1()
    CALL dwait1("mode,term")
    CALL dwait1("setpos,wr:b1,pt:1,7344,-13995,-2349,18000,10440,actl")
    CALL dwait1("setpos,wr:b1,hclu:1,-5543,13045,-21202,0,13700,actl")
    CALL dwait1("setpos,wr:b1,shtl:1,-11313,9458,-25144,9000,5000,actl")
    CALL dwait1("setpos,wr:b1,buf:1,4860,-10923,-20936,9000,3500,actl")
    CALL dwait1("setpos,wr:b1,buf:8,5795,-12687,-20108,9000,3500,actl")
    CALL dwait1("setpos,wr:b1,bl-rinse:1,-600,-1500,-25000,6000,36500,actl")
;*  AMAT Lab Tool Taught position                       /* SCRX11-032-1 a */
;    CALL dwait1("setpos,wr:b1,bl-rinse:1,-2994,-785,-22898,5998,38966,actl")
    CALL dwait1("savpos")
.END

.PROGRAM setposjnt2()
    CALL dwait2("mode,term")
.*                                                      /* SCRX11-032-1 m++ */
    CALL dwait2("lockcfg,all,0")
    CALL dwait2("setcfg,portarea:U5:CS4")
    CALL dwait2("setpos,r1:b1,p1:1,-5274,16501,-11227,3000,actl")
    CALL dwait2("setpos,r1:b1,p2:1,2504,6944,-9448,3000,actl")
    CALL dwait2("setpos,r1:b1,p3:1,-2504,-6945,9447,3000,actl")
    CALL dwait2("setpos,r1:b1,p4:1,5274,-16500,11227,3000,actl")
.*                                                      /* SCRX11-032-1 m-- */
    CALL dwait2("setpos,r1:b1,u1:1,-8996,12061,-16125,30000,actl")
    CALL dwait2("setpos,r1:b1,u2:1,-7021,7594,14927,30000,actl")
    CALL dwait2("setpos,r1:b1,u3:1,-1855,-3868,-12277,13500,actl")
    CALL dwait2("setpos,r1:b1,u4:1,6887,-15190,-9447,13500,actl")
    CALL dwait2("setpos,r1:b1,u5:1,4582,-15965,11882,6905,actl")        ;* Nova Metrology at P4 location
.*                                                      /* SCRX12-006 2 a++
    ;CALL dwait2("setpos,r1:b1,u6:1,224,6924,-16147,36000,actl") ;IMAP
    CALL dwait2("setpos,r1:b1,u7:1,-1358,8807,-16448,32000,actl") ; Dummy Wafer Storage
    ;call dwait2("setcfg,x1offset:u5:-6709")
    ;call dwait2("setcfg,slot:u5:2")
    ;call dwait2("setcfg,pitch:u5:1000")
    ;CALL dwait2("setpos,r1:b1,u5:1,2504,6944,-9448,3000,actl") ; metrology
    ;CALL dwait2("setpos,r1:b1,u5:1,-2504,-6945,9447,3000,actl"); metrology
    ;CALL dwait2("setpos,r1:b1,u5:1,5274,-16500,11227,3000,actl") ; metrology
.*                                                      /* SCRX12-006 2 a--
    CALL dwait2("savpos")
.END
.*                                                      /* SCRX11-032-1 d */
.*                                                      /* SCRX11-032-1 a++ */
.PROGRAM z_cyc()
    PROMPT "Select Robot to cycle -->?  Wet or FI:  ",.$key.rob

    SCASE $TOUPPER(.$key.rob) OF
      SVALUE "WET":
         z_cyc.step[0] = 4
        $z_cyc.step[1] = "GetWaf,R1:B1,PT:1"
        $z_cyc.step[2] = "PutWaf,R1:B1,HCLU:1"
        $z_cyc.step[3] = "GetWaf,R1:B1,HCLU:1"
        $z_cyc.step[4] = "PutWaf,R1:B1,SHTL:1"
      SVALUE "FI":
        PROMPT "Select POD to cycle -->  P1, P2, P3, or P4:  ",.$key.pod
        SCASE $TOUPPER(.$key.pod) OF
          SVALUE "P1":
             z_cyc.step[0] = 7
            $z_cyc.step[1] = "TrWaf,R1:B1,P1:11,U1:1"
            $z_cyc.step[2] = "TrWaf,R1:B1,P1:12,U2:1"
            $z_cyc.step[3] = "MovToLoc,R1:B1,U4:1,GTS"
            $z_cyc.step[4] = "GetWaf,R1:B1,U4:1"
            $z_cyc.step[5] = "PutWaf,R1:B1,P1:1"
            $z_cyc.step[6] = "GetWaf,R1:B1,U3:1"
            $z_cyc.step[7] = "PutWaf,R1:B1,P1:2"
          SVALUE "P2":
             z_cyc.step[0] = 7
            $z_cyc.step[1] = "TrWaf,R1:B1,P2:11,U1:1"
            $z_cyc.step[2] = "TrWaf,R1:B1,P2:12,U2:1"
            $z_cyc.step[3] = "MovToLoc,R1:B1,U4:1,GTS"
            $z_cyc.step[4] = "GetWaf,R1:B1,U4:1"
            $z_cyc.step[5] = "PutWaf,R1:B1,P2:1"
            $z_cyc.step[6] = "GetWaf,R1:B1,U3:1"
            $z_cyc.step[7] = "PutWaf,R1:B1,P2:2"
          SVALUE "P3":
             z_cyc.step[0] = 7
            $z_cyc.step[1] = "TrWaf,R1:B1,P3:11,U1:1"
            $z_cyc.step[2] = "TrWaf,R1:B1,P3:12,U2:1"
            $z_cyc.step[3] = "MovToLoc,R1:B1,U4:1,GTS"
            $z_cyc.step[4] = "GetWaf,R1:B1,U4:1"
            $z_cyc.step[5] = "PutWaf,R1:B1,P3:1"
            $z_cyc.step[6] = "GetWaf,R1:B1,U3:1"
            $z_cyc.step[7] = "PutWaf,R1:B1,P3:2"
          SVALUE "P4":
             z_cyc.step[0] = 7
            $z_cyc.step[1] = "TrWaf,R1:B1,P4:11,U1:1"
            $z_cyc.step[2] = "TrWaf,R1:B1,P4:12,U2:1"
            $z_cyc.step[3] = "MovToLoc,R1:B1,U4:1,GTS"
            $z_cyc.step[4] = "GetWaf,R1:B1,U4:1"
            $z_cyc.step[5] = "PutWaf,R1:B1,P4:1"
            $z_cyc.step[6] = "GetWaf,R1:B1,U3:1"
            $z_cyc.step[7] = "PutWaf,R1:B1,P4:2"
          ANY :
              TYPE "Incorrect or no POD was selected.  Program ended."
              RETURN
        END
      ANY :
        TYPE "Incorrect or no robot was selected.  Program ended."
        RETURN
    END

    PROMPT "Enter number of cycle to run (default 10):  ",z_cyc.cyc
    NOEXIST_SET_R z_cyc.cyc = 10

;*  *******************************************************

    FOR i = 1 TO z_cyc.cyc+1 STEP 1
        FOR j = 1 TO z_cyc.step[0]+1 STEP 1
            z_cyc.time[i,j] = 0
        END
    END

    TYPE ""
    FOR m = 1 to z_cyc.cyc STEP 1
        TYPE "---------------------"
        TYPE "Cycle",m," Move Times:"
        FOR n = 1 to z_cyc.step[0] STEP 1
            UTIMER .@move_time = 0
            SCASE $TOUPPER(.$key.rob) OF
              SVALUE "WET":
                CALL dwait1($z_cyc.step[n])
              SVALUE "FI":
                   CALL dwait2($z_cyc.step[n])
            END
            z_cyc.time[m,n] = UTIMER(.@move_time)
            TYPE "  Step",n," =",z_cyc.time[m,n]

;*          Calculate total cycle time and save it in z_cyc.time[*,z_cyc.step[0]+1]
            z_cyc.time[m,z_cyc.step[0]+1] = z_cyc.time[m,z_cyc.step[0]+1] + z_cyc.time[m,n]
        END

        TYPE ""
        TYPE "  Cycle",m," time = ",z_cyc.time[m,z_cyc.step[0]+1]
    END

    TYPE "===================="
    TYPE "Average Move Times:"

    FOR j = 1 TO z_cyc.step[0] STEP 1
        FOR i = 1 TO z_cyc.cyc STEP 1
;*          Calculate total move time for each cycle and save it in z_cyc.time[z_cyc.cyc+1,*]
            z_cyc.time[z_cyc.cyc+1,j] = z_cyc.time[z_cyc.cyc+1,j] + z_cyc.time[i,j]
        END
;*      Calculate averge move time for all cycles and save it in z_cyc.time[z_cyc.cyc+1,*]
        IF z_cyc.cyc <> 0 THEN
            z_cyc.time[z_cyc.cyc+1,j] = z_cyc.time[z_cyc.cyc+1,j] / z_cyc.cyc
        ELSE
            TYPE "ERROR: z_cyc.cyc equal to zero"
            RETURN
        END
        TYPE "  Step",j," =",z_cyc.time[z_cyc.cyc+1,j]

;*      Calculate total cycle time and save it in z_cyc.time[z_cyc.cyc+1,z_cyc.step[0]+1]
        z_cyc.time[z_cyc.cyc+1,z_cyc.step[0]+1] = z_cyc.time[z_cyc.cyc+1,z_cyc.step[0]+1] + z_cyc.time[z_cyc.cyc+1,j]
    END

     TYPE ""
     TYPE "Average Cycle Time =",z_cyc.time[z_cyc.cyc+1,z_cyc.step[0]+1]
     TYPE "===================="
     TYPE ""
.END

.PROGRAM z_move()
    PROMPT "Select Robot to cycle -->  Wet or FI:  ",.$key.rob

    SCASE $TOUPPER(.$key.rob) OF
      SVALUE "WET":
         z_move.step[0] = 13
        $z_move.step[1] = "GetWaf,R1:B1,PT:1"
        $z_move.step[2] = "GetWaf,R1:B1,HCLU:1"
        $z_move.step[3] = "GetWaf,R1:B1,BUF:1"
        $z_move.step[4] = "GetWaf,R1:B1,SHTL:1"
        $z_move.step[5] = "GetWaf,R1:B1,BUF:8"
        $z_move.step[6] = "MovToLoc,R1:B1,PT:1,GTS"
        $z_move.step[7] = "MovToLoc,R1:B1,BL-RINSE:1,TAUGHT"
        $z_move.step[8] = "MovToLoc,R1:B1,HCLU:1,GTS"
        $z_move.step[9] = "MovToLoc,R1:B1,BL-RINSE:1,TAUGHT"
        $z_move.step[10] = "MovToLoc,R1:B1,SHTL:1,GTS"
        $z_move.step[11] = "MovToLoc,R1:B1,BL-RINSE:1,TAUGHT"
        $z_move.step[12] = "MovToLoc,R1:B1,BUF:1,GTS"
        $z_move.step[13] = "MovToLoc,R1:B1,BL-RINSE:1,TAUGHT"
      SVALUE "FI":
         z_move.step[0] = 32
        $z_move.step[1] = "TrWaf,R1:B1,P1:1,U1:1"
        $z_move.step[2] = "TrWaf,R1:B1,P2:1,U1:1"
        $z_move.step[3] = "TrWaf,R1:B1,P3:1,U1:1"
        $z_move.step[4] = "TrWaf,R1:B1,P4:1,U1:1"
        $z_move.step[5] = "TrWaf,R1:B1,P1:1,U2:1"
        $z_move.step[6] = "TrWaf,R1:B1,P2:1,U2:1"
        $z_move.step[7] = "TrWaf,R1:B1,P3:1,U2:1"
        $z_move.step[8] = "TrWaf,R1:B1,P4:1,U2:1"
        $z_move.step[9] = "GetWaf,R1:B1,U3:1"
        $z_move.step[10] = "PutWaf,R1:B1,P1:1"
        $z_move.step[11] = "GetWaf,R1:B1,U3:1"
        $z_move.step[12] = "PutWaf,R1:B1,P2:1"
        $z_move.step[13] = "GetWaf,R1:B1,U3:1"
        $z_move.step[14] = "PutWaf,R1:B1,P3:1"
        $z_move.step[15] = "GetWaf,R1:B1,U3:1"
        $z_move.step[16] = "PutWaf,R1:B1,P4:1"
        $z_move.step[17] = "GetWaf,R1:B1,U4:1"
        $z_move.step[18] = "PutWaf,R1:B1,P1:1"
        $z_move.step[19] = "GetWaf,R1:B1,U4:1"
        $z_move.step[20] = "PutWaf,R1:B1,P2:1"
        $z_move.step[21] = "GetWaf,R1:B1,U4:1"
        $z_move.step[22] = "PutWaf,R1:B1,P3:1"
        $z_move.step[23] = "GetWaf,R1:B1,U4:1"
        $z_move.step[24] = "PutWaf,R1:B1,P4:1"
        $z_move.step[25] = "MovToLoc,R1:B1,U1:1,GTS"
        $z_move.step[26] = "MovToLoc,R1:B1,U3:1,GTS"
        $z_move.step[27] = "MovToLoc,R1:B1,U1:1,GTS"
        $z_move.step[28] = "MovToLoc,R1:B1,U4:1,GTS"
        $z_move.step[29] = "MovToLoc,R1:B1,U2:1,GTS"
        $z_move.step[30] = "MovToLoc,R1:B1,U3:1,GTS"
        $z_move.step[31] = "MovToLoc,R1:B1,U2:1,GTS"
        $z_move.step[32] = "MovToLoc,R1:B1,U4:1,GTS"
      ANY :
        TYPE "Incorrect or no robot was selected.  Program ended."
        RETURN
    END

    PROMPT "Enter number of cycle to run (default 10):  ",z_move.cyc
    NOEXIST_SET_R z_move.cyc = 10

;*  *******************************************************

    TYPE ""
    FOR m = 1 to z_move.cyc STEP 1
        TYPE "---------------------"
        TYPE "Cycle",m,", total of",z_move.step[0]," steps:"
        FOR n = 1 to z_move.step[0] STEP 1
            TYPE "  Step",n," -->  ",$z_move.step[n]
            SCASE $TOUPPER(.$key.rob) OF
              SVALUE "WET":
                CALL dwait1($z_move.step[n])
              SVALUE "FI":
                CALL dwait2($z_move.step[n])
            END
        END

        TYPE ""
    END

    TYPE "Cycle test completed."
    TYPE "===================="
    TYPE ""
.END
.*                                                      /* SCRX11-032-1 a-- */
