#InstallKeybdHook
#SingleInstance OFF
toggle = 0
#MaxThreadsPerHotkey 2
QueueCount = 0
ASFlowChartGUI()
return

ASFlowChartGUI() {
	global
	NumParents := 5
	NumChildren := 5
	Gui , 1:Default
	Gui , Add , Button , x10 y15 gSelectAPL , Select AS Code
	Gui , Add , Edit ,   x110 yp w280 R1 vApl
	Gui , Add , Button , x100 yp+30 w200 gGeneratePrograms , Generate Program List
	Gui , Add , Listview , x10 r15 w350 vProgramList gProgramList, Program|#Parents|#Children
	Gui , Add , Text , x100 , Selected Program: 
	Gui , Add , Edit , x200 yp-2 w100 R1 vProgramSel
	Gui , Add , Text , x100 , Parents to Trace: 
	Gui , Add , Edit , x200 yp-2 w100 R1 vNumParents, 3
	Gui , Add , Text , x100 , Children to Trace: 
	Gui , Add , Edit , x200 yp-2 w100 R1 vNumChildren, 3
	Gui , Add , Button , x100 w200 gStart , START
	LV_ModifyCol(1,175)
	LV_ModifyCol(2,75)
	LV_ModifyCol(3,75)
	Gui , Show
	return
SelectAPL:
	FileSelectFile, SelectedAPL
	GuiControl , , Edit1 , %SelectedAPL%
	Apl = %SelectedApl%
	return
GeneratePrograms:
	LV_Delete()	; Clear the list of all previous results when Generate Program List is pressed
	Loop , Read , %Apl%
	{
		AplLine = %A_LoopReadLine%
		IfInString, AplLine, .program
		{
			StringLower , Aplline , Aplline
			programstring = .program
			StringGetPos , LineStart , AplLine , %programstring%
			If (LineStart = 0) {
				argumentstring = (
				StringGetPos , ArgumentStart , AplLine , %argumentstring%
				ProgramNameStart := argumentstart - 9
				ProgramName := SubStr(AplLine,10,ProgramNameStart)
				LV_Add("",ProgramName,"?","?")
			}
		}
	}
	return
Start:
	Gui , Submit, NoHide
	FileDelete , trace.tmp
	FileAppend , %ProgramName% %NumParents% %NumChildren%, trace.tmp
	Run , %comspec% /c ASFlowChart.exe "%Apl%"
	sleep , 5000
	FileDelete , trace.tmp
	return
ProgramList:
	if A_GuiEvent = DoubleClick
	{
		LV_GetText(ProgramName, A_EventInfo, 1)
		GuiControl , , Edit2 , %ProgramName%
	}
	return
GuiClose:
	ExitApp
	return
GuiDropFiles:
	Loop, parse, A_GuiEvent, `n
	{
		Apl = %A_LoopField%
		Break
	}
	GuiControl , , Edit1 , %Apl%
	return
}
