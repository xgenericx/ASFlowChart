import sys, time
import os.path

SoftwareName = 'ASFlowChart'
SoftwareVersion = '0.9.05.3'
print('\n-----------------------------------------------\n'+SoftwareName+' Version '+SoftwareVersion+'\n-----------------------------------------------\n')
programname = ''
callname = ''
log = [('','')]*(10**6)
x = 0
def isprogramheader( line ):
    if line.lower().strip().startswith('.program '):
        return True
    else:
        return False
        
def iscall( line ):
    if line.lower().strip().startswith('call '    ):
        return True
    else:
        return False
        
def writeflow(output,programname,callname):
    output.write('    '+programname+' -> '+callname+';\n')
    return

def findlinksdown(program,down):
    global log, lognew, y
    #search down
    if down-1>0:
        blank=0
        for links in log:
            dupe = 0
            if program==links[0]:
                findlinksdown(links[1],down-1)
                #lognew[y] = (links[0],links[1])
                #y+=1
                num = 0
                while num <= y:
                    if (links[0],links[1]) == lognew[num]:
                        dupe = 1
                        break
                    num+=1
                if not dupe:
                    lognew[y] = (links[0],links[1])
                    y+=1
                    dupe = 0
            if links[0] == '':
                blank+=1
            if blank>2:
                break
    return

def findlinksup(call,up):
    global log, lognew, y
    #search up
    if up-1>0:
        blank=0
        for links in log:
            dupe = 0
            if call==links[1]:
                findlinksup(links[0],up-1)
                num = 0
                while num <= y:
                    if (links[0],links[1]) == lognew[num]:
                        dupe = 1
                        break
                    num+=1
                if not dupe:
                    lognew[y] = (links[0],links[1])
                    y+=1
                    dupe = 0
            if links[0] == '':
                blank+=1
            if blank>2:
                break
    return

def flow(inputpath, filename, outputpath, outputdir):
    global programname, callname, log, x, traces, ind, lognew, y, lastwrite
    input = open(inputpath,'r', encoding='Shift-JIS',errors='ignore')
    for line in input:
        dupe = 0
        if isprogramheader(line):
            oldprogramname = programname
            callname = callname + '_supercede1'
            programname = line.lower().strip().partition('.program ')[2].strip().partition('(')[0]
        elif iscall(line):
            oldcallname = callname
            callname    = line.lower().strip().partition('call '    )[2].strip().partition('(')[0]
            if oldprogramname!=programname and oldcallname!=callname:
                num = 0
                while num <= x:
                    if log[num] == [(programname,callname)]:
                        dupe = 1
                        break
                    num+=1
                if not dupe:
                    log[x] = (programname,callname)
                    x+=1
    input.close()
    batch = open(outputdir+'gvtopng.bat','w',encoding='Shift-JIS',errors='ignore')
    blankx=0
    for interest in traces :
        y = 0
        blank=0
        lognew = [('','')]*(10**6)
        if interest[0] != '':
            output = open(outputdir+interest[0]+'_'+interest[1]+'_'+interest[2].strip()+'.gv','w',encoding='Shift-JIS',errors='ignore')
            findlinksdown(interest[0],int(interest[2])+1)
            findlinksup(interest[0],int(interest[1])+1)
            output.write('digraph '+filename+' {\n')
            output.write('"'+interest[0]+'" [color=red,style=filled];\n')
            output.write('rankdir=LR\n')
            for link in lognew:
                if link[0] != '':
                    writeflow(output,'"'+link[0]+'"','"'+link[1]+'"')
                if link[0] == '':
                    blank+=1
                if blank>2:
                    break
            output.write('}\n')
            output.close()
            batch.write('dot -Tpng -o "'+outputdir+interest[0]+'_'+interest[1]+'_'+interest[2].strip()+'.png" "'+outputdir+interest[0]+'_'+interest[1]+'_'+interest[2].strip()+'.gv"\n')
            lastwrite = '"'+outputdir+interest[0]+'_'+interest[1]+'_'+interest[2].strip()+'.png"'
        if interest[0] == '':
            blankx+=1
        if blankx>20:
            break
    batch.close()
    return

inputpath = sys.argv[1]
filename = inputpath.rpartition('\\')[2].rpartition('.')[0]
curdir = os.path.dirname(os.path.abspath(inputpath))

if os.path.isfile(os.path.dirname(os.path.realpath(sys.argv[0]))+'\\trace.tmp'):
    trace = open (os.path.dirname(os.path.realpath(sys.argv[0]))+'\\trace.tmp','r')
elif os.path.isfile(curdir+'\\trace.txt'):
    trace = open(curdir+'\\trace.txt','r')
elif os.path.isfile(os.path.dirname(os.path.realpath(sys.argv[0]))+'\\trace.txt'):
    trace = open (os.path.dirname(os.path.realpath(sys.argv[0]))+'\\trace.txt','r')
else:
    print('Error: trace.txt does not exist, create one with the following content in the executable folder:')
    print('<Program Name> <Levels Above> <Levels Below>')
    print('\nWhere:\n     <Program Name> : Name of the program to track in a flow chart')
    print('     <Levels Above> : Number of levels above which lead to <Program Name>')
    print('     <Levels Below> : Number of levels below which lead from <Program Name>')
    sys.exit()
traces = [('','','')]*(10**6)
ind = 0
for line in trace:
    if line.isspace():
        continue
    traces[ind] = line.replace(';',' ').replace(',',' ').replace('\t',' ').split()
    ind+=1
ind-=1
trace.close()

if not os.path.exists(os.path.dirname(os.path.realpath(sys.argv[0]))+'\\ASFlowChart_Output'):
    os.makedirs(os.path.dirname(os.path.realpath(sys.argv[0]))+'\\ASFlowChart_Output')
outputpath= os.path.dirname(os.path.realpath(sys.argv[0]))+'\\ASFlowChart_Output\\'+filename+'.gv'
outputdir= os.path.dirname(os.path.realpath(sys.argv[0]))+'\\ASFlowChart_Output\\'
flow( inputpath, filename, outputpath, outputdir)
print('"'+outputdir+'GVtoPNG.bat"')
os.system('"'+outputdir+'GVtoPNG.bat"')
if (ind == 0):
    os.system(lastwrite)
    print(lastwrite)
