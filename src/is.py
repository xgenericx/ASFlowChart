import sys

def isprogramheader( line ):
    if ( line.lower().strip().startswith('.program ')
        return True
    else:
        return False
        
def iscall( line ):
    if ( line.lower().strip().startswith('call '    )
        return True
    else:
        return False
        
def writeflow(output,programname,callname)
    output.write('    '+programname+' -> '+callname)
    return
